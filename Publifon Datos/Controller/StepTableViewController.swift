//
//  StepTableViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 10/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SDWebImage

class StepTableViewController: UITableViewController {
    
    //Var
    var groupsOfApps = [[App]]()
    var groups = [Packet]()
    var pagesNumber = Int()
    var namePage = String()
    @IBOutlet var applicationsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Assigning delegates
        self.applicationsTableView.delegate = self
        self.applicationsTableView.dataSource = self
        //self.applicationsTableView.backgroundColor = .black
        self.applicationsTableView.register(UINib(nibName: "ChoiceTableViewCell", bundle: nil),
                               forCellReuseIdentifier: "choiceTableViewCell")
        
        self.tableView.separatorColor = .black
        self.view.backgroundColor = .white
        self.tableView.backgroundColor = .white
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupsOfApps[pagesNumber].count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //let cell = tableView.dequeueReusableCell(withIdentifier: "cellApps", for: indexPath)
        let cell = self.applicationsTableView.dequeueReusableCell(withIdentifier: "choiceTableViewCell", for: indexPath) as! ChoiceTableViewCell
        
        print("lolololol groupsOfApps ", groupsOfApps.debugDescription)
        print("lolololol groups ", groups.debugDescription)
                
        cell.selectionStyle = .none
        //cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.aplicationNameLabel?.text = groupsOfApps[pagesNumber][indexPath.row].nameApp
        //cell.aplicationNameLabel?.textColor = .white
        cell.freeTimeLabel?.text = Utils.formatHrsText(hrs: groupsOfApps[pagesNumber][indexPath.row].availableHours)
        //cell.freeTimeLabel?.textColor = .white
        cell.aplicationNameLabel?.adjustsFontSizeToFitWidth = true
        //cell.textLabel?.numberOfLines = 0
        //cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        //cell.detailTextLabel?.numberOfLines = 0
        cell.freeTimeLabel?.adjustsFontSizeToFitWidth = true
        //cell.detailTextLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        
        //cell.aplicationNameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        //cell.freeTimeLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cell.aplicationNameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cell.freeTimeLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        let couponObject = Utils.getCouponObject()
        //let iconURL = couponObject.applications[indexPath.row].logoURL
        
        /*let iconURL = URL(string: couponObject.applications[indexPath.row].logoURL)
        
        cell.appIconImageView.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""), completed: {
            (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
            // If coupon is a combo type then add voz item
            //if indexPath.row == 0 && self.couponType == Utils.CouponType.combo.rawValue {
                //cell.appIconImageView.image = UIImage(named: "phone_icon")
                //cell.backgroundColor =  Utils.hexToColor(hexString: self.accentuationColor)
                // Setting icon color
                //cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
                //cell.appIconImageView.tintColor = Utils.hexToColor(hexString: self.textColor)
            //}
        })*/
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HeaderTableViewCell", owner: self, options: nil)?.first as! HeaderTableViewCell
        headerView.headerTitleLabel.text = groups[section].groupName
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
}
