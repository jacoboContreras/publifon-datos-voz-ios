//
//  ExtendedValidationViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/25/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

protocol FailedToCheckCoupon {
    func onFailedToCheckCoupon(data: String)
    func onFinishCheckingCoupon(type: Utils.CouponType, phoneNumber: String, chosenGroup: String)
    func onFinishCheckingChoiceCoupon(type: Utils.CouponType,
                                      phoneNumber: String,
                                      selected: Utils.CouponSelectedChoice,
                                      view: UIViewController)
    func onFinishCheckingChoiceCoupon(type: Utils.CouponType,
                                      phoneNumber: String,
                                      selected: Utils.CouponSelectedChoice,
                                      view: UIViewController, chosenGroup: String)
}

class ExtendedValidationViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, CouponSelectedChoice, UIActionSheetDelegate, SendSelectedGroup {
    func sendSelectedGroup(groupType: Utils.CouponType, chosenGroup: Int) {
        startActivityIndicator()
        if groupType.rawValue == "voz" {
            self.couponSelectedChoice = Utils.CouponSelectedChoice.voz
        } else if groupType.rawValue == "datos" {
            self.couponSelectedChoice = Utils.CouponSelectedChoice.datos
        } else {
            self.couponCheck.couponType = Utils.CouponType.combo
        }
        
        
        delegate?.onFinishCheckingChoiceCoupon(type: self.couponCheck.couponType, phoneNumber: telNumberUITextField.text!, selected: self.couponSelectedChoice, view: self, chosenGroup: String(chosenGroup))
        
        //Dismissing this view controller
        //self.navigationController?.popViewController(animated: true)
        
        //antes de convertir el cuerpo de la app en nav controller
        //self.dismiss(animated: true, completion: nil)
        
        self.navigationController?.popViewController(animated: false)
    }
    
    // Class variables
    var coupon: String = ""
    var couponCheck = CheckCoupon(status: false, message: "", type: "", chosenGroup: "")
    var couponSelectedChoice = Utils.CouponSelectedChoice.none
    var groupType = ""
    var groupId = ""
    var delegate: FailedToCheckCoupon?
    public static var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var phoneNumber = ""
    var inputCount = 0
    var firstNumberInput = ""
    var datosJSON: JSON?
    var vozJSON: JSON?
    var isWifiActive = false
    let reachability = Reachability()!
    static var logoImageCompany : UIImage!
    var logoURL = URL(string: "")
    var timerVar = Timer()
    var finishedCheckCoupon = false
    
    // Linked outlets
    @IBOutlet weak var containerUIView: UIView!
    //@IBOutlet weak var acceptButton: UIView!
    @IBOutlet weak var telNumberUITextField: UITextField!
    @IBOutlet weak var confirmationTelNumberUITextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var logoCompanyImageView: UIImageView!
    @IBOutlet weak var notificationMessageLabel: UILabel!
    
    // Location variables
    let locationManager = CLLocationManager()
    
    //Var from qr coupon
    var isFromQR = false    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*if coupon == Utils.APPLE {// Check for Apple's Review //user@bluestudio.mx
         Utils.APP_MODE = Utils.AppMode.pre_production
         }*/
        
        print("\n\n\n\n\n\n", coupon)
        
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{ try reachability.startNotifier() }
        catch { print("Could not start notifier") }
        
        // Working animation indicator
        startActivityIndicator()
        //containerUIView.isHidden = true
        
        // Setting view controller background
        //self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Setting up rounded corner to container view
        //containerUIView.layer.cornerRadius = 10
        
        // Setting Tel number Textfield
        telNumberUITextField.delegate = self
        telNumberUITextField.keyboardType = .phonePad
        let tooBar: UIToolbar = UIToolbar()
        tooBar.barStyle = UIBarStyle.blackTranslucent
        tooBar.items=[
            UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil),
            //change cancel button to "ok" button or "continuar" button
            //UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.cancelPressed))
            UIBarButtonItem(title: "Continuar", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.ContinuePressed))]
        
        tooBar.sizeToFit()
        telNumberUITextField.inputAccessoryView = tooBar
        
        // Setting Tel number Textfield
        confirmationTelNumberUITextField.delegate = self
        confirmationTelNumberUITextField.keyboardType = .phonePad
        confirmationTelNumberUITextField.inputAccessoryView = tooBar
        
        // Code related to show keyboard UI elements when keyboard shows/hides
        NotificationCenter.default.addObserver(self, selector: #selector(InsertCodeViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InsertCodeViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        // Checks coupon code existence in CRM's
        
        self.checkCoupon(coupon: self.coupon)
        
        //location setup
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        
        
        view.addGestureRecognizer(tap)
        
        
        if isFromQR {
            //timer to execute when finish checking coupon 
            timerVar = Timer.scheduledTimer(withTimeInterval: 0.2, repeats: true, block: { timer in
                switch(self.finishedCheckCoupon) {
                    
                case false:
                    break;
                    
                case true:
                    self.telNumberUITextField.text = self.phoneNumber
                    self.firstNumberInput = self.phoneNumber
                    self.confirmationTelNumberUITextField.text = self.phoneNumber
                    self.inputCount = 1
                    self.checkPhoneNumber()
                    self.timerVar.invalidate()
                    break;
                }
            })
        }
        
        customView()
        
        
    }//Finish viewDidLoad
    
    func customView(){
        // Adds a place holder to text field
        self.title = "Número de teléfono"
        telNumberUITextField.attributedPlaceholder = NSAttributedString(string: "Número de teléfono a 10 dígitos", attributes: [
            .foregroundColor: UIColor.black
        ])
        
        confirmationTelNumberUITextField.attributedPlaceholder = NSAttributedString(string: "Confirma tu número de teléfono", attributes: [
            .foregroundColor: UIColor.black
        ])
        
        telNumberUITextField.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        telNumberUITextField.layer.borderWidth = 1
        telNumberUITextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        confirmationTelNumberUITextField.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        confirmationTelNumberUITextField.layer.borderWidth = 1
        confirmationTelNumberUITextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        confirmationTelNumberUITextField.isHidden = true
        
        acceptButton.backgroundColor = StandarColorAndForms.blue        
        
    }
    
    
    // Animates activity indicator
    func startActivityIndicator() {
        ExtendedValidationViewController.activityIndicator.center = self.view.center
        ExtendedValidationViewController.activityIndicator.hidesWhenStopped = true
        ExtendedValidationViewController.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(ExtendedValidationViewController.activityIndicator)
        ExtendedValidationViewController.activityIndicator.startAnimating()
    }
    
    // Stops activity indicator animation
    func stopActivityIndicator() {
        ExtendedValidationViewController.activityIndicator.stopAnimating()
    }
    
    // Code related to show UI elements when keyboard shows/hides
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    // Code related to show UI elements when keyboard shows/hides
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    // Kwyboard cancel button logic
    @objc func cancelPressed () {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ContinuePressed () {
        
        //Checking if wifi is active
        if self.coupon != Utils.APPLE {// Check for Apple's Review // user@bluestudio.mx //user@bluestudio.mx
            isWifiEnabled(isWifiOn: isWifiActive)
        }
        
        //isWifiEnabled(isWifiOn: isWifiActive)
        //let cleanedNumber = telNumberUITextField.text!.components(separatedBy:CharacterSet.decimalDigits.inverted).joined(separator: "")
        if (telNumberUITextField.text!.count) == 10 && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: telNumberUITextField.text!)) {
            //couponTypeManager(phoneNumber: telNumberUITextField.text!)
            checkPhoneNumber()
        } else {
            // If input has less than 10 characters toast a message
            telNumberUITextField.resignFirstResponder()
            showToast(title: "Aviso", text: Utils.NUMBER_MUST_CONTAIN_10_DIGITS, interval: 4.0)
        }
        
    }
    
    
    // Textfield delegate function
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        telNumberUITextField.resignFirstResponder()
        return true
    }
    
    // Accept button logic
    @IBAction func acceptButtonAction(_ sender: Any) {
        
        //Checking if wifi is active
        if self.coupon != Utils.APPLE {// Check for Apple's Review // user@bluestudio.mx //user@bluestudio.mx
            isWifiEnabled(isWifiOn: isWifiActive)
        }
        
        //isWifiEnabled(isWifiOn: isWifiActive)
        //let cleanedNumber = telNumberUITextField.text!.components(separatedBy:CharacterSet.decimalDigits.inverted).joined(separator: "")
        if (telNumberUITextField.text!.count) == 10 && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: telNumberUITextField.text!)) {
            //couponTypeManager(phoneNumber: telNumberUITextField.text!)
            checkPhoneNumber()
        } else {
            // If input has less than 10 characters toast a message
            telNumberUITextField.resignFirstResponder()
            showToast(title: "Aviso", text: Utils.NUMBER_MUST_CONTAIN_10_DIGITS, interval: 4.0)
        }
        
    }
    
    // Shows toast if Wi-fi is active and stops any further process
    func isWifiEnabled(isWifiOn: Bool) {
        
        #if targetEnvironment(simulator)
        return
        #endif
        if isWifiOn && Utils.APP_MODE == Utils.AppMode.production {
            //showToast(title: "Aviso", text: Utils.DISABLE_WIFI_TO_REDEEM, interval: 3.0)
            // Setting view controller background
            
            showDissableWifiPopup()
            return
        }
    }
    
    //Show dissable Wifi popup
    func showDissableWifiPopup(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let disableWifiPopupViewController = storyBoard.instantiateViewController(withIdentifier: "DisableWifiPopupViewController") as! DisableWifiPopupViewController
        
        disableWifiPopupViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0)
        
        self.present(disableWifiPopupViewController, animated: true, completion: nil)
        
    }
    
    // Shows Action Sheet
    var actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
    func showMessageVPN() {
        actionSheet = UIAlertController(title: "Aviso", message: Utils.DISABLE_WIFI_TO_REDEEM, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Aceptar", style: .default))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    // Listens for network changes
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            print("Internet", "wifi")
            isWifiActive = true
        case .cellular:
            print("Internet", "cellular")
            isWifiActive = false
        case .none:
            print("Internet", "none")
            
        case .vpn:
            print("Internet", "VPN")
        }
    }
    
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    
    
    // Checks if given phone numbers are correct
    func checkPhoneNumber() {
        // Saves first input and asks for a second number input
        if inputCount == 0 {
            firstNumberInput = telNumberUITextField.text!
            //telNumberUITextField.text! = ""
            inputCount = inputCount + 1
            /*notificationMessageLabel.text = "¡Confirma tu número!"
             notificationMessageLabel.font = .boldSystemFont(ofSize: 15)
             telNumberUITextField.attributedPlaceholder = NSAttributedString(string: "Confirma tu número", attributes: [
             .foregroundColor: UIColor.white,
             .font: UIFont.boldSystemFont(ofSize: 15.0)
             ])*/
            //acceptButton.setTitle("Confirmar", for: .normal)
            //cancelButton.setTitle("Corregir", for: .normal)
            //showToast(title: "Verificación", text: "Debes ingresar una vez más tu número")
            confirmationTelNumberUITextField.isHidden = false
            confirmationTelNumberUITextField.becomeFirstResponder()
            return
        }
        
        // Verifies given phone numbers and notifies user if they don't match
        // calls couponTypeManager to continue with redemption
        if inputCount == 1 {
            //if firstNumberInput == telNumberUITextField.text!{
            
            if telNumberUITextField.text == nil && telNumberUITextField!.text!.count != 10 {
                showToast(title: "Error", text: "Debes ingresar ambos campos")
            }
            
            if telNumberUITextField.text! == confirmationTelNumberUITextField.text!{
                
                if UserDefaults.standard.bool(forKey: "openedBefore") == false {
                    Utils().showTermsAndConditionsAux(viewController: self)
                    return
                }
                couponTypeManager(phoneNumber: telNumberUITextField.text!)
                //self.containerUIView.isHidden = true
            } else {
                //telNumberUITextField.text! = ""
                showToast(title: "Vuelve a intentarlo", text: "Los numeros no coinciden")
            }
            inputCount = 0
            /*notificationMessageLabel.font = .systemFont(ofSize: 15.0)
             notificationMessageLabel.text = "¡Ingresa tu número de\nteléfono a 10 dígitos*!"
             telNumberUITextField.attributedPlaceholder = NSAttributedString(string: "Ingresa tu número", attributes: [
             .foregroundColor: UIColor.white,
             .font: UIFont.boldSystemFont(ofSize: 15.0)
             ])*/
        }
        
    }
    
    // Manages coupon type behavior
    func couponTypeManager(phoneNumber: String) {
        //self.couponCheck.couponType = Utils.CouponType.choice
        if self.couponCheck.couponType == Utils.CouponType.choice {
            self.phoneNumber = phoneNumber
            print("self.phoneNumber", self.phoneNumber)
            //self.performSegue(withIdentifier: Utils.GO_TO_CHOICE_VIEW_CONTROLLER, sender: self)
            self.performSegue(withIdentifier: "goToChoosePacket", sender: self)
        } else {
            simpleCoupon()
        }
    }
    
    // If its a simple coupon go ahead and redeem coupon
    func simpleCoupon() {
        telNumberUITextField.resignFirstResponder()
        
        // If coupon only has voz then set variables to behave correctly
        if groupType == "voz" {
            self.couponCheck.couponType = Utils.CouponType.choice
            self.couponSelectedChoice = Utils.CouponSelectedChoice.voz
            delegate?.onFinishCheckingChoiceCoupon(type: self.couponCheck.couponType, phoneNumber: telNumberUITextField.text!, selected: self.couponSelectedChoice, view: self, chosenGroup: self.groupId)
        } else {
            delegate?.onFinishCheckingCoupon(type: self.couponCheck.couponType,
                                             phoneNumber: telNumberUITextField.text!,
                                             chosenGroup: self.couponCheck.chosenGroup)
        }
        
        //antes de ser nav controller
        //dismiss(animated: true, completion: nil)
        
        
        
    }
    
    // Cancel button logic
    @IBAction func cancelButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Notify user if location was not obtained
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        showToast(title: "Ocurrio un problema", text: "No se pudo obtener tu ubicación")
    }
    
    // Toast code
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 2, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    func showToast(title: String, text: String, interval: Double) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: interval, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // Gets user location coordinates and calls method to get API token
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if (location.horizontalAccuracy > 0) {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            let latitude = String(location.coordinate.latitude)
            let longitude = String(location.coordinate.longitude)
            
            // gets api token
            print("Latitude: ", latitude)
            print("Longitude: ", longitude)
        }
    }
    
    //    // Check coupon
    //    func checkCoupon(coupon: String) {
    //        // Making Request
    //        Alamofire.request(configureRequest(coupon: coupon, url: Utils.CHECK_COUPON_URL)).responseData {
    //            dataResponse in
    //
    //            if dataResponse.result.isSuccess {
    //                var tempType = ""
    //                let json : JSON = JSON(dataResponse.result.value!)
    //                self.datosJSON = json
    //                print("Type 0:", self.datosJSON ?? "no json")
    //
    //                if json.isEmpty {
    //                    self.delegate?.onFailedToCheckCoupon(data: "Ocurrió un fallo de conectividad.")
    //                    //antes de convertir el cuerpo de la app en nav controller
    //self.dismiss(animated: true, completion: nil)
    //                    return
    //                }
    //
    //                // Getting app versions
    //                guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
    //                    print("CASE 1")
    //                    self.displayActionSheet(message: Utils.ISSUES_TRYING_TO_REDEEM)
    //                    return
    //                }
    //                guard let appStoreVersion = json["app_version"]["ios"].string else {
    //                    print("CASE 2")
    //                    self.displayActionSheet(message: Utils.UPDATE_APP)
    //                    return
    //                }
    //
    //                // Showing Versions in logs
    //                print("Version Local: ", appVersion)
    //                print("Version AppStore: ", appStoreVersion)
    //
    //                // Redirect to Itunes if there is a new App Version
    //                if Utils.hasUpdatedAppVersion(localAppVersion: appVersion, appStoreVersion: appStoreVersion) {
    //                    self.displayAppStoreActionSheet(message: Utils.UPDATE_APP)
    //                 return
    //                 }
    //
    //                // Checking if parameters exists in reponse
    //                guard let message = json[Utils.MESSAGE].string else {
    //                    self.displayActionSheet(message: Utils.UPDATE_APP)
    //                    return
    //                }
    //
    //                guard var success = json[Utils.SUCCESS].bool else {
    //                    self.displayActionSheet(message: Utils.UPDATE_APP)
    //                    return
    //                }
    //
    //                guard var type = json[Utils.TYPE].string else {
    //                    self.displayActionSheet(message: Utils.UPDATE_APP)
    //                    return
    //                }
    //
    //                tempType = type
    //                // Checking if coupon exists in publifon datos CRM
    //                // If coupon exists in datos CRM create a coupon check object and assign its type
    //                if success {
    //                    self.stopActivityIndicator()
    //                    success = json[Utils.SUCCESS].bool!
    //                    let benefits = json["benefits"].array!
    //                    // If the array has more than 1 element then user has to choose a package
    //                    if benefits.count > 1 {
    //                        type = "seleccion"
    //                    }
    //
    //                    // Initializes coupon check object
    //                    self.couponCheck = CheckCoupon(success: success, message: message, type: type)
    //                    print("Type 1:", type)
    //
    //                    // If coupon type is combo or choice, check if coupon exists in voz crm
    //                    self.comboChoiceVozCheck (couponCheck: self.couponCheck)
    //
    //                } else {
    //                    // Checking if coupon exist in publifon voz CRM
    //                    if tempType != "seleccion" && tempType != "combo" {
    //                        self.vozCouponCheck(coupon: coupon, type: Utils.CouponType.none)
    //                    } else {
    //                        self.delegate?.onFailedToCheckCoupon(data: "Este cupón ya fue utilizado previamente")
    //                        //antes de convertir el cuerpo de la app en nav controller
    //self.dismiss(animated: true, completion: nil)
    //                    }
    //
    //                }
    //
    //            } else {
    //                print("Error: \(dataResponse.result.error!)")
    //                self.delegate?.onFailedToCheckCoupon(data: Utils.CONECTION_ISSUES)
    //                //antes de convertir el cuerpo de la app en nav controller
    //self.dismiss(animated: true, completion: nil)
    //            }
    //
    //        }
    //
    //    }
    
    // Check coupon
    func checkCoupon(coupon: String) {
        // Making Request
        Alamofire.request(configureRequest(coupon: coupon, url: Utils.CHECK_COUPON_URL)).responseData {
            dataResponse in
            
            if dataResponse.result.isSuccess {
                var tempType = ""
                let json : JSON = JSON(dataResponse.result.value!)
                self.datosJSON = json
                print("Type 0:", self.datosJSON ?? "no json")
                
                // Set company logo
                if let logo = self.datosJSON?["campaign"]["logo"].string{
                    self.logoURL = URL(string: logo)
                }
                self.logoCompanyImageView.sd_setImage(with: self.logoURL, placeholderImage: UIImage(named: "logo_publifon"))
                
                if json.isEmpty {
                    self.delegate?.onFailedToCheckCoupon(data: "Ocurrió un fallo de conectividad.")
                    //antes de convertir el cuerpo de la app en nav controller
                    //self.dismiss(animated: true, completion: nil)
                    
                    self.navigationController?.popViewController(animated: false)
                    return
                }
                
                // Getting app versions
                guard let appVersion: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
                    self.displayActionSheet(message: Utils.ISSUES_TRYING_TO_REDEEM)
                    return
                }
                
                guard let appStoreVersion = json["app_version"]["ios"].string else {
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                // Showing Versions in logs
                print("Version Local: ", appVersion)
                print("Version AppStore: ", appStoreVersion)
                // Service info maessage to user
                if appStoreVersion == "0.0.0" {
                    self.displayMaintainanceActionSheet(message: Utils.SERVER_MAINTENANCE)
                    return
                }
                
                // Redirect to Itunes if there is a new App Version
                if Utils.hasUpdatedAppVersion(localAppVersion: appVersion, appStoreVersion: appStoreVersion) {
                    self.displayAppStoreActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                // Checking if parameters exists in reponse
                guard let message = json[Utils.MESSAGE].string else {
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                guard var success = json[Utils.SUCCESS].bool else {
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                guard var type = json[Utils.TYPE].string else {
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                tempType = type
                // Checking if coupon exists in publifon datos CRM
                // If coupon exists in datos CRM create a coupon check object and assign its type
                if success {
                    self.stopActivityIndicator()
                    success = json[Utils.SUCCESS].bool!
                    let benefits = json["benefits"].array!
                    // If the array has more than 1 element then user has to choose a package
                    if benefits.count > 1 {
                        type = "seleccion"
                    } else if benefits.count != 0 {
                        // Getting default id,
                        // if user is not to choose then assume first group_id as default
                        if benefits[0]["group_id"].exists() {
                            self.groupId = String(benefits[0]["group_id"].int!)
                            self.groupType = benefits[0]["group_type"].string!
                        } else {
                            self.displayActionSheet(message: Utils.COUPON_NOT_VALID_FOR_THIS_APP_VERSION)
                            return
                        }
                        
                    }
                    
                    print("GROUP_ID:", self.groupId)
                    // Initializes coupon check object
                    self.couponCheck = CheckCoupon(success: success, message: message, type: type, chosenGroup: self.groupId)
                    self.finishedCheckCoupon = true
                    print("Type 1:", type)
                    
                    // If coupon type is combo or choice, check if coupon exists in voz crm
                    //self.comboChoiceVozCheck (couponCheck: self.couponCheck)
                    //self.containerUIView.isHidden = false
                    
                } else {
                    print("Dismiss")
                    self.delegate?.onFailedToCheckCoupon(data: message)
                    
                    //antes de convertir el cuerpo de la app en nav controller
                    ////antes de convertir el cuerpo de la app en nav controller
                    //self.dismiss(animated: true, completion: nil)
                    
                    self.navigationController?.popViewController(animated: false)
                    
                    // Checking if coupon exist in publifon voz CRM
                    //                     if tempType != "seleccion" && tempType != "combo" {
                    //                         self.vozCouponCheck(coupon: coupon, type: Utils.CouponType.none)
                    //                     } else {
                    //                         self.delegate?.onFailedToCheckCoupon(data: "Este cupón ya fue utilizado previamente")
                    //                         //antes de convertir el cuerpo de la app en nav controller
                    //self.dismiss(animated: true, completion: nil)
                    //                     }
                    //return
                }
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                self.delegate?.onFailedToCheckCoupon(data: Utils.CONECTION_ISSUES)
                
                //antes de convertir el cuerpo de la app en nav controller
                //self.dismiss(animated: true, completion: nil)
                
                self.navigationController?.popViewController(animated: false)
                
            }
            
        }
        
    }
    
    // <<<<<<<<<<< UNWIND
    @IBAction func unwindToExtendedViewController(_ sender: UIStoryboardSegue){
        
    }
    
    // Display Action Sheet
    func displayActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
            //antes de convertir el cuerpo de la app en nav controller
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: false)
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Display AppStore Action Sheet
    func displayAppStoreActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
            UIApplication.shared.open(NSURL(string: Utils.ITUNES_URL)! as URL)
            //antes de convertir el cuerpo de la app en nav controller
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: false)
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func displayMaintainanceActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
            //antes de convertir el cuerpo de la app en nav controller
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: false)
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // New method to check if coupon exists
    //    func checkCoupon(coupon: String) {
    //
    //        // Making Request
    //        Alamofire.request(configureRequest(coupon: coupon, url: Utils.CHECK_COUPON_URL)).responseData {
    //            dataResponse in
    //
    //            if dataResponse.result.isSuccess {
    //                let json : JSON = JSON(dataResponse.result.value!)
    //                self.datosJSON = json
    //                print("Type 0:", self.datosJSON ?? "no json")
    //                let message = json[Utils.MESSAGE].string!
    //                var type = ""
    //                var success = json[Utils.SUCCESS].bool
    //
    //                // Checking if coupon exists in publifon datos CRM
    //                // If coupon exists in datos CRM create a coupon check object and assign its type
    //                if success != nil && success! {
    //                    let benefits = self.datosJSON!["benefits"].array!
    //                    print("benefits: ", benefits.count)
    //                    for benefit in benefits {
    //                        print("Benefit group_name: ", benefit["group_name"].string!)
    //                        print("------------------", "------------------")
    //                        for application in benefit["applications"].array!{
    //                            print("Benefit application: ", application["application"].string!)
    //                            print("Benefit hours: ", application["hours"].int!)
    //                            print("Benefit mb: ", application["mb"].int!)
    //                            print("Benefit application_id: ", application["application_id"].int!)
    //                            print("----------------------")
    //                        }
    //                        print("------------------", "------------------")
    //                    }
    //                    self.stopActivityIndicator()
    //                    type = json[Utils.TYPE].string!
    //                    success = json[Utils.SUCCESS].bool!
    //
    //                    if benefits.count > 1 {
    //                        type = "seleccion"
    //                    }
    //
    //                    // Initializes coupon check object
    //                    self.couponCheck = CheckCoupon(success: success!, message: message, type: type)
    //                    print("Type 1:", type)
    //
    //                    // If coupon type is combo or choice, check if coupon exists in voz crm
    //                    self.comboChoiceVozCheck (couponCheck: self.couponCheck)
    //
    //                } else{
    //                    self.delegate?.onFailedToCheckCoupon(data: message)
    //                    //antes de convertir el cuerpo de la app en nav controller
    //self.dismiss(animated: true, completion: nil)
    //                }
    //
    //            } else {
    //                print("Error: \(dataResponse.result.error!)")
    //                self.delegate?.onFailedToCheckCoupon(data: Utils.CONECTION_ISSUES)
    //                //antes de convertir el cuerpo de la app en nav controller
    //self.dismiss(animated: true, completion: nil)
    //            }
    //
    //        }
    //    }
    
    // If coupon type is combo or choice, check if coupon exists in voz crm
    // else show container UI View
    func comboChoiceVozCheck (couponCheck: CheckCoupon) {
        
        if self.couponCheck.couponType == Utils.CouponType.choice
            || self.couponCheck.couponType == Utils.CouponType.combo {
            self.vozCouponCheck(coupon: coupon, type: self.couponCheck.couponType)
        } else {
            //self.containerUIView.isHidden = false
        }
        
    }
    
    //520022
    func vozCouponCheck(coupon: String, type: Utils.CouponType) {
        Alamofire.request(configureRequest(coupon: coupon, url: Utils.VOZ_COUPON_CHECK_URL)).responseData {
            dataResponse in
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                self.vozJSON = json
                print("Type 3 Voz:", self.vozJSON ?? "no json")
                let success = json["success"].bool!
                let message = json["message"].string!
                
                // If coupon exists in voz CRM create a coupon check object and assign its type
                if success {
                    self.stopActivityIndicator()
                    //self.containerUIView.isHidden = false
                    _ = json["validity"].int!
                    
                    // If coupon is not combo and not choice,
                    // then assign voz type to coupon.
                    print("Voz Coupon check:", type)
                    print("Voz Coupon check success:", success)
                    print("Voz Coupon check message:", message)
                    
                    if type != Utils.CouponType.combo && type != Utils.CouponType.choice {
                        self.couponCheck = CheckCoupon(success: success, message: message, type: "voz", chosenGroup: "")
                    }
                    //json
                } else {
                    self.stopActivityIndicator()
                    self.delegate?.onFailedToCheckCoupon(data: message)
                    //antes de convertir el cuerpo de la app en nav controller
                    //self.dismiss(animated: true, completion: nil)
                    
                    self.navigationController?.popViewController(animated: false)
                }
            } else {
                print("Error: \(dataResponse.result.error!)")
                self.delegate?.onFailedToCheckCoupon(data: Utils.CONECTION_ISSUES)
                //antes de convertir el cuerpo de la app en nav controller
                //self.dismiss(animated: true, completion: nil)
                
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String, url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url+coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.GET
        //request.timeoutInterval = 5
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        return request
    }
    
    //Preparing for segue, passing coupon code
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {        
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        
        if segue.identifier == Utils.GO_TO_VALIDATION_VIEW_CONTROLLER {
            let validationViewController = segue.destination as! ValidationViewController
            validationViewController.couponCode = self.coupon
            validationViewController.isFromQR = self.isFromQR
        }
        
        if segue.identifier == Utils.GO_TO_CHOICE_VIEW_CONTROLLER {
            let choiceViewController = segue.destination as! ChoiceViewController
            choiceViewController.delegate = self
            choiceViewController.datosJSON = self.datosJSON
            choiceViewController.vozJSON = self.vozJSON
        }
        
        if segue.identifier == "goToChoosePacket"{
            let packetAppsViewController = segue.destination as! PacketAppsViewController
            packetAppsViewController.delegate = self
            packetAppsViewController.coupon = self.coupon
            ScrollViewPacketViewController.datosJSON = self.datosJSON
            packetAppsViewController.logoURL = self.logoURL
            
            print("in prepare ", coupon)
            print("in prepare ", datosJSON)
        }
    }
    
    // Protocol method implementation
    func onCouponSelectedChoice(selected: Utils.CouponSelectedChoice) {
        self.couponSelectedChoice = selected
        startActivityIndicator()
        
        // Sending data back to extended validation controller
        // For some reason that i dont know yet,
        // this controller has to be dismissed in extended validation controller in addition to
        // dismissing it here
        delegate?.onFinishCheckingChoiceCoupon(type: self.couponCheck.couponType, phoneNumber: telNumberUITextField.text!, selected: self.couponSelectedChoice, view: self)
        
        // Dismissing this view controller
        //self.navigationController?.popViewController(animated: true)
        
        
        //antes de convertir el cuerpo de la app en nav controller
        //self.dismiss(animated: true, completion: nil)
        
        self.navigationController?.popViewController(animated: false)
    }
    
}
