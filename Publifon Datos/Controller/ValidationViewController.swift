//
//  ValidationViewController.swift
//  Publifon Datos
//
//  Created by Admin on 13/09/18.
//  Copyright © 2018 Bluelabs. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import CoreTelephony

import FirebaseInstanceID
import FirebaseCore
import Firebase
import Lottie

protocol FailedToRedeemCoupon {
    func onFailedToRedeemCoupon(data: String)
}

class ValidationViewController: UIViewController, CLLocationManagerDelegate {

    // Linked outlets
    @IBOutlet weak var waitingGif: UIImageView!
        
    // Class variables
    var delegate : FailedToRedeemCoupon?
    var couponCode: String = ""
    var deviceToken: String = ""
    var carrierName: String = ""
    var phoneNumber: String = ""
    var chosenGroup = ""
    let device: String = "iOS"
    let JSON_KEY : String = "json"
    var couponType = Utils.CouponType.none
    var couponSelectedChoice = Utils.CouponSelectedChoice.none
    var chosenServiceURL = ""
    var despisedServiceURL = ""
    
    //location variables
    let locationManager = CLLocationManager()
    var isFromQR = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("WebsereviceURL code 1:", couponCode)
        //print("WebsereviceURL code 2:", Utils.APPLE)
        print("WebsereviceURL url 2:", Utils.WEBSERVICE_URL)
        
        // Cleaning phone number
        let cleanedNumber = Utils.cleanPhoneNumber(phoneNumber: phoneNumber)
        phoneNumber = cleanedNumber
        
        // Cleaning coupon code
        if !couponCode.contains("@") {
            couponCode = couponCode.components(separatedBy: CharacterSet.alphanumerics.inverted).joined(separator: "")
        }
        // Stopping activity indicator started at Insert code view controller
        //InsertCodeViewController.stopActivityIndicator()
        InsertCodeViewController.activityIndicator.startAnimating()
        
        // Work around to dismiss extended view controller
        // because it does not dismiss from itself
        if InsertCodeViewController.extendedViewController != nil {
            InsertCodeViewController.extendedViewController!.dismiss(animated: true, completion: nil)
        }
        
        // Animates a sequence of images as a gif
        let animationGifView = AnimationView(name: "Activacion3")
        animationGifView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        animationGifView.contentMode = .scaleToFill
        animationGifView.play()
        animationGifView.loopMode = .loop
        waitingGif.insertSubview(animationGifView, at: 0)
        
        
        
        //??
//        let svc = self.storyboard?.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
//        self.present(svc, animated: true, completion: nil)
        
        //getting phone carrier name
        #if targetEnvironment(simulator)
        print("Running on simulator assigning telcel to carrier")
        self.carrierName = "telcel"
        //carrierName = "error de carrier"
        #else
        // Checking if device has a carrier name
        if  CTTelephonyNetworkInfo().subscriberCellularProvider?.carrierName != nil {
            let networkInfo = CTTelephonyNetworkInfo()
            let carrier = networkInfo.subscriberCellularProvider
            self.carrierName = Utils.getCarrier(carrier: (carrier?.carrierName)!)
            let allowedSet =  NSCharacterSet(charactersIn:"&").inverted
            let scapedCarrierName = self.carrierName.addingPercentEncoding(withAllowedCharacters: allowedSet)
            //self.carrierName = "carrier falso"//scapedCarrierName!
            self.carrierName = scapedCarrierName!
        } else {
            print("Real device but no Carrier")
            self.carrierName = "NOCARRIER"
        }
        #endif
        print("Carrier name:", carrierName)
        print("Coupon code:", couponCode)
        
        // Assigning firebase token
        deviceToken = getFCMToken()
        print("Firebase: ", deviceToken)
        
        //location setup
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Gets FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            UserDefaults.standard.set(token, forKey: Utils.DEVICE_TOKEN)
            return token
        } else {
            UserDefaults.standard.set(Utils.RESET, forKey: Utils.DEVICE_TOKEN)
            return ""
        }
    }
    
    // Notify user if location was not obtained
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
        //showToast(title: "Ocurrio un problema", text: "No se pudo obtener tu ubicación")
        self.delegate?.onFailedToRedeemCoupon(data: Utils.COULD_NOT_GET_LOCATION)
        dismiss(animated: true, completion: nil)
    }
    
    // Gets user location coordinates and calls method to get API token
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if (location.horizontalAccuracy > 0) {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            let latitude = String(location.coordinate.latitude)
            let longitude = String(location.coordinate.longitude)
            redeemCouponTypeManager(couponType: self.couponType, latitude: latitude, longitude: longitude, phoneNumber: self.phoneNumber, chosenGroup: self.chosenGroup)
        }
    }
    
    // Manages coupon type redemption
    func redeemCouponTypeManager(couponType: Utils.CouponType, latitude: String, longitude: String, phoneNumber: String, chosenGroup: String) {
        
        switch couponType {
            
            // Gets api token and redeems coupon
        case Utils.CouponType.datos:
            getAPIToken(latitude: latitude, longitude: longitude, carrierName: self.carrierName, deviceToken: self.deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
            
            // Voice coupon logic
        case Utils.CouponType.voz:
            getAPIToken(latitude: latitude, longitude: longitude, carrierName: self.carrierName, deviceToken: self.deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
//            getVozCouponValidation(websereviceURL: Utils.VOZ_WEBSERVICE_URL, coupon: couponCode,
//                                   lat: latitude, lng: longitude, device: device, phoneNumber: phoneNumber)
            // Choice coupon logic
        case Utils.CouponType.choice:
            getAPIToken(latitude: latitude, longitude: longitude, carrierName: self.carrierName, deviceToken: self.deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
            
            // Combo coupon logic
        case Utils.CouponType.combo:
            getAPIToken(latitude: latitude, longitude: longitude, carrierName: self.carrierName, deviceToken: self.deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
            
        default:
            self.delegate?.onFailedToRedeemCoupon(data: Utils.INVALID_COUPON)
            dismiss(animated: true, completion: nil)
        }
    }
    
    // Gets security token from API
    func getAPIToken (latitude: String, longitude: String, carrierName: String,
                      deviceToken: String, phoneNumber: String, chosenGroup: String) {
        print("TOKEN_URL: ", Utils.CREATE_API_TOKEN_URL)
        // Setting up alamofire request
        var request = URLRequest(url: NSURL.init(string: Utils.CREATE_API_TOKEN_URL)! as URL)
        request.httpMethod = Utils.GET
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        //request.timeoutInterval = 5 // 5 secs
        request.timeoutInterval = 60
        
        Alamofire.request(request).responseData {
            dataResponse in
            if dataResponse.result.isSuccess {
                let apiJSONToken : JSON = JSON(dataResponse.result.value!)
                print("apiJSONToken:", apiJSONToken)
                // Parsing reponse
                self.parseJSONResponse(json: apiJSONToken, latitude: latitude,
                longitude: longitude, carrierName: carrierName,
                deviceToken: deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                self.present(menuNavigation, animated: true, completion: {
                    self.delegate?.onFailedToRedeemCoupon(data: Utils.CONECTION_ISSUES)
                })
                //self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // JSON Parsing
    func parseJSONResponse(json : JSON, latitude: String, longitude: String,
                           carrierName: String, deviceToken: String,
                           phoneNumber: String, chosenGroup: String) {
        let apiToken = json[Utils.ACCESS_TOKEN].string
        
        // Saving apiToken into memory
        UserDefaults.standard.set(apiToken, forKey: Utils.API_TOKEN)
        
        // Redeem datos/only voz coupon
        if couponType == Utils.CouponType.datos {
            getDatosCouponValidation(websereviceURL: Utils.WEBSERVICE_URL, coupon: self.couponCode,
                                     lat: latitude, lng: longitude, carrierName: self.carrierName,
                                     apiToken: apiToken!, deviceToken: deviceToken,
                                     phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        }
        
        // Redeem only voz coupon
        if couponType == Utils.CouponType.voz {
            getDatosCouponValidation(websereviceURL: Utils.WEBSERVICE_URL, coupon: self.couponCode,
                                     lat: latitude, lng: longitude, carrierName: self.carrierName,
                                     apiToken: apiToken!, deviceToken: deviceToken,
                                     phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        }
        
        // Redeem choice coupon
        if couponType == Utils.CouponType.choice {
            print("************** a ver que odna")
            //print(apiToken)
            //print(Utils.WEBSERVICE_URL + "\n" + self.couponCode + "\n" + latitude + "\n" + longitude + "\n" + self.carrierName + "\n" + apiToken! + "\n" + deviceToken + "\n" + phoneNumber + "\n" + chosenGroup)
            getChoiceDatosRedemtion(websereviceURL: Utils.WEBSERVICE_URL, coupon: self.couponCode, lat: latitude, lng: longitude, carrierName: self.carrierName, apiToken: apiToken!, deviceToken: deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
//            if couponSelectedChoice == Utils.CouponSelectedChoice.datos {
//                getChoiceDatosRedemtion(websereviceURL: Utils.WEBSERVICE_URL, coupon: self.couponCode, lat: latitude, lng: longitude, carrierName: self.carrierName, apiToken: apiToken!, deviceToken: deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
//            }
//
//            if couponSelectedChoice == Utils.CouponSelectedChoice.voz {
//                getChoiceVozRedemption(websereviceURL: Utils.VOZ_WEBSERVICE_URL, coupon: self.couponCode, lat: latitude,
//                                       lng: longitude, device: self.device, phoneNumber: self.phoneNumber, apiToken: apiToken!, chosenGroup: chosenGroup)
//            }
        }
        
        // Redeem combo coupon
        if couponType == Utils.CouponType.combo {
            print("WebsereviceURL 2",  Utils.WEBSERVICE_URL)
            getChoiceDatosRedemtion(websereviceURL: Utils.WEBSERVICE_URL, coupon: self.couponCode, lat: latitude, lng: longitude, carrierName: self.carrierName, apiToken: apiToken!, deviceToken: deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        }
    }
    
    // Choice coupon type
    func getChoiceDatosRedemtion(websereviceURL: String, coupon: String, lat: String,
                              lng: String, carrierName: String,
                              apiToken: String, deviceToken: String,
                              phoneNumber: String, chosenGroup: String) {
        print("WebsereviceURL 3", websereviceURL)
        // Gets a datos configured request for alamofire
        let request = configureDatosRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                            lng: lng, carrierName: carrierName,
                                            apiToken: apiToken, deviceToken: deviceToken,
                                            phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        
        // Making redemption request
        Alamofire.request(request).responseJSON {
            response in
            print("response.result.isSuccess: ", response.result.isSuccess)
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
//*************************************************************************************************************************************************************
                print("couponJSON 3:", couponJSON)
                let message = couponJSON[Utils.MESSAGE].string
                let status = couponJSON[Utils.STATUS].bool
                
                if status == nil || !status! {
                    //self.dismiss(animated: true, completion: nil)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                    self.present(menuNavigation, animated: true, completion: {
                        self.delegate?.onFailedToRedeemCoupon(data: message!)
                    })
                } else {
                    self.saveDatosData(json: couponJSON)
                    self.saveVozData(json:couponJSON)
                    self.goToSuccessViewController()
//                    self.getChoiceDatosVozRedemption(websereviceURL: Utils.VOZ_WEBSERVICE_URL, coupon: self.couponCode, lat: lat,
//                                             lng: lng, device: self.device, phoneNumber: self.phoneNumber)
                }
            } else {
                print("Error: \(response.result.error!)")
                print("Error: \(JSON(response))")
                print("dentro de esta cosa: ", response)
                let b : JSON = JSON(response)
                print("dentro de esta cosa x2: ", b)
                print("dentro de esta cosa x3: ", self.carrierName)
                //self.dismiss(animated: true, completion: nil)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                self.present(menuNavigation, animated: true, completion: {
                    self.delegate?.onFailedToRedeemCoupon(data: Utils.COULD_NOT_VALIDATE_COUPON)
                })
            }
        }
    }
    
    func getChoiceDatosVozRedemption(websereviceURL: String, coupon: String, lat: String,
                             lng: String, device: String, phoneNumber: String) {
        // Gets a voz configured request for alamofire
        let request = configureVozRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                          lng: lng, device: device, phoneNumber: phoneNumber)
        Alamofire.request(request).responseJSON {
            response in
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
                _ = couponJSON[Utils.VOZ_SUCCESS].bool
                print("Voz JSON:", couponJSON)
                if self.couponType == Utils.CouponType.combo {
                    self.saveVozData(json: couponJSON)
                }
                self.goToSuccessViewController()
            } else {
               self.goToSuccessViewController()
            }
        }
    }
    
    func getChoiceVozRedemption(websereviceURL: String, coupon: String, lat: String,
                                lng: String, device: String, phoneNumber: String,
                                apiToken: String, chosenGroup: String) {
        // Gets a voz configured request for alamofire
        let request = configureVozRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                          lng: lng, device: device, phoneNumber: phoneNumber)
        Alamofire.request(request).responseJSON {
            response in
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
                let success = couponJSON[Utils.VOZ_SUCCESS].bool
                if success! {
                    print("Voz coupon successfully redeemed")
                    print("Voz JSON:", couponJSON)
                    self.saveVozData(json: couponJSON)
                    self.getChoiceVozDatosRedemption (websereviceURL: Utils.WEBSERVICE_URL, coupon: coupon, lat: lat, lng: lng, carrierName: self.carrierName, apiToken: apiToken, deviceToken: self.deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
                } else {
                    let message = couponJSON[Utils.MESSAGE].string
                    //self.dismiss(animated: true, completion: nil)
                    let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                    self.present(menuNavigation, animated: true, completion: {
                        self.delegate?.onFailedToRedeemCoupon(data: message!)
                    })
                }
            } else {
                print("Error: \(response.result.error!)")
                //self.dismiss(animated: true, completion: nil)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                self.present(menuNavigation, animated: true, completion: {
                    self.delegate?.onFailedToRedeemCoupon(data: Utils.COULD_NOT_VALIDATE_COUPON)
                })
            }
        }
    }
    

    func getChoiceVozDatosRedemption(websereviceURL: String, coupon: String, lat: String,
                                     lng: String, carrierName: String,
                                     apiToken: String, deviceToken: String,
                                     phoneNumber: String, chosenGroup: String) {
        // Gets a datos configured request for alamofire
        let request = configureDatosRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                            lng: lng, carrierName: carrierName,
                                            apiToken: apiToken, deviceToken: deviceToken,
                                            phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        Alamofire.request(request).responseJSON {
            response in
            print("log dentro de getChoiceVozDatosRedemption 1: ",  response.result.isSuccess)
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
                print("log dentro de getChoiceVozDatosRedemption: ",  couponJSON)
                let message = couponJSON[Utils.MESSAGE].string
                _ = couponJSON[Utils.STATUS].bool
                self.saveDatosData(json: couponJSON)
                print("Datos coupon response:", couponJSON)
                self.showToast(title: Utils.CONGRATULATIONS, text: message!)
                self.goToSuccessViewController()
            } else {
                self.showToast(title: Utils.CONGRATULATIONS, text: "Cupón redimido")
                self.goToSuccessViewController()
            }
        }
    }
    
    // Get datos coupon validation
    func getDatosCouponValidation(websereviceURL: String, coupon: String, lat: String, lng: String,
                             carrierName: String, apiToken : String,
                             deviceToken: String, phoneNumber: String, chosenGroup: String) {
        // Gets a datos configured request for alamofire
        print("Coupon:", coupon)
        let request = configureDatosRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                       lng: lng, carrierName: carrierName,
                                       apiToken: apiToken, deviceToken: deviceToken, phoneNumber: phoneNumber, chosenGroup: chosenGroup)
        Alamofire.request(request).responseJSON {
            response in
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
                print("CouponJSON:", couponJSON)
                self.datosCouponResponseValidation(json: couponJSON)
            } else {
                print("Error: \(response.result.error!)")
                
                //self.dismiss(animated: true, completion: nil)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                self.present(menuNavigation, animated: true, completion: {
                    self.delegate?.onFailedToRedeemCoupon(data: Utils.COULD_NOT_VALIDATE_COUPON)
                })
            }
        }
    }
    
    // Get voz coupon validation
    func getVozCouponValidation(websereviceURL: String, coupon: String, lat: String,
                                lng: String, device: String, phoneNumber: String) {
        // Gets a voz configured request for alamofire
        let request = configureVozRequest(websereviceURL: websereviceURL, coupon: coupon, lat: lat,
                                            lng: lng, device: device, phoneNumber: phoneNumber)
        Alamofire.request(request).responseJSON {
            response in
            if response.result.isSuccess {
                let couponJSON : JSON = JSON(response.result.value!)
                self.vozCouponResponseValidation(json: couponJSON)
            } else {
                print("Error: \(response.result.error!)")
                //self.dismiss(animated: true, completion: nil)
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
                self.present(menuNavigation, animated: true,  completion: {
                    self.delegate?.onFailedToRedeemCoupon(data: Utils.COULD_NOT_VALIDATE_COUPON)
                })
            }
        }
    }
    
    // Configuring Datos Request, returns a configured request
    func configureDatosRequest(websereviceURL: String, coupon: String, lat: String, lng: String,
                          carrierName: String, apiToken : String,
                          deviceToken: String, phoneNumber: String,
                          chosenGroup: String) -> URLRequest {
        
        // This code escapes unwanted characters
        let url : NSString = websereviceURL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Defining url for request and method
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.POST
        
        print("el token antes de enviar: ", apiToken)
        
        // Adding request headers
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        //request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: "content-type")
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        
        // Setting request body parameters
        var postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)&\(Utils.CARRIER_NAME)=\(carrierName)&\(Utils.LATITUDE)=\(lat)&\(Utils.LONGITUDE)=\(lng)&\("platform")=\("iOS")&\("phone_number")=\(phoneNumber)&\("chosen_group")=\(chosenGroup)"
        
        if isFromQR {
            print("isFromQR dentro del validation")
            if let data = UserDefaults.standard.value(forKey:"userData") as? Data {
                if let decoded = try? PropertyListDecoder().decode(UserData.self, from: data) {
                    
                    let bodyDictionary: [String: Any] = ["\(Utils.API_DEVICE_TOKEN)": "\(deviceToken)",
                        "\(Utils.CARRIER_NAME)": "\(carrierName)",
                        "\(Utils.LATITUDE)": lat,
                        "\(Utils.LONGITUDE)": lng,
                        "platform": "iOS",
                        "phone_number": "\(phoneNumber)",
                        "chosen_group": chosenGroup,
                        "isFromQR": true,
                        "prizeData": ["userID": "\(decoded.userID)",
                            "unicode": "\(decoded.unicode)",
                            "latitude": decoded.latitude,
                            "longitude": decoded.longitude,
                            "osVersion": "\(decoded.osVersion)",
                            "buildVersion": "\(decoded.buildVersion)",
                            "modelName":"\(decoded.modelName)",
                            "deviceID":"\(decoded.deviceID)"
                        ]
                    ]
                    
                    let postBodyData = try! JSONSerialization.data(withJSONObject: bodyDictionary, options: .prettyPrinted)
                    let jsonString = String(data: postBodyData, encoding: .utf8)
                    postString = jsonString!

                    request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: "content-type")
                    
                }
            }
        }
        
        request.httpBody = postString.data(using: .utf8)
        //request.timeoutInterval = 20 // 5 secs
        request.timeoutInterval = 60
//        print("REQUEST_DATOS:", request)
//        print("REQUEST_DATOS la x :", isFromQR ? "es desde qr" : "no es desde qr" )
//        print("*******************************************\n")
//        print("chosenGroup: ", chosenGroup)
        return request
    }
    
    // Configuring a Voz request, returns a configured request
    func configureVozRequest(websereviceURL: String, coupon: String, lat: String,
                             lng: String, device: String, phoneNumber: String) -> URLRequest {
        // This code escapes unwanted characters
        let url : NSString = websereviceURL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Defining url for request and method
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.POST
        
        // Setting request body parameters
        let postString = "\(Utils.PHONE_NUMBER)=\(phoneNumber)&\(Utils.COUPON_CODE)=\(coupon)&\(Utils.DEVICE)=\(device)&\(Utils.LATITUDE)=\(lat)&\(Utils.LONGITUDE)=\(lng)"
        request.httpBody = postString.data(using: .utf8)
        //request.timeoutInterval = 5 // 5 secs
        request.timeoutInterval = 60
        
        return request
        
        
    }
    
    // Validates coupon response, if saves coupon JSON
    // and goes to Success activity
    func datosCouponResponseValidation(json: JSON) {
        let message = json[Utils.MESSAGE].string
        let status = json[Utils.STATUS].bool
        
        if status == nil || !status! {
            //self.dismiss(animated: true, completion: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
            self.present(menuNavigation, animated: true, completion: {
                self.delegate?.onFailedToRedeemCoupon(data: message!)
            })
        } else {
            showToast(title: Utils.CONGRATULATIONS, text: message!)
            if couponType == Utils.CouponType.voz { saveVozData(json: json)
            } else { saveDatosData(json: json) }
            goToSuccessViewController()
            
        }
        
    }
    
    // Validates coupon response, if saves coupon JSON
    // and goes to Success activity
    func vozCouponResponseValidation(json: JSON) {
        let success = json[Utils.VOZ_SUCCESS].bool
        if success! {
            print("Voz coupon successfully redeemed")
            print("Voz JSON:", json)
            saveVozData(json: json)
            goToSuccessViewController()
        } else {
            let message = json[Utils.MESSAGE].string
            //self.dismiss(animated: true, completion: nil)
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
            self.present(menuNavigation, animated: true, completion: {
                self.delegate?.onFailedToRedeemCoupon(data: message!)
            })
        }
    }
    
    // Saves datos json to memory
    func saveDatosData(json:JSON) {
        // Deleting last coupon
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.JSON_KEY)
        
        // Saving new datos coupon state
        UserDefaults.standard.set(json.rawString()!, forKey: Utils.JSON_KEY)
        UserDefaults.standard.set(true, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
        UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
        UserDefaults.standard.set(["", "", ""], forKey: Utils.SLIDER_ARRAY)
        UserDefaults.standard.set(couponType.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(couponSelectedChoice.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        
    }
    
    // Saves voz json to memory
    func saveVozData(json:JSON) {
        // Deleting last coupon
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
        
        // Saving new voz coupon state
        UserDefaults.standard.set(couponCode, forKey: Utils.VOZ_COUPON_CODE)
        UserDefaults.standard.set(json.rawString()!, forKey: Utils.VOZ_JSON_KEY)
        UserDefaults.standard.set(true, forKey: Utils.VOZ_COUPON_ACTIVE)
        UserDefaults.standard.set(phoneNumber, forKey: Utils.PHONE_NUMBER)
        UserDefaults.standard.set(couponType.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(couponSelectedChoice.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.getVozCouponObject().homeURL, forKey: Utils.VOZ_HOME_IMAGE_NAME)
        UserDefaults.standard.set(Utils.imageDayCheck(), forKey: Utils.IMAGE_DAY_CHECK)
    }
    
    // Goes to Success View Controller
    func goToSuccessViewController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let successViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.SUCCESS_VIEW_CONTROLLER) as! SuccessViewController
        successViewController.couponType = self.couponType
        successViewController.couponSelectedChoice = self.couponSelectedChoice
        appDelegate.window?.rootViewController = successViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Goes to Success View Controller
    func goToMainViewController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.MAIN_VIEW_CONTROLLER) as! MainViewController
        appDelegate.window?.rootViewController = mainViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Preparing for segue, passing coupon code
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToSuccessViewController" {
            let successViewController = segue.destination as! SuccessViewController
            successViewController.couponSelectedChoice = couponSelectedChoice
            successViewController.couponType = couponType
        }
    }
    
    // Prints coupon JSON
    func getJSON(key: String) {
        let json: JSON = JSON.init(parseJSON: UserDefaults.standard.string(forKey: key)!)
        print("json <<<<<<: ")
        print(json)
    }   
    
    // Toast code
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    
    
    
    
    
    
    
}

