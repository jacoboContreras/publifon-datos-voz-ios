//
//  ConnectingViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/16/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import Lottie

class ConnectingViewController: UIViewController, CLLocationManagerDelegate {
    
    // Location and class variables
    let locationManager = CLLocationManager()
    var vozCouponCode = ""
    var phoneNumber = ""
    var timer: Timer?
    var contactName = ""
    var contactNumbersArray = [Numbers]()
    var recentCallsArray = [RecentCalls]()
    var contactImage = UIImage()
    var couponObject: CouponModel?
    var couponType = ""
    var couponSelectedChoice = ""

   
    @IBOutlet weak var messageLabel: UILabel!
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gets recent calls array
        recentCallsArray = loadRecentCalls()
        
        // Getting saved voz coupon code
        vozCouponCode = UserDefaults.standard.string(forKey: Utils.VOZ_COUPON_CODE)!

        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
       
        // If coupon is voz type then set default colors
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
           
            messageLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }else{
            couponObject = Utils.getCouponObject()
            let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
            self.messageLabel.textColor = accentuationColor
            
        }
       
        
  
        //location setup
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
        //set gif with lottie
        let animationGifView = AnimationView(name: "Activacion3")
        animationGifView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        animationGifView.contentMode = .scaleToFill
        animationGifView.play()
        animationGifView.loopMode = .loop
        view.insertSubview(animationGifView, at: 0)
       
        
        print("Connecting: " + phoneNumber)
    }
    
    // Gets user location coordinates and calls method to get API token
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        if (location.horizontalAccuracy > 0) {
            locationManager.stopUpdatingLocation()
            locationManager.delegate = nil
            
            let latitude = String(location.coordinate.latitude)
            let longitude = String(location.coordinate.longitude)
            
            // Extracting only numbers from phoneNumber
            let phone = self.phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
            // Making call request
            callRequest(couponCode: self.vozCouponCode, phoneNumber: phone, latitude: latitude, longitude: longitude)
            
        }
    }
    
    // Notify user if location was not obtained 
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    // Supend coupon logic
    func callRequest (couponCode: String, phoneNumber: String, latitude: String, longitude: String) {
        // Making Request
        print("Call Request response code 0: " + couponCode)
        print("Call Request response phoneNumber 0: " + phoneNumber)
        print("Call Request response latitude 0: " + latitude)
        print("Call Request response longitude 0: " + longitude)
        Alamofire.request(configureRequest(couponCode: couponCode, phoneNumber: phoneNumber, latitude: latitude, longitude: longitude)).responseData {
            dataResponse in
            print("Call Request response 1: ", dataResponse.result.isSuccess)
            print(dataResponse)
            if dataResponse.result.isSuccess {
                print("Call Request response 2: ", dataResponse.result.value!)
                print("Call Request response 3|: ", dataResponse)
                let json : JSON = JSON(dataResponse.result.value!)
                print("VOZ_CALL_JSON 1: ", json)
                let valid = json["Valid"].bool
                let message = json["message"].string
                print("VOZ_CALL_JSON 2: ", message)
                if valid! {
                    
                    // change label message text
                    self.messageLabel.text = "En menos de 15 segundos recibirás una llamada\n\n¡Contéstala!"
                    // Saving call to recent calls
                    self.saveRecentCalls(name: self.contactName, number: self.phoneNumber, time: Utils.getCurrentDate(), image: self.contactImage)
                    // Updates contacts table view in Call view controller
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "load"), object: nil)
                    // Dismiss view controller after 10 seconds
                    self.timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.timerAction), userInfo: nil, repeats: true)
                } else {
                    self.toastMessage(message!)
                    self.dismiss(animated: true, completion: nil)
                }
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                self.toastMessage("Problemas de enlace")
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
    }
    
    // Dismiss view controller after 10 seconds
    @objc func timerAction() {
        // Check the overall coupon expiration
        timer?.invalidate()
        dismiss(animated: true, completion: nil)
        
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(couponCode: String,phoneNumber: String, latitude: String, longitude: String) -> URLRequest {
        
        // Getting corresponding url
        let urlRequest = Utils.VOZ__MAKE_CALL_WEBSERVICE_URL//"https://publifonapp.com/v3_beta/secciones/POST_MakeCall.php"
        // This code escapes unwanted characters
        let url : NSString = urlRequest as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let expireURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: expireURL as URL)
        request.httpMethod = Utils.POST
        //request.timeoutInterval = 15 //5
        request.timeoutInterval = 60
        print("Number:", phoneNumber)
        /*print("Utils.COUPON ", Utils.COUPON)
        print("couponCode ", couponCode)
        print("Utils.DESTINATION_NUMBER ", Utils.DESTINATION_NUMBER)
        print("phoneNumber ", phoneNumber)
        print("Utils.LAT ", Utils.LAT)
        print("latitude ", latitude)
        print("Utils.LNG ", longitude)*/
        
        let postString = "\(Utils.COUPON)=\(couponCode)&\(Utils.DESTINATION_NUMBER)=\(phoneNumber)&\(Utils.LAT)=\(latitude)&\(Utils.LNG)=\(longitude)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Saving recent calls array
    func saveRecentCalls(name: String, number: String, time: String, image: UIImage) {
        
        // Inserting recent call to array
        recentCallsArray.insert(RecentCalls(name: name, number: number, time: time, image: image), at: 0)
        if recentCallsArray.count == 11 {
            recentCallsArray.remove(at: 10)
        }
        
        // Saving array in user defaults
        let recentCallsData = NSKeyedArchiver.archivedData(withRootObject: recentCallsArray)
        UserDefaults.standard.set(recentCallsData, forKey: "places")
    }
    
    // Gets an array of recent calls
    func loadRecentCalls() -> [RecentCalls] {
        let recentCallsData = UserDefaults.standard.object(forKey: "places") as? NSData
        
        // Gets an array of recent saved calls
        if let recentCallsData = recentCallsData {
            let recentCalls = NSKeyedUnarchiver.unarchiveObject(with: recentCallsData as Data) as? [RecentCalls]
            
            // If there are recent calls return an array of them
            if let recentCalls = recentCalls {
                return recentCalls
            }
        }
        // Returns an empty array if there are no recent calls
        return [RecentCalls]()
    }
    
    
    
}
