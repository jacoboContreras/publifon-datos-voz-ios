//
//  NumberSelectionViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/23/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class NumberSelectionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    // Class variables
    var contactName = ""
    var contactNumbersArray = [Numbers]()
    var indexFollower = 0;
    var recentCallsArray = [RecentCalls]()
    var contactImage = UIImage()
    var name = ""
    var number = ""
    var time = ""
    var couponType = ""
    var backgroundColor = ""
    var accentuationColor = ""
    var textColor = ""
    
    // Linked outlets
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var contactNameUILabel: UILabel!
    @IBOutlet weak var contactNumbersTableView: UITableView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var contactImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        
        // Gets recent calls array
        recentCallsArray = loadRecentCalls()
        
        // Setting up rounded corner to container view
        containerUIView.layer.cornerRadius = 10
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)

        // Setting up table view delegates
        contactNumbersTableView.delegate = self
        contactNumbersTableView.dataSource = self
        
        // Clears bottom lines of extra table view items
        contactNumbersTableView.tableFooterView = UIView()
        contactNumbersTableView.alwaysBounceVertical = false
        
        // Setting contact name to label
        contactNameUILabel.text = contactName
        contactImageView.image = contactImage
        contactImageView.layer.cornerRadius = contactImageView.frame.size.width/2
        
        // Registers a ContactTableViewCell to contactsTableView
        self.contactNumbersTableView.register(UINib(nibName: "PhoneNumberTableViewCell", bundle: nil), forCellReuseIdentifier: "phoneNumberTableViewCell")
        
        // Reset index to 0 when tab is opened again
        indexFollower = 0
        
        // If coupon is a combo type then customize modal
        customizeModal()
        
    }
    
    // If coupon is a Combo/Choice type then get datos coupon object
    // and customize application
    func customizeModal() {
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue {
            let couponObject = Utils.getCouponObject()
            // Getting customization colors
            backgroundColor = couponObject.campaign.backgroungColor
            accentuationColor = couponObject.campaign.accentuationColor
            textColor = couponObject.campaign.textColor
            
            contactNumbersTableView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            containerUIView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            contactNameUILabel.textColor = Utils.hexToColor(hexString: textColor)
            
            //to change contact image tint
            Utils.changeContactIconColor(imageView: contactImageView,
                                         image: contactImage,
                                         couponObject: couponObject)
            
            callButton.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            callButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            callButton.borderColor = Utils.hexToColor(hexString: textColor)
            callButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            cancelButton.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            cancelButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            cancelButton.borderColor = Utils.hexToColor(hexString: textColor)
            cancelButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        }
    }
    
    // Call button action
    @IBAction func callActionButton(_ sender: Any) {
        print("Selected row: ", indexFollower)
        print("Selected phone: ", contactNumbersArray[indexFollower].number)
        self.name = self.contactName
        self.number = contactNumbersArray[indexFollower].number
        self.time = Utils.getCurrentDate()
        self.performSegue(withIdentifier: "goToConnectingViewController", sender: self)
    }
    
    // Cancel button action
    @IBAction func cancelActionButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Setting table view number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactNumbersArray.count
    }
    
    // Setting table view cells
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactNumbersTableView.dequeueReusableCell(withIdentifier: "phoneNumberTableViewCell", for: indexPath) as! PhoneNumberTableViewCell
        cell.selectionStyle = .none
        cell.typeLabel.text = contactNumbersArray[indexPath.row].name
        cell.numberLabel.text = contactNumbersArray[indexPath.row].number
        
        // If coupon is Hybrid then customize with datos campaign colors
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue {
            cell.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        }
        
        return cell
    }

    // Highlight of selected table view cell
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PhoneNumberTableViewCell
        cell.setSelected(true, animated: true)
        indexFollower = indexPath.row
        print("Selected row: ", indexPath.row)
        print("Selected row: ", indexFollower)
    }
    
    // Saving recent calls array
    func saveRecentCalls(name: String, number: String, time: String, image: UIImage) {
        // Inserting recent call to array
        recentCallsArray.insert(RecentCalls(name: name, number: number, time: time, image: image), at: 0)
        if recentCallsArray.count == 11 {
            recentCallsArray.remove(at: 10)
        }
        
        // Saving array in user defaults
        let recentCallsData = NSKeyedArchiver.archivedData(withRootObject: recentCallsArray)
        UserDefaults.standard.set(recentCallsData, forKey: "places")
    }
    
    // Gets an array of recent calls
    func loadRecentCalls() -> [RecentCalls] {
        let recentCallsData = UserDefaults.standard.object(forKey: "places") as? NSData
        
        // Gets an array of recent saved calls
        if let recentCallsData = recentCallsData {
            let recentCalls = NSKeyedUnarchiver.unarchiveObject(with: recentCallsData as Data) as? [RecentCalls]
            
            // If there are recent calls return an array of them
            if let recentCalls = recentCalls {
                return recentCalls
            }
            
        }
        // Returns an empty array if there are no recent calls
        return [RecentCalls]()
    }
    
    // Preparing for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToConnectingViewController" {
            let connectingViewController = segue.destination as! ConnectingViewController
            connectingViewController.contactName = self.name
            connectingViewController.phoneNumber = self.number
            print("Number selection: " + self.number)
            connectingViewController.contactImage = self.contactImage
        }
        
    }
    
}
