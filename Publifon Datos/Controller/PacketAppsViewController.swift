//
//  PacketAppsViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 10/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol SendSelectedGroup {
    func sendSelectedGroup(groupType: Utils.CouponType, chosenGroup: Int)
}


class PacketAppsViewController: UIViewController, SelectedGroup {
    
    func onSelectedGroup(groupType: Utils.CouponType, chosenGroup: Int, controller: UIViewController) {
        DispatchQueue.main.async {
            self.delegate?.sendSelectedGroup(groupType: groupType, chosenGroup: chosenGroup)
            //PacketAppsViewController.packetViewController.dismiss(animated: true, completion: nil)
            //self.dismiss(animated: true, completion: nil)
            self.performSegue(withIdentifier: "unwindToExtendedViewController", sender: self)
            
        }
    }
    

    // Linked Outlets
    @IBOutlet weak var pagesController: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var logoCompanyImageView: UIImageView!
    @IBOutlet weak var packageTitleLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var packetUIView: UIView!
    @IBOutlet weak var leftArrowButton: UIButton!
    @IBOutlet weak var rightArrowButton: UIButton!
    
    // Class Var
    var frame = CGRect(x:0,y:0,width:0,height:0)
    var groups = [Packet]()
    var groupsOfApps = [[App]]()
    static public var currentPage = Int()
    static var packetViewController = UIViewController()
    var delegate: SendSelectedGroup?
    var coupon = ""
    var couponObject: CouponModel?
    var logoURL = URL(string: "")
    var isSelectedPacket = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PacketAppsViewController.packetViewController = self
                
        //custom container view
        //containerView.layer.cornerRadius = 10
        
        //assign number of pages
        pagesController.numberOfPages = ScrollViewPacketViewController.numberOfPages
        
        groups = ScrollViewPacketViewController.groups
        groupsOfApps = ScrollViewPacketViewController.groupsOfApps
                
        //Notification to knows when change current page
        NotificationCenter.default.addObserver(self, selector: #selector(updateCurrentPage), name: NSNotification.Name(rawValue: "myName"), object: nil)
        
        customView()

    }
    
    func customView(){
        self.title = "Selecciona tu paquete"
        // Set company logo
        self.logoCompanyImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
        selectButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        selectButton.backgroundColor = StandarColorAndForms.blue
        selectButton.clipsToBounds = true
        
        packageTitleLabel.text = "Paquete \(groups[0].groupName)"
        
        //leftArrowButton.setImage(UIImage(named: "leftArrow"))
        //rightArrowButton.setImage(UIImage(named: "rightArrow"))
        
    }
    
    func dismissCiewcontroller() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func nextGroupPacket(_ sender: Any) {
        
        if pagesController.currentPage != ScrollViewPacketViewController.numberOfPages-1 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "moveRight"), object: nil)
            
        }
        
        
        pagesController.currentPage = pagesController.currentPage == ScrollViewPacketViewController.numberOfPages-1 ? pagesController.currentPage : pagesController.currentPage + 1
        
        packageTitleLabel.text = "Paquete \(groups[pagesController.currentPage].groupName)"
        
        
    }
    
    
    @IBAction func previoustGroupPacket(_ sender: Any) {
                
        
        if pagesController.currentPage != 0 {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "moveLeft"), object: nil)
        }
        
        pagesController.currentPage = pagesController.currentPage == 0 ? pagesController.currentPage : pagesController.currentPage - 1

        packageTitleLabel.text = "Paquete \(groups[pagesController.currentPage].groupName)"
        
        
    }
    
    // Select a packet button
    @IBAction func selectPacket(_ sender: UIButton) {
        
        /*PacketAppsViewController.currentPage = pagesController.currentPage
         
         //self.view.alpha = 1
         
         performSegue(withIdentifier: "goToConfirmSelectedPacket", sender: Any?.self)*/
        
        if isSelectedPacket == false {
                    
            packageTitleLabel.text = "Se va a activar tu paquete \(groups[Int(pagesController.currentPage)].groupName)"
            
            selectButton.setTitle("Confirmar", for: .normal)
            editButton.isHidden = false
            pagesController.isHidden = true
            packetUIView.isUserInteractionEnabled = false
            isSelectedPacket = true
            leftArrowButton.isHidden = true
            rightArrowButton.isHidden = true
            return
        } else if isSelectedPacket {
            isSelectedPacket = false
            // Assign id group of group selected
            let groupId = groups[pagesController.currentPage].groupId
            
            // Assing Coupon Type
            let groupType = groups[pagesController.currentPage].groupType
                        
            onSelectedGroup(groupType: groupType, chosenGroup: groupId, controller: self)            
            
            //self.delegate?.sendSelectedGroup(groupType: groupType, chosenGroup: groupId)
            //self.performSegue(withIdentifier: "unwindToExtendedViewController", sender: self)
        }
    }
    
    
    @IBAction func cancelSelection(_ sender: Any) {
        editButton.isHidden = true
        selectButton.setTitle("Siguiente", for: .normal)
        packageTitleLabel.text = "Paquete \(groups[Int(pagesController.currentPage)].groupName)"
        
        pagesController.isHidden = false
        packetUIView.isUserInteractionEnabled = true
        isSelectedPacket = false
        leftArrowButton.isHidden = false
        rightArrowButton.isHidden = false
    }
    
    //objc func to update pageCurrent with position in scroll view in viewController
    @objc func updateCurrentPage(notification: Notification){
        let viewcontroller = notification.object as! ScrollViewPacketViewController
        
        //calculate number of page
        pagesController.currentPage = Int(viewcontroller.scrollView.contentOffset.x / viewcontroller.scrollView.frame.size.width)
        
        packageTitleLabel.text = "Paquete \(groups[pagesController.currentPage].groupName)"
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToConfirmSelectedPacket" {
            let confirmSelectedPacket = segue.destination as! SelectedAppPacketViewController
            confirmSelectedPacket.delegate = self
            confirmSelectedPacket.coupon = self.coupon
            confirmSelectedPacket.logoURL = self.logoURL
        }
    }
}


extension Dictionary {
    func percentEncoded() -> Data? {
        return map { key, value in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
        .data(using: .utf8)
    }
}
