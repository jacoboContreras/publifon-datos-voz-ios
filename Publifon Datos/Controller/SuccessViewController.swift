//
//  SuccessViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/26/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class SuccessViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Linked Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var successTableView: UITableView!
    //@IBOutlet var logoImageView: UIView!
    @IBOutlet weak var congratulationsLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var startButtonOutlet: UIButton!
    
    
    // Class constant variables
    //let couponModel: CouponModel
    
    // Class variables
    var couponObject: CouponModel?
    var vozCouponObject: VozCouponModel?
    var couponType: Utils.CouponType = Utils.CouponType.none
    var couponSelectedChoice: Utils.CouponSelectedChoice = Utils.CouponSelectedChoice.none
    var vozRemoveIndex = 0
    
    var areDynamicLinkRedentionError = false
    var dynamicLinkRedentionErrorMessage = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Coupon type: ", couponType)
        //couponType = Utils.CouponType.combo// <<<<<<<<<< eliminate
        // Setting table view
        self.successTableView.allowsSelection = false
        self.successTableView.delegate = self
        self.successTableView.dataSource = self
        self.successTableView.register(UINib(nibName: "ApplicationItemTableViewCell", bundle: nil), forCellReuseIdentifier: Utils.APPLICATION_ITEM_TABLE_VIEW_CELL)
        
        // Views setup behavior according coupon type
        switch couponType {
            
        // Datos
        case Utils.CouponType.datos:
            datosCoupon()
            
        // Voz
        case Utils.CouponType.voz:
            vozCoupon()
            
        // Choice
        case Utils.CouponType.choice:
            choiceCoupon()
            
        // Combo
        case Utils.CouponType.combo:
            comboCoupon()
            print("Combo coupon")
            
        default:
            print("Invalid coupon")
        }
        
        //Custom Outlet's view
        
        startButtonOutlet.cornerRadius = CGFloat(startButtonOutlet.cornerRadius)
        startButtonOutlet.backgroundColor = .none
        startButtonOutlet.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        
        DispatchQueue.main.async { [self] in
            if self.areDynamicLinkRedentionError {
                Alerts().alert(Title: "Error", Message: self.dynamicLinkRedentionErrorMessage, viewController: self)
            }
        }
    }
    
    // Start button action
    @IBAction func startButton(_ sender: Any) {
        
        switch couponType {
            
        // Datos
        case Utils.CouponType.datos:
            performSegue(withIdentifier: Utils.GO_TO_MAIN_VIEW_CONTROLLER, sender: self)
            
        // Voz
        case Utils.CouponType.voz:
            performSegue(withIdentifier: Utils.GO_TO_TAB_BAR_CONTROLLER, sender: self)
            
        // Choice
        case Utils.CouponType.choice:
            if couponSelectedChoice == Utils.CouponSelectedChoice.datos {
                performSegue(withIdentifier: Utils.GO_TO_MAIN_VIEW_CONTROLLER, sender: self)
            }
            if couponSelectedChoice == Utils.CouponSelectedChoice.voz {
                performSegue(withIdentifier: Utils.GO_TO_TAB_BAR_CONTROLLER, sender: self)
            }
            
        // Combo
        case Utils.CouponType.combo:
            performSegue(withIdentifier: Utils.GO_TO_MAIN_VIEW_CONTROLLER, sender: self)
            print("Combo")
            
        default:
            print("Invalid coupon")
        }        
        
    }
    
    //preparing for segue, passing coupon code
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToMainViewcontroller" {
            _ = segue.destination as! MainViewController
        }
        
        if segue.identifier == "goToTabBarController" {
            _ = segue.destination as! TabBarController
        }
        
    }
    
    // Table view item size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch couponType {
            
        // Datos
        case Utils.CouponType.datos:
            return self.couponObject!.applications.count
            
        // Voz
        case Utils.CouponType.voz:
            return 1
            
        // Choice
        case Utils.CouponType.choice:
            if couponSelectedChoice == Utils.CouponSelectedChoice.datos {
                return self.couponObject!.applications.count
            } else {
                return 1
            }
            
        // Combo
        case Utils.CouponType.combo:
            return self.couponObject!.applications.count
            
        default:
            return 0
        }
        
    }
    
    // Table view item visualization
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = successTableView.dequeueReusableCell(withIdentifier: "applicationItemTableViewCell", for: indexPath) as! ApplicationItemTableViewCell
        
        switch couponType {
        // Datos
        case Utils.CouponType.datos:
            self.datosCouponTableSetup(cell: cell, index: indexPath.row)
            self.welcomeLabel.text = "Te regala datos ilimitados"
            
        // Voz
        case Utils.CouponType.voz:
            self.vozCouponTableSetup(cell: cell)
            self.welcomeLabel.text = "Te regala llamadas gratis"
            
        // Choice
        case Utils.CouponType.choice:
            if couponSelectedChoice == Utils.CouponSelectedChoice.datos {
                self.datosCouponTableSetup(cell: cell, index: indexPath.row)
                self.welcomeLabel.text = "Te regala datos ilimitados"
            }
            if couponSelectedChoice == Utils.CouponSelectedChoice.voz {
                self.vozCouponTableSetup(cell: cell)
                self.welcomeLabel.text = "Te regala llamadas gratis"
            }
            
        // Combo
        case Utils.CouponType.combo:
            self.datosCouponTableSetup(cell: cell, index: indexPath.row)
            self.welcomeLabel.text = "Te regala datos ilimitados y llamadas gratis"
            
        default:
            print("Invalid coupon")
        }
        
        return cell
        
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    // Sets datos coupon views
    func datosCoupon() {
        // Gets coupon data
        couponObject = Utils.getCouponObject()
        
        if UserDefaults.standard.string(forKey: Utils.JSON_KEY) != nil &&  UserDefaults.standard.string(forKey: Utils.JSON_KEY) != "" {
            
            let backgroundColor = self.couponObject!.campaign.backgroungColor
            let accentuationColor = self.couponObject!.campaign.accentuationColor
            let textColor = self.couponObject!.campaign.textColor
            
            self.view.backgroundColor = Utils.hexToColor(hexString:
                backgroundColor)
            
            let logoURL = URL(string: self.couponObject!.company.logoURL)
            self.logoImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
            
            self.congratulationsLabel.textColor = Utils.hexToColor(hexString: textColor)
            
            self.successTableView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            self.successTableView.rowHeight = 50
            self.successTableView.tableFooterView = UIView()
            
            let freeHrs = Utils.getTotalCouponHrs(couponObject: self.couponObject!)
            let hours = Utils.formatHrsText(hrs: freeHrs)
            
           
            //self.welcomeLabel.text = "\(Utils.SPONSORED_MESSAGE + hours)"
            self.welcomeLabel.numberOfLines = 0
            self.welcomeLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            self.welcomeLabel.textColor = Utils.hexToColor(hexString: textColor)
            self.welcomeLabel.sizeToFit()
            
            self.startButtonOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: UIControlState.normal)
            self.startButtonOutlet.backgroundColor = .none
            startButtonOutlet.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            startButtonOutlet.borderColor = Utils.hexToColor(hexString: textColor)
            
            //If coupon type is combo then add Voz element
            addVozItem()
            
            // Setting Redemption and Expiration date
            UserDefaults.standard.set(Utils.getCurrentDate(), forKey: Utils.REDEMPTION_DATE)
            UserDefaults.standard.set(Utils.calculateCouponExpirationDate(couponObject: self.couponObject!), forKey: Utils.COUPON_EXPIRATION_DATE)
        }
    }
    
    var vozHours = 0
    var vozLogoURL = ""
    var vozPackageName = [String]()
    var vozName = ""
    // Add voz element to array of applications
    /*func addVozItem() {
        if couponType == Utils.CouponType.combo {
            
            for application in couponObject!.applications {
                if application.id == 33 {
                    vozHours = application.hours
                    vozLogoURL = application.logoURL
                    vozPackageName = application.iOSPackageNames
                    vozName = application.name
                }
            }
            vozCouponObject = Utils.getVozCouponObject()
            let hoursValid = Int(vozCouponObject!.hoursValid)
            couponObject?.applications.insert(Applications(androidPackageNames: [""]
                , iOSPackageNames: [""], id: 0, hours: vozHours, maxMegabytes: 0,
                  name: vozName, logoURL: vozLogoURL), at: 0)
//            vozCouponObject = Utils.getVozCouponObject()
//            let hoursValid = Int(vozCouponObject!.hoursValid)
//            couponObject?.applications.insert(Applications(androidPackageNames: [""]
//                , iOSPackageNames: [""], id: 0, hours: hoursValid!, maxMegabytes: 0,
//                  name: "Lamadas Gratis", logoURL: ""), at: 0)
        }
    }*/
    
    // Adding voz Item and removing voz item provided by API
    func addVozItem() {
        if couponType == Utils.CouponType.combo {
            for application in couponObject!.applications {
                if application.id == 33 {
                    vozHours = application.hours
                    vozLogoURL = application.logoURL
                    vozPackageName = application.iOSPackageNames
                    vozName = application.name
                    break
                }
                vozRemoveIndex = vozRemoveIndex + 1
            }
            couponObject?.applications.remove(at: vozRemoveIndex)
            vozCouponObject = Utils.getVozCouponObject()
            _ = Int(vozCouponObject!.hoursValid)
            couponObject?.applications.insert(Applications(androidPackageNames: [""]
                , iOSPackageNames: [""], id: 0, hours: vozHours, maxMegabytes: 0,
                  name: vozName, logoURL: vozLogoURL), at: 0)
            
        }
    }
    
    // Sets datos coupon table cells
    func datosCouponTableSetup(cell: ApplicationItemTableViewCell, index: Int) {
        
        cell.backgroundColor = Utils.hexToColor(hexString: self.couponObject!.campaign.backgroungColor)
        
        cell.appNameLabel.text = couponObject!.applications[index].name
        cell.appNameLabel.textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Getting service formatted sponsored time
        let formattedFreeHours = Utils.getFormatedTotalTime(totalHrs: couponObject!.applications[index].hours)
        
        cell.appExpirationTimeLabel.text = String(describing: formattedFreeHours)
        cell.appExpirationTimeLabel.textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        let iconURL = URL(string: couponObject!.applications[index].logoURL)
        cell.appIconImageView.sd_setImage(with:iconURL, placeholderImage: UIImage(named: ""), completed: {
        (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
            // If coupon is a combo type then add voz item
            if index == 0 && self.couponType == Utils.CouponType.combo {
                //cell.appIconImageView.image = UIImage(named: "phone_icon")
                //cell.backgroundColor =  Utils.hexToColor(hexString: self.couponObject!.campaign.accentuationColor)
                // Setting icon color
                cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
                cell.appIconImageView.tintColor = Utils.hexToColor(hexString: self.couponObject!.campaign.textColor)
            }
        })
        //.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""))
        
        // If coupon is a combo type then add voz item
//        if index == 0 && couponType == Utils.CouponType.combo {
//            cell.appIconImageView.image = UIImage(named: "phone_icon")
//            cell.backgroundColor =  Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
//            // Setting icon color
//            cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
//            cell.appIconImageView.tintColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
//        }
        
    }
    
    // Sets voz coupon views
    func vozCoupon() {
        
        vozCouponObject = Utils.getVozCouponObject()
        let hoursValid = Int(vozCouponObject!.hoursValid)
        
        // Setting Redemption and Expiration date
        UserDefaults.standard.set(Utils.getCurrentDate(), forKey: Utils.REDEMPTION_DATE)
        if couponType == Utils.CouponType.voz
            || couponSelectedChoice == Utils.CouponSelectedChoice.voz {
            // Gets an empty object
            couponObject = Utils.getCouponObject()
        }
        UserDefaults.standard.set(Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: hoursValid!), forKey: Utils.COUPON_EXPIRATION_DATE)

        
        self.successTableView.separatorStyle = .none
        vozCouponObject = Utils.getVozCouponObject()
        
        self.successTableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.successTableView.rowHeight = 50
        self.successTableView.tableFooterView = UIView()
        
        self.congratulationsLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
        let hours = Utils.formatHrsText(hrs: Int(vozCouponObject!.hoursValid)!)
        self.welcomeLabel.text = Utils.VOZ_SPONSORED_MESSAGE+hours
        //self.welcomeLabel.textAlignment = .center
        self.welcomeLabel.numberOfLines = 0
        self.welcomeLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.welcomeLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
        self.welcomeLabel.sizeToFit()
        
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.logoImageView.image = UIImage(named: "logo_publifon")
        self.startButtonOutlet.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
        self.startButtonOutlet.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
    }
    
    func vozChoiceCoupon() {
        vozCouponObject = Utils.getVozCouponObject()
        couponObject = Utils.getCouponObject()
        let freeHrs = Utils.getTotalCouponHrs(couponObject: self.couponObject!)
        let datosHours = Utils.formatHrsText(hrs: freeHrs)
        var welcomeLabelText = "\(couponObject!.company.name + Utils.SPONSORED_MESSAGE + datosHours)"
        let hoursValid = Int(vozCouponObject!.hoursValid)
        
        // Setting Redemption and Expiration date
        UserDefaults.standard.set(Utils.getCurrentDate(), forKey: Utils.REDEMPTION_DATE)
        if couponType == Utils.CouponType.voz
            || couponSelectedChoice == Utils.CouponSelectedChoice.voz {
            // Gets an empty object
            //couponObject = Utils.getCouponObject()
            let vozHours = Utils.formatHrsText(hrs: Int(vozCouponObject!.hoursValid)!)
            welcomeLabelText = "      " + couponObject!.company.name + Utils.VOZ_SPONSORED_MESSAGE+vozHours
        }
        UserDefaults.standard.set(Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: hoursValid!), forKey: Utils.COUPON_EXPIRATION_DATE)

        
        self.successTableView.separatorStyle = .none
        vozCouponObject = Utils.getVozCouponObject()
        
        let backgroundColor = self.couponObject!.campaign.backgroungColor
        let accentuationColor = self.couponObject!.campaign.accentuationColor
        let textColor = self.couponObject!.campaign.textColor
        
        self.view.backgroundColor = Utils.hexToColor(hexString:
            backgroundColor)
        
        let logoURL = URL(string: self.couponObject!.company.logoURL)
        self.logoImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
        
        self.congratulationsLabel.textColor = Utils.hexToColor(hexString: textColor)
        
        self.successTableView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        self.successTableView.rowHeight = 50
        self.successTableView.tableFooterView = UIView()
        
//        let freeHrs = Utils.getTotalCouponHrs(couponObject: self.couponObject!)
//        let hours = Utils.formatHrsText(hrs: freeHrs)
        
        self.welcomeLabel.text = welcomeLabelText//"\(couponObject!.company.name + Utils.SPONSORED_MESSAGE + hours)"
        self.welcomeLabel.numberOfLines = 0
        self.welcomeLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.welcomeLabel.textColor = Utils.hexToColor(hexString: textColor)
        self.welcomeLabel.sizeToFit()
        
        self.startButtonOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: UIControlState.normal)
        self.startButtonOutlet.backgroundColor = .none//Utils.hexToColor(hexString: accentuationColor)
        self.startButtonOutlet.cornerRadius = CGFloat(startButtonOutlet.cornerRadius)
        self.startButtonOutlet.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        self.startButtonOutlet.borderColor = Utils.hexToColor(hexString: textColor)
    }
    
    // Sets voz coupon table cells
    func vozCouponTableSetup(cell: ApplicationItemTableViewCell) {
        if couponType == Utils.CouponType.choice {
            let backgroundColor = self.couponObject!.campaign.backgroungColor
            _ = self.couponObject!.campaign.accentuationColor
            let textColor = self.couponObject!.campaign.textColor
            cell.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            cell.appNameLabel.text = self.couponObject?.applications[0].name//"Llamadas ilimitadas"
            cell.appNameLabel.textColor = Utils.hexToColor(hexString: textColor)
            let hours = Utils.formatHrsText(hrs: Int(vozCouponObject!.hoursValid)!)
            cell.appExpirationTimeLabel.text = hours
            cell.appExpirationTimeLabel.textColor = Utils.hexToColor(hexString: textColor)
            let iconURL = URL(string: couponObject!.applications[0].logoURL)
            cell.appIconImageView.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""), completed: {
            (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
                // Setting icon color
                cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
                cell.appIconImageView.tintColor = Utils.hexToColor(hexString: self.couponObject!.campaign.textColor)
            })
            //.image = UIImage(named: "phone_icon")
            
        } else {
            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            cell.appNameLabel.text = "Llamadas ilimitadas"
            cell.appNameLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            let hours = Utils.formatHrsText(hrs: Int(vozCouponObject!.hoursValid)!)
            cell.appExpirationTimeLabel.text = hours
            cell.appExpirationTimeLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            cell.appIconImageView.image = UIImage(named: "phone_icon")
            cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
            cell.appIconImageView.tintColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            
        }
        
    }
    
    // Sets views depending on what type was selected
    func choiceCoupon() {
        
        if couponSelectedChoice == Utils.CouponSelectedChoice.datos {
            datosCoupon()
        }
        if couponSelectedChoice == Utils.CouponSelectedChoice.voz {
            vozChoiceCoupon()
        }
        
    }
    
    func comboCoupon() {
        datosCoupon()
        let hours = Utils.formatHrsText(hrs: Int(vozCouponObject!.hoursValid)!)
        self.welcomeLabel.text =  (welcomeLabel.text ?? "") + " y llamadas gratis \(hours)"
    }
    
    // Sets datos coupon table cells
    func comboCouponTableSetup(cell: ApplicationItemTableViewCell, index: Int){
        cell.backgroundColor = Utils.hexToColor(hexString: self.couponObject!.campaign.backgroungColor)
        
        cell.appNameLabel.text = couponObject!.applications[index].name
        cell.appNameLabel.textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Getting service formatted sponsored time
        let formattedFreeHours = Utils.getFormatedTotalTime(totalHrs: couponObject!.applications[index].hours)
        
        cell.appExpirationTimeLabel.text = String(describing: formattedFreeHours)
        cell.appExpirationTimeLabel.textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        let iconURL = URL(string: couponObject!.applications[index].logoURL)
        cell.appIconImageView.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""))
    }

}
