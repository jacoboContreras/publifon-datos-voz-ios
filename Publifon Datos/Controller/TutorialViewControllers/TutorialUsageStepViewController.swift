//
//  StepViewController.swift
//  agendaDeDiez
//
//  Created by Jacobo on 9/18/19.
//  Copyright © 2019 Jacobo. All rights reserved.
//

import UIKit
import Lottie

class TutorialUsageStepViewController: UIViewController {
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var textTitleLabel: UILabel!
    @IBOutlet weak var textSubtitleLabel: UILabel!
    @IBOutlet weak var agreeButton: UIButton!
    
    
    var backgroundImage = String()
    var iconImage: UIImage?
    var textTitle = String()
    var textSubtitle = String()
    var agreeButtonIsHidden = Bool()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        //backgroundImageView.image = UIImage.animatedImageNamed(backgroundImage, duration: 4)
        //new line with lootie
        let animationGifView = AnimationView(name: backgroundImage)
        animationGifView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        animationGifView.contentMode = .scaleAspectFill
        animationGifView.play()
        animationGifView.loopMode = .loop
        backgroundImageView.addSubview(animationGifView)
        agreeButton.isHidden = agreeButtonIsHidden
        textTitleLabel.text = textTitle
        textSubtitleLabel.text = textSubtitle
        agreeButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        agreeButton.backgroundColor = .none
        agreeButton.layer.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        agreeButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        agreeButton.setTitleColor(.white, for: .normal)
        
        
    }
    
    @IBAction func showInsertCodeViewController(_ sender: UIButton) {
       
        //"if" for send user to terms conditions or init home page app
        
        if UserDefaults.standard.bool(forKey: "openedBefore") == false {
            
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "TermsConditionsViewController") as! TermsConditionsViewController
            self.present(newViewController, animated: true, completion: nil)
                        
        }else{
            //Show insert code view
            /* let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
             let newViewController = storyBoard.instantiateViewController(withIdentifier: "InsertCodeViewController") as! InsertCodeViewController
             self.present(newViewController, animated: true, completion: nil)*/
            
            
            //show menu options redention
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
            self.present(menuNavigation, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    
}
