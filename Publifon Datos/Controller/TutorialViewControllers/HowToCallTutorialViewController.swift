//
//  HowToCallTutorialViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 5/30/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class HowToCallTutorialViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)

    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
