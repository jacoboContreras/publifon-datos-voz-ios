//
//  TutorialViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/8/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SpriteKit

class TutorialViewController: UIViewController {
    
    // Linked outlets
    @IBOutlet weak var switchArrowImgeView: UIImageView!
    @IBOutlet weak var appIconArrowImageView: UIImageView!
    @IBOutlet weak var expirationArrowImageView: UIImageView!
    @IBOutlet weak var switchLabel: UILabel!
    @IBOutlet weak var appIconLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var vpnArrowImageView: UIImageView!
    @IBOutlet weak var vpnLabel: UILabel!
    @IBOutlet weak var switchExtendedLabel: UILabel!
    @IBOutlet weak var nextButtonOutlet: UIButton!
    @IBOutlet weak var labelTermsConditions: UILabel!
    @IBOutlet weak var containerViewBackgroundLabel: UIView!
    
    
    // Class variables
    var imageViewsArray: [UIImageView] = []
    var timer: Timer?
    var timer2: Timer?
    var switchTimer: Timer?
    var timeCounter = 0
    var timeCounter2 = 0
    var switchTimerCounter = 0
    var tutorialState = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTermsConditions.backgroundColor = nil
        
        containerViewBackgroundLabel.isHidden = true
        
        
        
        timer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(onTimerEvent(timer:)), userInfo: nil, repeats: true)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        switchArrowImgeView.image = switchArrowImgeView.image?.withRenderingMode(.alwaysTemplate)
        switchArrowImgeView.tintColor = UIColor.white
        switchArrowImgeView.isHidden = true
        
        let appArrowdDegrees: Double = 60
        appIconArrowImageView.image = appIconArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        appIconArrowImageView.tintColor = UIColor.white
        appIconArrowImageView.transform = CGAffineTransform(rotationAngle: CGFloat(appArrowdDegrees * .pi/180))
        appIconArrowImageView.isHidden = true
        
        let expirationArrowdDegrees: Double = 20
        expirationArrowImageView.transform = CGAffineTransform(rotationAngle: CGFloat(expirationArrowdDegrees * .pi/180))
        expirationArrowImageView.image = expirationArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        expirationArrowImageView.tintColor = UIColor.white
        expirationArrowImageView.isHidden = true
        
        // Setting array of UIImageViews
        imageViewsArray = [switchArrowImgeView, appIconArrowImageView, expirationArrowImageView]
        
        
        vpnArrowImageView.image = vpnArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        vpnArrowImageView.tintColor = UIColor.white
        vpnArrowImageView.isHidden = true
        vpnLabel.isHidden = true
        switchExtendedLabel.isEnabled = true
        switchExtendedLabel.isHidden = true
    }
    
    // Timer animation for arrows
    @objc func onTimerEvent(timer: Timer) {
        self.imageViewsArray[timeCounter].isHidden = !self.imageViewsArray[timeCounter].isHidden
        
//        if timeCounter != 0 {
//           self.imageViewsArray[timeCounter-1].isHidden = !self.imageViewsArray[timeCounter-1].isHidden
//        }
//        
//        if switchTimerCounter == 3 {
//             self.imageViewsArray[timeCounter-1].isHidden = !self.imageViewsArray[timeCounter-1].isHidden
//        }
        
        timeCounter = timeCounter + 1
        if timeCounter == 3 {
            timeCounter = 0
        }
        
    }
    
    // Switch timer animation
    @objc func onSwitchTimerEvent(timer: Timer) {
        //self.imageViewsArray[switchTimerCounter].isHidden = !self.imageViewsArray[switchTimerCounter].isHidden
        self.vpnArrowImageView.isHidden = !self.vpnArrowImageView.isHidden
        
    }
    
    // Next button action
    @IBAction func nextButton(_ sender: Any) {
        tutorialState = tutorialState + 1
        self.timer!.invalidate()
        hideViews()
        
        if tutorialState == 1 {
            showSwitchViews()
            labelTermsConditions.textColor = .black
            containerViewBackgroundLabel.isHidden = false
            
        }
        
        if tutorialState == 2 {
            stopTimers()
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    // Hide initial views
    func hideViews() {
        switchArrowImgeView.isHidden = true
        appIconArrowImageView.isHidden = true
        expirationArrowImageView.isHidden = true
        switchLabel.isHidden = true
        appIconLabel.isHidden = true
        expirationLabel.isHidden = true
    }
    
    // Show switch views
    func showSwitchViews(){
        vpnArrowImageView.isHidden = false
        vpnLabel.isHidden = false
        switchExtendedLabel.isHidden = false
        //switchExtendedLabel.text = "Cuando abras la aplicación asegurate de activar el switch. En el momento que el icono de VPN se muestre podrás usar tus datos patrocinados"
        switchExtendedLabel.text = "Cuando abras la aplicación asegurate de activar el switch. En el momento que el icono de VPN se muestre estaras listo para usar el servicio. Recuerda que la sesión se termina cada 10 minutos."
        //labelTermsConditions.text = "En el menú podrás encontrar los términos, condiciones y preguntas frecuentes."
        //labelTermsConditions.textColor = .black
        labelTermsConditions.attributedText = NSAttributedString(string: "En el menú podrás encontrar los términos, condiciones y preguntas frecuentes.", attributes:
            [.underlineStyle: NSUnderlineStyle.styleSingle.rawValue])
        switchTimer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(onSwitchTimerEvent(timer:)), userInfo: nil, repeats: true)
        nextButtonOutlet.setTitle("Has click aquí para terminar", for: .normal)
        
        
    }
    
    // Stops timers
    func stopTimers() {
        self.timer!.invalidate()
        self.switchTimer!.invalidate()
    }

}
