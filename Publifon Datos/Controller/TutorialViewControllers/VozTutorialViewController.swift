//
//  VozTutorialViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/22/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class VozTutorialViewController: UIViewController {
    
    // Class variables
    var imageViewsArray: [UIImageView] = []
    var timer: Timer?
    var timeCounter = 0
    
    // Linked outlets
    @IBOutlet weak var menuArrowImageView: UIImageView!
    @IBOutlet weak var callTallArrowImageView: UIImageView!
    @IBOutlet weak var contactsArrowImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        let menuArrowDegrees: Double = 60
        menuArrowImageView.transform = CGAffineTransform(rotationAngle: CGFloat(menuArrowDegrees * .pi/180))
        menuArrowImageView.image = menuArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        menuArrowImageView.tintColor = UIColor.white
        menuArrowImageView.isHidden = true
        
        callTallArrowImageView.image = callTallArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        callTallArrowImageView.tintColor = UIColor.white
        callTallArrowImageView.isHidden = true
        
        let contactArrowDegrees: Double = 220
        contactsArrowImageView.transform = CGAffineTransform(rotationAngle: CGFloat(contactArrowDegrees * .pi/180))
        contactsArrowImageView.image = contactsArrowImageView.image?.withRenderingMode(.alwaysTemplate)
        contactsArrowImageView.tintColor = UIColor.white
        contactsArrowImageView.isHidden = true
        
        // Setting array of UIImageViews
        imageViewsArray = [menuArrowImageView, callTallArrowImageView, contactsArrowImageView]
        timer = Timer.scheduledTimer(timeInterval: 0.75, target: self, selector: #selector(onTimerEvent(timer:)), userInfo: nil, repeats: true)
    }
    @IBAction func closeButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func howToCallButtonAction(_ sender: Any) {
        performSegue(withIdentifier: "goToHowToCallTutorialViewController", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToHowToCallTutorialViewController" {
            _ = segue.destination as! HowToCallTutorialViewController
        }
        
    }
    
    // Timer animation for arrows
    @objc func onTimerEvent(timer: Timer) {
        self.imageViewsArray[timeCounter].isHidden = !self.imageViewsArray[timeCounter].isHidden
        timeCounter = timeCounter + 1
        if timeCounter == 3 {
            timeCounter = 0
        }
        
    }
    
}
