//
//  ViewController.swift
//  tutorialTurisfon
//
//  Created by Jacobo on 11/12/19.
//  Copyright © 2019 Jacobo. All rights reserved.
//

import UIKit

struct PageTutorialUsage {
    var title: String
    var text: String
    var font: UIImage
    var image: UIImage
}

class TutorialUsageViewController: UIViewController, UIScrollViewDelegate {
    
    
    //Outlets
    @IBOutlet weak var tutorialScrollView: UIScrollView!
    @IBOutlet weak var tutorialPageControl: UIPageControl!
    
    //var
    var pages = [TutorialUsageStepViewController]()
    var tutorialStep = TutorialUsageStepViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tutorialScrollView.delegate = self
        tutorialScrollView.bringSubview(toFront: tutorialPageControl)
                
        
        let page1 = createAndAddTutorialStep(backgroundImageName: /*"tutorialGif1/Art"*/"Slide1", iconImageName: "firstPageImage", textTitle: "¡Bienvenido a Publifon!", textSubtitle: "Nuestra App te brinda la posibilidad de contar con llamadas ilimitadas o datos aún SIN SALDO.", agreeButtonIsHidden: true)
        
        let page2 = createAndAddTutorialStep(backgroundImageName: /*"tutorialGif2/Art"*/"Slide2", iconImageName: "secondPageImage", textTitle: "Antes de canjear tu cupón ¡Recuerda apagar tu Wi-fi!", textSubtitle: "No desactives la función de Datos ni Actives el modo avión.", agreeButtonIsHidden: true)
        
        let page3 = createAndAddTutorialStep(backgroundImageName: /*"tutorialGif3/Art"*/"Slide3", iconImageName: "thirdPageImage", textTitle: "¡Selecciona tu modalidad!", //textSubtitle: "Existen cupones que tienen Voz y/o Datos, asígnalo a tu número de teléfono.\n\n¡Esta acción no se puede cambiar!", agreeButtonIsHidden: false)
            textSubtitle: "Tu premio podría traer voz, datos o  ambos. Disfruta tu servicio", agreeButtonIsHidden: false)
            
        
        pages = [page1, page2, page3]
        tutorialPageControl.numberOfPages = pages.count
        
        
        let views : [String: UIView] = ["view": view, "page1": page1.view, "page2": page2.view, "page3": page3.view]
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[page1(==view)]|", options: [], metrics: nil, views: views)
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[page1(==view)][page2(==view)][page3(==view)]|", options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        NSLayoutConstraint.activate(verticalConstraints + horizontalConstraints)
        
    }
    
    private func createAndAddTutorialStep(backgroundImageName: String, iconImageName: String, textTitle: String, textSubtitle:String , agreeButtonIsHidden: Bool) -> TutorialUsageStepViewController{
        
    
        
            //let tutorialStep = storyboard?.instantiateViewController(identifier: "StepViewController") as! StepViewController
         let tutorialStep = storyboard?.instantiateViewController(withIdentifier: "StepViewController")  as! TutorialUsageStepViewController
      
        
        
        tutorialStep.view.translatesAutoresizingMaskIntoConstraints = false
        tutorialStep.backgroundImage = backgroundImageName
        tutorialStep.iconImage = UIImage(named: iconImageName)
        tutorialStep.textTitle = textTitle
        tutorialStep.textSubtitle = textSubtitle
        tutorialStep.agreeButtonIsHidden = agreeButtonIsHidden
        
        tutorialScrollView.addSubview(tutorialStep.view)
        addChildViewController(tutorialStep)
        tutorialStep.didMove(toParentViewController: self)
        return tutorialStep
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if tutorialScrollView.contentOffset.y > 0 || tutorialScrollView.contentOffset.y < 0{
            tutorialScrollView.contentOffset = CGPoint(x: tutorialScrollView.contentOffset.x, y: 0)
        }
    }
    
    //func to show how pages is current
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let numberPage = tutorialScrollView.contentOffset.x / tutorialScrollView.frame.size.width
        tutorialPageControl.currentPage = Int(numberPage)
        print("number page", numberPage)
        
    }
    
    
}

