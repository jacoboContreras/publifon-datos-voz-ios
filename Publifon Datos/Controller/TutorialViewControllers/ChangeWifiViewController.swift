//
//  ChangeWifiViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 12/4/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
//import CoreBluetooth
import Lottie
class ChangeWifiViewController: UIViewController {
    
    //Outlets
    
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var textTitleLabel: UILabel!
    @IBOutlet weak var textSubtitleLabel: UILabel!
    @IBOutlet weak var agreeButton: UIButton!
    
    
    //Var
    var typeConection = String()
    var isBluetothOff = Bool()
    
    
    override func viewDidLoad() {
        //Custom View
        
        agreeButton.backgroundColor = .none
        agreeButton.borderColor = .white
        agreeButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        agreeButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        agreeButton.setTitleColor(.white, for: .normal)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        print("type conecction", typeConection)
        print("bluetooth state", isBluetothOff.description)
        
        //simulated airplane mode
//        if isBluetothOff && typeConection == "none" {
//
//            let string = "¡Upps!\nparece que tienes activado el modo avión..." as NSString
//            let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
//            let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
//            // Part of string to be bold
//            attributedString.addAttributes(boldFontAttribute, range: string.range(of: "¡Upps!\nparece que tienes activado el modo avión..."))
//
//            // add attribute text
//            textTitleLabel.attributedText = attributedString
//
//
//            let subtitleString = "Recuerda lo siguiente:\n1.- Nunca actives el modo avión\n2.- No actives tu Wi-fi\n3.- Ten activos tus datos móviles." as NSString
//            let attributedStringSub = NSMutableAttributedString(string: subtitleString as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
//            let boldFontAttributeSub = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
//            // Part of string to be bold
//            attributedStringSub.addAttributes(boldFontAttributeSub, range: subtitleString.range(of: "3.- Ten activos tus datos móviles."))
//
//            // add attribute text
//            textSubtitleLabel.attributedText = attributedStringSub
//
//        }
        
        
        if typeConection == "Wifi"{
            
            let string = "¡Upps!\nparece que tienes activado tu Wi-Fi..." as NSString
            let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
            let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
            // Part of string to be bold
            attributedString.addAttributes(boldFontAttribute, range: string.range(of: "¡Upps!\nparece que tienes activado tu Wi-Fi..."))
            
            // add attribute text
            textTitleLabel.attributedText = attributedString
            
            
            let subtitleString = "Recuerda lo siguiente:\n1.- Apaga tu wifi\n2.- Ten activos tus datos móviles\n3.- Nunca actives el modo avión." as NSString
            let attributedStringSub = NSMutableAttributedString(string: subtitleString as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
            let boldFontAttributeSub = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
            // Part of string to be bold
            attributedStringSub.addAttributes(boldFontAttributeSub, range: subtitleString.range(of: "2.- Ten activos tus datos móviles"))
            
            // add attribute text
            textSubtitleLabel.attributedText = attributedStringSub
           
            
        }
        
        if typeConection == "none" /*&& isBluetothOff == false */{
            
            let string = "¡Upps! algo salió mal...\nRecuerda lo siguiente:" as NSString
            let attributedString = NSMutableAttributedString(string: string as String, attributes: [NSAttributedStringKey.font:UIFont.systemFont(ofSize: 15.0)])
            let boldFontAttribute = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16.0)]
            // Part of string to be bold
            attributedString.addAttributes(boldFontAttribute, range: string.range(of: "¡Upps! algo salió mal..."))
            
            // add attribute text
            textTitleLabel.attributedText = attributedString
            textSubtitleLabel.text = "Revisa que cuentes con señal telefónica y no tengas activado el modo avión."
            
            
        }
        
        //insert gif
        let animationGifView = AnimationView(name: "Slide2")
        animationGifView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        animationGifView.contentMode = .scaleAspectFill
        animationGifView.play()
        animationGifView.loopMode = .loop
        backgroundImageView.addSubview(animationGifView)
        
        //backgroundImageView.image = UIImage.animatedImageNamed("tutorialGif2/Art", duration: 4)
        agreeButton.isHidden = false
        agreeButton.layer.cornerRadius = 10
        
    }//end view did appear
    
    
    
    @IBAction func agreeDisableWifi(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
}
