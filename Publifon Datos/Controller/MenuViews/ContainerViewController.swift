//
//  ContainerViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/5/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        configureMainViewController()
        print("ContainerViewController")
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Handlers
    func configureMainViewController() {
        let mainViewController = MainViewController()
        let controller = UINavigationController(rootViewController: mainViewController)
        
        view.addSubview(controller.view)
        addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
    
    func configureMenuViewController(){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
