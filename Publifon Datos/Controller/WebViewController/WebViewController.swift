//
//  WebViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 5/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var terminosWebView: WKWebView!
    @IBOutlet weak var aggreButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        terminosWebView.contentMode = .scaleAspectFit
        terminosWebView.load(URLRequest(url: URL(string: "https://publifonapp.com/aviso_privacidad.html")!))
        aggreButton.backgroundColor = .none
        aggreButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        aggreButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        aggreButton.borderColor = StandarColorAndForms.grayColor
        aggreButton.setTitleColor(StandarColorAndForms.grayColor, for: .normal)
        aggreButton.isEnabled = true
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
