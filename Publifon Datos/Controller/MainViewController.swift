//
//  MainViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/4/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import FirebaseInstanceID
import FirebaseCore
import Firebase
import SDWebImage
import SystemConfiguration.CaptiveNetwork

// Protocols
protocol OnSuspendedCoupon {
    func onSuspendedCoupon(status: String)
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, AnimateCallButton, UIActionSheetDelegate {
    
    // VPN Variables
    var server = ""
    var domain = ""
    var port = ""
    var username = ""
    var password = ""
    //var currentManager: NETunnelProviderManager?
    //var status = NEVPNStatus.invalid
    
    var windowMy: UIWindow?
    
    // Class variables
    var menuViewController: MenuViewController!
    var couponObject: CouponModel?
    var vozCouponObject: VozCouponModel?
    var menuToggle = true
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    var animateButtonCounter = 0
    var animateButton = true
    let reachability = Reachability()!
    var observer: NSObjectProtocol?
    var lostVPNMesage : Bool = false
    var firstConection : Bool = false
    var firstLaunchAppTimerTutorial: Timer?
    var waitToChangeFirstLaunch : Timer?
    public static var vpnNotification = false
    var vozHours = 0
    var vozLogoURL = ""
    var vozPackageName = [String]()
    var vozName = ""
    var vozRemoveIndex = 0
    
    // It is static so its available in other classes
    public static var timer: Timer?
    var vpnTimer: Timer?
    public static var tutorialtimer: Timer?
    public static var firstLaunchTimer: Timer?
    public static var animateCallButtonTimer: Timer?
    public static var animateSwitchTimer: Timer?
    public static var disableVPNMessageTimer: Timer?
    public static var displayActionSheetTimer: Timer?
    var totalCouponhrs: Int?
    var size = 0 // dynamic aplications size for array
    var cellUpdateState: [Bool]? = Array(repeating: false, count: 0)
    var sliderPosition = 0
    var miliseconds = 0
    //public static var sliderArray: [String]?
    
    // This variable saves the index of a tapped cell
    // in order to obtain the array element of aplications array
    // in prepare segue method
    // when possible look for another way for doing this
    var cellIndex: Int = 0
    var couponType = ""
    var backgroundColor = ""
    var accentuationColor = ""
    var textColor = ""
    
    var areDynamicLinkRedentionError = false
    var dynamicLinkRedentionErrorMessage = String()
    
    // Linked Outlets
    @IBOutlet weak var mainActivityTableView: UITableView!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var sponsorImageView: UIImageView!
    @IBOutlet weak var vpnSwitch: UISwitch!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var sliderScrollView: UIScrollView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var disableWifiLabel: UILabel!
    
    //Protocol variables
    public static var onSuspendedCouponDelegate: OnSuspendedCoupon?
    
    //func to show tutorial on first launch of app
    @objc func showTutorialFirstTime(){
        
        if (UserDefaults.standard.value(forKey: "firstLaunchApp")) == nil {
            performSegue(withIdentifier: "goToVozTutorialViewController", sender: self)
            MainViewController.firstLaunchTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(firstLaunch), userInfo: nil, repeats: false)
        }
        
    }
    
    //quit tutorial after first launch app
    @objc func firstLaunch(){
        UserDefaults.standard.set(false, forKey: "firstLaunchApp")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //showTutorialFirstTime()
        showTutorialFirstTime()
        
        vpnSwitch.contentVerticalAlignment = .center
        vpnSwitch.inputView?.contentMode = .center
        
        
        // Getting coupon object
        couponObject = Utils.getCouponObject()
        
        /*if couponObject?.servers.name == Utils.APPLE {// Check for Apple's Review //user@bluestudio.mx
         Utils.changeAllAppMode(APP_MODE: Utils.AppMode.pre_production)
         }*/
        
        // Getting voz coupon object
        vozCouponObject = Utils.getVozCouponObject()
        
        // If coupon type is combo then add Voz elements
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        callButton.isHidden = true
        if couponType == Utils.CouponType.combo.rawValue { addVozItem(); callButton.isHidden = false}
        print("Coupon VPN CA: ", couponObject?.servers.caCert ?? "")
        
        // Initializing Certificate
        //ca = CryptoContainer(pem: (couponObject?.servers.caCert)!)
        
        // Assinging VPN Credentials
        setVPNCredentials(username: (couponObject?.vpnData.username)!,
                          password: (couponObject?.vpnData.password)!,
                          remote: (couponObject?.servers.remote)!,
                          port: (couponObject?.servers.port)!)
        
        // Getting customization colors
        backgroundColor = couponObject!.campaign.backgroungColor
        accentuationColor = couponObject!.campaign.accentuationColor
        textColor = couponObject!.campaign.textColor
        
        // Assign text color to disableWifiLabel
        disableWifiLabel.textColor = Utils.hexToColor(hexString: textColor)
        
        //Making floating button shape
        //        callButton.layer.cornerRadius = 25
        //        callButton.layer.masksToBounds = true
        callButton.layer.cornerRadius = callButton.frame.width/2
        callButton.backgroundColor = Utils.hexToColor(hexString: accentuationColor)
        callButton.layer.zPosition = 0
        
        // Setting floating button icon
        let icon = UIImage(named: "contact_cell_call")!
        callButton.setImage(icon, for: .normal)
        callButton.tintColor = Utils.hexToColor(hexString: textColor)
        callButton.imageView?.contentMode = .scaleAspectFit
        callButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        animateCallButton()
        
        // Show slider
        loadSlider(textColor: accentuationColor, array: UserDefaults.standard.stringArray(forKey: "sliderArray")!)
        
        // Getting coupon total sponsored time
        totalCouponhrs = Utils.getTotalCouponHrs(couponObject: couponObject!)
        
        // Getting size of aplications array, to use globally in this class
        self.size = couponObject!.applications.count
        cellUpdateState = Array(repeating: false, count: size)
        
        // Setting swipe gesture for showing side menu
        menuViewController = (self.storyboard?.instantiateViewController(withIdentifier: Utils.MENU_VIEW_CONTROLLER) as! MenuViewController)
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
        
        // Setting main view controller background color
        self.view.backgroundColor = Utils.hexToColor(hexString:
            backgroundColor)
        
        // Setting menu icon color
        let origImage = UIImage(named: "baseline_menu_black_18dp")
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        menuButton.setImage(tintedImage, for: .normal)
        menuButton.tintColor = Utils.hexToColor(hexString: textColor)
        
        // Setting table view
        mainActivityTableView.delegate = self
        mainActivityTableView.dataSource = self
        mainActivityTableView.register(UINib(nibName: "ApplicationItemTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "applicationItemTableViewCell")
        mainActivityTableView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        mainActivityTableView.tableFooterView = UIView()
        mainActivityTableView.rowHeight = 50
        mainActivityTableView.separatorColor = Utils.hexToColor(hexString: accentuationColor)
        
        
        // Setting up sonspor image
        let sponsorImageURL = URL(string: couponObject!.campaign.media)
        self.sponsorImageView.sd_setImage(with: sponsorImageURL, placeholderImage: UIImage(named: ""))
        
        //  Setting vpnSwitch state color
        vpnSwitch.setOn(isVPNRunning(), animated: false)
        if vpnSwitch.isOn {
            self.disableWifiLabel.text = Utils.ACTIVE_SERVICE//"Servicio Activo"
            vpnSwitch.onTintColor = Utils.hexToColor(hexString: accentuationColor)
        } else {
            vpnSwitch.tintColor = Utils.hexToColor(hexString: textColor)
            self.disableWifiLabel.text = Utils.INACTIVE_SERVICE///"Servicio Inactivo"
        }
        
        // Setting expiration label text
        let expirationLabelText = Utils.PROM_VALID_UNTIL + Utils.getReadableDate(date: Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.COUPON_EXPIRATION_DATE)!))
        expirationLabel.text = expirationLabelText
        //        let expirationDate = Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: totalCouponhrs!))
        //        expirationLabel.text = Utils.PROM_VALID_UNTIL + Utils.getReadableDate(date:expirationDate)
        expirationLabel.textColor = Utils.hexToColor(hexString: textColor)
        
        // Check the overall coupon expiration
        checkForCouponExpiration()
        
        // Setting timer to check coupon and service expirations
        MainViewController.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        
        // Setting timer to animate switch
        MainViewController.animateSwitchTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(animatingSwitch), userInfo: nil, repeats: true)
        
        // Starts animating floating button
        animateCallButtonTrigger()
        
        //Timer to show tutorial in the first launch app
        MainViewController.tutorialtimer = Timer.scheduledTimer(timeInterval: 0.7, target: self, selector: #selector(showTutorialFirstTime), userInfo: nil, repeats: false)
        
        // Timer to show disableVPN Message
        MainViewController.disableVPNMessageTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(showDisableVPNMessage), userInfo: nil, repeats: true)
        
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
            print("Could not start notifier")
        }
        
        DispatchQueue.main.async { [self] in
            if self.areDynamicLinkRedentionError {
                print("error por dynamic link")
                Alerts().alert(Title: "Error", Message: self.dynamicLinkRedentionErrorMessage, viewController: self)
            }
        }
    }
           
    // Show DisableVPN Message
    // Checking if call button from voz section
    // sent user back to main view controller.
    @objc func showDisableVPNMessage() {
        if MainViewController.vpnNotification {
            performSegue(withIdentifier: "goToTurisfonConnectViewController", sender: self)
            MainViewController.vpnNotification = false
        }
    }
    
    // Starts timer to animate call floating button
    func animateCallButtonTrigger() {
        MainViewController.animateCallButtonTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(animateCallButton), userInfo: nil, repeats: true)
    }
    
    // Listens for network changes
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            if Utils.APP_MODE != Utils.AppMode.production {return}
            self.disableWifiLabel.isHidden = false
            self.vpnSwitch.isHidden = true
            self.disableWifiLabel.text = Utils.DISABLE_WIFI
            //disconnect()
            print("Internet", "wifi")
        case .cellular:
            print("Internet", "cellular")
            self.vpnSwitch.isHidden = false
            self.disableWifiLabel.isHidden = false
            self.disableWifiLabel.text = Utils.INACTIVE_SERVICE
            self.vpnSwitch.isOn = false
            
        case .none:
            print("Internet", "none")
            //disconnect()
            self.disableWifiLabel.text = Utils.DISABLE_WIFI
            self.vpnSwitch.isHidden = false
            
        case .vpn:
            self.vpnSwitch.isHidden = false
            if self.vpnSwitch.isOn == false {
                //disconnect()
            }
            print("Internet VPN")
        }
    }
    
    // Animates call button
    @objc func animateCallButton() {
        if animateButton {
            UIView.animate(withDuration: 0.5) {
                self.callButton.transform = CGAffineTransform(translationX: 15, y: 0)
            }
            animateButton = false
        } else {
            UIView.animate(withDuration: 0.5) {
                self.callButton.transform = CGAffineTransform(translationX: 0, y: 0)
            }
            animateButton = true
            if animateButtonCounter > 3 {
                animateButtonCounter = 0
                MainViewController.animateCallButtonTimer?.invalidate()
            }
        }
        animateButtonCounter = animateButtonCounter + 1
    }
    
    // Animate Call button protocol method
    func startAnimatingCallButton(data: String) {
        animateCallButtonTrigger()
    }
    
    // Add voz element to array of applications
    //    func addVozItem() {
    //        let vozCouponObject = Utils.getVozCouponObject()
    //        let hoursValid = Int(vozCouponObject.hoursValid)
    //        couponObject?.applications.insert(Applications(androidPackageNames: [""]
    //            , iOSPackageNames: [""], id: 0, hours: hoursValid!, maxMegabytes: 0,
    //              name: "Lamadas Gratis", logoURL: ""), at: 0)
    //    }
    
    // Adding voz Item and removing voz item provided by API
    func addVozItem() {
        if couponType == Utils.CouponType.combo.rawValue {
            for application in couponObject!.applications {
                if application.id == 33 {
                    vozHours = application.hours
                    vozLogoURL = application.logoURL
                    vozPackageName = application.iOSPackageNames
                    vozName = application.name
                    break
                }
                vozRemoveIndex = vozRemoveIndex + 1
            }
            couponObject?.applications.remove(at: vozRemoveIndex)
            vozCouponObject = Utils.getVozCouponObject()
            _ = Int(vozCouponObject!.hoursValid)
            couponObject?.applications.insert(Applications(androidPackageNames: [""]
                , iOSPackageNames: [""], id: 0, hours: vozHours, maxMegabytes: 0,
                  name: vozName, logoURL: vozLogoURL), at: 0)
            
        }
    }
    
    // Call button action
    @IBAction func callButtonAction(_ sender: Any) {
        //changeSwitchState()
        if !isVPNRunning() {
            performSegue(withIdentifier: "goToTabBarController", sender: self)
        } else {
            showToast(title: "Aviso", text: Utils.DISCONNECT_VPN_TO_USE_VOZ_SERVICE)
        }
        
    }
    
    // If going to voz section, disconnect VPN
    // in order to use voz service
    /*func changeSwitchState() {
     if vpnSwitch.isOn {
     // This is a global variable that activates
     // a toast when going to voz section
     HomeViewController.isToastAcivated = true
     disconnect()
     }
     }*/
    
    // Should disconnect VPN but it doesnt
    @objc func disconnectVPNProgramatically() {
        print("Terminating disconnect VPN Programatically")
        //self.disconnect()
    }
    
    // vpn timer starter
    func vpnTimerStarter() {
        vpnTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(vpnTimerDisconnect), userInfo: nil, repeats: true)
        RunLoop.current.add(vpnTimer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    // Disconnects VPN every conection after 10 minutes
    @objc func vpnTimerDisconnect(notification : NSNotification) {
        if miliseconds > 60 {
            vpnTimer?.invalidate()
            //self.disconnect()
            miliseconds = 0
        }
        miliseconds = miliseconds + 1
    }
    
    // Load slider content
    let labels: [UILabel] = [UILabel(), UILabel(), UILabel()]
    func loadSlider(textColor: String, array: [String]) {
        sliderScrollView.showsHorizontalScrollIndicator = false
        for index in 0..<3 {
            //let label = UILabel()
            labels[index].text = array[index]
            labels[index].textAlignment = .center
            labels[index].textColor = Utils.hexToColor(hexString: textColor)
            
            // Assigning label position inside slider scroll views
            let xPosition = self.view.frame.width * CGFloat(index)
            labels[index].frame = CGRect(x: xPosition, y: 0, width: self.sliderScrollView.frame.width, height: self.sliderScrollView.frame.height)
            
            sliderScrollView.contentSize.width = sliderScrollView.frame.width * CGFloat(index + 1)
            
            sliderScrollView.addSubview(labels[index])
        }
        
    }
    
    // Move slider position dynamically
    func moveSlider(position: Int) {
        let point = CGPoint(x: self.view.frame.width * CGFloat(position), y: 0)
        sliderScrollView.contentOffset = point
    }
    var animateSwitch = true
    @objc func animatingSwitch() {
        animateSwitch = !animateSwitch
        if animateSwitch { vpnSwitch.tintColor = Utils.hexToColor(hexString: textColor)
        } else { vpnSwitch.tintColor = Utils.hexToColor(hexString: accentuationColor) }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        lostVPNMesage = false
    }
    
    var switchMoved = false
    // Timer for checking service expirations dynamically
    @objc func timerAction() {
        print("counter: ", counter)
        
        // Check the overall coupon expiration
        checkForCouponExpiration()
        
        // Updates table only when a service has expired
        updateTableView()
        
        // Update slider content
        loadSlider(textColor: couponObject!.campaign.accentuationColor, array: UserDefaults.standard.stringArray(forKey: "sliderArray")!)
        print("UserDefaults.standard.stringArray: ", UserDefaults.standard.stringArray(forKey: "sliderArray")!)
        
        // Move slider position
        if sliderPosition == UserDefaults.standard.stringArray(forKey: "sliderArray")!.count {
            sliderPosition = 0
        }
        // If item doesn't have text dont show it
        if UserDefaults.standard.stringArray(forKey: "sliderArray")![sliderPosition] != "" {
            moveSlider(position: sliderPosition)
        }
        
        sliderPosition = sliderPosition + 1
        
        // Changing switch state
        if self.isVPNRunning() {
            self.vpnSwitch.isOn = true
            self.disableWifiLabel.text = Utils.ACTIVE_SERVICE
            actionSheet.dismiss(animated: true, completion: nil)
        } else {
            if reachability.connection == .wifi {
                self.disableWifiLabel.text = Utils.DISABLE_WIFI
            } else {
                lostVPNMesage = true
                // self.vpnSwitch.isOn = false
                self.disableWifiLabel.text = Utils.INACTIVE_SERVICE
                self.vpnSwitch.isOn = false
                showMessageVPN()
            }
            
        }
        
    }
    
    @IBAction func changeSwitch(_ sender: UISwitch) {

        if isVPNRunning() {
            
        }else{
            firstConection = true
            lostVPNMesage = true
        }
        
    }
    
    // Shows Action Sheet
    var actionSheet = UIAlertController(title: "Servicio inactivo", message: "Por favor activa el switch para hacer uso de tu servicio de datos libres.", preferredStyle: .actionSheet)
    func showMessageVPN() {
        print("xxxxx4", counter)
        if(switchJustClicked && counter <= 1){return}
        print("xxxxx5", counter)
        if lostVPNMesage == true && firstConection == true {
            self.vpnSwitch.isOn = false
            actionSheet = UIAlertController(title: "Servicio inactivo", message: "Por favor activa el switch para hacer uso de tu servicio de datos libres.", preferredStyle: .actionSheet)
            
            let accept = UIAlertAction(title: "Aceptar", style: .default) { action in
            }
            
            // actionSheet.addAction(UIAlertAction(title: "Aceptar", style: .default))
            actionSheet.addAction(accept)
            
            self.present(actionSheet, animated: true, completion: nil)
            firstConection = false
            lostVPNMesage = false
        }
        
    }
    
    private func actionSheet(actionSheet: UIAlertController, clickedButtonAtIndex buttonIndex: Int) {
        print("\(buttonIndex)")
        switch (buttonIndex){

        case 0:
            print("Cancel")
        case 1:
            print("Save")
        case 2:
            print("Delete")
        default:
            print("Default")
            //Some code here..

        }
    }
    
    // Checking for coupon expiration state
    func checkForCouponExpiration() {
        
        let expirationDate = Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: totalCouponhrs!))
        let currentDate = Utils.convertStringToDate(stringDate: Utils.getCurrentDate())
        print("Expiration date",expirationDate)
        
        // checking for coupon expiration state
        if Utils.checkIfServiceHasExpired(currentDate: currentDate, expirationDate: expirationDate) {
            //expireCoupon(coupon: (self.couponObject?.vpnData.username)!)
            self.expiration()
        }
        
    }
    
    // Supend coupon logic
    /*func expireCoupon (coupon: String) {
     // Making Request
     Alamofire.request(configureRequest(coupon: coupon)).responseData {
     dataResponse in
     
     if dataResponse.result.isSuccess {
     let json : JSON = JSON(dataResponse.result.value!)
     _ = json[Utils.MESSAGE].string // not used for the moment
     let status = json[Utils.STATUS].bool
     
     if status! {
     // Telling Application that this coupon was expired
     UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
     self.expiration()
     }
     
     } else {
     print("Error: \(dataResponse.result.error!)")
     // Telling Application that this coupon has a pending expiration
     UserDefaults.standard.set(true, forKey: Utils.PENDING_EXPIRATION)
     self.expiration()
     }
     
     }
     
     }*/
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String) -> URLRequest{
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = getFCMToken()
        
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = Utils.EXPIRE_COUPON_URL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let expireURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: expireURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 5 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
        
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }
    
    // Logic for expiring a coupon
    func expiration() {
        // Deleting coupon state
        self.clearCoupon()
        // Stops expiration checker timer
        MainViewController.timer?.invalidate()
        // Sends user to insert view controller
        self.sendUserToInsertViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Clears coupon data stored in memory and sends back to insert view controller
    func clearCoupon() {
        resetCouponMemoryValues()
    }
    
    // Reset coupon memory state
    func resetCouponMemoryValues() {
        UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
        UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.REDEMPTION_DATE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.COUPON_EXPIRATION_DATE)
        UserDefaults.standard.set(["", "", ""], forKey: "sliderArray")
        AppDelegate.sliderArray = ["", "", ""]
        UserDefaults.standard.set(0, forKey: "index")
        AppDelegate.index = 0
    }
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        //send user to insert code vc
        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let insertCodeViewController = mainStoryboard.instantiateViewController(withIdentifier:
            Utils.INSERT_CODE_VIEW_CONTROLLER) as! InsertCodeViewController
        appDelegate.window?.rootViewController = insertCodeViewController
        appDelegate.window?.makeKeyAndVisible()*/
        
        //send to menu redemtion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let MenuNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = MenuNavigationController.topViewController as! SeleccionDeRedencionViewController
        appDelegate.window?.rootViewController = MenuNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Stops expiration timer
    func stopTimer(){
        MainViewController.timer!.invalidate()
    }
    
    // Switch action logic
    @IBAction func switchAction(_ sender: Any) {
        if !isVPNRunning() {
            checkCoupon(coupon: (couponObject?.vpnData.username)!)
        } else { vpnLauncher() }
    }
    
    var counter = 0
    var switchJustClicked = false
    @objc func displayActionSheetCounter(){
        print("xxxxx1", counter)
        counter = counter + 1
        print("xxxxx2", counter)
    }
    
    // Method for checking coupon
    public func checkCoupon(coupon: String) {
        print("xxxxx3", counter)
        switchJustClicked = true
        MainViewController.displayActionSheetTimer = Timer.scheduledTimer(timeInterval: 2.0,
                                                                          target: self,
                                                                          selector: #selector(displayActionSheetCounter),
                                                                          userInfo: nil, repeats: true)
        // Making Request
        Alamofire.request(Utils.configureRequest(coupon: coupon, url: Utils.CHECK_COUPON_URL)).responseData {
            dataResponse in
            
            if dataResponse.result.isSuccess {
                
                let json : JSON = JSON(dataResponse.result.value!)
                print("Utils_JSON:", json)
                if json[Utils.STATUS].exists() {
                    let status = json[Utils.STATUS].string
                    if status == Utils.SUSPENDED || status == Utils.AVAILABLE {
                        MainViewController.onSuspendedCouponDelegate?.onSuspendedCoupon(status: Utils.SUSPENDED_SERVICE)
                        self.expiration()
                    } else {
                        DispatchQueue.main.async {
                            MainViewController.displayActionSheetTimer?.invalidate()
                            self.counter = 0
                            self.switchJustClicked = false
                            self.vpnLauncher()
                        }
                        
                    }
                } else {
                    DispatchQueue.main.async {
                        MainViewController.displayActionSheetTimer?.invalidate()
                        self.counter = 0
                        self.switchJustClicked = false
                        self.vpnLauncher()
                    }
                }
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                DispatchQueue.main.async {
                    MainViewController.displayActionSheetTimer?.invalidate()
                    self.counter = 0
                    self.switchJustClicked = false
                    self.vpnLauncher()
                }
            }
            
        }
        
    }
    
    // Executes VPN connection and sends custom colors to Publifon Connect
    func vpnLauncher() {
        // local color variables
        switchMoved = true
        let accentuationColor = couponObject!.campaign.accentuationColor
        let textColor = couponObject!.campaign.textColor
        let backgroundColor = couponObject!.campaign.backgroungColor
        if vpnSwitch.isOn {
            // Changes switch tint color
            vpnSwitch.onTintColor = Utils.hexToColor(hexString: accentuationColor)
            //            startActivityIndicator()
            //            connect()
            //vpnTimerStarter()
            let certificate = couponObject!.servers.caCert as String
            let derverDomain = couponObject!.servers.remote as String
            let serverPort = String(couponObject!.servers.port)
            let user = couponObject!.vpnData.username as String
            let pass = couponObject!.vpnData.password as String
            
            let identifier = [certificate, derverDomain, serverPort, user, pass,
                              accentuationColor, textColor, backgroundColor]
            //let pasteboard = UIPasteboard.general
            let pasteboard = UIPasteboard(name: UIPasteboard.Name(rawValue: "publifon"), create: true)
            pasteboard!.strings = identifier
            
            let appScheme = URL(string: Utils.PUBLIFON_CONNECT)
            if UIApplication.shared.canOpenURL(appScheme! as URL) {
                UIApplication.shared.open(appScheme!, options: [:], completionHandler: nil)
            } else {
                performSegue(withIdentifier: "goToTurisfonConnectViewController", sender: self)
            }
        } else {
            let appScheme = URL(string: Utils.PUBLIFON_CONNECT)
            if UIApplication.shared.canOpenURL(appScheme! as URL) {
                UIApplication.shared.open(appScheme!, options: [:], completionHandler: nil)
            }
            // Changes switch tint color
            //            disconnect()
            //            vpnSwitch.tintColor = Utils.hexToColor(hexString: textColor)
            //            activityIndicator.stopAnimating()
            if isVPNRunning() {
                self.vpnSwitch.isOn = true
                firstConection = true
            }
        }
    }
    
    // Animates activity indicator
    func startActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    // Shows side menu when swipe gesture is made
    @objc func respondToGesture(gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            if menuToggle == true{showMenu()}            
        default:
            break
        }
    }
    
    // Hides side menu when clicked outsude range
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if touch?.view != menuViewController.menuTableView
            && touch?.view != menuViewController.viewContainer
            &&  touch?.view != menuViewController.menuLabelContainer {
            hideMenu()
        }
        
    }
    
    // Hamburger menu button action
    // Shows and hides side menu when clicked
    @IBAction func menuAction(_ sender: Any) {
        if menuToggle {
            showMenu()
            
        } else {
            hideMenu()
            
        }
    }
    
    // Shows side menu
    func showMenu() {
        // Setting menu animation
        self.menuViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.menuViewController.view.alpha = 0
        self.addChildViewController(self.menuViewController)
        self.view.addSubview(self.menuViewController.view)
        
        UIView.animate(withDuration: 0.3){  () -> Void in
            self.menuViewController.view.frame = CGRect(x: 0, y: 0,
                                                        width: UIScreen.main.bounds.size.width,
                                                        height: UIScreen.main.bounds.size.height)
            self.menuViewController.view.alpha = 1
        }
        self.menuToggle = false
    }
    
    // Hides side menu
    func hideMenu() {
        // Setting menu animation
        UIView.animate(withDuration: 0.3, animations: {  () -> Void in
            self.menuViewController.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0,
                                                        width: UIScreen.main.bounds.size.width,
                                                        height: UIScreen.main.bounds.size.height)
        }) {(finished) in
            self.menuViewController.view.removeFromSuperview()
        }
        menuToggle = true        
    }
    
    
    
    
    // Table view item size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if couponType == Utils.CouponType.combo.rawValue {
        //            //<<<<<<<<<<<<<<<<
        //            return couponObject!.applications.count// - 1
        //        }
        return couponObject!.applications.count
    }
    
    // Table view item visualization
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mainActivityTableView.dequeueReusableCell(withIdentifier: Utils.APPLICATION_ITEM_TABLE_VIEW_CELL,
                                                             for: indexPath) as! ApplicationItemTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        
        
        let formattedFreeHours = Utils.getFormatedTotalTime(totalHrs:
            couponObject!.applications[indexPath.row].hours)
        let expirationDate = Utils.convertStringToDate(stringDate:
            Utils.calculateExpirationDate(couponObject: self.couponObject!,
                                          serviceHrs: couponObject!.applications[indexPath.row].hours))
        let currentDate = Utils.convertStringToDate(stringDate: Utils.getCurrentDate())
        
        if Utils.checkIfServiceHasExpired(currentDate: currentDate, expirationDate: expirationDate) {
            cell.appNameLabel.text = Utils.EXPIRED_SERVICE
            cell.appExpirationTimeLabel.text = "---"
            cell.appNameLabel.textColor = UIColor.darkGray
            cell.appExpirationTimeLabel.textColor = UIColor.darkGray
            cell.isUserInteractionEnabled = false
        } else {
            cell.appNameLabel.text = couponObject!.applications[indexPath.row].name
            cell.appExpirationTimeLabel.text = String(describing: formattedFreeHours)
            cell.appNameLabel.textColor = Utils.hexToColor(hexString: textColor)
            cell.appExpirationTimeLabel.textColor = Utils.hexToColor(hexString: textColor)
        }
        
        cell.appNameLabel.tag = indexPath.row
        cell.appNameLabel.isUserInteractionEnabled = true
        let nameLabelGesture = UITapGestureRecognizer(target: self, action:#selector(self.appNameLabelClicked(sender:)))
        cell.appNameLabel.addGestureRecognizer(nameLabelGesture)
        nameLabelGesture.delegate = self
        
        // Getting icon image
        let iconURL = URL(string: couponObject!.applications[indexPath.row].logoURL)
        cell.appIconImageView.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""), completed: {
            (image: UIImage?, error: Error?, cacheType: SDImageCacheType, imageURL: URL?) in
            // If coupon is a combo type then add voz item
            if indexPath.row == 0 && self.couponType == Utils.CouponType.combo.rawValue {
                //cell.appIconImageView.image = UIImage(named: "phone_icon")
                //cell.backgroundColor =  Utils.hexToColor(hexString: self.accentuationColor)
                // Setting icon color
                cell.appIconImageView.image = cell.appIconImageView.image?.withRenderingMode(.alwaysTemplate)
                cell.appIconImageView.tintColor = Utils.hexToColor(hexString: self.textColor)
            }
        })
        //.sd_setImage(with: iconURL, placeholderImage: UIImage(named: ""))
        
        cell.appIconImageView.isUserInteractionEnabled = true
        cell.appIconImageView.tag = indexPath.row
        let appIconImageViewGesture = UITapGestureRecognizer(target: self,
                                                             action:#selector(self.appIconIamgeViewClicked(sender:)))
        //appIconImageViewGesture.setValue(5, forKey: "number")
        cell.appIconImageView.addGestureRecognizer(appIconImageViewGesture)
        appIconImageViewGesture.delegate = self
        
        cell.appExpirationTimeLabel.tag = indexPath.row
        cell.appExpirationTimeLabel.isUserInteractionEnabled = true
        let appExpirationTimeLabelGesture = UITapGestureRecognizer(target: self,
                                                                   action:#selector(self.expirationLabelClicked(sender:)))
        cell.appExpirationTimeLabel.addGestureRecognizer(appExpirationTimeLabelGesture)
        appExpirationTimeLabelGesture.delegate = self
        return cell
    }
    
    // Updates table view only if a service has expired
    // its a work around to the delayed problem when
    // reloading table view every 10 seconds
    func updateTableView() {
        for number in 0...self.size-1 {
            let expirationDate = Utils.convertStringToDate(stringDate:
                Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: couponObject!.applications[number].hours))
            let currentDate = Utils.convertStringToDate(stringDate: Utils.getCurrentDate())
            if Utils.checkIfServiceHasExpired(currentDate: currentDate, expirationDate: expirationDate) &&  !cellUpdateState![number] {
                print(number,"updated")
                cellUpdateState![number] = true
                //print(cellUpdateState![number])
                self.mainActivityTableView.reloadData()
            } else if cellUpdateState![number] {
                print(number," has expired!")
            } else {
                print(number," has not expired!")
            }
        }
    }
    
    // App Name label touch handler not in use for now
    @objc func appNameLabelClicked(sender: UITapGestureRecognizer) {
        let index = sender.view?.tag
        openApplication(index: index!)
    }
    
    // Expiration label touch handler
    @objc func expirationLabelClicked(sender: UITapGestureRecognizer) {
        cellIndex = (sender.view?.tag)!
        performSegue(withIdentifier: Utils.GO_TO_MODAL_INFO_VIEW_CONTROLLER, sender: self)
    }
    
    // Image touch handler
    @objc func appIconIamgeViewClicked(sender: UITapGestureRecognizer) {
        let index = sender.view?.tag
        openApplication(index: index!)
    }
    
    // Opens corresponding application
    func openApplication(index: Int){
        print(couponObject!.applications[index].iOSPackageNames)
        var appFound = false
        
        // If coupon type is combo the first element is call action
        if index == 0 && couponType == Utils.CouponType.combo.rawValue {
            //changeSwitchState()
            if !isVPNRunning() {
                performSegue(withIdentifier: "goToTabBarController", sender: self)
            } else {
                showToast(title: "Aviso", text: Utils.DISCONNECT_VPN_TO_USE_VOZ_SERVICE)
            }
            return
        }
        
        // Checking if app is installed and then opening it
        for name in couponObject!.applications[index].iOSPackageNames {
            let appName = name
            //let appName = "publifonconnect://"//name
            if URL(string: appName) != nil {
                let appSchemeURL = URL(string: appName)
                
                if UIApplication.shared.canOpenURL(appSchemeURL! as URL) {
                    UIApplication.shared.open(appSchemeURL!, options: [:], completionHandler: nil)
                    appFound = true
                    break
                }
            }
        }
        
        // If app is not found show a toast to the user
        if !appFound {
            //toastMessage(Utils.THIS_APP_IS_NOT_INSTALLED)
            showToast(title: "Aviso", text: Utils.THIS_APP_IS_NOT_INSTALLED)
        }
        //        var instagramHooks = "facebook://"
        //        var instagramUrl = NSURL(string: instagramHooks)
        //        if UIApplication.shared.canOpenURL(instagramUrl! as URL)
        //        {
        //            UIApplication.shared.openURL(instagramUrl! as URL)
        //
        //        } else {
        //            //redirect to safari because the user doesn't have Instagram
        //            //UIApplication.shared.openURL(NSURL(string: "http://instagram.com/")! as URL)
        //            print("you dont have this app installed")
        //        }
    }
    
    // Custom toast
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // Preparing for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToVozTutorialViewController" {
            _ = segue.destination as! TutorialViewController
            
        }
        
        if segue.identifier == "prueba" {
            _ = segue.destination as! TutorialViewController
            
        }
        
        
        if segue.identifier == Utils.GO_TO_MODAL_INFO_VIEW_CONTROLLER {
            let modalInfoViewController = segue.destination as! ModalInfoViewController
            modalInfoViewController.cellIndex = cellIndex
            passTimeParametersToModal(modalInfoViewController: modalInfoViewController)
            
        }
        
        if segue.identifier == "goToTabBarController" {
            _ = segue.destination as! TabBarController
            TabBarController.delegate = self
        }
        
        if segue.identifier == "goToLogoutViewController" {
            _ = segue.destination as! LogoutViewController
        }
        
        if segue.identifier == "goToTurisfonConnectViewController" {
            let turisfonConnectViewContoller = segue.destination as! TurisfonConnectViewController
            print("MainViewController:", MainViewController.vpnNotification)
            if MainViewController.vpnNotification {
                turisfonConnectViewContoller.modalText = Utils.DISABLE_SWITCH
            } else {
                turisfonConnectViewContoller.modalText = Utils.DOWNLOAD_TURISFON_CONNECT
            }
            
        }
        
    }
    
    // Assings values to ModalInfoViewcontroller for expiration date
    func passTimeParametersToModal(modalInfoViewController: ModalInfoViewController) {
        
        // Getting expiration date and parsing string to get individual parameters
        let expirationDate = Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: couponObject!.applications[cellIndex].hours)
        // Getting values
        let splitString = expirationDate.split(separator: " ")
        let splitDate = splitString[0].split(separator: "-")
        let splitTime = splitString[1].split(separator: ":")
        
        // Passing date parameters to ModalInfoViewController
        modalInfoViewController.expYear = Int(splitDate[0])!
        modalInfoViewController.expMonth = Int(splitDate[1])!
        modalInfoViewController.expDay = Int(splitDate[2])!
        modalInfoViewController.expHour = Int(splitTime[0])!
        modalInfoViewController.expMinute = Int(splitTime[1])!
        modalInfoViewController.expSecond = Int(splitTime[2])!
        
    }
    
    // VPN Related code
    func setVPNCredentials(username: String, password: String, remote: String, port: Int) {
        print("Credentials", "username:", username)
        server = ""
        //self.domain = "ec2-54-86-200-52.compute-1.amazonaws.com"// Publifon
        self.domain = remote
        //self.domain = "ec2-3-81-241-34.compute-1.amazonaws.com"// Chava
        //self.port = "443"
        self.port = String(port)
        //self.username = "2przi" // Publifon
        self.username = username
        //self.username = "qd1kq"// Chava
        self.password = password
        //self.password = "$2y$10$J/h/FhDGxYzUrR2u6zT.zuykDJq8FLMOXO1MhGKrjWz6vSjIwSQXu" // Publifon
        //self.password = "BlueLabsRodrig0"// Chava
        
        /*NotificationCenter.default.addObserver(self,
         selector: #selector(VPNStatusDidChange(notification:)),
         name: .NEVPNStatusDidChange,
         object: nil)
         
         reloadCurrentManager(nil)*/
    }
    
    // Reloads NetTunnelProviderManager
    /*func reloadCurrentManager(_ completionHandler: ((Error?) -> Void)?) {
     NETunnelProviderManager.loadAllFromPreferences { (managers, error) in
     if let error = error {
     completionHandler?(error)
     return
     }
     
     var manager: NETunnelProviderManager?
     
     for m in managers! {
     if let p = m.protocolConfiguration as? NETunnelProviderProtocol {
     if (p.providerBundleIdentifier == MainViewController.bundleIdentifier) {
     manager = m
     break
     }
     }
     }
     
     if (manager == nil) {
     manager = NETunnelProviderManager()
     }
     
     self.currentManager = manager
     self.status = manager!.connection.status
     //self.updateButton()
     completionHandler?(nil)
     }
     }*/
    
    // Listens for changes in VPN Connection
    /*@objc private func VPNStatusDidChange(notification: NSNotification) {
     guard let status = currentManager?.connection.status else {
     print("VPNStatusDidChange")
     return
     }
     self.status = status
     updateStatus(number: status.rawValue)
     }*/
    
    // VPN Connection
    /*func connect() {
     configureVPN({ (manager) in
     return self.makeProtocol()
     }, completionHandler: { (error) in
     if let error = error {
     print("configure error: \(error)")
     return
     }
     let session = self.currentManager?.connection as! NETunnelProviderSession
     do {
     //print("startTunnel")
     try session.startTunnel()
     } catch let e {
     print("error starting tunnel: \(e)")
     }
     })
     }*/
    
    // VPN Disconnection
    /*func disconnect() {
     print("Number", "Inside disconnect")
     configureVPN({ (manager) in
     print("Number Configure VPN")
     return nil
     }, completionHandler: { (error) in
     print("Number completionHandler")
     self.currentManager?.connection.stopVPNTunnel()
     })
     }*/
    
    // Configures a VPN
    /*func configureVPN(_ configure: @escaping (NETunnelProviderManager) -> NETunnelProviderProtocol?, completionHandler: @escaping (Error?) -> Void) {
     reloadCurrentManager { (error) in
     if let error = error {
     print("error reloading preferences: \(error)")
     completionHandler(error)
     return
     }
     
     let manager = self.currentManager!
     if let protocolConfiguration = configure(manager) {
     manager.protocolConfiguration = protocolConfiguration
     }
     manager.isEnabled = true
     
     manager.saveToPreferences { (error) in
     if let error = error {
     print("error saving preferences: \(error)")
     completionHandler(error)
     return
     }
     print("saved preferences")
     self.reloadCurrentManager(completionHandler)
     }
     
     }
     
     }*/
    
    // Updates VPN status and chages switch state
    /*func updateStatus(number: Int){
     switch statxus {
     case .connected, .connecting:
     if number == 2 { print("Status Connecting: ", number)}
     else {
     print("Status Connected: ", number)
     toastMessage("Sesión iniciada")
     self.disableWifiLabel.text = "Servicio Activo"
     activityIndicator.stopAnimating()
     }
     case .disconnected:
     print("Status Disconnect: ", number)
     case .disconnecting:
     print("Status Disconnected: ", number)
     self.disableWifiLabel.text = "Servicio Inactivo"
     if reachability.connection == .wifi && couponObject?.campaign.id != 105 {
     self.disableWifiLabel.text = "Desactiva tu Wifi para usar el servicio"
     }
     vpnSwitch.setOn(false, animated: true)
     toastMessage("Sesión cancelada")
     activityIndicator.stopAnimating()
     vpnTimer?.invalidate()
     miliseconds = 0
     
     default:
     break
     }
     }*/
    
    // Checks if VPN Running
    func isVPNRunning() -> Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") {
                    return true
                }
            }
        }
        return false
    }
    
    
    func isWIFIConected() -> Bool {
        var hasWiFiNetwork: Bool = false
        let interfaces: NSArray = CFBridgingRetain(CNCopySupportedInterfaces()) as! NSArray
        
        for interface  in interfaces {
            // let networkInfo = (CFBridgingRetain(CNCopyCurrentNetworkInfo(((interface) as! CFString))) as! NSDictionary)
            let networkInfo: [AnyHashable: Any]? = CFBridgingRetain(CNCopyCurrentNetworkInfo(((interface) as! CFString))) as? [AnyHashable : Any]
            if (networkInfo != nil) {
                hasWiFiNetwork = true
                break
            }
        }
        return hasWiFiNetwork;
        
    }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}


