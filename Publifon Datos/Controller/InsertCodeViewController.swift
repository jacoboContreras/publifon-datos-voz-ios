//
//  ViewController.swift
//  Publifon Datos
//
//  Created by Admin on 12/09/18.
//  Copyright © 2018 Bluelabs. All rights reserved.
//

import UIKit
import CoreLocation
import FirebaseInstanceID
import FirebaseCore
import Firebase
import Alamofire
import SwiftyJSON
//import CoreBluetooth

class InsertCodeViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate, FailedToRedeemCoupon, FailedToCheckCoupon, UIActionSheetDelegate, OnSuspendedCoupon {
    
    // Class variables
    let locationManager = CLLocationManager()
    var deviceToken : String = "";
    var timer: Timer?
    var toValidateVontrollerTimer: Timer?
    var couponType = Utils.CouponType.none
    var chosenGroup = ""
    var couponSelectedChoice = Utils.CouponSelectedChoice.none
    var phoneNumber = ""
    public static var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    public static var extendedViewController: UIViewController?
    var isBoxChecked = false
    let reachability = Reachability()!
    var isWifiActive = false
    var typeConection = String()
    
    //Var to QR
    var isFromQR = false
    var coupon = ""
    var initialTimeApp = false
    var publifonCouponScan = ""
    
    // Linked Outlets
    @IBOutlet weak var couponCodeTextField: UITextField!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var validationButtonView: UIButton!
    @IBOutlet weak var terminosLabel: UILabel!
    @IBOutlet weak var checkBoxLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    // General variables
    var inputImage: UIImage = #imageLiteral(resourceName: "baseline_input_black_18dp")
    let leftImage = UIImageView()
    let contentView = UIView()
    //var manager:CBCentralManager!
    var isBluetothOff = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView()
        print("getFCMToken(): ",getFCMToken())
        print("Pending expiration: ", UserDefaults.standard.bool(forKey: Utils.PENDING_EXPIRATION))
        print("Pending suspension: ", UserDefaults.standard.bool(forKey: Utils.PENDING_SUSPENSION))
        print("Coupon active: ", UserDefaults.standard.bool(forKey: Utils.COUPON_ACTIVE))
        
        // Setup App Version label
        let numberVersionApp = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        if Utils.WEBSERVICE_URL.contains("ec2") { appVersionLabel.text = "\(numberVersionApp!).PP" }
        else if Utils.WEBSERVICE_URL.contains("sandbox")  { appVersionLabel.text = "\(numberVersionApp!).S"
        }else { appVersionLabel.text = "\(numberVersionApp!).P" }
        
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{ try reachability.startNotifier() }
        catch { print("Could not start notifier") }
        
        // Setting terminos label touch action
        terminosLabel.isUserInteractionEnabled = true
        let terminosLabelTap = UITapGestureRecognizer(target: self, action: #selector(terminosTapFunction))
        terminosLabel.addGestureRecognizer(terminosLabelTap)
        
        // Setting checkbox label touch action
        //        checkBoxLabel.isUserInteractionEnabled = true
        //        let checkBoxLabelTap = UITapGestureRecognizer(target: self, action: #selector(checkBoxLabelTapFunction))
        //        checkBoxLabel.addGestureRecognizer(checkBoxLabelTap)
        
        //        let icon = UIImage(named: "unchecked_box")!
        //        checkBoxButton.setImage(icon, for: .normal)
        //        checkBoxButton.tintColor = Utils.hexToColor(hexString: "#00FF00")
        //        //checkBoxButton.tintColor = .blue
        //        checkBoxButton.imageView?.contentMode = .scaleAspectFit
        //        checkBoxButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        // Disabeling validation button until user accepts
        // terminos y condiciones
        //        validationButtonView.isUserInteractionEnabled = false
        //        validationButtonView.alpha = 0.5
        
        // Checking if there is a coupon pending of suspension
        if UserDefaults.standard.bool(forKey: Utils.PENDING_SUSPENSION) {
            //timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(suspensionAction), userInfo: nil, repeats: true)
        }
        
        // Cheking if there is a coupon pending of expiration
        if UserDefaults.standard.bool(forKey: Utils.PENDING_EXPIRATION) {
            //timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(expirationAction), userInfo: nil, repeats: true)
        }
        
        // Location setup
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        
        // Assigns delegate for return key pressed
        couponCodeTextField.delegate = self        
        
        // Code related to show keyboard UI elements when keyboard shows/hides
        NotificationCenter.default.addObserver(self, selector: #selector(InsertCodeViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(InsertCodeViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        // Sends to corresponding view controller if a coupon is active
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            let type = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)
            switch type {
                
            // Datos, Send to main view controller
            case Utils.CouponType.datos.rawValue:
                applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                
            // Voz, Send to tab bar controller
            case Utils.CouponType.voz.rawValue:
                applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                
            // Choice, Send to corresponding controller
            case Utils.CouponType.choice.rawValue:
                let selected = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
                if selected == Utils.CouponSelectedChoice.datos.rawValue {
                    applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                }
                if selected == Utils.CouponSelectedChoice.voz.rawValue {
                    applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                }
            // Combo, Send to main view controller
            case Utils.CouponType.combo.rawValue:
                applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                //applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                
            default:
                print("No coupon active")
            }
            
        }
        
        MainViewController.onSuspendedCouponDelegate = self
        
        if publifonCouponScan != ""{
            couponCodeTextField.text = publifonCouponScan
            let reachability = Reachability()
            if reachability?.connection == Reachability.Connection.wifi &&
                couponCodeTextField.text != Utils.APPLE {
                view.endEditing(true)
                typeConection = "Wifi"
                performSegue(withIdentifier: "goToDisableWifi", sender: self)
            }else if reachability?.connection == Reachability.Connection.none {
                view.endEditing(true)
                typeConection = "none"
                performSegue(withIdentifier: "goToDisableWifi", sender: self)
            } else {
                inputCheck()
            }            
            
        }
        
        
    }//finish viewdIDload
    
    /*func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
     return true
     }*/
    
    override func viewDidAppear(_ animated: Bool) {        
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        /*if initialTimeApp{
         //go to redention option menu
         let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
         
         let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
         self.present(menuNavigation, animated: true, completion: nil)
         
         //self.navigationController?.pushViewController(self, animated: true)
         return
         }*/
        
        //to redem qr coupon
        if isFromQR{
            couponCodeTextField.text = coupon
            inputCheck()
        }
    }
    
    func customView(){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.title = "Redención de cupón"
        couponCodeTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        couponCodeTextField.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        couponCodeTextField.layer.borderWidth = 1
        couponCodeTextField.clipsToBounds = true
        
        couponCodeTextField.attributedPlaceholder = NSAttributedString(string: "Código de cupón",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        validationButtonView.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        validationButtonView.backgroundColor = StandarColorAndForms.blue
        validationButtonView.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        validationButtonView.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        
    }
    
    //
    //    override func viewDidAppear(_ animated: Bool) {
    //        print("-----hola")
    //
    //        //Show Popup userPermits
    //
    //        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    //
    //
    //        let PopUpUserPermitsViewController = storyBoard.instantiateViewController(withIdentifier: "PopUpUserPermitsViewController") as! PopUpUserPermitsViewController
    //
    //
    //        //Change state of first launch app
    //
    //        //        UserDefaults.standard.set(true, forKey: "openedBefore")
    //        //        UserDefaults.standard.synchronize()
    //
    //
    //        //presente InsertCodeViewController
    //        self.present(PopUpUserPermitsViewController, animated: true, completion: nil)
    //    }
    //
    
    
    // Listens for network changes
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            print("Internet", "wifi")
            isWifiActive = true
            isWifiEnabled(isWifiOn: isWifiActive)
        case .cellular:
            print("Internet", "cellular")
            isWifiActive = false
            isWifiEnabled(isWifiOn: isWifiActive)
        case .none:
            print("Internet", "none")
            isWifiActive = false
        case .vpn:
            print("Internet", "VPN")
        }
    }
    
    // Terminos label touch action
    @objc func terminosTapFunction(sender:UITapGestureRecognizer) {
        performSegue(withIdentifier: "toWebViewController", sender: self)
    }
    
    // Check box action
    @objc func checkBoxLabelTapFunction() {
        checkBox()
    }
    
    // Check box action
    @IBAction func checkBoxButtonAction(_ sender: Any) {
        checkBox()
    }
    
    // Checkbox, check and uncheck behavior
    func checkBox() {
        isBoxChecked = !isBoxChecked
        var icon = UIImage(named: "unchecked_box")!
        if isBoxChecked {
            // Setting floating button icon
            icon = UIImage(named: "checked_box")!
            validationButtonView.isUserInteractionEnabled = true
            validationButtonView.alpha = 1.0
            
        } else {
            icon = UIImage(named: "unchecked_box")!
            validationButtonView.isUserInteractionEnabled = false
            validationButtonView.alpha = 0.5
        }
        checkBoxButton.setImage(icon, for: .normal)
        checkBoxButton.tintColor = Utils.hexToColor(hexString: "#00FF00")
        checkBoxButton.imageView?.contentMode = .scaleAspectFit
        checkBoxButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    // Shows toast if Wi-fi is active and stops any further process
    func isWifiEnabled(isWifiOn: Bool) {
        
        #if targetEnvironment(simulator)
        return
        #endif
        
        if isWifiOn && Utils.APP_MODE == Utils.AppMode.production {
            //showToast(title: "Aviso", text: Utils.DISABLE_WIFI_TO_REDEEM, interval: 3.0)
            //showMessageVPN()
            showDissableWifiPopup()
            return
        }
    }
    
    //Show Dissable wifi Popup
    
    func showDissableWifiPopup(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let disableWifiPopupViewController = storyBoard.instantiateViewController(withIdentifier: "DisableWifiPopupViewController") as! DisableWifiPopupViewController
        
        disableWifiPopupViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        self.present(disableWifiPopupViewController, animated: true, completion: nil)
        
    }
    
    // Shows Action Sheet
    var actionSheet = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
    func showMessageVPN() {
        actionSheet = UIAlertController(title: "Aviso", message: Utils.DISABLE_WIFI_TO_REDEEM, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Aceptar", style: .default))
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    // If there is an active coupon go to corresponding view controller
    func applicationModeSelector(controller: String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var viewController: UIViewController?
        
        // Creates corresponding view controller object
        switch controller {
        case Utils.MAIN_VIEW_CONTROLLER:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! MainViewController
        case Utils.TAB_BAR_CONTROLLER:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! TabBarController
        case Utils.SUCCESS_VIEW_CONTROLLER:
            viewController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! SuccessViewController
        default:
            print("Invalid choice")
        }
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Method not in use
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Timer for suspending coupon
    @objc func suspensionAction() {
        print("Waiting to suspend coupon")
        //suspendExpireCoupon(coupon: Utils.getCouponObject().vpnData.username, type: Utils.SUSPEND)
    }
    
    // Timer for expiring coupon
    @objc func expirationAction() {
        print("Waiting to expire coupon")
        //suspendExpireCoupon(coupon: Utils.getCouponObject().vpnData.username, type: Utils.EXPIRE)
    }
    
    // Supend coupon logic
    func suspendExpireCoupon (coupon: String, type: String) {
        // Making Request
        Alamofire.request(configureRequest(coupon: coupon, type: type)).responseData {
            dataResponse in
            
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                let message = json[Utils.MESSAGE].string // not used for the moment
                let status = json[Utils.STATUS].bool
                
                print("InsertCouponCode status: ", status!)
                print("InsertCouponCode message: ", message!)
                print("InsertCouponCode Pending expiration: ", UserDefaults.standard.bool(forKey: Utils.PENDING_EXPIRATION))
                print("InsertCouponCode Pending suspension: ", UserDefaults.standard.bool(forKey: Utils.PENDING_SUSPENSION))
                
                if status! || message!.contains("suspendido") || message!.contains("expirado")  {
                    // Notifying Application that this coupon was suspended/expired
                    DispatchQueue.main.async {
                        self.setCouponState(type: type)
                        self.timer?.invalidate()
                        self.timer = nil
                        print("InsertCouponCode succesfully suspended/expired coupon")
                    }
                }
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                // Notifying Application that this coupon has a pending suspension/expiration
                //self.setCouponState(type: type)
            }
            
        }
        
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String, type: String) -> URLRequest {
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = getFCMToken()
        
        // Getting corresponding url
        let urlRequest = getCorrespondingURL(coupon: coupon, type: type)
        // This code escapes unwanted characters
        let url : NSString = urlRequest as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let expireURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: expireURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 2 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Set corresponding coupon memory state value in memory
    func setCouponState(type: String) {
        print("Pending state", type)
        if type == Utils.SUSPEND {
            UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
            UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
            print("Pending suspension", type)
        }
        if type == Utils.EXPIRE {
            UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
            UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
            print("Pending expiration", type)
        }
    }
    
    // Get corresponding url
    func getCorrespondingURL(coupon: String, type: String) -> String {
        var urlRequest = ""
        if type == Utils.SUSPEND {
            urlRequest = Utils.SUSPEND_COUPON_URL + coupon
        }
        
        if type == Utils.EXPIRE {
            urlRequest = Utils.EXPIRE_COUPON_URL + coupon
        }
        
        return urlRequest
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }
    
    // Validation button functionality
    @IBAction func validationButton(_ sender: UIButton) {
        
        inputCheck()
        
    }
    
    // Toast for activating location service
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    func showToast(title: String, text: String, interval: Double) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: interval, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // Checking if input has text
    func inputCheck() {
        //Checking if wifi is active
        /*if couponCodeTextField.text == Utils.APPLE {// Check for Apple's Review //user@bluestudio.mx
         Utils.changeAllAppMode(APP_MODE: Utils.AppMode.production)
         } else {
         Utils.changeAllAppMode(APP_MODE: Utils.AppMode.production) // Enable only for Apple reviews
         }*/
        
        //Simulator
        #if targetEnvironment(simulator)
        // Validating user input
        if(couponCodeTextField.text != "") {
            if(CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied) {
                locationManager.requestWhenInUseAuthorization()
                couponCodeTextField.resignFirstResponder()
                //performSegue(withIdentifier: Utils.GO_TO_EXTENDED_VALIDATION_VIEW_CONTROLLER, sender: self)
                self.checkCoupon(coupon: couponCodeTextField.text!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil))
                
            } else {
                // If location service is not active show toast
                showToast(title: Utils.TO_VALIDATE_COUPON, text: Utils.YOU_HAVE_TO_ACTIVATE_LOCATION_SERVICE)
            }
        } else {
            // If coupon input is empty show toast
            self.toastMessage(Utils.YOU_HAVE_TO_ENTER_A_COUPON)
        }
        return
        
        #endif
        
        if couponCodeTextField.text != Utils.APPLE {// Check for Apple's Review
            let reachability = Reachability()
            if reachability?.connection == Reachability.Connection.wifi &&
                couponCodeTextField.text != Utils.APPLE {
                view.endEditing(true)
                typeConection = "Wifi"
                performSegue(withIdentifier: "goToDisableWifi", sender: self)
                return
            }else if reachability?.connection == Reachability.Connection.none {
                view.endEditing(true)
                typeConection = "none"
                performSegue(withIdentifier: "goToDisableWifi", sender: self)
                return
            }
            isWifiEnabled(isWifiOn: isWifiActive)
        }
        //
        //isWifiEnabled(isWifiOn: isWifiActive)
        
        // Validating user input
        if(couponCodeTextField.text != "") {
            if(CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != .denied) {
                locationManager.requestWhenInUseAuthorization()
                couponCodeTextField.resignFirstResponder()
                //performSegue(withIdentifier: Utils.GO_TO_EXTENDED_VALIDATION_VIEW_CONTROLLER, sender: self)
                self.checkCoupon(coupon: couponCodeTextField.text!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil))
                
            } else {
                // If location service is not active show toast
                showToast(title: Utils.TO_VALIDATE_COUPON, text: Utils.YOU_HAVE_TO_ACTIVATE_LOCATION_SERVICE)
            }
        } else {
            // If coupon input is empty show toast
            self.toastMessage(Utils.YOU_HAVE_TO_ENTER_A_COUPON)
        }
    }
    
    // Check coupon
    func checkCoupon(coupon: String) {
        // Making Request
        
        Utils().showChargerView(viewToCustom: self.view)
        Alamofire.request(configureRequest(coupon: coupon, url: Utils.CHECK_COUPON_URL)).responseData {
            dataResponse in
            
            print("***************")
            print(JSON(dataResponse.result.value))
            
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                
                if json.isEmpty {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.onFailedToCheckCoupon(data: "Ocurrió un fallo de conectividad.")
                    return
                }
                
                // Getting app versions
                guard let appVersion: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayActionSheet(message: Utils.ISSUES_TRYING_TO_REDEEM)
                    return
                }
                
                guard let appStoreVersion = json["app_version"]["ios"].string else {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                // Showing Versions in logs
                print("Version Local: ", appVersion)
                print("Version AppStore: ", appStoreVersion)
                // Service info maessage to user
                if appStoreVersion == "0.0.0" {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayMaintainanceActionSheet(message: Utils.SERVER_MAINTENANCE)
                    return
                }
                
                // Redirect to Itunes if there is a new App Version
                if Utils.hasUpdatedAppVersion(localAppVersion: appVersion, appStoreVersion: appStoreVersion) {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayAppStoreActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                // Checking if parameters exists in reponse
                guard let message = json[Utils.MESSAGE].string else {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                guard var success = json[Utils.SUCCESS].bool else {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                
                guard var type = json[Utils.TYPE].string else {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.displayActionSheet(message: Utils.UPDATE_APP)
                    return
                }
                // Checking if coupon exists in publifon datos CRM
                // If coupon exists in datos CRM create a coupon check object and assign its type
                if success {
                    Utils().stopChargerView(viewToCustom: self.view)
                    self.performSegue(withIdentifier: Utils.GO_TO_EXTENDED_VALIDATION_VIEW_CONTROLLER, sender: self)
                } else {
                    self.onFailedToCheckCoupon(data: message)
                }
                
                Utils().stopChargerView(viewToCustom: self.view)
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                Utils().stopChargerView(viewToCustom: self.view)
                self.onFailedToCheckCoupon(data: Utils.CONECTION_ISSUES)
            }
            
        }
        
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String, url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url+coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.GET
        //request.timeoutInterval = 5
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        return request
    }
    
    // Display Action Sheet
    func displayActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func displayMaintainanceActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
            //antes de convertir el cuerpo de la app en nav controller
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: false)
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Display AppStore Action Sheet
    func displayAppStoreActionSheet(message: String) {
        let actionSheet = UIAlertController(title: "Aviso", message: message, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Aceptar", style: .cancel){ action in
            UIApplication.shared.open(NSURL(string: Utils.ITUNES_URL)! as URL)
            //antes de convertir el cuerpo de la app en nav controller
            //self.dismiss(animated: true, completion: nil)
            
            self.navigationController?.popViewController(animated: false)
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    // Preparing for segue, passing coupon code
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Utils.GO_TO_VALIDATION_VIEW_CONTROLLER {
            let validationViewController = segue.destination as! ValidationViewController
            validationViewController.couponCode = couponCodeTextField.text!
            print("Coupon 3:", self.couponCodeTextField.text!)
            validationViewController.couponType = self.couponType
            validationViewController.phoneNumber = self.phoneNumber
            validationViewController.chosenGroup = self.chosenGroup
            validationViewController.couponSelectedChoice = self.couponSelectedChoice
            validationViewController.delegate = self
            
            validationViewController.isFromQR = self.isFromQR
        }
        
        //Segue to check type of coupon
        if segue.identifier == Utils.GO_TO_EXTENDED_VALIDATION_VIEW_CONTROLLER {
            
            //if is qr coupon
            if isFromQR {
                let extendedValidationViewController = segue.destination as! ExtendedValidationViewController
                let coupon = couponCodeTextField.text!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
                extendedValidationViewController.coupon = coupon
                extendedValidationViewController.isFromQR = self.isFromQR
                extendedValidationViewController.phoneNumber = self.phoneNumber
                extendedValidationViewController.delegate = self
                return
                
            }
            
            let extendedValidationViewController = segue.destination as! ExtendedValidationViewController
            let coupon = couponCodeTextField.text!.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            extendedValidationViewController.coupon = coupon
            extendedValidationViewController.delegate = self
            
            extendedValidationViewController.isFromQR = self.isFromQR
        }
        
        // Segue to terminos web view
        if segue.identifier == "toWebViewController" {
            _ = segue.destination as! WebViewController
        }
        
        if segue.identifier == "goToDisableWifi" {
            let changeWifiViewController = segue.destination as! ChangeWifiViewController
            changeWifiViewController.typeConection = typeConection
            //changeWifiViewController.isBluetothOff = isBluetothOff
            
            
        }
    }
    
    // Action performed when return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    // Code related to show UI elements when keyboard shows/hides
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    // Code related to show UI elements when keyboard shows/hides
    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.frame.origin.y = 0        
    }
    
    // PROTOCOL METHODS
    // Implemented method of FailToTedeemCoupon protocol notification
    func onFailedToRedeemCoupon(data: String) {
        InsertCodeViewController.activityIndicator.stopAnimating()
        toastMessage(data)
        print("change 1", data)
    }
    // Implemented method of FailToCheckCoupon protocol notificacion
    func onFailedToCheckCoupon(data: String) {
        InsertCodeViewController.activityIndicator.stopAnimating()
        toastMessage("\(data), favor de verificarlo.")
        print("change 2", data)
    }
    // Displays toast to inform user about coupon suspension
    func onSuspendedCoupon(status: String) {
        print("fffff", status)
        // toastMessage(status)
        showMessageVPN()
    }
    
    // Choice coupon redemtion behavior
    func onFinishCheckingChoiceCoupon(type: Utils.CouponType,
                                      phoneNumber: String,
                                      selected: Utils.CouponSelectedChoice,
                                      view: UIViewController) {
        startActivityIndicator()
        ExtendedValidationViewController.activityIndicator.startAnimating()
        self.couponType = type
        self.phoneNumber = phoneNumber
        self.couponSelectedChoice = selected
        InsertCodeViewController.extendedViewController = view
        toValidateVontrollerTimer = Timer.scheduledTimer(timeInterval: /*0.75*/0.1, target: self, selector: #selector(toValidateControllerAction), userInfo: nil, repeats: false)
        // For some reason this Method is needed to close ChoiceViewController
        // in ChoiceViewController there is also a dismiss method but
        // it only works if i execute this method here.
        //view.dismiss(animated: true, completion: nil)
        
    }
    
    // New Coupon redemtion behavior
    func onFinishCheckingChoiceCoupon(type: Utils.CouponType, phoneNumber: String, selected: Utils.CouponSelectedChoice, view: UIViewController, chosenGroup: String) {
        startActivityIndicator()
        ExtendedValidationViewController.activityIndicator.startAnimating()
        self.couponType = type
        self.phoneNumber = phoneNumber
        self.chosenGroup = chosenGroup
        self.couponSelectedChoice = selected
        InsertCodeViewController.extendedViewController = view
        toValidateVontrollerTimer = Timer.scheduledTimer(timeInterval: /*0.75*/0.1, target: self, selector: #selector(toValidateControllerAction), userInfo: nil, repeats: false)
        // For some reason this Method is needed to close ChoiceViewController
        // in ChoiceViewController there is also a dismiss method but
        // it only works if i execute this method here.
        //view.dismiss(animated: true, completion: nil)
    }
    
    // If simple coupon was succesfully found, then go to validation view controller
    func onFinishCheckingCoupon(type: Utils.CouponType,
                                phoneNumber: String,
                                chosenGroup: String) {
        startActivityIndicator()
        ExtendedValidationViewController.activityIndicator.startAnimating()
        // Stops activity indicator started at extended validation controller
        //ExtendedValidationViewController.activityIndicator.stopAnimating()
        // Assings coupon type variable for sendig it to validate view controller
        print("Coupon type: ", type.rawValue)
        self.couponType = type
        self.phoneNumber = phoneNumber
        self.chosenGroup = chosenGroup
        startActivityIndicator()
        // Setting timer to Validate controller
        toValidateVontrollerTimer = Timer.scheduledTimer(timeInterval: /*0.75*/0.1, target: self, selector: #selector(toValidateControllerAction), userInfo: nil, repeats: false)
    }
    
    // Timer selector action
    // sends user to Validation Controller after .75 of a second
    @objc func toValidateControllerAction(){
        performSegue(withIdentifier: Utils.GO_TO_VALIDATION_VIEW_CONTROLLER, sender: self)
    }
    
    // Animates activity indicator
    func startActivityIndicator() {
        InsertCodeViewController.activityIndicator.center = self.view.center
        InsertCodeViewController.activityIndicator.hidesWhenStopped = true
        InsertCodeViewController.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        view.addSubview(InsertCodeViewController.activityIndicator)
        InsertCodeViewController.activityIndicator.startAnimating()
    }
    
    // Stops activity indicator animation
    public static func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
    
    //
    //      func centralManagerDidUpdateState(_ central: CBCentralManager) {
    //
    //          switch central.state {
    //          case .poweredOn:
    //              print("Bluetooth is On.")
    //              isBluetothOff = false
    //
    //              break
    //          case .poweredOff:
    //              print("Bluetooth is Off.")
    //              isBluetothOff = true
    //              break
    //          case .resetting:
    //              break
    //          case .unauthorized:
    //
    //              break
    //          case .unsupported:
    //              isBluetothOff = true
    //              break
    //          case .unknown:
    //              isBluetothOff = false
    //
    //              break
    //          default:
    //              break
    //          }
    //      }
    
    
}



// Related UIButton code for border
@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    // related UIButton for border radius
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    // related UIButton for border color
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    //     func appUpdateAvailable() -> Bool {
    //        let identifier = info["CFBundleIdentifier"] as? String
    //        let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=YOURBUNDLEID"
    //        var upgradeAvailable = false
    //        // Get the main bundle of the app so that we can determine the app's version number
    //        let bundle = Bundle.main
    //        if let infoDictionary = bundle.infoDictionary {
    //            // The URL for this app on the iTunes store uses the Apple ID for the  This never changes, so it is a constant
    //            let urlOnAppStore = NSURL(string: storeInfoURL)
    //            if let dataInJSON = NSData(contentsOf: urlOnAppStore! as URL) {
    //                // Try to deserialize the JSON that we got
    //                if let dict: NSDictionary = try! JSONSerialization.jsonObject(with: dataInJSON as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject] as NSDictionary? {
    //                    if let results:NSArray = dict["results"] as? NSArray {
    //                        if let version = (results[0] as AnyObject).value("version") as? String {
    //                            // Get the version number of the current version installed on device
    //                            if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
    //                                // Check if they are the same. If not, an upgrade is available.
    //                                print("\(version)")
    //                                if version != currentVersion {
    //                                    upgradeAvailable = true
    //                                }
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        return upgradeAvailable
    //    }
}

// Toast code
extension UIViewController {
    func toastMessage(_ message: String){
        guard let window = UIApplication.shared.keyWindow else {return}
        let messageLbl = UILabel()
        messageLbl.text = message
        messageLbl.textAlignment = .center
        messageLbl.font = UIFont.systemFont(ofSize: 12)
        messageLbl.textColor = .white
        messageLbl.backgroundColor = UIColor(white: 0, alpha: 0.5)
        
        let textSize:CGSize = messageLbl.intrinsicContentSize
        let labelWidth = min(textSize.width, window.frame.width - 40)
        
        messageLbl.frame = CGRect(x: 20, y: window.frame.height - 90, width: labelWidth + 30, height: textSize.height + 20)
        messageLbl.center.x = window.center.x
        messageLbl.layer.cornerRadius = messageLbl.frame.height/2
        messageLbl.layer.masksToBounds = true
        window.addSubview(messageLbl)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            
            UIView.animate(withDuration: 1, animations: {
                messageLbl.alpha = 0
            }) { (_) in
                messageLbl.removeFromSuperview()
            }
        }
    }
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "i386", "x86_64":                          return "Simulator"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        default:                                        return identifier
        }
    }
    
    
    
}
