//
//  VozMenuViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/25/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FirebaseInstanceID
import FirebaseCore
import Firebase


class VozMenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Class variables
    let menuItemArray: [String] = ["Detalles de cupón",
                                   "Preguntas frecuentes",
                                   "Tutorial",
                                   "Suspender cupón"]
    
    let menuIconArray: [UIImage] = [UIImage(named: "user")!,
                                    UIImage(named: "answer")!,
                                    UIImage(named: "tutorial")!,
                                    UIImage(named: "suspend")!]
    
    
    var deviceToken = ""
    var couponObject: CouponModel?
    var couponType = ""
    var backgroundColor = ""
    var accentuationColor = ""
    var textColor = ""
    
    // Linked outlets
    @IBOutlet weak var menuLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuLabelContainerView: UIView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var aboutAppButton: UIButton!
    @IBOutlet weak var logoMenuVozImageView: UIImageView!
    @IBOutlet weak var tittleServiceMenuLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        }
        
        // Getting device token
        deviceToken = getFCMToken()
        
        // If coupon is combo then set customization colors
        if couponType == Utils.CouponType.combo.rawValue{
            setCustomizationColors()
        }

        // Stting table view delegates
        menuTableView.delegate = self
        menuTableView.dataSource = self
        
        menuTableView.tableHeaderView = UIView()
        menuTableView.tableFooterView = UIView()
        menuTableView.alwaysBounceVertical = false
      
        
        
        //Custom footer
        footerView.backgroundColor = menuTableView.backgroundColor
        aboutAppButton.setImage(UIImage(named: "aboutLogo"), for: .normal)
        aboutAppButton.tintColor = .black
        
        //Customer menu
         self.logoMenuVozImageView.image = UIImage(named: "logo_publifon")
        
        
        // Registers a ContactTableViewCell to contactsTableView
        self.menuTableView.register(UINib(nibName: "VozMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "vozMenuTableViewCell")
        
        //Utils.lineDraw(viewLi: menuLabel, borderColor: .black)
    }
    
    // Table view number of rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItemArray.count
    }
    
    // Table view cells setup
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuTableView.dequeueReusableCell(withIdentifier: "vozMenuTableViewCell", for: indexPath) as! VozMenuTableViewCell

        // Setting corresponding cell
        if couponType == Utils.CouponType.combo.rawValue {
            comboTableSetup(cell: cell, index: indexPath.row)
        } else {
            vozTableSetup(cell: cell, index: indexPath.row)
        }
        
         //custom size of cell
        cell.menuItem.lineBreakMode = NSLineBreakMode.byWordWrapping;
        cell.menuItem.numberOfLines = 0
        cell.frame.size.height = (cell.textLabel?.frame.height)!
        cell.frame.size.height = (cell.menuItem.frame.height)
        
        return cell
    }
    
    // Setup for a voz coupon
    func vozTableSetup(cell: VozMenuTableViewCell, index: Int) {
        cell.selectionStyle = .none
        cell.menuItem.text = menuItemArray[index]
        cell.menuItemIconImageView.image = menuIconArray[index].withRenderingMode(.alwaysTemplate)
        cell.menuItemIconImageView.tintColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        cell.menuItem.textColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        aboutAppButton.tintColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        aboutAppButton.imageView?.image?.withRenderingMode(.alwaysTemplate)
        aboutAppButton.imageView?.tintColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        menuLabel.textColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        tittleServiceMenuLabel.textColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
    }
    
    func comboTableSetup(cell: VozMenuTableViewCell, index: Int) {
        cell.selectionStyle = .none
        
        cell.menuItem.text = menuItemArray[index]
        cell.menuItem.textColor = Utils.hexToColor(hexString: textColor)
        
        cell.menuItemIconImageView.image = menuIconArray[index]
        cell.menuItemIconImageView.image = cell.menuItemIconImageView.image?.withRenderingMode(.alwaysTemplate)
        cell.menuItemIconImageView.tintColor = Utils.hexToColor(hexString: accentuationColor)
        tittleServiceMenuLabel.textColor = Utils.hexToColor(hexString: accentuationColor)
    }
    
    // Table view selected item behavior
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell(row: indexPath.row)
    }
    
    // Cell Selection response method
    func selectedCell(row: Int) {
        switch row {
            
        case 0:
            hideMenu()
            performSegue(withIdentifier: "goToCouponDetailsModalViewController", sender: self)
            break
        case 1:
            hideMenu()
            performSegue(withIdentifier: "goToFrequentQuestionsViewController", sender: self)
            break
        case 2:
            hideMenu()
            performSegue(withIdentifier: "goToVozTutorialViewController", sender: self)
            break
        case 3:
            print("Suspend")
            hideMenu()
            performSegue(withIdentifier: "goToSuspendCouponModalViewController", sender: self)
            break
        default:
            break
            
        }
        
    }
    
    
    @IBAction func goToAboutApp(_ sender: Any) {
        hideMenu()
        //go to about pop up
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let aboutPopUpViewController = storyBoard.instantiateViewController(withIdentifier: "AboutPopUpViewController") as! AboutPopUpViewController
        
        //presente InsertCodeViewController
        self.present(aboutPopUpViewController, animated: true, completion: nil)
        
        
        
    }
    
    // Preparing for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("prepare")
        if segue.identifier == "goToSuspendCouponModalViewController" {
            _ = segue.destination as! SuspendCouponModalViewController
        }
        
        if segue.identifier == "goToCouponDetailsModalViewController" {
            _ = segue.destination as! CouponDetailsModalViewController
        }
        
        if segue.identifier == "goToVozTutorialViewController" {
            _ = segue.destination as! VozTutorialViewController
        }
        
        
        if segue.identifier == "AboutPopUpViewController" {
            let aboutPopUpViewController = segue.destination as! AboutPopUpViewController
            aboutPopUpViewController.fromMenuOnlyVoz = true
            
        }
                
        if segue.identifier == "goToFrequentQuestionsViewController" {
            _ = segue.destination as! FrequentQuestionsViewController
        }
        
    }

    // Hides side menu (which is itself, this is the menu view controller)
    func hideMenu() {
        // Setting menu animation
        UIView.animate(withDuration: 0.3, animations: {  () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0,
                                     width: UIScreen.main.bounds.size.width,
                                     height: UIScreen.main.bounds.size.height)
        }) {(finished) in
            self.view.removeFromSuperview()
        }
    }
    
    // Sets customization colors
    func setCustomizationColors() {
        // Getting coupon from data base
        couponObject = Utils.getCouponObject()
        
        // Getting customization colors
        backgroundColor = couponObject!.campaign.backgroungColor
        accentuationColor = couponObject!.campaign.accentuationColor
        textColor = couponObject!.campaign.textColor
    }
    
    // Logic for suspending a coupon
    func suspension() {
        // Deleting coupon state
        self.clearCoupon()
        // Sends user to insert view controller
        self.sendUserToInsertViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        //send user to insert code vc
        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let insertCodeViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.INSERT_CODE_VIEW_CONTROLLER) as! InsertCodeViewController
        appDelegate.window?.rootViewController = insertCodeViewController
        appDelegate.window?.makeKeyAndVisible()*/
        
        //send to menu redemtion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let MenuNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = MenuNavigationController.topViewController as! SeleccionDeRedencionViewController
        appDelegate.window?.rootViewController = MenuNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Clears coupon data stored in memory and sends back to insert view controller
    func clearCoupon() {
        resetVozCouponMemoryValues()
        resetDatosCouponMemoryValues()
    }
    
    // Reset coupon memory state
    func resetVozCouponMemoryValues() {
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.PHONE_NUMBER)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
    }
    
    // Reset coupon memory state
    func resetDatosCouponMemoryValues() {
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.REDEMPTION_DATE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.COUPON_EXPIRATION_DATE)
        UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(["", "", ""], forKey: "sliderArray")
        AppDelegate.sliderArray = ["", "", ""]
        UserDefaults.standard.set(0, forKey: "index")
        AppDelegate.index = 0
    }
    
    // Supend coupon logic
    func suspendCoupon (coupon: String) {
        // Making Request
        Alamofire.request(configureRequest(coupon: coupon)).responseData {
            dataResponse in
            
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                _ = json[Utils.MESSAGE].string // not used for the moment
                let status = json[Utils.STATUS].bool
                
                if status! {
                    // Telling Application that this coupon was suspended
                    UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
                    self.suspension()
                }
            } else {
                print("Error: \(dataResponse.result.error!)")
                // Telling Application that this coupon has a pending suspension
                UserDefaults.standard.set(true, forKey: Utils.PENDING_SUSPENSION)
                self.suspension()
            }
            
        }
        
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String) -> URLRequest{
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = self.deviceToken
        
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = Utils.SUSPEND_COUPON_URL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let suspendURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: suspendURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 5 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }
    
}
