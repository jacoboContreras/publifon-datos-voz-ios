//
//  MenuViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/5/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    // Linked outlets
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var menuCompanyLogo: UIImageView!
    @IBOutlet weak var menuCompanyNameLabel: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var menuLabelContainer: UIView!
    @IBOutlet weak var aboutLogoButton: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var serviceTittleLabel: UILabel!
    
    
    
    // Class variables
    var menuViewController: MenuViewController!
    let menuItemArray: [String] = ["Detalles de cupón",
                                   "Preguntas frecuentes",
                                   "Tutorial",
                                   //"Aviso de privacidad",
                                   "Términos y condiciones & Aviso de privacidad.",
                                   "Suspender cupón"
                                   ]  
    
    let menuIconArray: [UIImage] = [UIImage(named: "user")!,
                                    UIImage(named: "answer")!,
                                    UIImage(named: "tutorial")!,
                                    UIImage(named: "about")!,
                                    UIImage(named: "suspend")!
                                    ]
    
 
    let couponObject = Utils.getCouponObject()
    var isCalledFromVoz = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("isCalledFromVoz", isCalledFromVoz);
               
        
        // Getting customization colors
        let backgroundColor = couponObject.campaign.backgroungColor
        let accentuationColor = couponObject.campaign.accentuationColor
        let textColor = couponObject.campaign.textColor
        
        
        //custom footer view
        aboutLogoButton.setImage(UIImage(named: "aboutLogo"), for: .normal)
        aboutLogoButton.backgroundColor = .none
        aboutLogoButton.tintColor = Utils.hexToColor(hexString: accentuationColor)
        footerView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: backgroundColor)
        
        // Setting up table view delegates
        menuTableView.delegate = self
        menuTableView.dataSource = self
        menuTableView.allowsSelection = true
        menuTableView.register(UINib(nibName: "MenuTableViewCell", bundle: nil),
                                       forCellReuseIdentifier: "menuTableViewCell")
        
        // Setting menu table components
        menuTableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)//Utils.hexToColor(hexString: backgroundColor)
        menuTableView.separatorColor = Utils.hexToColor(hexString: accentuationColor)
        
        
        // Clears bottom lines of extra table view items
        menuTableView.tableFooterView = UIView()
        menuTableView.alwaysBounceVertical = false
        //menuTableView.rowHeight = 50
        
                
        // Setting up menu logo in slide menu
        menuCompanyLogo.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        let logoURL = URL(string: couponObject.company.logoURL)
        self.menuCompanyLogo.sd_setImage(with: logoURL, placeholderImage: UIImage(named: ""))
        
        // Setting menu label
        menuCompanyNameLabel.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: backgroundColor)
        menuCompanyNameLabel.text = "Menú"
        menuCompanyNameLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) //Utils.hexToColor(hexString: textColor)
        serviceTittleLabel.textColor = Utils.hexToColor(hexString: textColor)
        // Setting label bottom line
        //Utils.lineDraw(viewLi: menuCompanyNameLabel, borderColor: Utils.hexToColor(hexString: textColor))
        viewContainer.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
        
        //menuLabelContainer.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
    }   
    
    // Table view item size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuIconArray.count
    }
   
    
    // Table view item visualization
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = menuTableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.selectionStyle = .none
        cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: self.couponObject.campaign.backgroungColor)
        cell.menuItemIcon.image = menuIconArray[indexPath.row]
        
        cell.menuItemIcon.image = cell.menuItemIcon.image?.withRenderingMode(.alwaysTemplate)
        cell.menuItemIcon.tintColor = Utils.hexToColor(hexString: self.couponObject.campaign.accentuationColor)
        
        //cell.menuItemIcon.tintColor = Utils.hexToColor(hexString: self.couponObject.campaign.accentuationColor)
        cell.menuItemLabel.text = menuItemArray[indexPath.row]
        
        cell.menuItemLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1) //Utils.hexToColor(hexString: couponObject.campaign.textColor)
        
         //custom size of cell
        cell.menuItemLabel.lineBreakMode = NSLineBreakMode.byWordWrapping;
        cell.menuItemLabel.numberOfLines = 0
        cell.frame.size.height = (cell.textLabel?.frame.height)!
        cell.frame.size.height = (cell.menuItemLabel.frame.height)
        
        return cell
    }
    
    
    // Table view selected item behavior
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCell(row: indexPath.row)
    }
    
   
    
    // Cell Selection response method
    func selectedCell(row: Int) {
        switch row {
            
        case 0:
            hideMenu()
            performSegue(withIdentifier: "goToCouponDetailsModalViewController", sender: self)
            break
        case 1:
            hideMenu()
            performSegue(withIdentifier: "goToFrequentQuestionsViewController", sender: self)
            break
            
        case 2:
            hideMenu()
            if !isCalledFromVoz {
                performSegue(withIdentifier: "goToTutorialViewcontroller", sender: self)
            } else {
                performSegue(withIdentifier: "goToVozTutorialViewController", sender: self)
            }
            break
        case 3:
            hideMenu()
            performSegue(withIdentifier: "goToTermsConditionsViewController", sender: self)
            break
        case 4:
            hideMenu()
            performSegue(withIdentifier: "goToSuspendCouponModalViewController", sender: self)
            break
        default:
            break
            
        }
        
    }
    
//
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//
//        //add footer to table and insert a image logo
//        var tableViewHeight: CGFloat {
//            menuTableView.layoutIfNeeded()
//
//            return menuTableView.contentSize.height
//        }
//
//        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: menuTableView.frame.size.width, height: )
//
//
//        //(tableView.tableFooterView?.frame.size.height)!
//       //menuTableView.frame.size.height - tableViewHeight)
//
//        //set action to image in footer
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//
//        let imageLogo = UIImage(named: "aboutLogo")
//        let image = UIImageView(image: imageLogo)
//        image.isUserInteractionEnabled = true
//        image.addGestureRecognizer(tapGestureRecognizer)
//
//        //image.center = CGPoint(x: footerView.frame.size.width  / 2,
//                               //y: footerView.frame.size.height / 2)
//        image.frame.size = footerView.frame.size
//
//        image.backgroundColor = .none
//        footerView.backgroundColor = .red
//
//
//        //footerView.insertSubview(image, at: 0)
//
//        footerView.addSubview(image)
//        return footerView
//    }ç
    
    @IBAction func goToAboutApp(_ sender: Any) {
        hideMenu()
        performSegue(withIdentifier: "goToAboutPopUpViewController", sender: self)
    }
    
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView

        // go to termsConditions
        hideMenu()
        performSegue(withIdentifier: "goToTermsConditionsViewController", sender: self)
    }
    
    // Preparing for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToTutorialViewcontroller" {
            _ = segue.destination as! TutorialViewController
        }
        
        if segue.identifier == "goToCouponDetailsModalViewController" {
            _ = segue.destination as! CouponDetailsModalViewController
        }
        
        if segue.identifier == "goToSuspendCouponModalViewController" {
            _ = segue.destination as! SuspendCouponModalViewController
        }
        
        if segue.identifier == "goToVozTutorialViewController" {
            _ = segue.destination as! VozTutorialViewController
        }
        
        if segue.identifier == "goToTermsConditionsViewController" {
            _ = segue.destination as! TermsConditionsPopUpViewController
        }
        
        if segue.identifier == "goToFrequentQuestionsViewController" {
            _ = segue.destination as! FrequentQuestionsViewController
        }
        
    }
    
    // Hides side menu (which is itself, this is the menu view controller)
    func hideMenu() {
        // Setting menu animation
        UIView.animate(withDuration: 0.3, animations: {  () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0,
                                                        width: UIScreen.main.bounds.size.width,
                                                        height: UIScreen.main.bounds.size.height)
        }) {(finished) in
            self.view.removeFromSuperview()
        }
    }
    
    
    // Reset coupon memory state
    func resetCouponMemoryValues() {
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.REDEMPTION_DATE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.COUPON_EXPIRATION_DATE)
        //UserDefaults.standard.set(Utils.RESET_COUPON, forKey: Utils.JSON_KEY)
        UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
        //UserDefaults.standard.set(Utils.RESET_COUPON, forKey: Utils.API_TOKEN)
    }
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let successViewController = mainStoryboard.instantiateViewController(withIdentifier: "InsertCodeViewController") as! InsertCodeViewController
        appDelegate.window?.rootViewController = successViewController
        appDelegate.window?.makeKeyAndVisible()
    }
}

