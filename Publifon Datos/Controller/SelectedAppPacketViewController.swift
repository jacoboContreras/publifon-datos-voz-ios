//
//  SelectedAppPacketViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 10/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

protocol SelectedGroup {
    func onSelectedGroup(groupType: Utils.CouponType, chosenGroup: Int, controller: UIViewController)
}

class SelectedAppPacketViewController: UIViewController {
    
    // Class Variables
    var groupName = String()
    var groupId = Int()
    var groupType = Utils.CouponType.none
    var delegate: SelectedGroup?
    var isWifiActive = false
    let reachability = Reachability()!
    var coupon = ""
    var logoURL = URL(string: "")
    
    // Linked outlets
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var acceptButton: UIView!
    @IBOutlet weak var labelMesagge: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var logoCompanyImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{ try reachability.startNotifier() }
        catch { print("Could not start notifier") }
        
        containerUIView.isHidden = false
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.0)
        
        // Setting up rounded corner to container view
        containerUIView.layer.cornerRadius = 10
        labelMesagge.text = groupName
        
        // Assign name of packet selected
        groupName = ScrollViewPacketViewController.groups[PacketAppsViewController.currentPage].groupName
        
        // Assign id group of group selected
        groupId = ScrollViewPacketViewController.groups[PacketAppsViewController.currentPage].groupId
        
        // Assing Coupon Type
        groupType = ScrollViewPacketViewController.groups[PacketAppsViewController.currentPage].groupType
        
        //Custom Outlet's view
        
        acceptButton.layer.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        cancelButton.layer.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        
        acceptButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cancelButton.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        acceptButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        cancelButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        
        // Set company logo
        logoCompanyImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        labelMesagge.text = "¿Estás seguro de elegir el paquete \(groupName)?"
    }
    
    // Accept button logic
    @IBAction func acceptButtonAction(_ sender: Any) {
        
        //Checking if wifi is active
        /*if self.coupon != Utils.APPLE {// Check for Apple's Review
            isWifiEnabled(isWifiOn: isWifiActive)
         }*/
        delegate?.onSelectedGroup(groupType: groupType, chosenGroup: groupId, controller: self)
        //dismiss(animated: true, completion: nil)
        
        performSegue(withIdentifier: "unwindToExtendedViewController", sender: self)
        print("EXECUTEDD")
        
    }
        
    /*func canPerformSegue(withIdentifier id: String) -> Bool {
        guard let segues = self.value(forKey: "storyboardSegueTemplates") as? [NSObject] else { return false }
        return segues.first { $0.value(forKey: "identifier") as? String == id } != nil
    }*/

    
    // Cancel button logic
    @IBAction func cancelButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        //performSegue(withIdentifier: "unwindPrincipal", sender: self)
    }
    
    // Shows toast if Wi-fi is active and stops any further process
    func isWifiEnabled(isWifiOn: Bool) {
        if isWifiOn && Utils.APP_MODE == Utils.AppMode.production {
            showToast(title: "Aviso", text: Utils.DISABLE_WIFI_TO_REDEEM, interval: 3.0)
            return
        }
    }
    
    func showToast(title: String, text: String, interval: Double) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: interval, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // Listens for network changes
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            print("Internet", "wifi")
            isWifiActive = true
        case .cellular:
            print("Internet", "cellular")
            isWifiActive = false
        case .none:
            print("Internet", "none")
            
        case .vpn:
            print("Internet", "VPN")
        }
    }
    
}
