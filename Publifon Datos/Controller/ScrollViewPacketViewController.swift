//
//  ScrollViewPacketViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 10/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Contacts
import SwiftyJSON

struct Page {
    var title: String
    var text: String
    var font: UIImage
    var image: UIImage
}

struct Packet {
    var groupName = String()
    var groupId = Int()
    var groupType: Utils.CouponType = Utils.CouponType.none
}

struct App {
    var nameApp = String()
    var availableHours = Int()
}

class ScrollViewPacketViewController: UIViewController, UIScrollViewDelegate {
    
    // Linked Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    // Class Variables
    public static var scrollViewController = UIViewController()
    static public var datosJSON: JSON?
    var appData = App()
    var pages = [StepTableViewController]()
    var dataApps = App()
    var applications = [App]()
    var groupsOfApps = [[App]]()
    var packet = Packet()
    var groups = [Packet]()
    var views = [String:UIView]()
    var visualFormat = String()
    static public var numberOfPages = Int()
    static public var groups = [Packet]()
    static public var groupsOfApps = [[App]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ScrollViewPacketViewController.scrollViewController = self
        
        // Assign data json
        let json = ScrollViewPacketViewController.datosJSON
        let benefits = json!["benefits"].array!
        
        // Add groups of apps and packet groups
        for benefit in benefits {
            print("Benefit group_name: ", benefit["group_name"].string!)
            print("------------------", "------------------")
            let groupName = benefit["group_name"].string!
            let groupId = benefit["group_id"].int!
            let groupType = benefit["group_type"].string!
            packet.groupName = groupName
            packet.groupId = groupId
            packet.groupType = assignCouponType(type: groupType)
            //get names of apps in group
            for application in benefit["applications"].array!{
                print("Benefit application: ", application["application"].string!)
                print("Benefit hours: ", application["hours"].int!)
                print("Benefit mb: ", application["mb"].int!)
                print("Benefit application_id: ", application["application_id"].int!)
                print("----------------------")
                let applicationName = application["application"].string!
                let availableHours = application["hours"].int!
                appData.nameApp = applicationName
                appData.availableHours = availableHours
                applications.append(appData)
                
            }
            //add array of apps and clean self array
            groupsOfApps.append(applications)
            applications.removeAll()
            groups.append(packet)
        }
        
        //size to content of scroll view
        scrollView.contentSize = CGSize(width:(scrollView.frame.size.width * CGFloat(groupsOfApps.count)), height: scrollView.frame.size.height)
        scrollView.delegate = self
        
        
        //number of pages
        ScrollViewPacketViewController.numberOfPages = groups.count
        ScrollViewPacketViewController.groups = groups
        ScrollViewPacketViewController.groupsOfApps = groupsOfApps
        
        //create array of views
        for iCont in 0..<groups.count{
            let tempPage = createAndAddTutorialStep(groupsOfApps[iCont], group: groups[iCont], namePage: "page\(iCont+1)")
            pages.append(tempPage)
        }
        
        //init dictionary
        views = ["view": view]
        
        //add in dictionary views and key
        for iCont in 0..<groups.count{
            let view = pages[iCont].view
            views["\(pages[iCont].namePage)"] = view
        }
        
        //init visual format string
        visualFormat = "H:|"
        
        //Add horizontal constraints between views
        for iCont in 0..<views.count-1{
            visualFormat.append("[page\(iCont+1)(==view)]")
        }
        
        //add final char to visual format
        visualFormat.append("|")
        
        //add vertical constraint
        let verticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[page1(==view)]|", options: [], metrics: nil, views: views)
        
        //add horizontal constraints
        let horizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: visualFormat, options: [.alignAllTop, .alignAllBottom], metrics: nil, views: views)
        
        //add vertical and horizontal constraints
        NSLayoutConstraint.activate(verticalConstraints + horizontalConstraints)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(moveToRightScrollView(_:)), name: Notification.Name(rawValue: "moveRight"), object: nil)
                
        NotificationCenter.default.addObserver(self, selector: #selector(moveToLeftScrollView(_:)), name: Notification.Name(rawValue: "moveLeft"), object: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0{
            //disable vertical scrolling
            scrollView.contentOffset = CGPoint(x: scrollView.contentOffset.x, y: 0)
        }
    }
    
    //func to change background color when pages change
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        // Access to pop up View Controller to change current page
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "myName"), object: self)
    }
    
    @objc func moveToRightScrollView(_ notification: Notification) {
        print("tamaño del scroll: " ,scrollView.frame.width)
        print("moviendo a la derecha: " ,scrollView.contentOffset.x)
        scrollView.contentOffset.x +=  self.view.bounds.width
        /*if scrollView.contentOffset.x <= 0 {
            scrollView.contentOffset.x +=  self.view.bounds.width
        }*/
    }
    
    @objc func moveToLeftScrollView(_ notification: Notification) {
        print("moviendo a la izquierda: ", scrollView.contentOffset.x)
        scrollView.contentOffset.x -=  self.view.bounds.width
        /*if scrollView.contentOffset.x > 0 {
            scrollView.contentOffset.x -=  self.view.bounds.width
        }*/
    }
    
    /*func rightButtonTapped() {
     if scrollView.contentOffset.x < self.view.bounds.width * CGFloat(DetailScrollArray.count-2) {
     scrollView.contentOffset.x +=  self.view.bounds.width
     }
     }*/
    
    //func to ctomize tutorial pages
    private func createAndAddTutorialStep( _ groupOfApps: [App], group: Packet, namePage: String) -> StepTableViewController{
        
        let tutorialStep = storyboard!.instantiateViewController(withIdentifier: "StepTableViewController") as! StepTableViewController
        tutorialStep.view.translatesAutoresizingMaskIntoConstraints = false
        tutorialStep.groupsOfApps.append(groupOfApps)
        tutorialStep.groups.append(group)
        tutorialStep.namePage = namePage
        
        scrollView.addSubview(tutorialStep.view)
        addChildViewController(tutorialStep)
        tutorialStep.didMove(toParentViewController: self)
        return tutorialStep
        
    }
    
    // Returns a valid groupType
    func assignCouponType(type: String) -> Utils.CouponType {
        var groupType = Utils.CouponType.none
        switch type {
        case "datos":
            groupType = Utils.CouponType.datos
        case "voz":
            groupType = Utils.CouponType.voz
        case "seleccion":
            groupType = Utils.CouponType.choice
        case "combo":
            groupType = Utils.CouponType.combo
        default:
            print("none")
        }
        
        return groupType
    }
    
}
