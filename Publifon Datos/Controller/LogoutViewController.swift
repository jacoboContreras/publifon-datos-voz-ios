//
//  LogoutViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 6/13/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import FirebaseInstanceID
import FirebaseCore
import Firebase
import Alamofire
import SwiftyJSON

class LogoutViewController: UIViewController {
    
    var couponObject: CouponModel?
    var couponType = ""
    var deviceToken = ""

    // Linked outlets
    @IBOutlet weak var viewsContainerUIView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponObject = Utils.getCouponObject()
        
        // Getting device token
        deviceToken = getFCMToken()        
        
        // Getting campaign colors
        let backgroundColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        viewsContainerUIView.backgroundColor = backgroundColor
        viewsContainerUIView.layer.cornerRadius = 10
        cancelButton.backgroundColor = accentuationColor
        cancelButton.setTitleColor(textColor, for: .normal)
        acceptButton.backgroundColor = accentuationColor
        acceptButton.setTitleColor(textColor, for: .normal)

        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
    }

    @IBAction func cancelActionButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func acceptButtonAction(_ sender: Any) {
        if isVPNRunning() {
            toastMessage("Desactiva el switch para suspender el servicio")
            return
        }
        // If choice cupon or combo, then suspend datos from CRM
        suspendCoupon(coupon: couponObject!.vpnData.username)
//        if couponType == Utils.CouponType.choice.rawValue
//            || couponType == Utils.CouponType.datos.rawValue
//            || couponType == Utils.CouponType.combo.rawValue{
//            suspendCoupon(coupon: couponObject!.vpnData.username)
//        } else {
//            suspension()
//        }
        
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }
    
    // Checks if VPN Running
    func isVPNRunning() -> Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") {
                    return true
                }
            }
        }
        return false
    }
    
    // Supend coupon logic
    func suspendCoupon (coupon: String) {
        // Making Request
        Alamofire.request(configureRequest(coupon: coupon)).responseData {
            dataResponse in
            
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                let message = json[Utils.MESSAGE].string // not used for the moment
                let status = json[Utils.STATUS].bool
                print("Message 1:", message!)
                print("Status 1:", status!)
                if status! {
                    // Telling Application that this coupon was suspended
                    UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
                    self.suspension()
                    
                } else if message!.contains("suspendido") {
                    // Telling Application that this coupon was already suspended from CRM
                    UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
                    self.suspension()
                }
                
            } else {
                print("Error: \(dataResponse.result.error!)")
                // Telling Application that this coupon has a pending suspension
                UserDefaults.standard.set(true, forKey: Utils.PENDING_SUSPENSION)
                self.suspension()
            }
            
        }
        
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String) -> URLRequest{
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = self.deviceToken
        
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = Utils.SUSPEND_COUPON_URL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let suspendURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: suspendURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 5 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Logic for suspending a coupon
    func suspension() {
        // Deleting coupon state
        self.clearCoupon()
        // Stops expiration checker timer
        MainViewController.timer?.invalidate()
        HomeViewController.timer?.invalidate()
        // Sends user to insert view controller
        self.sendUserToInsertViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Clears coupon data stored in memory
    func clearCoupon() {
        resetCouponMemoryValues()
        resetVozCouponMemoryValues()
    }
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        //send user to insert code vc
        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let insertCodeViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.INSERT_CODE_VIEW_CONTROLLER) as! InsertCodeViewController
        appDelegate.window?.rootViewController = insertCodeViewController
        appDelegate.window?.makeKeyAndVisible()*/
        
        //send to menu redemtion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let MenuNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = MenuNavigationController.topViewController as! SeleccionDeRedencionViewController
        appDelegate.window?.rootViewController = MenuNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Reset coupon memory state
    func resetVozCouponMemoryValues() {
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.PHONE_NUMBER)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_COUPON_CODE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_HOME_IMAGE_NAME)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.IMAGE_DAY_CHECK)
    }
    
    
    // Reset coupon memory state
    func resetCouponMemoryValues() {
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.REDEMPTION_DATE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.COUPON_EXPIRATION_DATE)
        UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(["", "", ""], forKey: "sliderArray")
        AppDelegate.sliderArray = ["", "", ""]
        UserDefaults.standard.set(0, forKey: "index")
        AppDelegate.index = 0
    }
}
