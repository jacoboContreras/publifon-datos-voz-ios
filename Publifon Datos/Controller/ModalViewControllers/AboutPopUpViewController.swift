//
//  AboutPopUpViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 9/27/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//
import UIKit


class AboutPopUpViewController: UIViewController {
    
var couponObject: CouponModel?
var couponType = ""
var couponSelectedChoice = ""
var modalText = ""
var fromMenuOnlyVoz = Bool()
var backgroungColor = UIColor()
var accentuationColor = UIColor()
var textColor = UIColor()
var totalCouponhrs: Int = 0
      var vozCouponObject: VozCouponModel?

    
// Linked outlets
@IBOutlet weak var containerUIView: UIView!
@IBOutlet weak var titleLabel: UILabel!
@IBOutlet weak var appImageView: UIImageView!
@IBOutlet weak var numberVersionAppLabel: UILabel!
@IBOutlet weak var acceptButton: UIButton!
    
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get JSON
        couponObject = Utils.getCouponObject()
        
        // Getting Voz coupon object
        vozCouponObject = Utils.getVozCouponObject()
        
        containerUIView.isHidden = false
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Setting up rounded corner to container view
        containerUIView.layer.cornerRadius = 10
        
        // vars to customize popUp
        var typeOfCoupon = ""
        var selectedChoice = ""
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            selectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        if typeOfCoupon == Utils.CouponType.voz.rawValue || selectedChoice == Utils.CouponSelectedChoice.voz.rawValue {
            totalCouponhrs = Int(vozCouponObject!.hoursValid)!
        }
        
        
        backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        
        let numberVersionApp = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        
        
        //customize popUp
        containerUIView.backgroundColor = backgroungColor
        containerUIView.layer.cornerRadius = 10
        
        //customize title
        titleLabel.textColor = textColor
        
        //customize view of aboutApp
        numberVersionAppLabel.textColor = textColor
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.borderColor = textColor
        
        if Utils.WEBSERVICE_URL.contains("ec2") {
            numberVersionAppLabel.text = "Ver. \(numberVersionApp!).PP"
        } else if Utils.WEBSERVICE_URL.contains("sandbox")  {
            numberVersionAppLabel.text = "Ver. \(numberVersionApp!).S"
        }else {
            numberVersionAppLabel.text = "Ver. \(numberVersionApp!).P"
        }
        appImageView.image = UIImage(named: "publifon_datos")//UIApplication.shared.icon
        appImageView.layer.cornerRadius = 30
                
        // If coupon is voz type then set default colors
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // Customize to only Voz mode
        customizeOnlyVozMode()
        
        // If coupon is hybrid choice
        if typeOfCoupon == Utils.CouponType.choice.rawValue {
            customizeVozHybrid()
        }
        
    }
    
    // Accept button logic
    @IBAction func acceptButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    
    // Customizes
    func customizeOnlyVozMode() {
        
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
            containerUIView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            titleLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            numberVersionAppLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            acceptButton.backgroundColor = .none
            acceptButton.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
            acceptButton.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            
        }
    }
    
    func customizeVozHybrid() {
        
        // Getting campaign colors
        let backgroundColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Setting views custumized colors
        containerUIView.backgroundColor = backgroundColor
        titleLabel.textColor = textColor
        //set text color to labels array
        
        numberVersionAppLabel.textColor = textColor
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.borderColor = textColor
    
    }

    
}

extension UIApplication {
    var icon: UIImage? {
        guard let iconsDictionary = Bundle.main.infoDictionary?["CFBundleIcons"] as? NSDictionary,
            let primaryIconsDictionary = iconsDictionary["CFBundlePrimaryIcon"] as? NSDictionary,
            let iconFiles = primaryIconsDictionary["CFBundleIconFiles"] as? NSArray,
            // First will be smallest for the device class, last will be the largest for device class
            let lastIcon = iconFiles.lastObject as? String,
            let icon = UIImage(named: lastIcon) else {
                return nil
        }
        
        return icon
    }
}
