//
//  ViewController.swift
//  DropDownTable2.0
//
//  Created by Jacobo on 8/20/19.
//  Copyright © 2019 Jacobo. All rights reserved.
//

import UIKit

//struct to fill cells in tableView
struct cellData {
    var question: String = String()
    var answer : [String] = [String]()
    var opened : Bool = Bool()
}

// var's to customize table
var backgroungColorTable: UIColor = UIColor()
var accentuationColorTable: UIColor = UIColor()
var textColorTable: UIColor = UIColor()
var tableViewData  = [cellData]()

var couponObject: CouponModel?
var couponType = ""
var couponSelectedChoice = ""
var modalText = ""
var typeOfCoupon = ""
   


class TableViewViewController: UITableViewController {
    
    
    // cupon var's
    var couponObject: CouponModel?
    var couponType = ""
    var couponSelectedChoice = ""
    var modalText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Fill data for the cell's
        tableViewData = [cellData(question: "1. ¿Cómo puedo hacer una llamada gratuita?", answer: ["Debes descargar la APP " + Utils.COMPANY_NAME + " desde la librería que te corresponda, ya sea GOOGLE PLAY o APP STORE. Al abrir la APP deberás ingresar el código de tu promoción y posteriormente tu número celular. Al validar tu cupón, serás beneficiado con llamadas ilimitadas gratuitas y en tu pantalla principal tendrás dos opciones de marcado “LLAMAR” y “CONTACTOS”. Si quieres marcar desde tus contactos, solo debes presionar el nombre de la persona a la que le quieres marcar, si eliges marcar manualmente, en el teclado debes marcar a 10 dígitos si el número es celular o fijo NACIONAL, pero si tu llamada es al extranjero, deberás marcar 00 + clave de país + clave de ciudad + número. Al marcar, cualquiera que sea la forma que elijas, recibirás una llamada, al contestarla se enlazará a la persona que le marcaste. *Asegúrate de aceptar que la aplicación tenga acceso a tu libreta telefónica."], opened: false),
                         cellData(question: "2. ¿Dónde encuentro las instrucciones de marcado?", answer: ["Dentro de la pantalla principal deberás ir al menú de opciones (parte superior derecha de la pantalla), encontrarás una opción llamada “Tutorial” y en ella podrás encontrar, entre otra información, la forma de marcar."], opened: false),
                         cellData(question: "3. ¿Desde qué celular puedo realizar las llamadas gratuitas?", answer: ["Desde cualquier SMARTPHONE con sistema operativo IOS o ANDROID, con línea celular mexicana activa, con Telcel, Movistar o AT&T, sin importar si el plan es de prepago o de post pago."], opened: false),
                         cellData(question: "4. ¿Debo tener saldo en mi celular para realizar una llamada?", answer: ["No, no es necesario contar con SALDO, WIFI o DATOS MÓVILES, solo necesita WIFI, DATOS o saldo suficiente para descargar la app, y podrás hacer uso del beneficio siempre y cuando tu código esté vigente."], opened: false),
                         cellData(question: "5. ¿A dónde puedo llamar con este beneficio?", answer: ["A celulares y fijos en México, EUA, Canadá, Alemania, Corea Del Sur, Francia, Italia, Puerto Rico, Argentina, Croacia, Grecia, Japón, R. Checa, Austria, Dinamarca, Holanda, Latvia, Rumania, Brasil, Eslovaquia, Hong Kong, Luxemburgo, Rusia, Canadá, Eslovenia, Hungría, Malta, Singapur, Chile, España, India, N. Zelanda, Suecia, China, Estonia, Inglaterra, Perú, Suiza, Cd. Vaticano, Irlanda, Polonia, Tailandia, Colombia, Finlandia, Israel, Portugal, Venezuela."], opened: false),
                         cellData(question: "6. ¿Por qué se corta la llamada a los 10 minutos?", answer: ["El beneficio de llamadas ilimitadas está programado para funcionar de esta forma, pero puedes volver a llamar cuantas veces quieras siempre y cuando tu código este vigente."], opened: false),
                         cellData(question: "7. ¿Cuánto tiempo tengo para hacer llamadas ilimitadas?", answer: ["Depende de las condiciones de tu promoción, las cuáles las podrás consultar en los Términos y Condiciones que se encuentran en el menú de la APP."], opened: false),
                         cellData(question: "8. ¿Qué puedo hacer para seguir hablando si mi código ya expiró?", answer: ["Deberás ingresar otro código premiado de alguna promoción vigente."], opened: false),
                         cellData(question: "9. ¿Puedo hacer llamadas fuera de México?", answer: ["No, el servicio solamente se puede usar dentro de la República Mexicana."], opened: false),
                         cellData(question: "10. ¿Puedo acumular los códigos de llamadas ilimitadas?", answer: ["No, el código deberá expirar o suspenderse de manera manual para poder ingresar un nuevo código."], opened: false),
                         cellData(question: "11. ¿Qué vigencia tiene esta promoción?", answer: ["Consultar en Términos y Condiciones."], opened: false),
                         cellData(question: "1. ¿Cómo puedo tener whatsapp gratis?", answer: ["Debes descargar la APP " + Utils.COMPANY_NAME + " desde la librería que te corresponda, ya sea GOOGLE PLAY o APP STORE. Al abrir la APP deberás ingresar el código de tu promoción y posteriormente tu número celular. Al validar tu cupón, si tienes el beneficio de WHATSAPP, se abrirá una pantalla personalizada donde se muestra tu beneficio de WhatsApp. Puedes ingresar a whatsapp desde la APP " + Utils.COMPANY_NAME + " o directamente desde la APP WHATSAPP, solo debes asegurarte de que el switch que se encuentra en la parte superior de la pantalla mencionada anteriormente, se encuentre activo. Este switch se desactiva cada 10 minutos, así que deberás asegurarte que esté activo cada vez que quieras hacer uso de tu servicio. Nota: Cuando el usuario tenga iPhone, además de seguir los pasos anteriores, deberá descargar la aplicación " + Utils.COMPANY_NAME + " Connect, y activar desde aquí el servicio de datos, deberás mantener ambas aplicaciones descargadas."], opened: false),
                         cellData(question: "2. ¿Puedo hacer llamadas por whatsapp?", answer: ["Los servicios incluidos dependen de cada promoción, esto lo podrás checar en los Términos y Condiciones."], opened: false),
                         cellData(question: "3. ¿Puedo mandar videos?", answer: ["Los servicios incluidos dependen de cada promoción, esto lo podrás checar en los Términos y Condiciones."], opened: false),
                         cellData(question: "4. ¿Qué puedo hacer para seguir usando whatsapp gratis si mi código ya expiró?", answer: ["Deberás ingresar un nuevo código, de alguna promoción vigente."], opened: false),
                         cellData(question: "5. ¿Dónde encuentro las instrucciones del servicio?", answer: ["Dentro de la pantalla principal deberás ir al menú (parte superior derecha de la pantalla), encontrarás una opción llamada “Tutorial” y en ella podrás ver la forma de utilizar el servicio."], opened: false),
                         cellData(question: "6. ¿Debo tener saldo en mi celular para usar WhatsApp?", answer: ["No, no es necesario contar con SALDO, WIFI o DATOS MÓVILES, solo necesita WIFI, DATOS o SALDO SUFICIENTE para descargar la app, siempre y cuando tu código esté vigente."], opened: false),
                         cellData(question: "7. ¿Por qué debo tener activado el botón/servicio de datos de mi celular?", answer: ["Deberá estar activado para que puedan ser transmitidos los datos a tu celular, y de esta forma puedas hacer uso del servicio, es importante señalar que esto no consume datos de tu saldo, asegúrate siempre que quieras usar el servicio, de activar el switch que viene en la parte superior de la pantalla principal, de esta forma el beneficio será totalmente gratis."], opened: false),
                         cellData(question: "8. ¿Puedo utilizar el servicio de WhatsApp fuera de México?", answer: ["No, el servicio solamente se puede usar dentro de la República Mexicana."], opened: false),
                         cellData(question: "9. ¿Al ingresar un código de WhatsApp, tengo datos para navegar en otras aplicaciones o internet?", answer: ["No, el código únicamente es para utilizar la aplicación de WhatsApp."], opened: false)]
        
        //get JSON
        couponObject = Utils.getCouponObject()
        
        typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // vars to customize popUp
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        backgroungColorTable = backgroungColor

        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        accentuationColorTable = accentuationColor

        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        textColorTable = textColor
        
        //change background to containerView
        view.backgroundColor = backgroungColorTable
        
        
        //delete extra cell's in tableView
        tableView.tableFooterView = UIView()
        
        // Customize to only Voz mode
        customizeOnlyVozMode()
        
        if typeOfCoupon == Utils.CouponType.choice.rawValue {
            customizeVozHybrid()
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        //number of questions
        return tableViewData.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true{
            return tableViewData[section].answer.count + 1
        }else{
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //assign question
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellQuestions") else {return UITableViewCell()}
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.textLabel?.text = tableViewData[indexPath.section].question
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
            cell.textLabel?.numberOfLines = 0
            
            cell.frame.size.height = (cell.textLabel?.frame.height)!
            cell.backgroundColor = backgroungColorTable
            cell.textLabel?.textColor = textColorTable
            cell.imageView?.image = UIImage(named: "baseline_keyboard_arrow_down_black_18dp")
            cell.imageView?.image = cell.imageView?.image?.withRenderingMode(.alwaysTemplate)
            cell.imageView!.tintColor = textColorTable
            
            return cell
            
        }else /*assign answer*/ {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "cellQuestions") else {return UITableViewCell()}
            cell.textLabel?.text =  (tableViewData[indexPath.section].answer[indexPath.row - 1])
            cell.backgroundColor = backgroungColorTable
            cell.textLabel?.textColor = textColorTable
            //cell.imageView?.image = nil
            //set only padding to answer
            cell.imageView?.image = UIImage(named: "baseline_keyboard_arrow_down_black_18dp")
            cell.imageView?.isHidden = true
            cell.textLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
            cell.textLabel?.numberOfLines = 0
            
            cell.frame.size.height = (cell.textLabel?.frame.height)!
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //if the cell is a question
        if indexPath.row == 0 {
            
            if tableViewData[indexPath.section].opened == true{
                
                tableViewData[indexPath.section].opened = false
                tableView.reloadSectionIndexTitles()
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
                
                
            }else /*the cell is an answer*/{
                
                tableViewData[indexPath.section].opened = true
                let sections =  IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
            
            
            // change indicator open/close
            if tableViewData[indexPath.section].opened == false{
                tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "baseline_keyboard_arrow_down_black_18dp")

                tableView.cellForRow(at: indexPath)?.imageView?.image = tableView.cellForRow(at: indexPath)?.imageView?.image?.withRenderingMode(.alwaysTemplate)
                tableView.cellForRow(at: indexPath)?.imageView!.tintColor = textColorTable
                
            }else{
                tableView.cellForRow(at: indexPath)?.imageView?.image = UIImage(named: "baseline_keyboard_arrow_up_black_18dp")
                tableView.cellForRow(at: indexPath)?.imageView?.image = tableView.cellForRow(at: indexPath)?.imageView?.image?.withRenderingMode(.alwaysTemplate)
                tableView.cellForRow(at: indexPath)?.imageView!.tintColor = textColorTable
            }
        }
    }
    
    func customizeOnlyVozMode(){
        //customize onlyVoz
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue {
          
            
            backgroungColorTable = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            accentuationColorTable = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
            textColorTable = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            tableView.backgroundColor = backgroungColorTable
            self.tableView.backgroundColor = backgroungColorTable
            self.view.backgroundColor = backgroungColorTable
            print("colors", backgroungColorTable.debugDescription)
        }
    }
    
    func customizeVozHybrid() {
        
        // Getting campaign colors
        backgroungColorTable = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        accentuationColorTable = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        textColorTable = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Setting views custumized colors
        tableView.backgroundColor = backgroungColorTable
        self.view.backgroundColor = backgroungColorTable
        
    }
    
}

