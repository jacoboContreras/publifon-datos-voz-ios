//
//  TurisfonConnectViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 7/4/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Lottie
class TurisfonConnectViewController: UIViewController {
    
    // Class variables
    var couponObject: CouponModel?
    var couponType = ""
    var couponSelectedChoice = ""
    var modalText = ""
    
    // Linked outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoTextLabel: UILabel!
    @IBOutlet weak var acceptButtonOutlet: UIButton!

    @IBOutlet weak var viewsContainerUIView: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Getting coupon from data base
        couponObject = Utils.getCouponObject()
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Getting campaign colors
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Customizing views
        viewsContainerUIView.backgroundColor = backgroungColor
        viewsContainerUIView.layer.cornerRadius = 10
        titleLabel.textColor = textColor
        infoTextLabel.textColor = textColor
        infoTextLabel.text = modalText
        acceptButtonOutlet.backgroundColor = .none
        acceptButtonOutlet.setTitleColor(textColor, for: .normal)
        acceptButtonOutlet.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButtonOutlet.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButtonOutlet.borderColor = textColor
        
        // If coupon is voz type then set default colors
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
            viewsContainerUIView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            titleLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            infoTextLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            acceptButtonOutlet.backgroundColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            acceptButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            acceptButtonOutlet.backgroundColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            acceptButtonOutlet.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        }
    }
    
 
    @IBAction func acceptButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
