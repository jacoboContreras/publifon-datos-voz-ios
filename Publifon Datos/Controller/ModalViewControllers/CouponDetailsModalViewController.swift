//
//  CouponDetailsModalViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/9/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class CouponDetailsModalViewController: UIViewController {
        
    // Linked outlets
    //@IBOutlet weak var redentionTitleLabel: UILabel!
    //@IBOutlet weak var remainingTimeTitleLabel: UILabel!
    //@IBOutlet weak var expirationDateTitleLabel: UILabel!
    //@IBOutlet weak var yourCouponIsLabel: UILabel!
    @IBOutlet weak var viewsContainerUIView: UIView!
    @IBOutlet weak var numberCouponLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var remainingTimeLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var redentionAndExpirationDateLabel: UILabel!
    
    
    // Class variables
    var cellIndex: Int = 0
    var expYear = 0
    var expMonth = 0
    var expDay = 0
    var expHour = 0
    var expMinute = 0
    var expSecond = 0
    var totalCouponhrs: Int = 0
    var couponType = ""
    var couponSelectedChoice = ""
    var coupon: String = ""
    var datosJSON: JSON?
    var groupType = ""
    
    var couponObject: CouponModel?
    var vozCouponObject: VozCouponModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Getting Datos coupon object
        couponObject = Utils.getCouponObject()
        
        // Getting Voz coupon object
        vozCouponObject = Utils.getVozCouponObject()
        
        // If coupon type is combo then add Voz elements
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        if couponType == Utils.CouponType.combo.rawValue { addVozItem()}
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Getting campaign colors
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Setting views custumized colors
        viewsContainerUIView.backgroundColor = backgroungColor
        viewsContainerUIView.layer.cornerRadius = 10
        titleLabel.textColor = textColor
        titleLabel.text = "Servicio patrocinado por"
        //yourCouponIsLabel.textColor = accentuationColor
        numberCouponLabel.textColor = textColor
        //redentionTitleLabel.textColor = textColor
        redentionAndExpirationDateLabel.textColor = textColor
        let expireDate = Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(couponObject: self.couponObject!,serviceHrs: totalCouponhrs))
        print("Expiration date:", expireDate)
        let redemtionDate = Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.REDEMPTION_DATE)!)
        redentionAndExpirationDateLabel.text = "Redención: \(Utils.getReadableDate(date: redemtionDate))\nExpiración: \(Utils.getReadableDate(date: Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.COUPON_EXPIRATION_DATE)!)))"
        numberCouponLabel.text = "Cupón: \(UserDefaults.standard.string(forKey: Utils.VOZ_COUPON_CODE) ?? "")" //couponObject!.vpnData.username
        numberCouponLabel.adjustsFontSizeToFitWidth = true
        
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.borderColor = textColor
        
        
        // Setting date parameters for expiration date
        totalCouponhrs = Utils.getTotalCouponHrs(couponObject: couponObject!)
        var typeOfCoupon = ""
        var selectedChoice = ""
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            selectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        if typeOfCoupon == Utils.CouponType.voz.rawValue || selectedChoice == Utils.CouponSelectedChoice.voz.rawValue {
            totalCouponhrs = Int(vozCouponObject!.hoursValid)!
        }
        
        //Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(serviceHrs: totalCouponhrs))
        let expirationDate = Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: totalCouponhrs)
        let splitString = expirationDate.split(separator: " ")
        let splitDate = splitString[0].split(separator: "-")
        let splitTime = splitString[1].split(separator: ":")
        print("Time expirationDate 1:", expirationDate)
        print("Time expirationDate 2:", Utils.getReadableDate(date: Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.COUPON_EXPIRATION_DATE)!)))
                
        expYear = Int(splitDate[0])!
        expMonth = Int(splitDate[1])!
        expDay = Int(splitDate[2])!
        expHour = Int(splitTime[0])!
        expMinute = Int(splitTime[1])!
        expSecond = Int(splitTime[2])!
        
        let expDate: Date = {
            var future = DateComponents (
                year: expYear,
                month: expMonth,
                day: expDay,
                hour: expHour,
                minute: expMinute,
                second: expSecond
            )
            return Calendar.current.date(from: future)!
        }()
        
        var countdown: DateComponents {
            return Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: expDate)
        }
        
        let countdowns = countdown
        let days = countdowns.day!
        let hours = countdowns.hour!
        let minutes = countdowns.minute!
        let seconds = countdowns.second!
        let result = String(format: "%02dd %02dh %02dm %02ds", days, hours, minutes, seconds)
        
        
        //remainingTimeTitleLabel.textColor = textColor
        remainingTimeLabel.textColor = textColor
        remainingTimeLabel.text = "Tiempo restante: \(result)"
        
        // Set company logo
        let logoURL = URL(string: couponObject!.company.logoURL)
        companyLogoImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
        
        // If coupon is voz type then set default colors
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // Customize to only Voz mode
        customizeOnlyVozMode()
        
        // If coupon is hybrid choice
        if typeOfCoupon == Utils.CouponType.choice.rawValue {
            customizeVozHybrid()
        }
        
        customizedVozChoice()
    }
    
  
    
    // Accept button action
    @IBAction func acceptButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Add voz element to array of applications
    func addVozItem() {
        let vozCouponObject = Utils.getVozCouponObject()
        let hoursValid = Int(vozCouponObject.hoursValid)
        couponObject?.applications.insert(Applications(androidPackageNames: [""]
            , iOSPackageNames: [""], id: 0, hours: hoursValid!, maxMegabytes: 0,
              name: "Lamadas Gratis", logoURL: ""), at: 0)
    }
    
    func customizeOnlyVozMode() {
        if couponType == Utils.CouponType.voz.rawValue {
            //yourCouponIsLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            viewsContainerUIView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            titleLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            //redentionTitleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            numberCouponLabel.text = "Cupón: \(UserDefaults.standard.string(forKey: Utils.VOZ_COUPON_CODE) ?? "")"
            numberCouponLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            
            
            redentionAndExpirationDateLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            //remainingTimeTitleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            remainingTimeLabel.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            
            // Set company logo
            self.companyLogoImageView.image = UIImage(named: "logo_publifon")
                       
            //custom accept button
            self.acceptButton.backgroundColor = .none
            self.acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            self.acceptButton.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            self.acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            self.acceptButton.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
            
        }
    }
    
    func customizedVozChoice(){
        if couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
            // Set company logo
            let logoURL = URL(string: couponObject!.company.logoURL)
            companyLogoImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: "logo_publifon"))
        }
    }
    
    func customizeVozHybrid() {
        
        // Setting views custumized colors
        // Getting campaign colors
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        viewsContainerUIView.backgroundColor = backgroungColor
        titleLabel.textColor = textColor
        //redentionTitleLabel.textColor = textColor
        
        redentionAndExpirationDateLabel.textColor = textColor
        
        //remainingTimeTitleLabel.textColor = textColor
        remainingTimeLabel.textColor = textColor
        
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.borderColor = textColor
        numberCouponLabel.textColor = textColor
        
    }
    
    
    func configureRequest(coupon: String, url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url+coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.GET
        //request.timeoutInterval = 5
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        return request
    }
}
