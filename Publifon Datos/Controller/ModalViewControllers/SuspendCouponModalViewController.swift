//
//  SuspendCouponModalViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/11/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import FirebaseInstanceID
import FirebaseCore
import Firebase

class SuspendCouponModalViewController: UIViewController {
    
    // Linked outlets
    @IBOutlet weak var viewsContainerUIView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var contentLabel: [UILabel]!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    
    
    // Class constant variables
    let DEVICE_TOKEN = "device_token"
    let ACCEPT = "Accept"
    
    //Class variables
    var couponObject: CouponModel?
    var deviceToken = ""
    var couponType = ""
    var couponSelectedChoice = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Getting coupon from data base
        couponObject = Utils.getCouponObject()
        
        // Getting device token
        deviceToken = getFCMToken()

        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Getting campaign colors
        let backgroundColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Customizing views
        viewsContainerUIView.backgroundColor = backgroundColor
        viewsContainerUIView.layer.cornerRadius = 10
        titleLabel.textColor = textColor
        
        //set text color to labels array
        for iCont in 0..<contentLabel.count{
            contentLabel[iCont].textColor = textColor
        }
        
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        cancelButton.backgroundColor = .none
        cancelButton.setTitleColor(textColor, for: .normal)
        
        acceptButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderColor = textColor
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        cancelButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        cancelButton.borderColor = textColor
        cancelButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        
        
        // If coupon is voz type then set default colors
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // Customize to only Voz mode
        customizeOnlyVozMode()
        
        // If coupon is hybrid choice
        if couponType == Utils.CouponType.choice.rawValue {
            customizeVozHybrid()
        }
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }
    
    // Accept button action logic, triggers suspend coupon method
    @IBAction func acceptButtonAction(_ sender: Any) {
        if isVPNRunning() {
            toastMessage("Desactiva el switch para suspender el servicio")
            return
        }
        // Suspending coupon
        self.suspension(pendingOfSuspension: false)
    }
    
    // Cancel Button action logic, closes current view controller
    @IBAction func cancelButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // Clears coupon data stored in memory 
    func clearCoupon(pendingOfSuspension: Bool) {
        resetCouponMemoryValues(pendingOfSuspension: pendingOfSuspension)
        resetVozCouponMemoryValues(pendingOfSuspension: pendingOfSuspension)
    }
    
    // Reset voz coupon memory state
    func resetVozCouponMemoryValues(pendingOfSuspension: Bool) {
        UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
        UserDefaults.standard.set(pendingOfSuspension, forKey: Utils.PENDING_SUSPENSION)
        UserDefaults.standard.set(pendingOfSuspension, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.PHONE_NUMBER)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_COUPON_CODE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_HOME_IMAGE_NAME)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.IMAGE_DAY_CHECK)
    }
    
    
    // Reset datos coupon memory state
    func resetCouponMemoryValues(pendingOfSuspension: Bool) {
        UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
        UserDefaults.standard.set(pendingOfSuspension, forKey: Utils.PENDING_SUSPENSION)
        UserDefaults.standard.set(pendingOfSuspension, forKey: Utils.COUPON_ACTIVE)
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.REDEMPTION_DATE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.COUPON_EXPIRATION_DATE)
        UserDefaults.standard.set(["", "", ""], forKey: "sliderArray")
        AppDelegate.sliderArray = ["", "", ""]
        UserDefaults.standard.set(0, forKey: "index")
        AppDelegate.index = 0
    }
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        //send to insert code vc
        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let insertCodeViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.INSERT_CODE_VIEW_CONTROLLER) as! InsertCodeViewController
        appDelegate.window?.rootViewController = insertCodeViewController
        appDelegate.window?.makeKeyAndVisible()*/
        
        //send to menu redemtion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let MenuNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = MenuNavigationController.topViewController as! SeleccionDeRedencionViewController
        appDelegate.window?.rootViewController = MenuNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String) -> URLRequest{
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = self.deviceToken
        
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = Utils.SUSPEND_COUPON_URL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let suspendURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: suspendURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 5 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Logic for suspending a coupon
    func suspension(pendingOfSuspension: Bool) {
        // Deleting coupon state
        self.clearCoupon(pendingOfSuspension: pendingOfSuspension)
        // Stops expiration checker timer
//        MainViewController.timer?.invalidate()
//        HomeViewController.timer?.invalidate()
        stopTimers()
        // Sends user to insert view controller
        self.sendUserToInsertViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Checks if VPN Running
    func isVPNRunning() -> Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") {
                    return true
                }
            }
        }
        return false
    }

    // Stopping expiration timers
    func stopTimers() {
        if MainViewController.timer != nil {
            MainViewController.timer?.invalidate()
            MainViewController.timer = nil
        }
        if HomeViewController.timer != nil {
            HomeViewController.timer?.invalidate()
            HomeViewController.timer = nil
        }
    }
    
    // Customizes
    func customizeOnlyVozMode() {
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
            viewsContainerUIView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            titleLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            //set text color to labels array
            for iCont in 0..<contentLabel.count{
                contentLabel[iCont].textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            }
            
            acceptButton.backgroundColor = .none
            acceptButton.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
            acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            acceptButton.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            cancelButton.backgroundColor = .none
            cancelButton.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
            cancelButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            cancelButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            cancelButton.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
        }
    }
    
    func customizeVozHybrid() {
        
        // Getting campaign colors
        let backgroundColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Setting views custumized colors
        viewsContainerUIView.backgroundColor = backgroundColor
        titleLabel.textColor = textColor
        //set text color to labels array
        for iCont in 0..<contentLabel.count{
            contentLabel[iCont].textColor = textColor
        }
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.borderColor = textColor
        cancelButton.backgroundColor = .none
        cancelButton.setTitleColor(textColor, for: .normal)
        cancelButton.borderColor = textColor
    }
}
