//
//  PopUpUserPermitsViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 2/11/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit

//Struct to userpermits required Publifon

struct UserPermit {
    
    var description : String
    var icon : UIImage
}

class PopUpUserPermitsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    //Var
    
    var userPermits : [UserPermit] =  [
        
        
        UserPermit(description: "Turisfon\nNecesita de los siguientes permisos:", icon: UIImage(named: "publifon_datos")!),
        UserPermit(description: "Ubicación aproximada ()basada en red) para hacer redencion de cupones", icon: UIImage(named: "ubicationIcon")!),
        UserPermit(description: "Llamar directamente a números de teléfono con un click", icon: UIImage(named: "contactsIcon")!)]
      
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        //degree alpha  view
        
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
//        tableView.layer.borderWidth = 1
//        tableView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        
        
        //set background color
        //tableView.backgroundColor = StandarColorAndForms.backgroundColorPopUp
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userPermits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "permitCell", for: indexPath) as! PermitTableViewCell
        
        let permit = userPermits[indexPath.row]
        
        cell.permitTextLabel.text = permit.description
        cell.iconPermitImage.image = permit.icon
        
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        guard section == 0 else { return nil }

        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 0))
        let doneButton = UIButton(frame: CGRect(x: 0, y: 0, width: footerView.frame.size.width, height: footerView.frame.size.height))
        
        
        // here is what you should add:
        doneButton.center = footerView.center

        doneButton.setTitle("ACEPTAR", for: .normal)
        
        
        
        
        //doneButton.shadow = true
       // doneButton.addTarget(self, action: #selector(hello(sender:)), for: .touchUpInside)
        footerView.addSubview(doneButton)

        return footerView
    }
    
    
}
