//
//  DisableWifiPopupViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 2/18/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import Lottie

class DisableWifiPopupViewController: UIViewController {
    
    //Outlets
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var wifiImageView: UIImageView!
    @IBOutlet weak var acceptButton: UIButton!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Custom view
        
        popupView.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        
        acceptButton.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.borderColor = .white
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        let animationGifView = AnimationView(name: "Wifi")
        animationGifView.frame = CGRect(x: 0, y: 0, width: self.wifiImageView.frame.width, height: self.wifiImageView.frame.height)
        animationGifView.contentMode = .scaleToFill
        animationGifView.play()
        animationGifView.loopMode = .loop
        animationGifView.sizeToFit()
        wifiImageView.insertSubview(animationGifView, at: 0)
    }
    
    
    
    
    @IBAction func acceptDissableWifi(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}
