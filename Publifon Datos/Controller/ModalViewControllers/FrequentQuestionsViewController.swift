//
//  FrequentQuestionsViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 8/15/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON

class FrequentQuestionsViewController: UIViewController {
    
    
    // Class variables
    var couponObject: CouponModel?
    var couponType = ""
    var couponSelectedChoice = ""
    var modalText = ""
    var typeOfCoupon = ""
    
    
    // Linked outlets
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerFAQsView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        containerUIView.isHidden = false
        
      //get JSON
        couponObject = Utils.getCouponObject()
                
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Setting up rounded corner to container view
        containerUIView.layer.cornerRadius = 10
        
        typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        
        // vars to customize popUp
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        //let textTermsCondition = couponObject?.campaign.termsCondition
        
        //customize popUp
        containerUIView.backgroundColor = backgroungColor
        containerUIView.layer.cornerRadius = 10
        
        //customize title
        titleLabel.textColor = textColor
        
        //customize text of termsConditions
        
        //customize button accept
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderColor = textColor
        
        
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        
        // Customize to only Voz mode
        customizeOnlyVozMode()
        
        // If coupon is hybrid choice
        if typeOfCoupon == Utils.CouponType.choice.rawValue {
            customizeVozHybrid()
        }
        
        
        
    }
    
    // Accept button logic
    @IBAction func acceptButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func customizeOnlyVozMode() {
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue {
            
            self.containerUIView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.titleLabel.textColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            
            //custom accept button
            self.acceptButton.backgroundColor = .none
            self.acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
            self.acceptButton.borderColor = #colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1)
            self.acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
            self.acceptButton.setTitleColor(#colorLiteral(red: 0.06376030296, green: 0.196352154, blue: 0.4713711143, alpha: 1), for: .normal)
            self.containerFAQsView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
    }
    
    func customizeVozHybrid() {
        
        // Getting campaign colors
        let backgroundColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        
        // Setting views custumized colors
        containerUIView.backgroundColor = backgroundColor
        titleLabel.textColor = textColor
        
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.borderColor = textColor
        
    }
}



