//
//  VPNPopupViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 2/27/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import Lottie

class VPNPopupViewController: UIViewController {
    
    //Outltes
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var subtittleLabel: UILabel!
    @IBOutlet weak var vpnView: UIView!
    @IBOutlet weak var acceptButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Custom Outlet´s view
        tittleLabel.text = "Aviso"
        subtittleLabel.text = "Debes descargar la app de Publifon Connect desde App Store para hacer uso de tu servicio de datos"
        popupView.layer.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //insert Lottie gif
        let animation = AnimationView(name: "vpn")
        vpnView.contentMode = .scaleToFill
        self.vpnView.insertSubview(animation, at: 0)
        animation.frame = self.vpnView.bounds
        animation.loopMode = .loop
        animation.play()
        
    }
    
    
    
    @IBAction func acceptVPNService(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
