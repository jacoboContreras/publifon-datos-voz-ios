//
//  ModalInfoViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/9/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class ModalInfoViewController: UIViewController {

    // Linked outlets
    @IBOutlet weak var viewsContainerUIView: UIView!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var timeRemainingLabel: UILabel!
    @IBOutlet weak var redemptionLabel: UILabel!
    @IBOutlet weak var expirationLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    
    // Class variables
    var cellIndex: Int = 0
    var expYear = 0
    var expMonth = 0
    var expDay = 0
    var expHour = 0
    var expMinute = 0
    var expSecond = 0
    var couponObject: CouponModel?
    var couponType = ""
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponObject = Utils.getCouponObject()    
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        
        // Getting campaign colors
        let backgroungColor = Utils.hexToColor(hexString: couponObject!.campaign.backgroungColor)
        let accentuationColor = Utils.hexToColor(hexString: couponObject!.campaign.accentuationColor)
        let textColor = Utils.hexToColor(hexString: couponObject!.campaign.textColor)
        // If coupon type is combo then add Voz elements
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        let redemption = Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.REDEMPTION_DATE)!)
        let redemptionDate =  Utils.getReadableDate(date: redemption)
        
        
        if couponType == Utils.CouponType.combo.rawValue { addVozItem()}
        
        let appName = couponObject!.applications[cellIndex].name
        let expiration = Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(couponObject: self.couponObject!, serviceHrs: couponObject!.applications[cellIndex].hours))
        let expirationDate = Utils.getReadableDate(date: expiration)
        
        // Setting date parameters for expiration date
        let expDate: Date = {
            var future = DateComponents (
                year: expYear,
                month: expMonth,
                day: expDay,
                hour: expHour,
                minute: expMinute,
                second: expSecond
            )
            return Calendar.current.date(from: future)!
        }()
        // Time components
        var countdown: DateComponents {
            return Calendar.current.dateComponents([.day, .hour, .minute, .second], from: Date(), to: expDate)
        }
        
        // Setting values for displaying them
        let countdowns = countdown
        let days = countdowns.day!
        let hours = countdowns.hour!
        let minutes = countdowns.minute!
        let seconds = countdowns.second!
        let result = String(format: "%02dd %02dh %02dm %02ds", days, hours, minutes, seconds)
        
        // Setting views customized colors
        viewsContainerUIView.backgroundColor = backgroungColor
        viewsContainerUIView.layer.cornerRadius = 10
        
        contentLabel.text = "Tu servicio de \(appName) patrocinado por"
        timeRemainingLabel.text = "Tiempo restante: \(result)"
        redemptionLabel.text = "Redención: \(redemptionDate)"
        expirationLabel.text = "Expiración: \(expirationDate)"
        
        
        contentLabel.textColor = textColor
        timeRemainingLabel.textColor = textColor
        timeRemainingLabel.textColor = textColor
        redemptionLabel.textColor = textColor
        expirationLabel.textColor = textColor
        acceptButton.backgroundColor = .none
        acceptButton.setTitleColor(textColor, for: .normal)
        acceptButton.borderColor = textColor
        acceptButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        acceptButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        
        // Set company logo
        if let logoURL = URL(string: couponObject!.company.logoURL){
            companyLogoImageView.sd_setImage(with: logoURL, placeholderImage: UIImage(named: ""))
        }else{
            self.companyLogoImageView.image = UIImage(named: "logo_publifon")
        }
        
        
    }
    
    // Add voz element to array of applications
    func addVozItem() {
        let vozCouponObject = Utils.getVozCouponObject()
        let hoursValid = Int(vozCouponObject.hoursValid)
        couponObject?.applications.insert(Applications(androidPackageNames: [""]
            , iOSPackageNames: [""], id: 0, hours: hoursValid!, maxMegabytes: 0,
              name: "Lamadas Gratis", logoURL: ""), at: 0)
    }
    
    // Accept button action, dismiss this view controller
    @IBAction func acceptButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
