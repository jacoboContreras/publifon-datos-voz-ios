//
//  ChoiceViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/8/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol CouponSelectedChoice {
    func onCouponSelectedChoice(selected: Utils.CouponSelectedChoice)
}

// This view controller sends back to extended view controller user selected choice
class ChoiceViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Class variables
    var delegate: CouponSelectedChoice?
    var applicationsArray = [Applications]()
    var datosJSON: JSON?
    var vozJSON: JSON?
    var vozHours = 0
    
    // Linked outlets
    @IBOutlet weak var containerUIView: UIView!
    @IBOutlet weak var applicationsTableView: UITableView!
    @IBOutlet weak var datosTitleLable: UILabel!
    @IBOutlet weak var vozTitleLabel: UILabel!
    @IBOutlet weak var vozInfoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Getting datos coupon benefits
        let benefits = datosJSON!["benefits"].array!
        for application in benefits {
            applicationsArray.append(Applications.init(id: application["application_id"].int!,
                                                  hours: application["hours"].int!,
                                                  maxMegabytes: application["mb"].int!,
                                                  name: application["application"].string!))
        }
        
//        for benefit in benefits {
//            print("Benefit group_name: ", benefit["group_name"].string!)
//            print("------------------", "------------------")
//            for application in benefit["applications"].array! {
//                applicationsArray.append(Applications.init(id: application["application_id"].int!,
//                                                                  hours: application["hours"].int!,
//                                                                  maxMegabytes: application["mb"].int!,
//                                                                  name: application["application"].string!))
//            }
//        }
        
        // Getting voz coupon hours
        self.vozHours = vozJSON!["validity"].int!
        
        // Assigning delegates
        self.applicationsTableView.delegate = self
        self.applicationsTableView.dataSource = self
        self.applicationsTableView.backgroundColor = .black
        self.applicationsTableView.register(UINib(nibName: "ChoiceTableViewCell", bundle: nil),
                               forCellReuseIdentifier: "choiceTableViewCell")
        
        // Setting view controller background
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.50)
        
        // Setting up rounded corner to container view
        containerUIView.layer.cornerRadius = 10
        
        // Setting Voz info label text
        vozInfoLabel.text = "Llamadas gratis por " + Utils.formatHrsText(hrs: self.vozHours)
        
        // Setting underlines for Titles(Datos/Voz)
//        Utils.lineDraw2(viewLi: datosTitleLable, borderColor:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
//        Utils.lineDraw2(viewLi: vozTitleLabel, borderColor:#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        // Setting underlines for Title(Voz)
        let vozStringUnderline = "Voz"
        let vozAttributeString = NSMutableAttributedString(string: vozStringUnderline)
        vozAttributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, vozAttributeString.length))
        vozTitleLabel.attributedText = vozAttributeString
        
        // Setting underlines for Title(Datos)
        let datosStringUnderline = "Datos"
        let datosAttributeString = NSMutableAttributedString(string: datosStringUnderline)
        datosAttributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, datosAttributeString.length))
        datosTitleLable.attributedText = datosAttributeString
    }
    
    

    @IBAction func datosAcceptButtonAction(_ sender: Any) {
        delegate?.onCouponSelectedChoice(selected: Utils.CouponSelectedChoice.datos)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func vozAcceptButtonAction(_ sender: Any) {
        delegate?.onCouponSelectedChoice(selected: Utils.CouponSelectedChoice.voz)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return applicationsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.applicationsTableView.dequeueReusableCell(withIdentifier: "choiceTableViewCell", for: indexPath) as! ChoiceTableViewCell
        
        cell.selectionStyle = .none
        cell.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        cell.aplicationNameLabel.text = applicationsArray[indexPath.row].name
        cell.aplicationNameLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        cell.freeTimeLabel.text = Utils.formatHrsText(hrs: applicationsArray[indexPath.row].hours)
        cell.freeTimeLabel.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return cell
    }
}
