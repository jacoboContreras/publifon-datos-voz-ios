//
//  MenuTicketViewController.swift
//  
//
//  Created by José Jacobo Contreras Trejo on 16/12/20.
//

import UIKit

class MenuTicketViewController: UIViewController {
    
    //outlets
    @IBOutlet weak var insertCodeManualButton: SupernovaButton!
    @IBOutlet weak var scanBarCodeButton: SupernovaButton!
    
    //Vars
    var scanBarCode = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func configureButton(button: SupernovaButton){
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
    }
    
    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = .black
        
        configureButton(button: insertCodeManualButton)
        configureButton(button: scanBarCodeButton)
        
    }
    
    @IBAction func goToInsertManualCode(_ sender: Any) {
        scanBarCode = false
        self.performSegue(withIdentifier: "goToRegisterBarCode", sender: self)
    }
    
    @IBAction func goToScanBarCode(_ sender: Any) {
        
        guard Utils().isCameraPermission() == true else {
            Alerts().alert(Title: "No tenemos permisos para utilizar la cámara", Message: "Dirígete a Configuración > Publifon y activa el permiso de la cámara ", viewController: self)
            return
        }
        
        scanBarCode = true
        self.performSegue(withIdentifier: "goToRegisterBarCode", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToRegisterBarCode" {
            let registerTicketCodeVC = segue.destination as! RegisterTicketCodeViewController
            
            #if targetEnvironment(simulator)
            self.scanBarCode = false
            #endif

            registerTicketCodeVC.scanBarCode = self.scanBarCode
            
        }
    }
    
    
    
}

  
