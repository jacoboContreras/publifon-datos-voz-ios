//
//  CaptureTicketViewController.swift
//  Publifon Datos
//
//  Created by José Jacobo Contreras Trejo on 16/12/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit

class CaptureTicketViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Outlets
    @IBOutlet weak var ticketImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    //Vars
    var barCode = String()
    var retakePhoto = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("el bar cpde ", barCode)
        captureTicket()
        customView()
    }
    
    func customView(){
        nextButton.cornerRadius = 10
    }
    
    func captureTicket(){
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }else{
            print("La cámara no está disponible")
        }
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            ticketImageView.image = image
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
        if retakePhoto {
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    @IBAction func retakeTicketPhoto(_ sender: Any) {
        captureTicket()
        retakePhoto = true
    }
    
    
    @IBAction func continueToInsertAmount(_ sender: Any) {
        self.performSegue(withIdentifier: "goToInsertAmount", sender: self)
    }
    
}
