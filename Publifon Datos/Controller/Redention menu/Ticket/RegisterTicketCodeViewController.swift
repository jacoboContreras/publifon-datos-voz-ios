//
//  RegisterTicketCodeViewController.swift
//  Publifon Datos
//
//  Created by José Jacobo Contreras Trejo on 16/12/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON

class RegisterTicketCodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    
    //Outlets
    @IBOutlet weak var insertBarCodeTextField: UITextField!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    //Var
    var scanBarCode = Bool()
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    let captureButton = UIButton(frame: CGRect(x: 20, y: 60, width: 60 , height: 60))
    var userSelectedScanCamera = false
    let DoneToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    var barCode = ""
    var isWifiActive = false
    let reachability = Reachability()!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
        
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }
        
        if scanBarCode {
            startToReadBarCode()
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{ try reachability.startNotifier() }
        catch { print("Could not start notifier") }
        
        DoneToolbar.barStyle = .default
        DoneToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Listo", style: .done, target: self, action: #selector(done))]
        DoneToolbar.sizeToFit()
        
        insertBarCodeTextField.inputAccessoryView = DoneToolbar
        
    }
    
    
    @objc func done() {
        //Done with number pad
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        view.endEditing(true)
        checkBarCode()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            print("Internet", "wifi")
            isWifiActive = true
        case .cellular:
            print("Internet", "cellular")
            isWifiActive = false
        case .none:
            print("Internet", "none")
            
        case .vpn:
            print("Internet", "VPN")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if (captureSession?.isRunning == false) {
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            view.addSubview(captureButton)
            captureSession.startRunning()
        }
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        
        titleLabel.text = scanBarCode ? "Tu código de ticket" : "Ingresa ticket manual"
    }

    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = .black
        
        insertBarCodeTextField.attributedPlaceholder = NSAttributedString(string: "Código de barras",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        insertBarCodeTextField.delegate = self
        insertBarCodeTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        insertBarCodeTextField.layer.borderWidth = 1
        insertBarCodeTextField.layer.cornerRadius = 10
        validateButton.cornerRadius = 10
       
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        //dismiss(animated: true)
        /*self.navigationController?.setNavigationBarHidden(false, animated: true)
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }*/
        
    }
    
    func found(code: String) {
        //previewLayer.removeFromSuperlayer()
        print("código encontrado: ", code)
        //dataQRTextView.text = code
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        
        barCode = code
        checkBarCode()
        /*if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }*/
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    
    func startToReadBarCode(){
                
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .code128]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        //Add Button
        captureButton.backgroundColor = .white
        captureButton.setImage(UIImage(named: "boton-regresar"), for: .normal)
        captureButton.layer.cornerRadius = 0.5 * captureButton.bounds.size.width
        captureButton.clipsToBounds = true
        captureButton.addTarget(self, action: #selector(self.actionCapture(sender:)), for: .touchUpInside)
        captureButton.tag = 123
        
        view.addSubview(captureButton)
        
        captureSession.startRunning()
        
    }
  
    
    @objc func actionCapture(sender: UIButton){
        previewLayer.removeFromSuperlayer()
        captureButton.removeFromSuperview()
        
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No existe view para remover")
        }
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if !userSelectedScanCamera {
            self.navigationController?.popViewController(animated: false)
        } 
    }
    
    
    @IBAction func scanWithCamera(_ sender: Any) {
        
        guard Utils().isCameraPermission() == true else {
            Alerts().alert(Title: "No tenemos permisos para utilizar la cámara", Message: "Dirígete a Configuración > Publifon y activa el permiso de la cámara ", viewController: self)
            return
        }        
        
        userSelectedScanCamera = true
        scanBarCode = true
        startToReadBarCode()
    }
    
    
    @IBAction func validateCode(_ sender: Any) {
        checkBarCode()
    }
    
    
    func checkBarCode(){
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        view.endEditing(true)
        
        guard barCode != "" || insertBarCodeTextField.text != nil && insertBarCodeTextField.text != "" else {
            Alerts().alert(Title: "Datos requeridos faltantes", Message: "Debes ingresar un código", viewController: self)
            return
        }
        
        barCode = scanBarCode ? barCode : insertBarCodeTextField.text!
        
        checkBarcoupon()
        
        //self.performSegue(withIdentifier: "goToCaptureTicket", sender: self)
    }
    
    
    func checkBarcoupon(){
        /*if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }*/
        guard isWifiEnabled(isWifiOn: isWifiActive) != true else {
            return
        }
        
        Utils().showChargerView(viewToCustom: self.view)
        Alamofire.request(configureRequest(url: Utils.CHECK_TUTORIAL)).responseData {
            dataResponse in
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                print("\n\n*************")
                print(json)
                if json["success"].boolValue {
                    print("resultado: ", json["message"].stringValue)
                    print("resultado: ", json["showTutorial"].boolValue)
                    if json["showForm"].exists() {
                        /*if json["showForm"].boolValue {
                            self.performSegue(withIdentifier: "goToFormUserVC", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "goToInsertCode", sender: self)
                        }*/
                    } else {
                        self.performSegue(withIdentifier: "goToInsertCode", sender: self)
                    }
                } else {
                    print("resultado: ", json["message"].stringValue)
                    print("resultado: ", json["showTutorial"].boolValue)
                    Utils().stopChargerView(viewToCustom: self.view)
                    
                    if self.scanBarCode {
                        Alerts().alertQR(Title: "Error", Message: json["message"].stringValue, viewController: self, captureSession: self.captureSession)
                    } else {
                        Alerts().alert(Title: "Error", Message: json["message"].stringValue, viewController: self)
                    }
                    
                }
            } else {
                Utils().stopChargerView(viewToCustom: self.view)
                if self.scanBarCode {
                    Alerts().alertQR(Title: "Error", Message: dataResponse.error?.localizedDescription, viewController: self, captureSession: self.captureSession)
                } else {
                    Alerts().alert(Title: "Error", Message: dataResponse.error?.localizedDescription, viewController: self)
                    
                }
            }
            
        }
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.POST
        request.httpBody = ["clave": barCode].percentEncoded()
        //request.timeoutInterval = 10
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        print(["clave": barCode])
        return request
    }
    
    // Shows toast if Wi-fi is active and stops any further process
    func isWifiEnabled(isWifiOn: Bool) -> Bool {
        
        #if targetEnvironment(simulator)
        return false
        #endif
        
        if isWifiOn && Utils.APP_MODE == Utils.AppMode.production {
       
            //showToast(title: "Aviso", text: Utils.DISABLE_WIFI_TO_REDEEM, interval: 3.0)
            // Setting view controller background
            showDissableWifiPopup()
            return true
        }
        return false
    }
    
    //Show dissable Wifi popup
    func showDissableWifiPopup(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let disableWifiPopupViewController = storyBoard.instantiateViewController(withIdentifier: "DisableWifiPopupViewController") as! DisableWifiPopupViewController
        
        disableWifiPopupViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.55)
        
        
        self.present(disableWifiPopupViewController, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        /*if segue.identifier == "goToCaptureTicket" {
            let captureTicketVC = segue.destination as! CaptureTicketViewController
            
         if let viewWithTag = self.view.viewWithTag(123) {
         viewWithTag.removeFromSuperview()
         }
         
         captureTicketVC.barCode = insertBarCodeTextField.text!
         
         }*/
        
        if segue.identifier == "goToInsertCode" {
            let insertCodeVC = segue.destination as! InsertCodeViewController
            if let viewWithTag = self.view.viewWithTag(123) {
                viewWithTag.removeFromSuperview()
            }
            Utils().stopChargerView(viewToCustom: self.view)
            insertCodeVC.publifonCouponScan = barCode
        }
    }
    
}


extension RegisterTicketCodeViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {        

            do {
                let regex = try NSRegularExpression(pattern: ".*[^A-Za-z0-9 ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
        return true
    }
}

