//
//  AmountTicketViewController.swift
//  Publifon Datos
//
//  Created by José Jacobo Contreras Trejo on 16/12/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit

class AmountTicketViewController: UIViewController {
    
    //Outlets
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    //Vars
    let DoneToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        DoneToolbar.barStyle = .default
        DoneToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Listo", style: .done, target: self, action: #selector(done))]
        DoneToolbar.sizeToFit()
        
        amountTextField.inputAccessoryView = DoneToolbar
        
        amountTextField.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
    }
    
    
    @objc func done() {
        //Done with number pad
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        view.endEditing(true)
        //checkBarCode()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = .black
        
        amountTextField.attributedPlaceholder = NSAttributedString(string: "Importe total",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        
        amountTextField.delegate = self
        amountTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        amountTextField.layer.borderWidth = 1
        amountTextField.layer.cornerRadius = 10
        amountTextField.keyboardType = .numberPad
        nextButton.cornerRadius = 10
       
    }
    
    @objc func myTextFieldDidChange(_ textField: UITextField) {

        if let amountString = textField.text?.currencyInputFormatting() {
            textField.text = amountString
        }
    }


}

extension AmountTicketViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

            do {
                let regex = try NSRegularExpression(pattern: "[^0-9.]", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
        return true
    }
}


extension String {

    // formatting text for currency textField
    func currencyInputFormatting() -> String {

        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2

        var amountWithPrefix = self

        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")

        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))

        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }

        return formatter.string(from: number)!
    }
}
