//
//  QRViewController.swift
//  Publifon2.6-SuperNova
//
//  Created by BlueLabs.
//  Copyright © 2018 BlueLabs. All rights reserved.
//

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
// MARK: - Import

import UIKit
import AVFoundation
import Alamofire
import SwiftyJSON

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
// MARK: - Implementation

class QRViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {


    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Properties

    @IBOutlet weak var footerdesignView: UIView!
    @IBOutlet weak var botonregresarButton: SupernovaButton!
    @IBOutlet weak var tutorialButton: SupernovaButton!

    
    //Var
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var dataQR = String()
    let captureButton = UIButton(frame: CGRect(x: 20, y: 60, width: 60 , height: 60))
    var showsTutorial = true
    var isFirsLoadView = true

    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Lifecycle

    override public func viewDidLoad()  {
        super.viewDidLoad()
        self.setupUI()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
        startScan()
        print("dentro del view did load")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isFirsLoadView {
            isFirsLoadView = false
        } else {
            //self.navigationController?.popViewController(animated: false)
            startScan()
        }
        
        /*if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
        
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No existe view para remover")
        }*/
    }

        // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Setup

       
    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        
        //Botón tutorial
        /*self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "tutorial"), style:.plain, target: nil, action: nil)*/
        self.navigationController?.navigationBar.tintColor = .black
       
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        //dismiss(animated: true)
        //self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    func found(code: String) {
        //previewLayer.removeFromSuperlayer()
        print(code)
        //dataQRTextView.text = code
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        dataQR = code
        checkQRcoupon()
        
        /*if let url = URL(string: code) {
            print("Your incoming link parameter is \(url.absoluteString)")
            
            dataQR = url.absoluteString
            /*let paramvalue = getQueryStringParameter(url: url.absoluteString, param: "qr")
            print("Valor de mi parámetro: ",paramvalue != nil ? paramvalue! : "no se encontró el parámetro")
            if paramvalue != nil {
                //dataQRTextView.text = dataQRTextView.text + "\n\n\nParámetro message es igual a: " + paramvalue!
                dataQR = paramvalue!
            }*/
        } else {
            print("No se pudo transformar a url")
        }*/

    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
  
    
    @IBAction func readQRCode(_ sender: Any) {
        startScan()
    }
    
    
    func startScan(){
        #if targetEnvironment(simulator)
            //self.dataQR = "https://www.nutrileche.com/?qr=walliaCodePruebas\(Int.random(in: 0..<100))"
            self.dataQR = "https://test.1uc1.com/KK2B2WUM5DKD28NV" //"\(Utils.qrData(mode: Utils.APP_MODE))"
            checkQRcoupon()
            return
        #endif
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        
        //Add Button
        captureButton.backgroundColor = .white
        captureButton.setImage(UIImage(named: "boton-regresar"), for: .normal)
        captureButton.layer.cornerRadius = 0.5 * captureButton.bounds.size.width
        captureButton.clipsToBounds = true
        captureButton.addTarget(self, action: #selector(self.actionCapture(sender:)), for: .touchUpInside)
        captureButton.tag = 123
        
        view.addSubview(captureButton)
        
        captureSession.startRunning()
        
    }
    
    
    @objc func actionCapture(sender: UIButton){
        previewLayer.removeFromSuperlayer()
        captureButton.removeFromSuperview()
        
        if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No existe view para remover")
        }
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.popViewController(animated: false)
    }
    

    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Actions

    @IBAction public func onBotonRegresarPressed(_ sender: UIButton)  {
    
    }
    
    @IBAction public func onTutorialPressed(_ sender: UIButton)  {
        self.performSegue(withIdentifier: "Push Tutorial Lala", sender: nil)
    }
    
    func checkQRcoupon(){
        /*if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }*/
        Utils().showChargerView(viewToCustom: self.view)
        Alamofire.request(configureRequest(url: Utils.CHECK_TUTORIAL)).responseData {
            dataResponse in
            print("****************************************\n")
            print(dataResponse.result.isSuccess)
            if dataResponse.result.isSuccess {
                let json : JSON = JSON(dataResponse.result.value!)
                print("\n\n*************")
                print(json["success"])
                if json["success"].boolValue {
                    print("resultado: ", json["message"].stringValue)
                    print("resultado: ", json["showTutorial"].boolValue)
                    self.showsTutorial = json["showTutorial"].boolValue
                    if json["showForm"].exists() {
                        if json["showForm"].boolValue {
                            self.performSegue(withIdentifier: "goToFormUserVC", sender: self)
                        } else {
                            self.performSegue(withIdentifier: "goToInsertCode", sender: self)
                        }
                    } else {                        
                        self.performSegue(withIdentifier: "goToInsertCode", sender: self)
                    }
                } else {
                    print("resultado: ", json["message"].stringValue)
                    print("resultado: ", json["showTutorial"].boolValue)
                    Utils().stopChargerView(viewToCustom: self.view)
                    Alerts().alertQR(Title: "Error", Message: json["message"].stringValue, viewController: self, captureSession: self.captureSession)
                }
            } else {
                Utils().stopChargerView(viewToCustom: self.view)
                Alerts().alertQR(Title: "Error", Message: dataResponse.error?.localizedDescription, viewController: self, captureSession: self.captureSession)
            }
            
        }
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.POST
        request.httpBody = ["clave": dataQR].percentEncoded()
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        print(["clave": dataQR])
        return request
    }
        
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToFormUserVC" {
            let formUserVC = segue.destination as! FormUserViewController
            if let viewWithTag = self.view.viewWithTag(123) {
                viewWithTag.removeFromSuperview()
            }
            Utils().stopChargerView(viewToCustom: self.view)
            formUserVC.dataQR = self.dataQR
            formUserVC.showTutorialApi = self.showsTutorial
            
            if let data = UserDefaults.standard.value(forKey:"userData") as? Data {
                if let decoded = try? PropertyListDecoder().decode(UserData.self, from: data) {
                    formUserVC.userDataBody = decoded
                    formUserVC.dataUserFromUserDefaults = true
                }
            }
            
        }
        
        if segue.identifier == "goToInsertCode" {
            let insertCodeVC = segue.destination as! InsertCodeViewController
            if let viewWithTag = self.view.viewWithTag(123) {
                viewWithTag.removeFromSuperview()
            }
            Utils().stopChargerView(viewToCustom: self.view)
            insertCodeVC.publifonCouponScan = self.dataQR
        }
    }
    
    
    
}

