//
//  FormUserViewController.swift
//  Publifon Datos
//
//  Created by José Jacobo Contreras Trejo on 18/11/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import Lottie


struct UserData: Codable {
    
    var userID: String = "0"
    var firstName = String()
    var lastName = String()
    var cp = String()
    var age = Int()
    var email = String()
    var mobileNumber = String()
    var latitude = CLLocationDegrees()
    var longitude = CLLocationDegrees()
    var osVersion = String()
    var buildVersion = String()
    var modelName = String()
    var deviceID: String = "NOUUID"
    var unicode = String()
    
}


class FormUserViewController: UserLocalization {
     
    //Outlets
    @IBOutlet weak var tradeMarkImage: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //outlets fragment view
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var stepsLabel: UILabel!
    
    
    //Vars
    var dataQR = String()
    var formData = ["Nombre", "Apellidos", "cp", "Age", "Email", "Phone"]
    var iCont = 0
    var userLocation = CLLocation()
    var userDataBody = UserData()//["firstName": "", "lastName": "", "email": "", "mobileNumber": ""]
    var couponRespondCode = ""
    
  
    var coupon: String = ""
    var isWifiActive = false
    let reachability = Reachability()!
    let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
    private var animationView: AnimationView?
    var showTutorialApi = true
    var dataUserFromUserDefaults = false
    var isFinishedTutorial = false
    
    //var currentUserLocation: CLLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        coupon = dataQR
        configureView()
        configureFragment()
        //checkCoupon(coupon: coupon)
        
        // Detects wifi state changes
        NotificationCenter.default.addObserver(self, selector: #selector(internetChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{ try reachability.startNotifier() }
        catch { print("Could not start notifier") }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(doneWithNumberPad))]
        numberToolbar.sizeToFit()
        
        displayTutorial()
        
        NotificationCenter.default.addObserver(self, selector: #selector(restartAnimation), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(restartAnimation), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(pauseAnimation), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)

        /*navigationController?.navigationBar.backIndicatorImage = UIImage(named: "boton-regresar")
         self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "boton-regresar")*/
        //self.navigationController?.setNavigationBarHidden(true, animated: false)
        //self.navigationController?.isNavigationBarHidden = false
    }
  
    
    
    
    
    
    @objc func restartAnimation() {
        
        animationView?.play { (finished) in
            if finished {
                print("ha finalizado")
                print("Finalizó: \(Date())")
                self.isFinishedTutorial = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.navigationController?.isNavigationBarHidden = false
                self.animationView?.removeFromSuperview()
                Utils().showTermsAndConditionsAux(viewController: self)
            }
        }
    }
        
    @objc func pauseAnimation() {
        animationView?.pause()
    }
    
    
    @objc func doneWithNumberPad() {
        //Done with number pad
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    
     @objc func back(sender: UIBarButtonItem) {
        goBackFormData()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        configureView()
        if isFinishedTutorial {
            navigationController?.setNavigationBarHidden(false, animated: false)
            Utils().showTermsAndConditionsAux(viewController: self)
            //Utils().showTermsAndConditionsAux(viewController: self)
        } else {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    func configureView(){
        nextButton.cornerRadius = 10
        firstTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        secondTextField.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        firstTextField.layer.borderWidth = 1
        secondTextField.layer.borderWidth = 1
        
        firstTextField.layer.cornerRadius = 10
        secondTextField.layer.cornerRadius = 10
        
        firstTextField.delegate = self
        secondTextField.delegate = self
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.back(sender:)))
        
        newBackButton.image = UIImage(named: "boton-regresar")
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
        //contador de datos faltantes
        /*let rightButton = UIBarButtonItem()
        rightButton.tintColor = .black
        rightButton.title = "\(iCont+1)/\(formData.count)"
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationController?.navigationBar.tintColor = .black*/
        stepsLabel.text = "Paso \(iCont+1)/\(formData.count)"
        
        //pasar al usuario a verificar solo su número de teléfono
        if dataUserFromUserDefaults {
            iCont = formData.count-1
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
    }
    
    
    func displayTutorial(){
        print("******* form user\n")
        print("******* userDataBody.userID \(userDataBody.userID)\n")
        print("******* showTutorialApi \(showTutorialApi)\n")
        //if self.userDataBody.userID == "0" && showTutorialApi {
        if showTutorialApi {
            /*#if targetEnvironment(simulator)
            return
            #endif*/
           showTotorial()
        }
                   
    }
    
    
    
    func showTotorial(){
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        animationView = .init(name: "Nutrileche")
        animationView!.loopMode = .playOnce
        animationView!.animationSpeed = 1
        animationView!.contentMode = .scaleAspectFill//.scaleAspectFit
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        animationView!.play { (finished) in
            self.navigationController?.setNavigationBarHidden(true, animated: false)
            if finished {
                print("ha finalizado")
                print("Finalizó: \(Date())")
                self.isFinishedTutorial = true
                self.navigationController?.setNavigationBarHidden(false, animated: false)
                self.navigationController?.isNavigationBarHidden = false
                self.animationView?.removeFromSuperview()
                Utils().showTermsAndConditionsAux(viewController: self)
            }
        }
        print("Inicio: \(Date())")
        animationView!.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.view.addSubview(animationView!)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
        
    @IBAction func onNextPressed(_ sender: UIButton) {
                
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            view.endEditing(true)
        }
        
        //Simulator
        /*#if targetEnvironment(simulator)
        //couponTypeManager(phoneNumber: phoneNumber)
        //verifyFragment()
        self.performSegue(withIdentifier: "goToInsertCodeVC", sender: self)
        return
        #endif*/
                
        verifyFragment()
    }
    
    
    func configureFragment(){
                
        let padding = 8
        let size = 20
        let outerView = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        let iconView  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
        
        let outerView2 = UIView(frame: CGRect(x: 0, y: 0, width: size+padding, height: size) )
        let iconView2  = UIImageView(frame: CGRect(x: padding, y: 0, width: size, height: size))
        
        changeEmailButton.isHidden = true
        //self.navigationItem.rightBarButtonItem?.title = "\(iCont+1)/\(formData.count)"
        
        stepsLabel.text = "Paso \(iCont+1)/\(formData.count)"
        
        switch formData[iCont] {
        case "Nombre":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuál es tu nombre sin apellidos?"
            
            //firstTextField.placeholder = "Nombre(s) sin apellidos"
            firstTextField.attributedPlaceholder = NSAttributedString(string: "Nombre(s) sin apellidos",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            iconView.image = UIImage(named: "icon-metro-file-text")
            outerView.addSubview(iconView)
            
            firstTextField.leftView = outerView
            secondTextField.leftView = outerView
            firstTextField.leftViewMode = .always
            
            firstTextField.textContentType = .name
            firstTextField.autocapitalizationType = .words
            //firstTextField.becomeFirstResponder()
            
            self.title = "Nombre"
            
            //firstTextField.text = userDataBody["firstName"] != "" && userDataBody["firstName"] != nil ? userDataBody["firstName"] : nil
            firstTextField.text = userDataBody.firstName != "" && userDataBody.firstName != nil ? userDataBody.firstName
            : nil
                        
            break
            
        case "Apellidos":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuales son tus apellidos?"
            
            //firstTextField.placeholder = "Apellidos"
            
            firstTextField.attributedPlaceholder = NSAttributedString(string: "Apellidos",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            firstTextField.textContentType = .name
            firstTextField.autocapitalizationType = .words
            firstTextField.becomeFirstResponder()
            
            self.title = "Apellidos"
            
            //firstTextField.text = userDataBody["lastName"] != "" && userDataBody["lastName"] != nil ? userDataBody["lastName"] : nil
            firstTextField.text = userDataBody.lastName != "" && userDataBody.lastName != nil ? userDataBody.lastName
                : nil
            
            break
            
        case "cp":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuál es tu código postal?"
            
            //firstTextField.placeholder = "Nombre(s) sin apellidos"
            firstTextField.attributedPlaceholder = NSAttributedString(string: "CP",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            //firstTextField.textContentType = .telephoneNumber
            firstTextField.keyboardType = .numberPad
            //firstTextField.autocapitalizationType = .words
            //firstTextField.becomeFirstResponder()
            
            self.title = "Código postal"
            
            //firstTextField.text = userDataBody["firstName"] != "" && userDataBody["firstName"] != nil ? userDataBody["firstName"] : nil
            firstTextField.text = userDataBody.cp != "" && userDataBody.cp != nil ? userDataBody.cp
                : nil
            
            break
            
        case "Age":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuántos años tienes?"
            
            //firstTextField.placeholder = "Nombre(s) sin apellidos"
            firstTextField.attributedPlaceholder = NSAttributedString(string: "Edad",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            //firstTextField.textContentType = .telephoneNumber
            firstTextField.keyboardType = .numberPad
            //firstTextField.autocapitalizationType = .words
            //firstTextField.becomeFirstResponder()
            
            self.title = "Edad"
            
            //firstTextField.text = userDataBody["firstName"] != "" && userDataBody["firstName"] != nil ? userDataBody["firstName"] : nil
            firstTextField.text = userDataBody.age != Int() && userDataBody.age != nil ? String(userDataBody.age)
                : nil
            
            break
            
        case "Email":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuál es tu correo?"
            
            //firstTextField.placeholder = "Email"
            //secondTextField.placeholder = "Email"
            
            firstTextField.attributedPlaceholder = NSAttributedString(string: "Correo electrónico",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            secondTextField.attributedPlaceholder = NSAttributedString(string: "Confirmación correo electrónico",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            iconView.image = UIImage(named: "icon-metro-file-text")
            outerView.addSubview(iconView)
            
            iconView2.image = UIImage(named: "icon-metro-file-text")
            outerView2.addSubview(iconView2)
            
            
            firstTextField.leftViewMode = .always
            firstTextField.leftView = outerView
            firstTextField.textContentType = .emailAddress
            firstTextField.autocapitalizationType = .none
            firstTextField.keyboardType = .emailAddress
  
            secondTextField.leftViewMode = .always
            secondTextField.leftView = outerView2
            secondTextField.textContentType = .emailAddress
            secondTextField.autocapitalizationType = .none
            secondTextField.keyboardType = .emailAddress
            
            firstTextField.becomeFirstResponder()
            
            
            self.title = "Correo electrónico"
            
            //firstTextField.text = userDataBody["email"] != "" && userDataBody["email"] != nil ? userDataBody["email"] : nil
            firstTextField.text = userDataBody.email != "" && userDataBody.email != nil ? userDataBody.email
            : nil
            
            
            //secondTextField.isHidden = userDataBody["email"] != "" && userDataBody["email"] != nil ? false : true
            secondTextField.isHidden = userDataBody.email != "" && userDataBody.email != nil ? false : true
            
            
            //secondTextField.text = userDataBody["email"] != "" && userDataBody["email"] != nil ? userDataBody["email"] : nil
            
            secondTextField.text = userDataBody.email != "" && userDataBody.email != nil ? userDataBody.email
            : nil
            
            
            break
            
        case "Phone":
            firstTextField.isHidden = false
            secondTextField.isHidden = true
            
            titleLabel.text = "¿Cuál es tu celular?"
            
            //firstTextField.placeholder = "Número celular"
            //secondTextField.placeholder = "Número celular"
            
            firstTextField.attributedPlaceholder = NSAttributedString(string: "Número celular a 10 dígitos",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            secondTextField.attributedPlaceholder = NSAttributedString(string: "Confirmación número celular",
                                                                       attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
            
            iconView.image = UIImage(named: "contact_cell_call")
            outerView.addSubview(iconView)
            
            firstTextField.leftViewMode = .always
            firstTextField.leftView = outerView
            firstTextField.textContentType = .telephoneNumber
            firstTextField.keyboardType = .phonePad
            
            iconView2.image = UIImage(named: "contact_cell_call")
            outerView2.addSubview(iconView2)
            secondTextField.leftViewMode = .always
            secondTextField.leftView = outerView2
            secondTextField.textContentType = .telephoneNumber
            secondTextField.keyboardType = .phonePad
            
            if dataUserFromUserDefaults {
                
            } else {
                firstTextField.becomeFirstResponder()
            }
            
            self.title = "Número de teléfono"
            
            //firstTextField.text = userDataBody["mobileNumber"] != "" && userDataBody["mobileNumber"] != nil ? userDataBody["mobileNumber"] : nil
            
            firstTextField.text = userDataBody.mobileNumber != "" && userDataBody.mobileNumber != nil ? userDataBody.mobileNumber
                   : nil
            
            
            //changeEmailButton.isHidden = false
            
            break
            
        default:
            break
        }
    }
    
    
    
    func verifyFragment(){
        
        switch formData[iCont] {
        case "Nombre":
            guard firstTextField.text != "" else {
                Alerts().alert(Title: "Error", Message: "Debes de ingresar tu nombre sin apellidos", viewController: self)
                return
            }
            iCont += 1
            //userDataBody["firstName"] = firstTextField.text
            userDataBody.firstName = firstTextField.text!
            cleanTextFields()
            
            break
        case "Apellidos":
            guard firstTextField.text != "" else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar tus apellidos", viewController: self)
                return
            }
            iCont += 1
            //userDataBody["lastName"] = firstTextField.text
            userDataBody.lastName = firstTextField.text!
            cleanTextFields()
            
            break
            
        case "cp":
            guard firstTextField.text != "" && firstTextField.text!.count == 5 else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar un código postal válido", viewController: self)
                return
            }
            iCont += 1
            //userDataBody["firstName"] = firstTextField.text
            userDataBody.cp = firstTextField.text!
            cleanTextFields()
            
            break
            
        case "Age":
            guard firstTextField.text != "" && firstTextField.text!.count <= 3 && firstTextField.text!.count > 0 && Int(firstTextField.text!)! < 120
                && Int(firstTextField.text!)! > 0 else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar una edad válida", viewController: self)
                return
            }
            iCont += 1
            //userDataBody["firstName"] = firstTextField.text
            userDataBody.age = Int(firstTextField.text!)!
            cleanTextFields()
            
            break
        case "Email":
            
            guard firstTextField.text != "" else {
                Alerts().alert(Title: "Error", Message: "Debes de ingresar tu correo electrónico", viewController: self)
                return
            }
            
            guard firstTextField.text?.firstIndex(of: "@") != nil && firstTextField.text!.count > 2 else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar un correo electrónico válido", viewController: self)
                return
            }
            
            if secondTextField.isHidden == true{
                secondTextField.isHidden = false
                secondTextField.becomeFirstResponder()
                /*iconView.image = UIImage(named: "icon-metro-file-text")
                outerView.addSubview(iconView)
                secondTextField.leftView = outerView
                secondTextField.leftView?.backgroundColor = .red*/
                return
            }
            
            guard firstTextField.text?.firstIndex(of: "@") != nil || secondTextField.text?.firstIndex(of: "@") != nil else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar un correo electrónico válido", viewController: self)
                return
            }
            
            guard firstTextField.text == secondTextField.text else {
                Alerts().alert(Title: "Error", Message: "No coincide el correo electrónico ingresado", viewController: self)
                return
            }
            
            iCont += 1
            //userDataBody["email"] = firstTextField.text
            userDataBody.email = firstTextField.text!
            cleanTextFields()
            
            break
        case "Phone":
            guard firstTextField.text != "" else {
                Alerts().alert(Title: "Error", Message: "Debes de ingresar tu nombre sin apellidos", viewController: self)
                return
            }
            
            guard firstTextField.text?.count == 10 else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar un número celular válido", viewController: self)
                return
            }
            
            if secondTextField.isHidden == true{
                secondTextField.isHidden = false
                secondTextField.becomeFirstResponder()
                return
            }
            
            guard firstTextField.text?.count == 10 || secondTextField.text?.count == 10 else {
                Alerts().alert(Title: "Error", Message: "Debes ingresar un número celular válido", viewController: self)
                return
            }
            
            guard firstTextField.text == secondTextField.text else {
                Alerts().alert(Title: "Error", Message: "No coincide el número celular ingresado", viewController: self)
                return
            }
            
            //userDataBody["mobileNumber"] = firstTextField.text
            userDataBody.mobileNumber = firstTextField.text!
            //cleanTextFields()
            print("\n\n\n\n***************** ", userDataBody)
            
            guard isWifiEnabled(isWifiOn: isWifiActive) != true else {
                return
            }
            
            verifyPopup()
            
            break
            
        default:
            
            break
        }
    }
    
    
    func goBackFormData(){
        //["firstName": "", "lastName": "", "email": "", "mobileNumber": ""]
        
        if dataUserFromUserDefaults {
            _ = navigationController?.popViewController(animated: true)
            return
        }
        
        if formData[iCont] == "Phone" {
            //userDataBody["mobileNumber"] = firstTextField.text
            userDataBody.mobileNumber = firstTextField.text!
        }
        
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
            view.endEditing(true)
        }
        
        if iCont-1 < 0 {
            // Go back to the previous ViewController
            _ = navigationController?.popViewController(animated: true)
            return
        } else {
            iCont = iCont-1
        }
        
        changeEmailButton.isHidden = true
        //self.navigationItem.rightBarButtonItem?.title = "\(iCont+1)/\(formData.count)"
        stepsLabel.text = "Paso \(iCont+1)/\(formData.count)"
        
        switch formData[iCont] {
        case "Nombre":
            //firstTextField.text = userDataBody["firstName"]
            firstTextField.text = userDataBody.firstName
            secondTextField.isHidden = true
            self.title = "Nombre"
            titleLabel.text = "¿Cuál es tu nombre sin apellidos?"
            firstTextField.keyboardType = .default
            
            break
            
        case "Apellidos":
            //firstTextField.text = userDataBody["lastName"]
            firstTextField.text = userDataBody.lastName
            secondTextField.isHidden = true
            self.title = "Apellidos"
            titleLabel.text = "¿Cuales son tus apellidos?"
            firstTextField.keyboardType = .default
            break
            
        case "cp":
            //firstTextField.text = userDataBody["firstName"]
            firstTextField.text = userDataBody.cp
            secondTextField.isHidden = true
            self.title = "Código postal"
            titleLabel.text = "¿Cuál es tu código postal?"
            firstTextField.keyboardType = .numberPad
            
            break
            
        case "Age":
            //firstTextField.text = userDataBody["firstName"]
            firstTextField.text = String(userDataBody.age)
            secondTextField.isHidden = true
            self.title = "Edad"
            titleLabel.text = "¿Cuántos años tienes?"
            firstTextField.keyboardType = .numberPad
            
            break
            
        case "Email":
            
            //firstTextField.text = userDataBody["email"]
            firstTextField.text = userDataBody.email
            //secondTextField.text = userDataBody["email"]
            secondTextField.text = userDataBody.email
            self.title = "Correo electrónico"
            titleLabel.text = "¿Cuál es tu correo?"
            firstTextField.keyboardType = .emailAddress
            secondTextField.keyboardType = .emailAddress
            break
            
        case "Phone":
            //firstTextField.text = userDataBody["mobileNumber"]
            firstTextField.text = userDataBody.mobileNumber
            //secondTextField.text = userDataBody["mobileNumber"]
            secondTextField.text = userDataBody.mobileNumber
            self.title = "Número de teléfono"
            titleLabel.text = "¿Cuál es tu celular?"
            firstTextField.keyboardType = .phonePad
            secondTextField.keyboardType = .phonePad
            //changeEmailButton.isHidden = false
            break
            
        default:
            break
        }
    }
    
    
    @IBAction func changeEmail(_ sender: Any) {
        goBackFormData()
    }
    
    
    func verifyPopup(){
        let alert = UIAlertController(title: "Publifon", message: "Estas a punto de redimir tu cupón, verifica tus datos antes de continuar", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Continuar", style: .default, handler: { action in
            self.makeRedentionBody()
        }))
        alert.addAction(UIAlertAction(title: "Corregir", style: .cancel, handler: { action in
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
 
    func cleanTextFields(){
        firstTextField.text = ""
        secondTextField.text = ""
        configureFragment()
    }
    
    func makeRedentionBody(){
              
        let body: [String:Any] = [
            "firstName": userDataBody.firstName,
            "lastName": userDataBody.lastName,
            "cp": userDataBody.cp,
            "email": userDataBody.email,
            "mobileNumber": String(userDataBody.mobileNumber),
            "age": userDataBody.age,
            "latitude": userLocation.coordinate.latitude,    //Double(userLocation.coordinate.latitude),
            "longitude": userLocation.coordinate.longitude, //Double(userLocation.coordinate.longitude),
            "osVersion": "IOS",
            "buildVersion": UIDevice.current.systemVersion,
            "modelName": UIDevice.modelName,
            "deviceID": UIDevice.current.identifierForVendor?.uuidString ??  "NOUUID",
            "userID": userDataBody.userID,
            "unicode": dataQR
        ]
           
        userDataBody.latitude = userLocation.coordinate.latitude
        userDataBody.longitude = userLocation.coordinate.longitude
        userDataBody.osVersion = "IOS"
        userDataBody.buildVersion = UIDevice.current.systemVersion
        userDataBody.modelName = UIDevice.modelName
        userDataBody.deviceID = UIDevice.current.identifierForVendor?.uuidString ??  "NOUUID"
        userDataBody.userID = userDataBody.userID
        userDataBody.unicode = dataQR
                
        if couponRespondCode != ""{
            //dataQR = couponRespondCode
            self.performSegue(withIdentifier: "goToInsertCodeVC", sender: self)
            return
        }
        
        print("\na ver que se envía:\n")
        print(body)
        makeRedention(bodyToSend: body)
    }
    
    
    
    func makeRedention(bodyToSend: [String:Any]){
        
        DispatchQueue.main.async {
            Utils().showChargerView(viewToCustom: self.view)
        }
        
        let headers = [
            "content-type": "application/json",
            "cache-control": "no-cache",
            "postman-token": "ab1ae4e8-9f88-62da-7fa8-1b5cabec360e"
        ]
        
        do {
            
            
            /*let x : [String : Any] = ["firstName": "José Jacobo",
            "lastName": "Contreras Trejo",
            "email": "jacobo@blue.gen.in",
            "mobileNumber": "5523024637",
            "age": 22,
            "cp":"09200",
            "latitude": 19.427772,
            "longitude": -99.147277,
            "osVersion": "IOS",
            "buildVersion": "12.4",
            "modelName":"iPhone 7",
            "deviceID": "CD03DFEC-CC15-4F24-B60F-75BFE2725041",
            "userID": "01ESWBNTN5302EBHNXY8NPP549",
            "unicode": "https://nutripromo.com.mx/?qr=walliaCodeSa"]*/
            
            let postData = try JSONSerialization.data(withJSONObject: bodyToSend, options: [])
            //let postData = try JSONSerialization.data(withJSONObject: x, options: [])
            
            //let request = NSMutableURLRequest(url: NSURL(string: "http://127.0.0.1:8000/api/wallia/redeem")! as URL,
            let request = NSMutableURLRequest(url: NSURL(string: Utils.REDEEM_QR_COUPON)! as URL,
            cachePolicy: .useProtocolCachePolicy,
            //timeoutInterval: 20.0)
            timeoutInterval: 60)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                
                print("********* error: ", error)
                print("\n")
                print("********* data: ", data)
                print("\n")
                print("********* response: ", response)
                
                guard error == nil else {
                    print("Error: ", error?.localizedDescription)
                    DispatchQueue.main.async {
                        Utils().stopChargerView(viewToCustom: self.view)
                        Alerts().alert(Title: "Error", Message: error?.localizedDescription, viewController: self)
                    }
                    return
                }
                
    
                if let data = data, let response = response as? HTTPURLResponse {
                    switch response.statusCode {
                    case 500...599:
                        let errorResponseString = String(data: data, encoding: .utf8)
                        print("Error: ", errorResponseString)
                        DispatchQueue.main.async {
                            Utils().stopChargerView(viewToCustom: self.view)
                            Alerts().alert(Title: "Error", Message: "Existe un error en el servidor, intenta más tarde.", viewController: self)
                        }
                        return
                    default:
                        break
                    }
                }
                
                do {
                    print("data: ", data.debugDescription)
                    print("response: ", response.debugDescription)
                    if let jsonResult = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print(jsonResult)
                        if (jsonResult["success"] as! Bool) == true {
                            DispatchQueue.main.async {
                                Utils().stopChargerView(viewToCustom: self.view)
                                self.dataQR = jsonResult["newCoupon"] as! String
                                self.couponRespondCode = jsonResult["newCoupon"] as! String
                                //UserDefaults.standard.set(jsonResult["userID"], forKey: "userID")
                                self.userDataBody.userID = jsonResult["userID"] as! String
                                //UserDefaults.standard.set(self.userDataBody, forKey: "userData")
                                UserDefaults.standard.set(try? PropertyListEncoder().encode(self.userDataBody), forKey:"userData")
                                self.performSegue(withIdentifier: "goToInsertCodeVC", sender: self)
                            }
                        } else {
                            DispatchQueue.main.async {
                                Utils().stopChargerView(viewToCustom: self.view)
                                Alerts().alert(Title: "Error", Message: jsonResult["message"] as? String, viewController: self)
                                return
                            }
                        }
                    }
                } catch let error as NSError {
                    print("Error catch: ", error.localizedDescription)
                    DispatchQueue.main.async {
                        Utils().stopChargerView(viewToCustom: self.view)
                        Alerts().alert(Title: "Error", Message: error.localizedDescription, viewController: self)
                    }
                }
            })
            dataTask.resume()
            
        } catch {
            print("Error: ", error.localizedDescription)
            DispatchQueue.main.async {
                Utils().stopChargerView(viewToCustom: self.view)
                Alerts().alert(Title: "Error", Message: error.localizedDescription, viewController: self)
            }
        }
        
        Utils().stopChargerView(viewToCustom: self.view)
        
    }

    func blackBackgroundView(){
        let chargerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        chargerView.tag = 100
        chargerView.backgroundColor = UIColor.black.withAlphaComponent(0.55)
        self.view.addSubview(chargerView)
    }
        
    // Shows toast if Wi-fi is active and stops any further process
    func isWifiEnabled(isWifiOn: Bool) -> Bool {
        
        #if targetEnvironment(simulator)
        return false
        #endif
        
        if isWifiOn && Utils.APP_MODE == Utils.AppMode.production {
       
            //showToast(title: "Aviso", text: Utils.DISABLE_WIFI_TO_REDEEM, interval: 3.0)
            // Setting view controller background
            showDissableWifiPopup()
            return true
        }
        return false
    }
    
    //Show dissable Wifi popup
    func showDissableWifiPopup(){
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let disableWifiPopupViewController = storyBoard.instantiateViewController(withIdentifier: "DisableWifiPopupViewController") as! DisableWifiPopupViewController
        
        disableWifiPopupViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.55)
        
        
        self.present(disableWifiPopupViewController, animated: true, completion: nil)
        
    }
    
    @objc func internetChanged (note: Notification){
        let reachability = note.object as! Reachability
        print("Internet reachability state", reachability.connection)
        switch reachability.connection {
        case .wifi:
            print("Internet", "wifi")
            isWifiActive = true
        case .cellular:
            print("Internet", "cellular")
            isWifiActive = false
        case .none:
            print("Internet", "none")
            
        case .vpn:
            print("Internet", "VPN")
        }
    }
    
    //Preparing for segue, passing coupon code
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
             
        if segue.identifier == "goToInsertCodeVC"{
            let insertCodevC = segue.destination as! InsertCodeViewController
            insertCodevC.isFromQR = true
            insertCodevC.coupon = self.couponRespondCode//self.dataQR
            //insertCodevC.phoneNumber = userDataBody["mobileNumber"]!
            insertCodevC.phoneNumber = userDataBody.mobileNumber
        }
        
        
    }
    
    
    override func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        
        guard let location = locations.last else { return }
        userLocation = location
    }

}


extension FormUserViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        switch formData[iCont] {
        case "Nombre", "Apellidos":
            do {
                let regex = try NSRegularExpression(pattern: ".*[^A-Za-zÁÉÍÓÚáéíóú ].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            break
        case "Phone":
            do {
                let regex = try NSRegularExpression(pattern: ".*[^0-9].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            break
            
        case "cp":
            do {
                let regex = try NSRegularExpression(pattern: ".*[^0-9].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            break
            
        case "Age":
            do {
                let regex = try NSRegularExpression(pattern: ".*[^0-9].*", options: [])
                if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                    return false
                }
            }
            catch {
                print("ERROR")
            }
            break
            
            /*case "email":
             do {
             let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: [])
             if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
             return false
                }
            }
            catch {
                print("ERROR")
            }
            break*/
        default:
            break
        }
        
        return true
    }
}


