//
//  TutorialQRViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 11/18/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import AVFoundation

class TutorialQRViewController: UIViewController {
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Properties
    
    @IBOutlet weak var rectangulo510View: UIView!
    @IBOutlet weak var grupo543Button: SupernovaButton!
    
    //Var
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Lifecycle
    
    override public func viewDidLoad()  {
        super.viewDidLoad()
        //self.setupComponents()
        //self.setupUI()
        //self.setupGestureRecognizers()
        //self.setupLocalization()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = false

    }
    
    override public func viewWillAppear(_ animated: Bool)  {
        super.viewWillAppear(animated)

        // Navigation bar, if any
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Setup
    
    private func setupComponents()  {
        // Setup rectangulo510View
        self.rectangulo510View.layer.borderColor = UIColor(red: 0.439, green: 0.439, blue: 0.439, alpha: 1).cgColor /* #707070 */
        self.rectangulo510View.layer.borderWidth = 1
        
        
        // Setup grupo543Button
        self.grupo543Button.layer.borderColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor /* #FFFFFF */
        self.grupo543Button.layer.borderWidth = 1
        
        self.grupo543Button.layer.cornerRadius = 10
        self.grupo543Button.layer.masksToBounds = true
        self.grupo543Button.snImageTextSpacing = 10
        
    }
    
    private func setupUI()  {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
        
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Status Bar
    /*
     override public var prefersStatusBarHidden: Bool  {
     return true
     }
     
     override public var preferredStatusBarStyle: UIStatusBarStyle  {
     return .default
     }
     */
    
    // --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
    // MARK: - Actions
    
    @IBAction public func onGrupo543Pressed(_ sender: UIButton)  {
        self.performSegue(withIdentifier: "Push QR", sender: nil)
    }
    
}

