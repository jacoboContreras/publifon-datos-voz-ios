//
//  SeleccionDeRedencionViewController.swift
//  Publifon2.6-SuperNova
//
//  Created by BlueLabs.
//  Copyright © 2018 BlueLabs. All rights reserved.
//

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
// MARK: - Import

import UIKit
import CoreLocation

// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---
// MARK: - Implementation

class SeleccionDeRedencionViewController: UserLocalization {
    
    
    
    //Outlets
    
    @IBOutlet weak var QRButton: SupernovaButton!
    @IBOutlet weak var cuponButton: SupernovaButton!
    @IBOutlet weak var ticketButton: SupernovaButton!
    //var locationManager = CLLocationManager()
    
    //vars
    var dynamicLinkError = false
    var dynamicLinkErrorMessage = String()
    
    override func viewDidLoad() {
        setComponents()
        checkLocationServices()
        errorInScanDynamicLink(message: dynamicLinkErrorMessage)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setComponents()
    }
    
    @IBAction func unwindToSelectRedention( _ seg: UIStoryboardSegue) {
    }
    
    
    func setComponents(){
        
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        let statusBarColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        statusBarView.backgroundColor = statusBarColor
        self.view.addSubview(statusBarView)
        
        
        self.navigationController?.isNavigationBarHidden = true
        let backgroundImage = UIImageView(image: UIImage(named: "fondo"))
        backgroundImage.contentMode = .scaleAspectFill
        
        backgroundImage.frame = view.frame
        self.view.insertSubview(backgroundImage, at: 0)
        self.navigationController?.isNavigationBarHidden = true
        
        configureButton(button: QRButton)
        configureButton(button: ticketButton)
        configureButton(button: cuponButton)
        
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "boton-regresar")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "boton-regresar")
        self.navigationController?.navigationBar.backItem?.title = nil        
    }
    
    func configureButton(button: UIButton){
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.cornerRadius = 10.0
        
        let spacingImageText = CGFloat((self.view.frame.width / 100) * 10)
        
        print("esta cosa ggg: ", spacingImageText)
        
        switch button {
        case QRButton:
            
            QRButton.titleLabel!.lineBreakMode = .byWordWrapping
            QRButton.titleLabel!.textAlignment = .center
            QRButton.setTitle("Tengo un \ncódigo QR", for: .normal)
            QRButton.titleLabel!.text = "Tengo un \ncódigo QR"
            QRButton.titleLabel!.textColor = UIColor.black
            QRButton.titleLabel!.changeFont(ofText: "código QR", with: UIFont.systemFont(ofSize: 16, weight: .bold))
            QRButton.snImageTextSpacing = spacingImageText
            
            //QRButton.snImagePosition = .left
            //QRButton.snImageTextSpacing = 30
            
            break
        case cuponButton:
            cuponButton.titleLabel!.lineBreakMode = .byWordWrapping
            cuponButton.titleLabel!.textAlignment = .center
            cuponButton.setTitle("Tengo un \ncupón", for: .normal)
            cuponButton.titleLabel!.text = "Tengo un \ncupón"
            cuponButton.titleLabel!.textColor = UIColor.black
            cuponButton.titleLabel!.changeFont(ofText: "cupón", with: UIFont.systemFont(ofSize: 16, weight: .bold))
            
            cuponButton.snImageTextSpacing = spacingImageText
            
            break
        case ticketButton:
            ticketButton.titleLabel!.lineBreakMode = .byWordWrapping
            ticketButton.titleLabel!.textAlignment = .center
            ticketButton.setTitle("Tengo un \nticket", for: .normal)
            ticketButton.titleLabel!.text = "Tengo un \nticket"
            ticketButton.titleLabel!.textColor = UIColor.black
            ticketButton.titleLabel!.changeFont(ofText: "ticket", with: UIFont.systemFont(ofSize: 16, weight: .bold))
            
            ticketButton.snImageTextSpacing = spacingImageText
            
            break
        default:
            break
        }
    }
    
    
    @IBAction func goToScanQR(_ sender: Any) {
        
        guard Utils().isCameraPermission() == true else {
            Alerts().alert(Title: "No tenemos permisos para utilizar la cámara", Message: "Dirígete a Configuración > Publifon y activa el permiso de la cámara ", viewController: self)
            return
        }
        
        self.performSegue(withIdentifier: "goToScanQR", sender: self)
    }
    
    
    @IBAction func reportProblem(_ sender: Any) {
        guard let url = URL(string: "http://auditor.app") else { return }
        UIApplication.shared.open(url)
    }
    
    
    func errorInScanDynamicLink(message: String){
        if dynamicLinkError {
            Alerts().alert(Title: "Error", Message: message, viewController: self)
        }
    }
    
    
}



public extension ChangableFont {
    
    var rangedAttributes: [RangedAttributes] {
        guard let attributedText = getAttributedText() else {
            return []
        }
        var rangedAttributes: [RangedAttributes] = []
        let fullRange = NSRange(
            location: 0,
            length: attributedText.string.count
        )
        attributedText.enumerateAttributes(
            in: fullRange,
            options: []
        ) { (attributes, range, stop) in
            guard range != fullRange, !attributes.isEmpty else { return }
            rangedAttributes.append(RangedAttributes(attributes, inRange: range))
        }
        return rangedAttributes
    }
    
    func changeFont(ofText text: String, with font: UIFont) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeFont(inRange: range, with: font)
    }
    
    func changeFont(inRange range: NSRange, with font: UIFont) {
        add(attributes: [.font: font], inRange: range)
    }
    
    func changeTextColor(ofText text: String, with color: UIColor) {
        guard let range = (self.getAttributedText()?.string ?? self.getText())?.range(ofText: text) else { return }
        changeTextColor(inRange: range, with: color)
    }
    
    func changeTextColor(inRange range: NSRange, with color: UIColor) {
        add(attributes: [.foregroundColor: color], inRange: range)
    }
    
    private func add(attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        guard !attributes.isEmpty else { return }
        
        var rangedAttributes: [RangedAttributes] = self.rangedAttributes
        
        var attributedString: NSMutableAttributedString
        
        if let attributedText = getAttributedText() {
            attributedString = NSMutableAttributedString(attributedString: attributedText)
        } else if let text = getText() {
            attributedString = NSMutableAttributedString(string: text)
        } else {
            return
        }
        
        rangedAttributes.append(RangedAttributes(attributes, inRange: range))
        
        rangedAttributes.forEach { (rangedAttributes) in
            attributedString.addAttributes(
                rangedAttributes.attributes,
                range: rangedAttributes.range
            )
        }
        
        set(attributedText: attributedString)
    }
    
    func resetFontChanges() {
        guard let text = getText() else { return }
        set(attributedText: NSMutableAttributedString(string: text))
    }
}


public protocol ChangableFont: AnyObject {
    var rangedAttributes: [RangedAttributes] { get }
    func getText() -> String?
    func set(text: String?)
    func getAttributedText() -> NSAttributedString?
    func set(attributedText: NSAttributedString?)
    func getFont() -> UIFont?
    func changeFont(ofText text: String, with font: UIFont)
    func changeFont(inRange range: NSRange, with font: UIFont)
    func changeTextColor(ofText text: String, with color: UIColor)
    func changeTextColor(inRange range: NSRange, with color: UIColor)
    func resetFontChanges()
}

public struct RangedAttributes {

    public let attributes: [NSAttributedString.Key: Any]
    public let range: NSRange

    public init(_ attributes: [NSAttributedString.Key: Any], inRange range: NSRange) {
        self.attributes = attributes
        self.range = range
    }
}

public extension String {

    func range(ofText text: String) -> NSRange {
        let fullText = self
        let range = (fullText as NSString).range(of: text)
        return range
    }
}


extension UILabel: ChangableFont {

    public func getText() -> String? {
        return text
    }

    public func set(text: String?) {
        self.text = text
    }

    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }

    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }

    public func getFont() -> UIFont? {
        return font
    }
}

extension UITextField: ChangableFont {

    public func getText() -> String? {
        return text
    }

    public func set(text: String?) {
        self.text = text
    }

    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }

    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }

    public func getFont() -> UIFont? {
        return font
    }
}

extension UITextView: ChangableFont {

    public func getText() -> String? {
        return text
    }

    public func set(text: String?) {
        self.text = text
    }

    public func getAttributedText() -> NSAttributedString? {
        return attributedText
    }

    public func set(attributedText: NSAttributedString?) {
        self.attributedText = attributedText
    }

    public func getFont() -> UIFont? {
        return font
    }
}


/*extension UIButton {
    func moveImageLeftTextCenter(imagePadding: CGFloat = 30.0){
        guard let imageViewWidth = self.imageView?.frame.width else{return}
        guard let titleLabelWidth = self.titleLabel?.intrinsicContentSize.width else{return}
        self.contentHorizontalAlignment = .left
        imageEdgeInsets = UIEdgeInsets(top: 0.0, left: (bounds.width / 100) * 40,  bottom: 0.0, right: 0.0)
        titleEdgeInsets = UIEdgeInsets(top: 0.0, left: (bounds.width - titleLabelWidth) / 2 - imageViewWidth, bottom: 0.0, right: 0.0)
    }
}*/
