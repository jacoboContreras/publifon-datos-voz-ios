//
//  HomeViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/20/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import Contacts
import FirebaseInstanceID
import FirebaseCore
import Firebase

class HomeViewController: UIViewController {
    
    // Class variables
    var vozMenuViewController: VozMenuViewController!
    var menuViewController: MenuViewController!
    var menuToggle = true
    var phoneNumber = ""
    public static var timer: Timer?
    var vozCouponObject: VozCouponModel?
    var couponType = ""
    var couponSelectedChoice = ""
    var sponsorImageURL: URL?
    var couponCode = ""
    var imageURL = ""
    var couponObject: CouponModel?
    var backgroundColor = ""
    var accentuationColor = ""
    var textColor = ""
    public static var isToastAcivated = false
    
    // Linked outlets
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var sponsorImageView: UIImageView!
    @IBOutlet weak var screenUIView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If service was not redeemed correctly then show advice toast
        if !Utils.getVozCouponObject().success {
            showToast(title: "Aviso", text: "Este servicio no se configuró correctamente.")
        }
        
        // This toast is activated from MainViewControllerf
        // isToastAcivated value is changed in MainViewController
        if HomeViewController.isToastAcivated {
            showToast(title: "Tu servicio de datos ha sido desactivado para poder realizar llamadas.", text: "Para volver a usar datos tendrás que activarlo de nuevo.")
            HomeViewController.isToastAcivated  = false
        }
        
        // Getting couponObject
        vozCouponObject = Utils.getVozCouponObject()
        
        // Getting given phone number
        phoneNumber = UserDefaults.standard.string(forKey: Utils.PHONE_NUMBER)!
        phoneNumberLabel.text = "Cupón válido hasta " + Utils.getReadableDate(date: Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.COUPON_EXPIRATION_DATE)!))
        
        // Back button si hidden unless coupon type
        // is combo
        backButton.isHidden = true
        // Setting side menu and swipe gesture to show it
        vozMenuViewController = (self.storyboard?.instantiateViewController(withIdentifier: "VozMenuViewController") as! VozMenuViewController)
        menuViewController = (self.storyboard?.instantiateViewController(withIdentifier: Utils.MENU_VIEW_CONTROLLER) as! MenuViewController)
        menuViewController.isCalledFromVoz = true
        
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        if couponType == Utils.CouponType.combo.rawValue {
            backButton.isHidden = false
        }
        
        // If coupon is not combo type then create an empty coupon object
        // for the system to have and not crash
        if couponType == Utils.CouponType.voz.rawValue
            || couponType == Utils.CouponType.choice.rawValue {
            // Gets an empty object
            couponObject = Utils.getCouponObject()
        }
        
        // Preparing slide menu
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeRight)
        self.view.addGestureRecognizer(swipeLeft)
        
        // Setting menu icon color
        let origImage = UIImage(named: "baseline_menu_black_18dp")
        let tintedImage = origImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        menuButton.setImage(tintedImage, for: .normal)
        menuButton.tintColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        phoneNumberLabel.textColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
        
        // Prepraring to check image changes URL
        let homeURL = Utils.VOZ_HOME_URL
        let campaignId = vozCouponObject!.campaignId + "/"
        let system = "ios/"
        let screenWidth = String(Int(UIScreen.main.nativeBounds.width))
        let screenHeight = String(Int(UIScreen.main.nativeBounds.height))
        let screenSize = screenWidth + "x" + screenHeight
        let jpg = ".jpg"
        imageURL = homeURL+campaignId+system+screenSize+jpg
        
        print("ImageURL:", imageURL)
        
        print("Screen campaignId:", campaignId)
        print("Screen Width:", screenWidth)
        print("Screen Height:", screenHeight)
        
        // Setting up sonspor image
        sponsorImageURL = URL(string: imageURL)
        self.sponsorImageView.sd_setImage(with: sponsorImageURL, placeholderImage: UIImage(named: ""))
        
        // Checking if image has changed, if it did download new image
        let lastDay = UserDefaults.standard.string(forKey: Utils.IMAGE_DAY_CHECK) ?? ""
        couponCode = UserDefaults.standard.string(forKey: Utils.VOZ_COUPON_CODE) ?? ""
        if lastDay != Utils.imageDayCheck() {
            UserDefaults.standard.set(Utils.imageDayCheck(), forKey: Utils.IMAGE_DAY_CHECK)
            checkImage(coupon: couponCode)
            
        }
        
        // If coupon is voz type or choice voz, start expiration timer
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            couponSelectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        if couponType == Utils.CouponType.voz.rawValue || couponSelectedChoice == Utils.CouponSelectedChoice.voz.rawValue{
            // Starting timer expiration checker
            HomeViewController.timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
        }
        
        // Asking for permission to fetch contacts in contacts tab
        let store  = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, error) in
            if let error = error {
                print("Failed to request contacts: ", error)
                return
            }
        }
        
        // If coupon is a combo type then customize application
        customizeApp()
        
    }
    
    // Custom toast
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // If coupon is a Combo type then get datos coupon object
    // and customize application
    func customizeApp() {
        if couponType == Utils.CouponType.combo.rawValue  || couponType == Utils.CouponType.choice.rawValue {
            couponObject = Utils.getCouponObject()
            
            // Getting customization colors
            backgroundColor = couponObject!.campaign.backgroungColor
            accentuationColor = couponObject!.campaign.accentuationColor
            textColor = couponObject!.campaign.textColor
            
            // Screen background
            screenUIView.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            
            // Menu button
            menuButton.tintColor = Utils.hexToColor(hexString: textColor)
            
            // Phone number label
            phoneNumberLabel.textColor = Utils.hexToColor(hexString: textColor)
            
            //Making floating button shape
            backButton.layer.cornerRadius = 30
            backButton.layer.masksToBounds = true
            backButton.backgroundColor = Utils.hexToColor(hexString: accentuationColor)
            backButton.layer.zPosition = 1
            
            // Setting floating button icon
            let icon = UIImage(named: "data_arrows")!
            backButton.setImage(icon, for: .normal)
            backButton.tintColor = Utils.hexToColor(hexString: textColor)
            backButton.imageView?.contentMode = .scaleAspectFit
            backButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    // Timer for checking service expirations dynamically
    @objc func timerAction() {
        // Check the overall coupon expiration
        checkForCouponExpiration()
        
    }
    
    // Checking for coupon expiration state
    func checkForCouponExpiration() {
        
        let hoursValid = Int(vozCouponObject!.hoursValid)
        // Getting coupon expiration date
        let expirationDate = Utils.convertStringToDate(stringDate: Utils.calculateExpirationDate(couponObject: self.couponObject!,serviceHrs: hoursValid!))
        let currentDate = Utils.convertStringToDate(stringDate: Utils.getCurrentDate())
        
        print(expirationDate)
        // If coupon is a voz choice coupon then also expire coupon from datos CRM
        
        if Utils.checkIfServiceHasExpired(currentDate: currentDate, expirationDate: expirationDate) {
            expireCoupon()

        }
        
    }
    
    // Expire voz coupon
    func expireCoupon() {
        // Stop timer
        HomeViewController.timer?.invalidate()
        // Deleting coupon state
        self.clearCoupon()
        // Sends user to insert view controller
        self.sendUserToInsertViewController()
        self.dismiss(animated: true, completion: nil)
    }
    
    // Expire datos coupon
//    func expireCoupon (coupon: String) {
//        // Making Request
//        Alamofire.request(configureExpirationRequest(coupon: coupon)).responseData {
//            dataResponse in
//
//            if dataResponse.result.isSuccess {
//                let json : JSON = JSON(dataResponse.result.value!)
//                _ = json[Utils.MESSAGE].string // not used for the moment
//                let status = json[Utils.STATUS].bool
//
//                if status! {
//                    // Telling Application that this coupon was expired
//                    UserDefaults.standard.set(false, forKey: Utils.PENDING_EXPIRATION)
//                    self.expireCoupon()
//                }
//
//            } else {
//                print("Error: \(dataResponse.result.error!)")
//                // Telling Application that this coupon has a pending expiration
//                UserDefaults.standard.set(true, forKey: Utils.PENDING_EXPIRATION)
//                self.expireCoupon()
//            }
//
//        }
//
//    }
    
    
    // Sends user to insert view controller
    func sendUserToInsertViewController() {
        //send to inser code vc
        /*let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let insertCodeViewController = mainStoryboard.instantiateViewController(withIdentifier: Utils.INSERT_CODE_VIEW_CONTROLLER) as! InsertCodeViewController
        appDelegate.window?.rootViewController = insertCodeViewController
        appDelegate.window?.makeKeyAndVisible()*/
        
        //send to menu redemtion
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: Utils.MAIN, bundle: nil)
        let MenuNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = MenuNavigationController.topViewController as! SeleccionDeRedencionViewController
        appDelegate.window?.rootViewController = MenuNavigationController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    // Clears coupon data stored in memory and sends back to insert view controller
    func clearCoupon() {
        resetVozCouponMemoryValues()
    }
    
    // Reset coupon memory state
    func resetVozCouponMemoryValues() {
        UserDefaults.standard.set(Utils.CouponType.none.rawValue, forKey: Utils.ACTIVE_COUPON_TYPE)
        UserDefaults.standard.set(Utils.CouponSelectedChoice.none.rawValue, forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.PHONE_NUMBER)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_JSON_KEY)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_COUPON_CODE)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.VOZ_HOME_IMAGE_NAME)
        UserDefaults.standard.set(Utils.RESET, forKey: Utils.IMAGE_DAY_CHECK)
        UserDefaults.standard.set(false, forKey: Utils.PENDING_SUSPENSION)
        UserDefaults.standard.set(false, forKey: Utils.COUPON_ACTIVE)
    }
    
    // Sends User back to MainViewController
    @IBAction func backButtonAction(_ sender: Any) {
        MainViewController.animateCallButtonTimer?.invalidate()
        TabBarController.delegate?.startAnimatingCallButton(data: "Animate call button")
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func menuButtonAction(_ sender: Any) {
        
        if menuToggle {
            if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
                showDatosMenu()
            } else {
                showMenu()
            }
            
        } else {
            if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
                hideDatosMenu()
            } else {
                hideMenu()
            }
            
        }
        
    }
    
    // Shows side menu
    func showMenu() {
        // Setting menu animation
        self.vozMenuViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.vozMenuViewController.view.alpha = 0
        self.addChildViewController(self.vozMenuViewController)
        self.view.addSubview(self.vozMenuViewController.view)
        
        UIView.animate(withDuration: 0.3){  () -> Void in
            self.vozMenuViewController.view.frame = CGRect(x: 0, y: 0,
                                                        width: UIScreen.main.bounds.size.width,
                                                        height: UIScreen.main.bounds.size.height)
            self.vozMenuViewController.view.alpha = 1
        }
        self.menuToggle = false
    }
    
    func showDatosMenu() {
        // Setting menu animation
        self.menuViewController.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.menuViewController.view.alpha = 0
        self.addChildViewController(self.menuViewController)
        self.view.addSubview(self.menuViewController.view)
        
        UIView.animate(withDuration: 0.3){  () -> Void in
            self.menuViewController.view.frame = CGRect(x: 0, y: 0,
                                                           width: UIScreen.main.bounds.size.width,
                                                           height: UIScreen.main.bounds.size.height)
            self.menuViewController.view.alpha = 1
        }
        self.menuToggle = false
    }
    
    // Hides side menu
    func hideMenu() {
        // Setting menu animation
        UIView.animate(withDuration: 0.3, animations: {  () -> Void in
            self.vozMenuViewController.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0,
                                                        width: UIScreen.main.bounds.size.width,
                                                        height: UIScreen.main.bounds.size.height)
        }) {(finished) in
            self.vozMenuViewController.view.removeFromSuperview()
        }
        menuToggle = true
    }
    
    // Hides datops side menu
    func hideDatosMenu() {
        // Setting menu animation
        UIView.animate(withDuration: 0.3, animations: {  () -> Void in
            self.menuViewController.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0,
                                                           width: UIScreen.main.bounds.size.width,
                                                           height: UIScreen.main.bounds.size.height)
        }) {(finished) in
            self.menuViewController.view.removeFromSuperview()
        }
        menuToggle = true
    }
    
    // Shows side menu when swipe gesture is made
    @objc func respondToGesture(gesture: UISwipeGestureRecognizer){
        switch gesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue {
                if menuToggle == true{showDatosMenu()}
            } else {
                if menuToggle == true{showMenu()}
            }
            
        default:
            break
        }
    }
    
    // Hides side menu when clicked outsude range
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
            if touch?.view != menuViewController.menuTableView
                && touch?.view != menuViewController.viewContainer
                &&  touch?.view != menuViewController.menuLabelContainer {
                hideDatosMenu()
            }
        } else {
            if touch?.view != self.vozMenuViewController.menuTableView && touch?.view != self.vozMenuViewController.menuLabelContainerView {
                hideMenu()
            }
        }
        
        
    }
    
    // Supend coupon logic
    func checkImage (coupon: String) {
        // Making Request
        Alamofire.request(configureRequest(coupon: coupon)).responseData {
            dataResponse in
            if dataResponse.result.isSuccess {
                if let data = dataResponse.data, let utf8Text = String(data: data, encoding: .utf8) {
                    let savedImage: String = UserDefaults.standard.string(forKey: Utils.VOZ_HOME_IMAGE_NAME) ?? ""
                    print("Dispatch 1", savedImage)
                    print("Dispatch 2", utf8Text)
                    if savedImage != utf8Text {
                        DispatchQueue.main.async {
                            // Clearing up cache web image cache
                            SDImageCache.shared().clearMemory()
                            SDImageCache.shared().clearDisk()
                            print("Dispatch 3", savedImage)
                            self.sponsorImageView.sd_setImage(with: self.sponsorImageURL, placeholderImage: UIImage(named: ""))
                            UserDefaults.standard.set(utf8Text, forKey: Utils.VOZ_HOME_IMAGE_NAME)
                        }
                    }
                }
            } else {
                print("Error: \(dataResponse.result.error!)")
            }
        }
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(coupon: String) -> URLRequest {
        
        // Getting corresponding url
        let urlRequest = Utils.VOZ_IMAGE_URL//"https://publifonapp.com/v3_beta/secciones/POST_getImageFromCoupon.php"
        // This code escapes unwanted characters
        let url : NSString = urlRequest as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let expireURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: expireURL as URL)
        request.httpMethod = Utils.POST
        //request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        //request.timeoutInterval = 2 // 2 secs
        request.timeoutInterval = 60
        let couponCode = "CouponCode"
        let postString = "\(couponCode)=\(coupon)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    func configureExpirationRequest(coupon: String)  -> URLRequest {
        // Getting API and Device Token
        let apiToken = UserDefaults.standard.string(forKey: Utils.API_TOKEN)!
        let deviceToken = getFCMToken()
        
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = Utils.EXPIRE_COUPON_URL + coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let expireURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: expireURL as URL)
        request.httpMethod = Utils.POST
        request.setValue(Utils.APPLICATION_JSON, forHTTPHeaderField: Utils.ACCEPT)
        request.setValue(apiToken, forHTTPHeaderField: Utils.AUTHORIZATION)
        //request.timeoutInterval = 2 // 5 secs
        request.timeoutInterval = 60
        let postString = "\(Utils.API_DEVICE_TOKEN)=\(deviceToken)"
        request.httpBody = postString.data(using: .utf8)
        
        return request
    }
    
    // Get FCM Token
    func getFCMToken() -> String {
        if let token = Messaging.messaging().fcmToken {
            // saving current fcmToken
            return token
        } else {
            return ""
        }
    }

}
