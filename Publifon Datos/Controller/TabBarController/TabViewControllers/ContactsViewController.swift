//
//  ContactsViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/20/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Contacts

class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    // Lonked outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet var screenUIView: UIView!
    
    // Class variables
    var contacts = [Contact]()
    var sortedContacts = [Contact]()
    var searchedContact = [Contact]()
    var isSearching = false
    var timer: Timer?
    var couponObject: CouponModel?
    var couponType = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If coupon type is combo then customize search bar
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
            couponObject = Utils.getCouponObject()
            searchBar.searchBarStyle = .minimal
            searchBar.backgroundColor = Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
//            searchBar.layer.borderWidth = StandarColorAndForms.borderWidht
//            searchBar.layer.borderColor = Utils.hexToColor(hexString: (couponObject?.campaign.accentuationColor)!)
            
            screenUIView.backgroundColor = Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
            contactsTableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
        }
        
        // Assigning delegates
        contactsTableView.delegate = self
        contactsTableView.dataSource = self
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
        
        
        
        
        // Clears bottom lines of extra table view items
        contactsTableView.tableHeaderView = UIView()
        contactsTableView.tableFooterView = UIView()
        contactsTableView.alwaysBounceVertical = false
        
        // Gets a sorted list of contacts
        sortedContacts = fetchContact()
       
        // Registers a ContactTableViewCell to contactsTableView
        self.contactsTableView.register(UINib(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "contactTableViewCell")

    }
    
    // Fetches and sorts alphabetically a list of contacts
    private func fetchContact() -> [Contact] {
        
        // Removing elements to avoid duplicate cotacts
        contacts.removeAll()
        
        // Requesting permision to access user contacts
        let store  = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, error) in
            if let error = error {
                print("Failed to request contacts: ", error)
                return
            }
            
            // If we have permission go ahead and fetch user contacts
            if granted {
                print("Access to user contacts granted...")
                // Asking system for contact property keys
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataAvailableKey, CNContactImageDataKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    
                    try store.enumerateContacts(with: request, usingBlock:
                    { (contact, stopPointerIfYouWantToStopEnumerating) in
                        var numbers = [Numbers]()
                        
                        // Checking if user contact has an image and getting it
                        // if not assign a default contact image
                        var image = UIImage()
                        if contact.imageDataAvailable && contact.imageData != nil  {
                            image = UIImage(data: contact.imageData!)!
                        } else {
                            image = UIImage(named: "user")!
                        }
                        
                        //Getting all contact numbers
                        for phoneNumber in contact.phoneNumbers {
                            if let number = phoneNumber.value as? CNPhoneNumber,
                                let label = phoneNumber.label {
                                let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                                numbers.append(Numbers(name: localizedLabel, number: number.stringValue))
                            }
                        }
                        print("Access contact name:", contact.givenName)
                        // Adding contact to a list of contacts
                        self.contacts.append(Contact(name: contact.givenName, lastName: contact.familyName, numbers: numbers, image: image))
                        
                    })
                    
                } catch let err {
                    print("Failed to enumerate contacts: ", err)
                }
                
            } else {
                print("Access denied...")
            }
        }
        
        // return a sorted list of contacts by alphabet
        return self.contacts.sorted { $0.name.lowercased() < $1.name.lowercased()}
    }
    
    // Table view size
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows()
    }
    
    // Returns number of rows depending on which table view is shown. (Searching, Sorted)
    func numberOfRows() -> Int {
        if isSearching {
            return searchedContact.count
        } else {
            return sortedContacts.count
        }
    }
    
    // Table view cell populate
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactsTableView.dequeueReusableCell(withIdentifier: "contactTableViewCell", for: indexPath) as! ContactTableViewCell
        var name = ""
        
        // Setting no selection style for table cell
        cell.selectionStyle = .none
        
        // Table view behavior depending on table view mode
        if isSearching {
            name = searchedContact[indexPath.row].name + " " + searchedContact[indexPath.row].lastName
            cell.contactLabel.text = name
            cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.size.width/2
            cell.contactImageView.clipsToBounds = true
            cell.contactImageView.image = searchedContact[indexPath.row].image
//            cell.callImageView.image = cell.callImageView.image?.withRenderingMode(.alwaysTemplate)
//            cell.callImageView.tintColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//
            //to change contact image tint
//            let originalImage = searchedContact[indexPath.row].image
//            let tintedImage = originalImage.withRenderingMode(.alwaysTemplate)
//            cell.contactImageView.image = tintedImage
//            cell.contactImageView.tintColor = Utils.hexToColor(hexString: ((couponObject?.campaign.accentuationColor)!))
            
            //to change contact image tint
            Utils.changeContactIconColor(imageView: cell.contactImageView,
                                         image: searchedContact[indexPath.row].image,
                                         couponObject: couponObject)
            
        } else {
            name = sortedContacts[indexPath.row].name + " " + sortedContacts[indexPath.row].lastName
            cell.contactLabel.text = name
            cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.size.width/2
            cell.contactImageView.clipsToBounds = true
            cell.contactImageView.image = sortedContacts[indexPath.row].image
//            cell.callImageView.image = cell.callImageView.image?.withRenderingMode(.alwaysTemplate)
//            cell.callImageView.tintColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
          
            //to change contact image tint
            Utils.changeContactIconColor(imageView: cell.contactImageView,
                                         image: sortedContacts[indexPath.row].image,
                                         couponObject: couponObject)
            
        }
        
        // Customize Cell background if necessary
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
        }
        
        return cell
    }
    
    var currentContactIndex = 0;
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isVPNRunning() {
            //showToast(title: "Aviso", text: Utils.DISCONNECT_VPN)
            MainViewController.vpnNotification = true
            self.dismiss(animated: true, completion: nil)
            return
        }
        print("Selected row: ", indexPath.row)
        currentContactIndex = indexPath.row
        // Perform segue in main thread, if not, it has a delay
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: "goToNumberSelectionViewController",sender: self)
        }
    }
    
    // Checks if VPN Running
    func isVPNRunning() -> Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") {
                    return true
                }
            }
        }
        return false
    }
    
    // Custom toast
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
    
    // Searches user input in name and last name properties
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedContact = sortedContacts.filter({$0.name.lowercased().prefix(searchText.count) == searchText.lowercased() || $0.lastName.lowercased().prefix(searchText.count) == searchText.lowercased()})
        isSearching = true
        contactsTableView.reloadData()
    }
    
    // Search bar cancel button, resets search bar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = Utils.RESET
        contactsTableView.reloadData()
    }
    
    // Hides keyboard if accept button is clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToNumberSelectionViewController" {
            let numberSelectionViewController = segue.destination as! NumberSelectionViewController
            if isSearching {
                // Searched array
                numberSelectionViewController.contactName = searchedContact[currentContactIndex].name + " " + searchedContact[currentContactIndex].lastName
                numberSelectionViewController.contactNumbersArray = searchedContact[currentContactIndex].numbers
                numberSelectionViewController.contactImage = searchedContact[currentContactIndex].image
            } else {
                // Sorted array
                numberSelectionViewController.contactName = sortedContacts[currentContactIndex].name + " " + sortedContacts[currentContactIndex].lastName
                numberSelectionViewController.contactNumbersArray = sortedContacts[currentContactIndex].numbers
                numberSelectionViewController.contactImage = sortedContacts[currentContactIndex].image
                
            }
        }
    }  
    
}


