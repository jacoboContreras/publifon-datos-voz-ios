//
//  CallViewController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/20/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CallViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    // Linked outlets
    @IBOutlet weak var recentCallsLabel: UILabel!
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var numberInputLabel: UILabelPadding!
    @IBOutlet weak var buttonOneOutlet: UIButton!
    @IBOutlet weak var buttonTwoOutlet: UIButton!
    @IBOutlet weak var buttonThreeOutlet: UIButton!
    @IBOutlet weak var buttonFourOutlet: UIButton!
    @IBOutlet weak var buttonFiveOutlet: UIButton!
    @IBOutlet weak var buttonSixOutlet: UIButton!
    @IBOutlet weak var buttonSevenOutlet: UIButton!
    @IBOutlet weak var buttonEightOutlet: UIButton!
    @IBOutlet weak var buttonNineOutlet: UIButton!
    @IBOutlet weak var buttonZeroOutlet: UIButton!
    @IBOutlet weak var buttonAsteriskOutlet: UIButton!
    @IBOutlet weak var buttonHashOutlet: UIButton!
    @IBOutlet weak var callButtonOutlet: UIButton!
    @IBOutlet weak var deleteButtonOutlet: UIButton!
    @IBOutlet var screenUIView: UIView!
    
    // Class variables
    var recentCalls = [RecentCalls]()
    var searchedContact = [Contact]()
    var contacts = [Contact]()
    var isSearching = false
    var cleanedPhoneNumber = ""
    var currentContactIndex = 0;
    var searchIndex = 0
    var timerTableUpdater: Timer?
    var couponObject: CouponModel?
    var backgroundColor = ""
    var accentuationColor = ""
    var textColor = ""
    var couponType = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Getting coupon type
        couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
            couponObject = Utils.getCouponObject()
            screenUIView.backgroundColor = Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
            recentCallsLabel.backgroundColor = Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
            contactsTableView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
        }

        // Setting table view delegates
        contactsTableView.delegate = self
        contactsTableView.dataSource = self

        // Clears bottom lines of extra table view items
        contactsTableView.tableFooterView = UIView()
        contactsTableView.alwaysBounceVertical = false
        
        // Updates contacts table view when a successful call is made
        NotificationCenter.default.addObserver(self, selector: #selector(updateContactsTable), name: NSNotification.Name(rawValue: "load"), object: nil)

        // Labels setup
        numberInputLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
        numberInputLabel.backgroundColor = .white
        numberInputLabel.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        numberInputLabel.layer.cornerRadius = 4
        numberInputLabel.layer.borderWidth = 1.0

        // Buttons styles
        styleButton(button: buttonOneOutlet, number: "  1", letters: "")
        styleButton(button: buttonTwoOutlet, number: "  2\n", letters: "abc")
        styleButton(button: buttonThreeOutlet, number: "  3\n", letters: "def")
        styleButton(button: buttonFourOutlet, number: "  4\n", letters: "ghi")
        styleButton(button: buttonFiveOutlet, number: "  5\n", letters: "jkl")
        styleButton(button: buttonSixOutlet, number: "  6\n", letters: "mno")
        styleButton(button: buttonSevenOutlet, number: "  7\n", letters: "pqrs")
        styleButton(button: buttonEightOutlet, number: "  8\n", letters: "tuv")
        styleButton(button: buttonNineOutlet, number: "  9\n", letters: "wxyz")
        styleButton(button: buttonZeroOutlet, number: "  0", letters: "")
        styleButton(button: buttonAsteriskOutlet, number: "  *", letters: "")
        styleButton(button: buttonHashOutlet, number: "  #", letters: "")
        styleCallButton(button: callButtonOutlet)

        // Setting delete button icon color
        let deleteImage = UIImage(named: "backspace")
        let deleteTintedImage = deleteImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        deleteButtonOutlet.setImage(deleteTintedImage, for: .normal)
                
        // Setting call button icon color
        let callImage = UIImage(named: "contact_cell_call")
        let callTintedImage = callImage?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        callButtonOutlet.setImage(callTintedImage, for: .normal)
        callButtonOutlet.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        // Getting recent calls array
        recentCalls = loadRecentCalls()

        // Getting user contacts
        contacts = Utils.fetchContact()
        
        print("contacts:", contacts)

        // Registers a ContactTableViewCell to contactsTableView
        self.contactsTableView.register(UINib(nibName: "RecentCallsTableViewCell", bundle: nil), forCellReuseIdentifier: "recentCallsTableViewCell")

        // Reloading table view content if not it will not show recent made calls
        contactsTableView.reloadData()
        
        // If coupon is a combo type then customize application
        customizeApp()
    }
    
    // Contacts table view updater
    @objc func updateContactsTable() {
        self.recentCalls = loadRecentCalls()
        self.contactsTableView.reloadData()
    }
    
    // Styles button views
    func styleButton(button: UIButton, number: String, letters: String) {
        button.backgroundColor = .white
        button.layer.cornerRadius = 2
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        //applying the line break mode
        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        button.setTitle(number+letters, for: .normal)
        
    }
    
    // Call button style
    func styleCallButton(button: UIButton) {
        button.backgroundColor = #colorLiteral(red: 0.167394314, green: 0.8376824239, blue: 0.4227770789, alpha: 1)
        button.layer.cornerRadius = 20
        button.layer.borderWidth = 1
        button.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    }
    
    // Listens to changes in numberInputLabel
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        let searchedNumber = self.numberInputLabel.getNewText()
        searchedContact.removeAll()
        searchIndex = 0
        
        // Searches for input number
        for contact in self.contacts {
            for (index, number) in contact.numbers.enumerated() {
                
                if searchedNumber.count <= number.parseNumber().count {
                    let index1 = number.parseNumber().index(number.parseNumber().startIndex, offsetBy: searchedNumber.count)
                    
                    let substring1 = number.parseNumber()[..<index1]
                    
                    if substring1 == searchedNumber {
                        searchedContact.insert(contact, at: 0)
                        print("Substring: ", substring1)
                        searchIndex = index
                        break
                    }
                }
            }
            
        }
        
        // This variable helps to know which list to show
        isSearching = true
        
        recentCallsLabel.isHidden = true
        // Updating contacts table view
        contactsTableView.reloadData()
        
    }
    
    //  Button actions
    @IBAction func numberOneButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "1"
    }
    
    @IBAction func numberTwoButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "2"
    }
    
    @IBAction func numberThreeButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "3"
    }
    
    @IBAction func numberFourButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "4"
    }
    
    @IBAction func numberFiveButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "5"
    }
    
    @IBAction func numberSixButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "6"
    }
    @IBAction func numberSevenButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "7"
    }
    
    @IBAction func numberEightButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "8"
    }
    
    @IBAction func numberNineButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "9"
    }
    
    @IBAction func numberZeroButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "0"
    }
    
    @IBAction func asteriskButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "*"
    }
    
    @IBAction func hashButtonAction(_ sender: Any) {
        numberInputLabel.text = (numberInputLabel.text ?? "") + "#"
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        if isVPNRunning() {
            //showToast(title: "Aviso", text: Utils.DISCONNECT_VPN)
            MainViewController.vpnNotification = true
            self.dismiss(animated: true, completion: nil)
            return
        }
        let cleanedPhoneNumber = Utils.cleanPhoneNumber(phoneNumber: numberInputLabel.text!)
        if cleanedPhoneNumber.count != 0 {
            self.cleanedPhoneNumber = cleanedPhoneNumber
            performSegue(withIdentifier: "goToConnectingViewController", sender: self)            
        } else {
            showToast(title: "Aviso", text: Utils.INVALID_NUMBER)
        }
    }
    
    // Checks if VPN Running
    func isVPNRunning() -> Bool {
        if let settings = CFNetworkCopySystemProxySettings()?.takeRetainedValue() as? Dictionary<String, Any>,
            let scopes = settings["__SCOPED__"] as? [String:Any] {
            for (key, _) in scopes {
                if key.contains("tap") || key.contains("tun") || key.contains("ppp") {
                    return true
                }
            }
        }
        return false
    }
    
    @IBAction func deleteButtonAction(_ sender: Any) {
        // Deleting last character
        numberInputLabel.text = String(numberInputLabel.text!.dropLast())
        
        if numberInputLabel.text == "" {
            print("Text input has nothing!...")
            self.isSearching = false
            self.recentCallsLabel.isHidden = false
            //self.isRecentCallsActive = false
            self.contactsTableView.reloadData()
        }
        
    }
    
    // Table view row numbers
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfRows()
    }
    
    // Returns number of rows depending on which table view is shown. (Searching, recent calls)
    func numberOfRows() -> Int {
        if isSearching/* && !isRecentCallsActive*/ {
            return searchedContact.count
        } else {
            return recentCalls.count
        }
    }
    
    // Table view cells setup
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactsTableView.dequeueReusableCell(withIdentifier: "recentCallsTableViewCell", for: indexPath) as! RecentCallsTableViewCell
        
        // Setting no selection style for table cell
        cell.selectionStyle = .none
        
        if isSearching {
            cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.size.width/2
            cell.contactImageView.clipsToBounds = true
            cell.contactImageView.image = searchedContact[indexPath.row].image
            
            // If there is no image in array then assign a default image
            if searchedContact[indexPath.row].image.size == .zero {
                cell.contactImageView.image = UIImage(named: "user")!
                print("lllll", " 1 has nothing...")
            }
            
            Utils.changeContactIconColor(imageView: cell.contactImageView,
            image: searchedContact[indexPath.row].image,
            couponObject: couponObject)
            
            // Showing number depending on search numbers
            cell.contactNameLabel.text = searchedContact[indexPath.row].name
            if searchedContact[indexPath.row].numbers.count > searchIndex {
                cell.callTimeLabel.text = searchedContact[indexPath.row].numbers[searchIndex].number
            } else {
                cell.callTimeLabel.text = searchedContact[indexPath.row].numbers[0].number
            }
            
        } else {
            
            cell.contactImageView.layer.cornerRadius = cell.contactImageView.frame.size.width/2
            cell.contactImageView.clipsToBounds = true
            cell.contactImageView.image = recentCalls[indexPath.row].image
            
//            // If there is no image in array then assign a default image
            if recentCalls[indexPath.row].image.size == .zero {
                print("lllll", "2 has nothing...")
                cell.contactImageView.image = UIImage(named: "user")!

            }
            
            Utils.changeContactIconColor(imageView: cell.contactImageView,
            image: recentCalls[indexPath.row].image,
            couponObject: couponObject)
            
            cell.contactNameLabel.text = recentCalls[indexPath.row].name
            //get readableDate
            let time = Utils.convertStringToDate(stringDate:recentCalls[indexPath.row].time)
            let timeDate =  Utils.getReadableDate(date: time)
            cell.callTimeLabel.text = recentCalls[indexPath.row].number + "   " +  timeDate
            
            
            
        }
        
        
        // Customize Cell background if necessary
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue  {
            cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0) //Utils.hexToColor(hexString: (couponObject?.campaign.backgroungColor)!)
        }
        
        
        if couponType == Utils.CouponType.voz.rawValue{
            Utils.changeDefaultContactIconColor(imageView: cell.contactImageView,
                                         image: cell.contactImageView.image!)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isVPNRunning() {
            //showToast(title: "Aviso", text: Utils.DISCONNECT_VPN)
            MainViewController.vpnNotification = true
            self.dismiss(animated: true, completion: nil)
            return
        }
        currentContactIndex = indexPath.row
        //isRecentCallsActive = true
        self.performSegue(withIdentifier: "goToNumberSelectionViewController",sender: self)
    }
    
    // Gets an array of recent calls
    func loadRecentCalls() -> [RecentCalls] {
        let recentCallsData = UserDefaults.standard.object(forKey: "places") as? NSData

        // Gets an array of recent saved calls
        if let recentCallsData = recentCallsData {
            let recentCalls = NSKeyedUnarchiver.unarchiveObject(with: recentCallsData as Data) as? [RecentCalls]

            // If there are recent calls return an array of them
            if let recentCalls = recentCalls {
                print("recent calls roy")
                return recentCalls
            }

        }
        // Return an empty array if there are no recent calls
        print("fail recent calls roy")
        return [RecentCalls]()
        
    }
    
    // Preparing for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToNumberSelectionViewController" {
            let numberSelectionViewController = segue.destination as! NumberSelectionViewController
            if self.isSearching {
                numberSelectionViewController.contactName = searchedContact[currentContactIndex].name
                numberSelectionViewController.contactNumbersArray = searchedContact[currentContactIndex].numbers
                numberSelectionViewController.contactImage = searchedContact[currentContactIndex].image
            } else {
                // Recent calls array
                numberSelectionViewController.contactName = recentCalls[currentContactIndex].name
                numberSelectionViewController.contactNumbersArray = [Numbers(name: "", number: recentCalls[currentContactIndex].number)]
                numberSelectionViewController.contactImage = recentCalls[currentContactIndex].image
            }
        }
        
        if segue.identifier == "goToConnectingViewController" {
            let connectingViewController = segue.destination as! ConnectingViewController
            connectingViewController.contactName = "Desconocido"
            connectingViewController.phoneNumber = self.cleanedPhoneNumber
            connectingViewController.contactImage = UIImage(named: "user")!
        }
        
    }
    
    // If coupon is a Combo type then get datos coupon object
    // and customize application
    func customizeApp() {
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue {
            couponObject = Utils.getCouponObject()
            
            // Getting customization colors
            backgroundColor = couponObject!.campaign.backgroungColor
            accentuationColor = couponObject!.campaign.accentuationColor
            textColor = couponObject!.campaign.textColor
            
            numberInputLabel.textColor = Utils.hexToColor(hexString: textColor)
            
            numberInputLabel.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            recentCallsLabel.textColor = Utils.hexToColor(hexString: textColor)
            
            buttonOneOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonOneOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonTwoOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonTwoOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonThreeOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonThreeOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonFourOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonFourOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonFiveOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonFiveOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonSixOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonSixOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonSevenOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonSevenOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonEightOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonEightOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonNineOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonNineOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonZeroOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonZeroOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonAsteriskOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonAsteriskOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            buttonHashOutlet.backgroundColor = Utils.hexToColor(hexString: backgroundColor)
            buttonHashOutlet.setTitleColor(Utils.hexToColor(hexString: textColor), for: .normal)
            
            if couponType == Utils.CouponType.voz.rawValue{
                deleteButtonOutlet.tintColor = #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1)
            }else{
                deleteButtonOutlet.tintColor = Utils.hexToColor(hexString: accentuationColor)
            }
            
        }
    }
    
    // Toast code
    func showToast(title: String, text: String) {
        let messageVC = UIAlertController(title: title, message: text , preferredStyle: .actionSheet)
        present(messageVC, animated: true) {
            Timer.scheduledTimer(withTimeInterval: 6, repeats: false, block: { (_) in
                messageVC.dismiss(animated: true, completion: nil)})}
    }
}

