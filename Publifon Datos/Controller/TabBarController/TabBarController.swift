//
//  TabBarController.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/20/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

protocol AnimateCallButton {
    func startAnimatingCallButton(data: String)
}

class TabBarController: UITabBarController {
    
    // Class Constants
    public static var couponObject: CouponModel?
    public static var staticBackgroundColor = ""
    public static var staticAccentuationColor = ""
    public static var staticTextColor = ""
    
    // Class Variables
    public static var delegate: AnimateCallButton?
        
    var areDynamicLinkRedentionError = false
    var dynamicLinkRedentionErrorMessage = String()
    
    //@IBOutlet weak var homeLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // If coupon is only voz
        UITabBar.appearance().barTintColor = .white
        self.tabBar.unselectedItemTintColor =  .gray
        self.tabBar.tintColor = .blue
        TabBarController.staticBackgroundColor = ""
        TabBarController.staticAccentuationColor = ""
        TabBarController.staticTextColor = ""
        
        // If coupon is a combo type the customize tab bar 
        let couponType = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
        if couponType == Utils.CouponType.combo.rawValue || couponType == Utils.CouponType.choice.rawValue {
            TabBarController.couponObject = Utils.getCouponObject()
            // Getting customization colors
            TabBarController.staticBackgroundColor = TabBarController.couponObject!.campaign.backgroungColor
            TabBarController.staticAccentuationColor = TabBarController.couponObject!.campaign.accentuationColor
            TabBarController.staticTextColor = TabBarController.couponObject!.campaign.textColor
            
            UITabBar.appearance().barTintColor = Utils.hexToColor(hexString: TabBarController.staticBackgroundColor)
            self.tabBar.unselectedItemTintColor =  Utils.hexToColor(hexString: TabBarController.staticTextColor)
            self.tabBar.tintColor = Utils.hexToColor(hexString: TabBarController.staticAccentuationColor)
        }
    
        DispatchQueue.main.async { [self] in
            if self.areDynamicLinkRedentionError {
                Alerts().alert(Title: "Error", Message: self.dynamicLinkRedentionErrorMessage, viewController: self)
            }
        }
    }
    

}
