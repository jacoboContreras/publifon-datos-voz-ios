//
//  TermsConditionsViewController.swift
//  Publifon Datos
//
//  Created by Jacobo on 2/7/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import UIKit
import WebKit
import Lottie

class TermsConditionsViewController: UIViewController {
    
    //Outlets
    
    
    @IBOutlet weak var termsConditionsWebView: WKWebView!
    @IBOutlet weak var radiusAgreeButton: UIButton!
    @IBOutlet weak var agreeButton: UIButton!
    @IBOutlet weak var agreeLabel: UIButton!
    
    
    //var
    var agreeTerms = false
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Custom view
      
        agreeButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        
        //Change tint color to radius button image
        let origImage = UIImage(named: "radio_button_unchecked_black_18dp")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        radiusAgreeButton.setImage(tintedImage, for: .normal)
        radiusAgreeButton.tintColor = StandarColorAndForms.grayColor
        
        
        termsConditionsWebView.contentMode = .scaleAspectFit
        termsConditionsWebView.load(URLRequest(url: URL(string: "https://publifonapp.com/aviso_privacidad.html")!))
        termsConditionsWebView.layer.borderColor = StandarColorAndForms.grayColor.cgColor
        termsConditionsWebView.layer.borderWidth = 2
        
        agreeButton.cornerRadius = CGFloat(StandarColorAndForms.cornerRadius)
        agreeButton.borderWidth = CGFloat(StandarColorAndForms.borderWidht)
        agreeButton.backgroundColor = .none
        agreeButton.borderColor = StandarColorAndForms.grayColor
        agreeButton.setTitleColor(StandarColorAndForms.grayColor, for: .normal)
        
        
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        }        
    }
    
    
    @IBAction func agreeTermsCondition(_ sender: UIButton) {
        
        
        if agreeTerms == false{
            
            let origImage = UIImage(named: "radio_button_checked_black_18dp")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            radiusAgreeButton.setImage(tintedImage, for: .normal)
            agreeButton.isEnabled = true
            agreeButton.borderColor = .black
            agreeButton.titleLabel?.textColor = .black
            agreeTerms = true
            
        }else{
            
            let origImage = UIImage(named: "radio_button_unchecked_black_18dp")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            radiusAgreeButton.setImage(tintedImage, for: .normal)
            agreeButton.isEnabled = false
            agreeButton.borderColor = StandarColorAndForms.grayColor
        agreeButton.setTitleColor(StandarColorAndForms.grayColor, for: .normal)
            agreeTerms = false
            
        }
        
        
        
        
        
    }
    
    @IBAction func continueNextView(_ sender: UIButton) {
             
        //Go to insert code view
        /*let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

        let insertCodeViewController = storyBoard.instantiateViewController(withIdentifier: "InsertCodeViewController") as! InsertCodeViewController
        insertCodeViewController.initialTimeApp = true
       
        
        //Change state of first launch app
        
        UserDefaults.standard.set(true, forKey: "openedBefore")
        UserDefaults.standard.synchronize()
        
       
         //presente InsertCodeViewController
         self.present(insertCodeViewController, animated: true, completion: nil)*/
        
        //go to redention option menu
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! MenuNavigationController
        
        
        //Change state of first launch app
        
        UserDefaults.standard.set(true, forKey: "openedBefore")
        UserDefaults.standard.synchronize()
        
        
        //presente InsertCodeViewController
        self.present(menuNavigation, animated: true, completion: nil)

    }
}
