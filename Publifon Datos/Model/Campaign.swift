//
//  Campaign.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/28/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class Campaign {
    var mbMaximum: Int
    var startDateEs: String
    var media: String
    var startDate: String
    var id: Int
    var mediaType: String
    var termsCondition : String
    var endDateEs: String
    var name: String
    var backgroungColor: String
    var endDate: String
    var accentuationColor: String
    var textColor: String
    
    init(mbMaximum: Int, startDateEs: String, media: String, startDate: String, id: Int,
         mediaType: String, termsCondition: String, endDateEs: String, name: String, backgroungColor: String,
         endDate: String, accentuationColor: String, textColor: String) {
        self.mbMaximum = mbMaximum
        self.startDateEs = startDateEs
        self.media = media
        self.startDate = startDate
        self.id = id
        self.mediaType = mediaType
        self.termsCondition = termsCondition
        self.endDateEs = endDateEs
        self.name = name
        self.backgroungColor = backgroungColor
        self.endDate = endDate
        self.accentuationColor = accentuationColor
        self.textColor = textColor
    }
}
