//
//  RecentCalls.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/23/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation
import UIKit

class RecentCalls: NSObject, NSCoding {
    var name: String
    var number: String
    var time: String
    var image: UIImage
    
    init(name: String, number: String, time: String, image: UIImage) {
        self.name = name
        self.number = number
        self.time = time
        self.image = image
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.number = aDecoder.decodeObject(forKey: "number") as? String ?? ""
        self.time = aDecoder.decodeObject(forKey: "time") as? String ?? ""
        self.image = aDecoder.decodeObject(forKey: "image") as? UIImage ?? UIImage()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(number, forKey: "number")
        aCoder.encode(time, forKey: "time")
        aCoder.encode(image, forKey: "image")
    }
    
}
