//
//  VozCouponModel.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/9/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class VozCouponModel {
    
    var endingDate: String
    var homeURL: String
    var campaignId: String
    var hoursValid: String
    var success: Bool
    
    init(endingDate: String, homeURL: String, campaignId: String, hoursValid: String, success: Bool) {
        self.endingDate = endingDate
        self.homeURL = homeURL
        self.campaignId = campaignId
        self.hoursValid = hoursValid
        self.success = success
    }
}
