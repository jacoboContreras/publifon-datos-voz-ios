//
//  CouponModel.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/27/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class CouponModel {
    var status: Bool
    var message: String
    var vpnData: VPNData
    var servers: Servers
    var company: Company
    var campaign: Campaign
    var applications: [Applications]
    
    init(status: Bool, message: String, vpnData: VPNData, servers: Servers, company: Company, campaign: Campaign, applications: [Applications]) {
        self.status = status
        self.message = message
        self.vpnData = vpnData
        self.servers = servers
        self.company = company
        self.campaign = campaign
        self.applications = applications
    }
    
}
