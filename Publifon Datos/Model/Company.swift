//
//  Company.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/28/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class Company {
    var name: String
    var logoURL: String
    
    init(name: String, logoURL: String) {
        self.name = name
        self.logoURL = logoURL
    }
}


