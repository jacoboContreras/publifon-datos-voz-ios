//
//  Applications.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/28/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class Applications {
    var androidPackageNames: [String]
    var iOSPackageNames: [String]
    var id: Int
    var hours: Int
    var maxMegabytes: Int
    var name: String
    var logoURL: String
    
    init(androidPackageNames: [String], iOSPackageNames: [String], id: Int, hours: Int, maxMegabytes: Int, name: String, logoURL: String) {
        self.androidPackageNames = androidPackageNames
        self.iOSPackageNames = iOSPackageNames
        self.id = id
        self.hours = hours
        self.maxMegabytes = maxMegabytes
        self.name = name
        self.logoURL = logoURL
    }
    
    init(id: Int, hours: Int, maxMegabytes: Int, name: String) {
        self.androidPackageNames = [""]
        self.iOSPackageNames = [""]
        self.id = id
        self.hours = hours
        self.maxMegabytes = maxMegabytes
        self.name = name
        self.logoURL = ""
    }

}
