//
//  VozMenuTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/25/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class VozMenuTableViewCell: UITableViewCell {
    @IBOutlet weak var menuItem: UILabel!
    @IBOutlet weak var menuItemIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
