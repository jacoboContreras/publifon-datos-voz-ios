//
//  SuccessTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/27/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class SuccessTableViewCell: UITableViewCell {

    @IBOutlet weak var appIconImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var appExpirationTime: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
