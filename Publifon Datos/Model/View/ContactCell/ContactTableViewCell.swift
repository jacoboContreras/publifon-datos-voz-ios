//
//  ContactTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/21/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    // Class Variables
    var couponType = ""
    
    // Linked outlets
    @IBOutlet weak var callImageView: UIImageView!
    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Coupon is hybrid then use campaign colors to highlight
        // else use default colors
        if TabBarController.staticTextColor != "" {
            contactLabel.textColor = .black//Utils.hexToColor(hexString: TabBarController.staticTextColor)
            callImageView.image = callImageView.image?.withRenderingMode(.alwaysTemplate)
            callImageView.tintColor = Utils.hexToColor(hexString: TabBarController.staticAccentuationColor)
            
            contactLabel.textColor = .black//selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            callImageView.image = callImageView.image?.withRenderingMode(.alwaysTemplate)
            callImageView.tintColor =  selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            
        } else {
            contactLabel.textColor = .black//selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : UIColor.black
            callImageView.image = callImageView.image?.withRenderingMode(.alwaysTemplate)
            callImageView.tintColor =  selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : #colorLiteral(red: 0.167394314, green: 0.8376824239, blue: 0.4227770789, alpha: 1)
        }
        
    }
    
}
