//
//  RecentCallsTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/23/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class RecentCallsTableViewCell: UITableViewCell {

    @IBOutlet weak var contactImageView: UIImageView!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var callTimeLabel: UILabel!
    @IBOutlet weak var phoneImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Coupon is hybrid then use campaign colors to highlight
        // else use default colors
        if TabBarController.staticTextColor != "" {
            contactNameLabel.textColor = .black//Utils.hexToColor(hexString: TabBarController.staticTextColor)
            callTimeLabel.textColor = .black//Utils.hexToColor(hexString: TabBarController.staticTextColor)
            phoneImageView.image = phoneImageView.image?.withRenderingMode(.alwaysTemplate)
            phoneImageView.tintColor = .black//Utils.hexToColor(hexString: TabBarController.staticAccentuationColor)
            
            contactNameLabel.textColor = .black//selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            callTimeLabel.textColor = .black//selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            phoneImageView.tintColor =  .black//selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
        } else {
            contactNameLabel.textColor = .black//selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : UIColor.black
            callTimeLabel.textColor = .black//selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : UIColor.black
            phoneImageView.image = phoneImageView.image?.withRenderingMode(.alwaysTemplate)
            phoneImageView.tintColor =  .black//selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : #colorLiteral(red: 0.167394314, green: 0.8376824239, blue: 0.4227770789, alpha: 1)
        }
        
    }
    
}
