//
//  ChoiceTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/10/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class ChoiceTableViewCell: UITableViewCell {
    @IBOutlet weak var aplicationNameLabel: UILabel!
    @IBOutlet weak var freeTimeLabel: UILabel!
    @IBOutlet weak var appIconImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
