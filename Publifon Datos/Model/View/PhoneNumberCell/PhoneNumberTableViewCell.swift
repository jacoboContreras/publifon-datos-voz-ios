//
//  PhoneNumberTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/23/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class PhoneNumberTableViewCell: UITableViewCell {
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var selectedItemImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Coupon is hybrid then use campaign colors to highlight
        // else use default colors
        if TabBarController.staticTextColor != "" {
            typeLabel.textColor = Utils.hexToColor(hexString: TabBarController.staticTextColor)
            numberLabel.textColor = Utils.hexToColor(hexString: TabBarController.staticTextColor)
            
//            typeLabel.textColor = selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
//            numberLabel.textColor = selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            
            // selectedItemImageView.image = selectedItemImageView.image?.withRenderingMode(.alwaysTemplate)
            selectedItemImageView.image = selected ? UIImage(named: "radio_button_checked_black_18dp")?.withRenderingMode(.alwaysTemplate) : UIImage(named: "radio_button_unchecked_black_18dp")?.withRenderingMode(.alwaysTemplate)
            
            selectedItemImageView.tintColor =  selected ? Utils.hexToColor(hexString: TabBarController.staticAccentuationColor) : Utils.hexToColor(hexString: TabBarController.staticTextColor)
            
        } else {
            typeLabel.textColor = selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : UIColor.black
            numberLabel.textColor = selected ? #colorLiteral(red: 0.08535802186, green: 0.2630095836, blue: 0.617623731, alpha: 1) : UIColor.black
            // selectedItemImageView.image = selectedItemImageView.image?.withRenderingMode(.alwaysTemplate)
            selectedItemImageView.image = selected ? UIImage(named: "radio_button_checked_black_18dp") : UIImage(named: "radio_button_unchecked_black_18dp")
            selectedItemImageView.tintColor =  selected ? #colorLiteral(red: 0.167394314, green: 0.8376824239, blue: 0.4227770789, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        
    }
  
}

