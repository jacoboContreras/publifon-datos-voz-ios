//
//  HeaderTableViewCell.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 10/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {
    
    //Outlet
    @IBOutlet weak var headerTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
