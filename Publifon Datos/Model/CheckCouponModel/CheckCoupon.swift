//
//  CheckCoupon.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/5/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class CheckCoupon {
    var status: Bool = false
    var message: String
    var type: String = ""
    var success: Bool = false
    var couponType: Utils.CouponType = Utils.CouponType.none
    var chosenGroup: String
    
    init(status: Bool, message: String, type: String, chosenGroup: String) {
        self.status = status
        self.message = message
        self.type = type
        self.chosenGroup = chosenGroup
        self.assignCouponType(type: self.type)
    }
    
    init(status: Bool, message: String, chosenGroup: String) {
        self.status = status
        self.message = message
        self.chosenGroup = chosenGroup
    }
    
    init(success: Bool, message: String, type: String, chosenGroup: String) {
        print("dentro del init ", type)
        self.success = success
        self.message = message
        self.type = type
        self.chosenGroup = chosenGroup
        self.assignCouponType(type: self.type)
    }
    
    func assignCouponType(type: String) {
        switch type {
        case "datos":
            couponType = Utils.CouponType.datos
        case "voz":
            couponType = Utils.CouponType.voz
        case "seleccion":
            couponType = Utils.CouponType.choice
        case "combo":
            couponType = Utils.CouponType.combo
        default:
            print("none")
        }
    }
}
