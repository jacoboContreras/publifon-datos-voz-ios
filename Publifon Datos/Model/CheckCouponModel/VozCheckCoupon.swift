//
//  VozCheckCoupon.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 4/7/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class VozCheckCoupon {
    
    var success: Bool
    var hoursValid: String
    var homeURL: String
    var endingDate: String
    var campaignId: String
    
    init(success: Bool, hoursValid: String, homeURL: String, endingDate: String, campaignId: String) {
        self.success = success
        self.hoursValid = hoursValid
        self.homeURL = homeURL
        self.endingDate = endingDate
        self.campaignId = campaignId
    }
    
}
