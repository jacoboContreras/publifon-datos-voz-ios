//
//  Numbers.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/21/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class Numbers {
    
    var name: String
    var number: String
    
    init(name: String, number: String) {
        self.name = name
        self.number = number
    }
    
    public func parseNumber() -> String {
        var result = Utils.BLANK_STRING
//        var finalResult = Utils.BLANK_STRING
//        var parse = [String]() // this variable is recycled several times
//
//        if self.number != Utils.BLANK_STRING {
//            parse = self.number.components(separatedBy: "(")
//            if parse.count > 0 {
//                parse = parse[1].components(separatedBy: ")")
//                for number in parse { result = result + number }
//                parse = result.components(separatedBy: " ")
//                for number in parse { finalResult = finalResult + number }
//                return finalResult
//            } else {
//                return number
//            }
//        }
        result = self.number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        return result
        
    }
}
