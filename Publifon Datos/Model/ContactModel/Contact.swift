//
//  Contact.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/21/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation
import UIKit

class Contact {
    var name: String
    var lastName: String
    var numbers: [Numbers]
    var image: UIImage
    
    init(name: String, lastName: String, numbers: [Numbers], image: UIImage) {
        self.name = name
        self.lastName = lastName
        self.numbers = numbers
        self.image = image
    }
    
}
