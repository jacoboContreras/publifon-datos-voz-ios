//
//  Servers.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 2/28/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation

class Servers {
    var port: Int
    var remote: String
    var caCert: String
    var name: String
    
    init(port: Int, remote: String, caCert: String, name: String) {
        self.port = port
        self.remote = remote
        self.caCert = caCert
        self.name = name
    }
}
