//
//  AppDelegate.swift
//  Publifon Datos
//
//  Created by Admin on 12/09/18.
//  Copyright © 2018 Bluelabs. All rights reserved.
//

import UIKit
import Firebase
import FirebaseCore
import UserNotifications
import NetworkExtension
import SwiftyBeaver
import FirebaseDynamicLinks
import Alamofire
import SwiftyJSON

private let log = SwiftyBeaver.self

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let COUPON_ACTIVE = "couponActive"
    public static var menuToggle = true
    let gcmMessageIDKey = "gcm.message_id"
    public static var sliderArray: [String]?
    public static var index = 0
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let logDestination = ConsoleDestination()
        logDestination.minLevel = .debug
        logDestination.format = "$DHH:mm:ss$d $L $N.$F:$l - $M"
        log.addDestination(logDestination)
        // Override point for customization after application launch.
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        Messaging.messaging().delegate = self
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : UIViewController
        
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) == nil || UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) == Utils.CouponType.none.rawValue   {
            viewController = storyBoard.instantiateViewController(withIdentifier: "TutorialUsageViewController")
            /*let navigationController = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
             let vc = navigationController.topViewController as! SeleccionDeRedencionViewController
             //var to know's if is first launch of app to user
             UserDefaults.standard.bool(forKey: "openedBefore")
             self.window?.rootViewController = navigationController
             self.window?.makeKeyAndVisible()
             return true*/
            
        }else{
            viewController = storyBoard.instantiateInitialViewController()!
        }
        
        //var to know's if is first launch of app to user
        UserDefaults.standard.bool(forKey: "openedBefore")
        
        
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
        
        //UIApplication.shared.statusBarStyle = .lightContent
        
        
        return true
    }
    
    // Setup app delegate for push notifications
    func setupPushNotification(application: UIApplication) -> () {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]){(granted, error) in
            if granted {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")" )
            }
            
        }
    }
    
    // Failed registration
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for remote notifications \(error.localizedDescription)")
    }
    
    // Code to make token string
    func tokenString(_ deviceToken: Data) -> String {
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes {
            token += String(format: "%02x", byte)
        }
        
        return token
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        print("Terminating applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("Terminating applicationDidEnterBackground")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("Terminating applicationWillEnterForeground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("Terminating applicationDidBecomeActive")
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("Terminating applicationWillTerminate")
        //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "vpnDisconnect"), object: nil)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
    
    func handleIncomingDynamicLink(_ dynamicLink: DynamicLink){
        print("baia , este es el dynamic link: \(dynamicLink)")
        guard let url = dynamicLink.url else {
            print("that's weird, my dynamic link object has not url")
            return
        }
        print("la url del dynamic link: ", url.absoluteString)
        if isPublifonCouponCoupon(url: url){
            return
        }
        
        checkCouponNew(clave: url.absoluteString)
    }
        
    func isPublifonCouponCoupon(url: URL) -> Bool{
        let publifongCoupon = getQueryStringParameter(url: url.absoluteString, param: "publifonCoupon")
        print("el cupon de  publi ggg \(publifongCoupon)")
        if publifongCoupon != nil {
            checkCurrentCupon()
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let navigationController = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
            let insertCodeVC = storyBoard.instantiateViewController(withIdentifier: "InsertCodeViewController") as! InsertCodeViewController
            insertCodeVC.publifonCouponScan = publifongCoupon!
            navigationController.pushViewController(insertCodeVC, animated: false)
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            return true
        }
        return false
    }
    
    func handleIncomingUrl(_ url: URL){
        /*guard let url = urlParam else {
            print("that's weird, my dynamic link object has not url")
            return
        }*/
        /*print("Your incoming link parameter is \(url.absoluteString)")
        let paramvalue = getQueryStringParameter(url: url.absoluteString, param: "value")
        print("Valor de mi parámetro: ",paramvalue != nil ? paramvalue! : "no se encontró el parámetro")
        if paramvalue != nil {
                        
        }*/
        print("llega aqui")
        checkCouponNew(clave: url.absoluteString)
    }
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let incomingUrl = userActivity.webpageURL {
            print("Incoming url is: ", incomingUrl)
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl) { (dynamicLink, error) in
                guard error == nil else {
                    print("Found an error: \(error!.localizedDescription)")
                    return
                }
                                
                if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkHandled {
                return true
            } else {
                return false
            }
        }
        return false
    }
    
    
    /*func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
     if let incomingUrl = userActivity.webpageURL {
     print("Incoming url is: ", incomingUrl)
     let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingUrl) { (dynamicLink, error) in
     guard error == nil else {
     print("Found an error: \(error!.localizedDescription)")
     return
     }
     
     if let dynamicLink = dynamicLink {
     self.handleIncomingDynamicLink(dynamicLink)
     }
     }
     if linkHandled {
     return true
     } else {
     return false
     }
     }
     return false
     }*/
}

@available(iOS 10, *)
extension AppDelegate:  UNUserNotificationCenterDelegate{
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        let aps: [String: Any] = userInfo["aps"] as! [String:Any]
        let alert : [String: Any] = aps["alert"] as! [String:Any]
        let body: String = alert["body"] as! String
        //var alert = aps![""]
        print("Push Notification message: ", body)
        
        if AppDelegate.index == 3 {
            AppDelegate.index = 0
        }
        
        //        AppDelegate.sliderArray![AppDelegate.index] = body
        //        print("AppDelegate.sliderArray: ",  AppDelegate.sliderArray)
        //        UserDefaults.standard.set(AppDelegate.sliderArray!, forKey: "sliderArray")
        
        //        UserDefaults.standard.stringArray(forKey: "sliderArray")
        AppDelegate.index = AppDelegate.index + 1
        //UserDefaults.standard.set(AppDelegate.index, forKey: "index")
        // Change this to your preferred presentation option
        completionHandler([.alert])
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
    func checkCurrentCupon(){
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
                let type = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)
                switch type {
                
                // Datos, Send to main view controller
                case Utils.CouponType.datos.rawValue:
                    applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    return
                // Voz, Send to tab bar controller
                case Utils.CouponType.voz.rawValue:
                    applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                    return
                // Choice, Send to corresponding controller
                case Utils.CouponType.choice.rawValue:
                    let selected = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
                    if selected == Utils.CouponSelectedChoice.datos.rawValue {
                        applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    }
                    if selected == Utils.CouponSelectedChoice.voz.rawValue {
                        applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                    }
                    return
                // Combo, Send to main view controller
                case Utils.CouponType.combo.rawValue:
                    applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    return
                //applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                
                default:
                    print("No coupon active")
                }
            }
        }
    }
        
        
    
    // checar cupón desde deep link
    func checkCouponNew(clave: String){
        
        //verificar que no exista un cupón activo en la app
        print("en el check coupon new app delegate")
        print(UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE))
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
                let type = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)
                switch type {
                    
                // Datos, Send to main view controller
                case Utils.CouponType.datos.rawValue:
                    applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    return
                // Voz, Send to tab bar controller
                case Utils.CouponType.voz.rawValue:
                    applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                    return
                // Choice, Send to corresponding controller
                case Utils.CouponType.choice.rawValue:
                    let selected = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)
                    if selected == Utils.CouponSelectedChoice.datos.rawValue {
                        applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    }
                    if selected == Utils.CouponSelectedChoice.voz.rawValue {
                        applicationModeSelector(controller: Utils.TAB_BAR_CONTROLLER)
                    }
                    return
                // Combo, Send to main view controller
                case Utils.CouponType.combo.rawValue:
                    applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                    return
                    //applicationModeSelector(controller: Utils.MAIN_VIEW_CONTROLLER)
                
                    
                default:
                    print("No coupon active")
                }
                
            }
            
            //return
        }
        
        /*if let viewWithTag = self.view.viewWithTag(123) {
            viewWithTag.removeFromSuperview()
        }*/

        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let navigationController = storyBoard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
        let vc = navigationController.topViewController as! SeleccionDeRedencionViewController
        let formUserVC = storyBoard.instantiateViewController(withIdentifier: "FormUserViewController") as! FormUserViewController
        let insertCodeVC = storyBoard.instantiateViewController(withIdentifier: "InsertCodeViewController") as! InsertCodeViewController

        print("dentro del checkCouponNew penuultimo")
        //vc.dataQR = paramvalue!
        print("clave: ", clave)
        Alamofire.request(configureRequest(url: Utils.CHECK_TUTORIAL, clave: clave)).responseData {
            dataResponse in
            print("****************************************\n")
            print(dataResponse.result.isSuccess)
            if dataResponse.result.isSuccess {
                print("dentro del checkCouponNew ultimo")
                let json : JSON = JSON(dataResponse.result.value!)
                print("\n\n*************")
                print(json["success"])
                print(json["message"])
                print(clave)
                if json["success"].boolValue {
                    print("resultado: ", json["message"].stringValue)
                    print("resultado: ", json["showTutorial"].boolValue)
                    if json["showForm"].exists() {
                        formUserVC.dataQR = clave
                        formUserVC.showTutorialApi = json["showTutorial"].boolValue
                        
                        if let data = UserDefaults.standard.value(forKey:"userData") as? Data {
                            if let decoded = try? PropertyListDecoder().decode(UserData.self, from: data) {
                                formUserVC.userDataBody = decoded
                                formUserVC.dataUserFromUserDefaults = true
                            }
                        }
                        navigationController.pushViewController(formUserVC, animated: false)
                    } else {
                        navigationController.pushViewController(insertCodeVC, animated: false)
                    }
                } else {
                    //navigationController.pushViewController(vc, animated: false)
                    vc.dynamicLinkError = true
                    vc.dynamicLinkErrorMessage = json["message"].stringValue
                    //Alerts().alert(Title: "Error", Message: json["message"].stringValue, viewController: vc)
                }
            } else {
                //navigationController.pushViewController(vc, animated: false)
                vc.dynamicLinkError = true
                vc.dynamicLinkErrorMessage = dataResponse.error?.localizedDescription ?? "problemas al conectar con el servidor, intenta nuevamente"
                //Alerts().alert(Title: "Error", Message: dataResponse.error?.localizedDescription, viewController: vc)
            }
            
            self.window?.rootViewController = navigationController
            self.window?.makeKeyAndVisible()
            
        }
    }
    
    // Configuring Request, returns a configured request
    func configureRequest(url: String, clave: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.POST
        request.httpBody = ["clave": clave].percentEncoded()
        //request.timeoutInterval = 10
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        return request
    }
    
    // If there is an active coupon go to corresponding view controller
    func applicationModeSelector(controller: String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var viewController: UIViewController?
        
        print("Dentro de redirección en cupón activo")
        print(controller)
        // Creates corresponding view controller object
        switch controller {
        case Utils.MAIN_VIEW_CONTROLLER:
            print("case main view controller")
            let mainViewController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! MainViewController
            mainViewController.areDynamicLinkRedentionError = true
            mainViewController.dynamicLinkRedentionErrorMessage = "Ya cuentas con un cupón activo, antes de redimir uno nuevo deberás suspender el cupón actual."
            viewController = mainViewController
            break
        case Utils.TAB_BAR_CONTROLLER:
            let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! TabBarController
            tabBarController.areDynamicLinkRedentionError = true
            tabBarController.dynamicLinkRedentionErrorMessage = "Ya cuentas con un cupón activo, antes de redimir uno nuevo deberás suspender el cupón actual."
            viewController = tabBarController
            break
        case Utils.SUCCESS_VIEW_CONTROLLER:
            let successViewController = mainStoryboard.instantiateViewController(withIdentifier: controller) as! SuccessViewController
            successViewController.areDynamicLinkRedentionError = true
            successViewController.dynamicLinkRedentionErrorMessage = "Ya cuentas con un cupón activo, antes de redimir uno nuevo deberás suspender el cupón actual."
            viewController = successViewController
            break
        default:
            print("Invalid choice")
            let navigationController = mainStoryboard.instantiateViewController(withIdentifier: "MenuNavigationController") as! UINavigationController
            let vc = navigationController.topViewController as! SeleccionDeRedencionViewController
            appDelegate.window?.rootViewController = viewController
            appDelegate.window?.makeKeyAndVisible()
            return
        }
        
        appDelegate.window?.rootViewController = viewController
        appDelegate.window?.makeKeyAndVisible()
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    /*func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Message received: ", remoteMessage.appData)
    }*/
}

