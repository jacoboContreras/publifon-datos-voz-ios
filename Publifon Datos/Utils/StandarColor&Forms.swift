//
//  StandarColor&Forms.swift
//  Publifon Datos
//
//  Created by Jacobo on 2/7/20.
//  Copyright © 2020 Bluelabs. All rights reserved.
//

import Foundation
import UIKit


class StandarColorAndForms{
    
    static let grayColor = #colorLiteral(red: 0.4391798377, green: 0.4392357767, blue: 0.439160794, alpha: 1)
    static let cornerRadius = 10
    static let greenColor = #colorLiteral(red: 0.119675912, green: 0.8750196695, blue: 0.3158476651, alpha: 1)
    static let backgroundColorPopUp = #colorLiteral(red: 0.1724602878, green: 0.1724911332, blue: 0.1724535227, alpha: 1)
    static let borderWidht = 1
    static let heightButtonsPopup = 115
    
    
    static let blue = #colorLiteral(red: 0.2361257672, green: 0.32858634, blue: 0.7007226348, alpha: 1)
    
}


