//
//  Utils.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/4/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
import Contacts
import Alamofire
import AVFoundation

class Utils {
    
    // UserDefaults Constants
    public static let COUPON_ACTIVE = "couponActive"
    public static let COUPON_EXPIRATION_DATE = "couponExpirationDate";
    public static let REDEMPTION_DATE = "redemptionDate"
    public static let RESET = ""
    public static let BLANK_STRING = ""
    public static let API_TOKEN = "apiToken"
    public static let DEVICE_TOKEN = "deviceToken"
    public static let PENDING_EXPIRATION = "pendingExpiration"
    public static let PENDING_SUSPENSION = "pendingSuspension"
    public static let VOZ_JSON_KEY = "vozJson"
    public static let JSON_KEY = "json"
    public static let SLIDER_ARRAY = "sliderArray"
    public static let VOZ_COUPON_ACTIVE = "vozCouponActive"
    public static let ACTIVE_COUPON_TYPE = "activeCouponType"
    public static let ACTIVE_SELECTED_COUPON_CHOICE = "activeSelectedCouponChoice"
    public static let VOZ_COUPON_CODE = "vozCouponCode"
    public static let VOZ_HOME_IMAGE_NAME = "homeImageName"
    public static let IMAGE_DAY_CHECK = "imageDayCheck"
    
    // Constants
    public static let SPONSORED_MESSAGE = " te regala datos ilimitados por "
    public static let VOZ_SPONSORED_MESSAGE = " te regala \n       llamadas ilimitadas "
    public static let ONLY_VOZ_SPONSORED_MESSAGE = "Tu servicio de llamadas ilimitadas ha sido activado, cuentas con "
    public static let LOCALE_IDENTIFIER = "es_MX"
    public static let READABLE_DATE_FORMAT = "dd/MMM/yyyy HH:mm:ss"
    public static let DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
    public static let TIME_ZONE_IDENTIFIER = "GMT"
    public static let PROM_VALID_UNTIL = "Cupón válido hasta el "
    public static let EXPIRED_SERVICE = "Servicio expirado"
    public static let SWITCH_EXPLANATION = "Cuando abras la aplicación asegurate de activar el switch. En el momento que el icono de VPN se muestre podras usar tus datos patrocinados"
    public static let THIS_APP_IS_NOT_INSTALLED = "Esta aplicación no esta instalada, debes instalarla para poder usar este servicio."
    public static let MODAL_INFO_CONTENT_TEXT_1 = "Tu servicio de "
    public static let MODAL_INFO_CONTENT_TEXT_2 = " expirara el "
    public static let MODAL_INFO_CONTENT_TEXT_3 = " a las "
    public static let MODAL_INFO_CONTENT_TEXT_4 = ". Te quedan "
    public static let CONGRATULATIONS = "Felicidades"
    public static let CONECTION_ISSUES = "Problemas de conexión"
    public static let COULD_NOT_VALIDATE_COUPON = "No se pudo validar tu servicio"
    public static let MAIN = "Main"
    public static let SUSPEND = "suspend"
    public static let EXPIRE = "expire"
    public static let TO_VALIDATE_COUPON = "Para validar tu servicio"
    public static let YOU_HAVE_TO_ACTIVATE_LOCATION_SERVICE = "Debes activar el servicio de ubicación"
    public static let YOU_HAVE_TO_ENTER_A_COUPON = "Debes introducir un cupón válido."
    public static let INVALID_NUMBER = "Te recordamos que para marcar a Teléfonos fijos y celulares en México deberás de marcar a 10 digitos."
    public static let DISCONNECT_VPN = "Para usar el servicio de llamadas debes desactivar la conexión VPN"
    public static let INSTALL_TURISFON_CONNECT = "Debes instalar Turisfon Connect para usar el servicio"
    public static let INACTIVE_SERVICE = "Servicio Inactivo"
    public static let ACTIVE_SERVICE = "Servicio Activo. Recuerda que tu switch se desactiva cada 10 minutos."
    public static let DISABLE_WIFI = "Desactiva tu Wifi para usar el servicio"
    public static let COULD_NOT_GET_LOCATION = "No se pudo obtener tu ubicación"
    public static let INVALID_COUPON = "Cupón inválido"
    public static let INSERT_SIM_CARD = "Para proceder es necesario que tengas una Tarjeta SIM en tu dispositivo. Revisa que esté funcionando correctamente y vuelve a intentar."
    public static let ADVICE = "Aviso"
    public static let DISABLE_SWITCH = "Debes desactivar el switch para realizar la llamada."
    public static let DOWNLOAD_TURISFON_CONNECT = "Debes descargar la app Publifon Connect desde App Store para hacer uso de tu servicio de datos."
    public static let DISABLE_WIFI_TO_REDEEM = "Para redimir tu cupón debes desactivar el servicio de Wi-fi."
    public static let UPDATE_APP = "Posiblemente estes usando una versión anterior de la aplicación, te recomendamos actualizar a la última versión."
    public static let SERVER_MAINTENANCE = "Estamos realizando mantenimiento a nuestros servidores intenta acceder más tarde."
    public static let ISSUES_TRYING_TO_REDEEM = "Ha ocurrido un problema al redimir, intenta nuevamente."
    public static let DISCONNECT_VPN_TO_USE_VOZ_SERVICE = "Para hacer uso del servicio de llamadas deberás desactivar el switch."
    public static let COUPON_NOT_VALID_FOR_THIS_APP_VERSION = "El cupón introducido no es válido para esta versión de la aplicación."
    public static let NUMBER_MUST_CONTAIN_10_DIGITS = "El número debe contener 10 digitos: Lada + Teléfono"
    public static let APPLE = "user@bluestudio.mx"//"41738"//"user@bluestudio.mx"// Apple Reviews
    public static let COMPANY_NAME = "Publifon"
    public static let SUSPENDED_SERVICE = "Servicio suspendido"
    //public static let DISABLE_SWITCH = "Para hacer uso del servicio de llamadas, debes desactivar el switch."
    
    // Schemes
    public static let PUBLIFON_CONNECT = "publifonconnect://"
    
    // Network, JSON Constants
    public static let API_DEVICE_TOKEN = "device_token"
    public static let ACCESS_TOKEN = "access_token"
    public static let CARRIER_NAME = "carrier_name"
    public static let LATITUDE = "lat"
    public static let LONGITUDE = "lng"
    public static let ACCEPT = "Accept"
    public static let APPLICATION_JSON = "application/json"
    public static let AUTHORIZATION = "Authorization"
    public static let MESSAGE = "message"
    public static let STATUS = "status"
    public static let POST = "POST"
    public static let GET = "GET"
    public static let PATCH = "PATCH"
    public static let TYPE = "type"
    public static let SUCCESS = "success"
    public static let PHONE_NUMBER = "phoneNumber"
    public static let COUPON_CODE = "CouponCode"
    public static let DEVICE = "device"
    public static let HOME_URL = "HomeURL"
    public static let ENDING_DATE = "EndingDate"
    public static let CAMPAIGN_ID = "campaignId"
    public static let HOURS_VALID = "HoursValid"
    public static let VOZ_SUCCESS = "Success"
    public static let COUPON = "coupon"
    public static let DESTINATION_NUMBER = "destinationNumber"
    public static let LAT = "lat"
    public static let LNG = "lng"
    public static let SUSPENDED = "Suspendido"
    public static let AVAILABLE = "Disponible"
    
    // Apple iTunes Connect
    public static let ITUNES_URL = "https://apps.apple.com/us/app/turisfon/id1463997921?l=es&ls=1"
    
    // Segue, Cell Constant identifiers
    public static let APPLICATION_ITEM_TABLE_VIEW_CELL = "applicationItemTableViewCell"
    public static let INSERT_CODE_VIEW_CONTROLLER = "InsertCodeViewController"
    public static let SUCCESS_VIEW_CONTROLLER = "SuccessViewController"
    public static let MAIN_VIEW_CONTROLLER = "MainViewController"
    public static let TAB_BAR_CONTROLLER = "TabBarController"
    public static let VOZ_TABS_VIEW_CONTROLLER = "VozTabsViewController"
    public static let GO_TO_VALIDATION_VIEW_CONTROLLER = "goToValidationViewController"
    public static let MENU_VIEW_CONTROLLER = "MenuViewController"
    public static let GO_TO_MODAL_INFO_VIEW_CONTROLLER = "goToModalInfoViewController"
    public static let GO_TO_CHOICE_VIEW_CONTROLLER = "goToChoiceViewController"
    public static let GO_TO_MAIN_VIEW_CONTROLLER = "goToMainViewcontroller"
    public static let GO_TO_TAB_BAR_CONTROLLER = "goToTabBarController"
    public static let GO_TO_EXTENDED_VALIDATION_VIEW_CONTROLLER = "goToExtendedValidationViewController"
    
    // AppMode Setup
    public static var APP_MODE = AppMode.production
    // Resturn's corresponding datos API url's
    public static func datosAppMode(mode: AppMode) -> String {
        switch mode {
        case AppMode.production:
            return "https://crm.datoslibres.mx"
        case AppMode.pre_production:
            return "http://ec2-3-228-109-66.compute-1.amazonaws.com"
        case AppMode.sandbox:
            return "https://crm.sandbox.datoslibres.mx"
        case AppMode.localHost:
            return "http://127.0.0.1:8000"
        }
    }
    
    // Resturn's corresponding voz API url's
    public static func vozAppMode(mode: AppMode) -> String {
        switch mode {
        case AppMode.production:
            return "https://publifonapp.com/v3_beta"
        case AppMode.pre_production:
            return "https://sandbox.publifonapp.com/v3_beta"
        case AppMode.sandbox:
            return "https://sandbox.publifonapp.com/v3_beta"
        case AppMode.localHost:
            return "http://127.0.0.1:8000/v3_beta"
        }
    }
    // Resturn's corresponding voz Image API url's
    public static func vozCheckAppMode(mode: AppMode) -> String {
        switch mode {
        case AppMode.production:
            return "https://api.publifonapp.com"
        case AppMode.pre_production:
            return "https://api.sandbox.publifonapp.com"
        case AppMode.sandbox:
            return "https://api.sandbox.publifonapp.com"
        case AppMode.localHost:
            return "http://127.0.0.1:8000"
        }
    }
    
    public static func qrData(mode: AppMode) -> String {
        switch mode {
        case AppMode.production:
            return "https://nutripromo.com.mx/?qr=walliaCodeProduccion\(Int.random(in: 0..<100))"
        case AppMode.pre_production:
            return "https://www.nutrileche.com/?qr=walliaCodePreProduccion\(Int.random(in: 0..<100))"
        case AppMode.sandbox:
            return "https://nutripromo.com.mx/?qr=walliaCodeSandbox\(Int.random(in: 0..<100))" //"5HMH5"
        case AppMode.localHost:
            return "https://nutripromo.com.mx/?qr=walliaCodeSandbox\(Int.random(in: 0..<100))" //"5HMH5"
        }
    }
    
    public static func changeAllAppMode(APP_MODE: AppMode) {
        SUSPEND_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/suspend/"
        EXPIRE_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/expire/"
        WEBSERVICE_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/redeem/"
        CREATE_API_TOKEN_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/token/create"
        CHECK_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/check/"
        VOZ_COUPON_CHECK_URL = "\(Utils.vozCheckAppMode(mode: APP_MODE))/api/check-coupon/"
        VOZ_WEBSERVICE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_ValidateCouponV2.php/"
        VOZ__MAKE_CALL_WEBSERVICE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_MakeCall.php"
        VOZ_HOME_URL = "\(vozAppMode(mode: APP_MODE))/secciones/home/"
        VOZ_IMAGE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_getImageFromCoupon.php"
    }
    
    // API URL's produccion
    public static var SUSPEND_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/suspend/"
    public static var EXPIRE_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/expire/"
    public static var WEBSERVICE_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/redeem/"
    public static var CREATE_API_TOKEN_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/token/create"
    public static var CHECK_COUPON_URL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/check/"
    public static var VOZ_COUPON_CHECK_URL = "\(Utils.vozCheckAppMode(mode: APP_MODE))/api/check-coupon/"
    public static var VOZ_WEBSERVICE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_ValidateCouponV2.php/"
    public static var VOZ__MAKE_CALL_WEBSERVICE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_MakeCall.php"
    public static var VOZ_HOME_URL = "\(vozAppMode(mode: APP_MODE))/secciones/home/"
    public static var VOZ_IMAGE_URL = "\(vozAppMode(mode: APP_MODE))/secciones/POST_getImageFromCoupon.php"
    
    public static var IMAGE_COMPANY_URL = "https://crm.datoslibres.mx/download/campaigns/"
    
    public static var REDEEM_QR_COUPON = "\(Utils.datosAppMode(mode: APP_MODE))/api/wallia/redeem"
    public static var CHECK_TUTORIAL = "\(Utils.datosAppMode(mode: APP_MODE))/api/coupon/check_new"
    
     // API URL's produccion
//    public static let SUSPEND_COUPON_URL = "https://crm.datoslibres.mx/api/coupon/suspend/"
//    public static let EXPIRE_COUPON_URL = "https://crm.datoslibres.mx/api/coupon/expire/"
//    public static let WEBSERVICE_URL = "https://crm.datoslibres.mx/api/coupon/redeem/"
//    public static let CREATE_API_TOKEN_URL = "https://crm.datoslibres.mx/api/coupon/token/create"
//    public static let CHECK_COUPON_URL = "https://crm.datoslibres.mx/api/coupon/check/"
//    public static let VOZ_COUPON_CHECK_URL = "https://api.publifonapp.com/api/check-coupon/"
//    public static let VOZ_WEBSERVICE_URL = "https://publifonapp.com/v3_beta/secciones/POST_ValidateCouponV2.php/"
//    public static let VOZ_HOME_URL = "https://publifonapp.com/v3_beta/secciones/home/"
    
    // Sandbox
//    public static let SUSPEND_COUPON_URL = "https://crm.sandbox.datoslibres.mx/api/coupon/suspend/"
//    public static let EXPIRE_COUPON_URL = "https://crm.sandbox.datoslibres.mx/api/coupon/expire/"
//    public static let WEBSERVICE_URL = "https://crm.sandbox.datoslibres.mx/api/coupon/redeem/"
//    public static let CREATE_API_TOKEN_URL = "https://crm.sandbox.datoslibres.mx/api/coupon/token/create"
//    public static let CHECK_COUPON_URL = "https://crm.sandbox.datoslibres.mx/api/coupon/check/"
//    public static let VOZ_COUPON_CHECK_URL = "https://api.publifonapp.com/api/check-coupon/"
//    public static let VOZ_WEBSERVICE_URL = "https://publifonapp.com/v3_beta/secciones/POST_ValidateCouponV2.php/"
//    public static let VOZ_HOME_URL = "https://publifonapp.com/v3_beta/secciones/home/"
    
    // Pre-Produccion
//    public static let SUSPEND_COUPON_URL = "http://ec2-3-228-109-66.compute-1.amazonaws.com/api/coupon/suspend/"
//    public static let EXPIRE_COUPON_URL = "http://ec2-3-228-109-66.compute-1.amazonaws.com/api/coupon/expire/"
//    public static let WEBSERVICE_URL = "http://ec2-3-228-109-66.compute-1.amazonaws.com/api/coupon/redeem/"
//    public static let CREATE_API_TOKEN_URL = "http://ec2-3-228-109-66.compute-1.amazonaws.com/api/coupon/token/create"
//    public static let CHECK_COUPON_URL = "http://ec2-3-228-109-66.compute-1.amazonaws.com/api/coupon/check/"
//    public static let VOZ_COUPON_CHECK_URL = "https://api.sandbox.publifonapp.com/api/check-coupon/"
//    public static let VOZ_WEBSERVICE_URL = "https://sandbox.publifonapp.com/v3_beta/secciones/POST_ValidateCouponV2.php/"
//    public static let VOZ_HOME_URL = "https://sandbox.publifonapp.com/v3_beta/secciones/home/"
    
    // enums
    enum CouponType: String {
        case combo = "combo"
        case choice = "choice"
        case datos = "datos"
        case voz = "voz"
        case none = "none"
    }
    
    enum CouponSelectedChoice: String {
        case datos = "datos"
        case voz = "voz"
        case combo = "combo"
        case none = "none"
    }
    
    enum AppMode: String {
        case production = "production"
        case pre_production = "pre_production"
        case sandbox = "sandbox"
        case localHost = "localHost"
    }
    
    //get formated text of coupons total time, in day and hrs
    public static func getFormatedTotalTime(totalHrs: Int) -> String {
        var formatedFreeHrs: String
        
        if(totalHrs == 24) {
            formatedFreeHrs = String(totalHrs/24) + " dia";
        } else if(totalHrs/24 == 1 && totalHrs % 24 != 0) {
            formatedFreeHrs = String(totalHrs/24) + " dia y " + String(totalHrs%24) + " hrs"
        } else if(totalHrs / 24 != 0 && totalHrs % 24 == 0){
            formatedFreeHrs = String(totalHrs/24) + " dias"
        } else if (totalHrs / 24 != 0 && totalHrs % 24 != 0) {
            formatedFreeHrs = String(totalHrs/24) + " dias y " + String(totalHrs%24) + " hrs"
        } else {
            formatedFreeHrs = String(totalHrs) + " hrs";
        }
        return formatedFreeHrs;
    }
    
    // Gets total amount of sponsored coupon
    public static func getTotalCouponHrs (couponObject: CouponModel) -> Int {
        var higher = 0;
        
        for application in couponObject.applications {
            if application.hours > higher {
                higher = application.hours
            }
        }
        
        return higher
    }    
    
    // Carrier parser
    public static func getCarrier(carrier: String) -> String {
        let result = "";
        let carrierLower = carrier.lowercased
        let carriers = ["telcel", "movistar", "at&t"]
        
        for carrierName in carriers {
            if(carrierLower().contains(carrierName)) {
                if (carrierName == "at&t") {
                    return "at&t 4g";
                }
                return carrierName;
            }
        }
        
        return result;
    }
    
    // Calculates coupon expiration date
    public static func calculateCouponExpirationDate(couponObject: CouponModel) -> String {
        
        // Getting days and hours of coupon sponsored time
        let days = Utils.getTotalCouponHrs(couponObject: couponObject) / 24
        let hrs = Utils.getTotalCouponHrs(couponObject: couponObject) % 24
        
        // If time is added only by hours, after 720 hrs it has a gap of 1 hour
        // to work around this, first add days and after add the remaning hours
        let date = Date()
        let tempDate = Calendar.current.date(byAdding: .day, value: days, to: date)
        let expireDate = Calendar.current.date(byAdding: .hour, value: hrs, to: tempDate!)
        let dateFormatter = DateFormatter()
        // For some reason this line gives the wrong time in this method
        //dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
        dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        
        dateFormatter.dateFormat = DATE_FORMAT
        var result = dateFormatter.string(from: expireDate!)
        // checking if campaign expiration date is before coupon expiration date
        var typeOfCoupon = ""
        var selectedChoice = ""
        
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            selectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // Expiration for datos
        if typeOfCoupon == Utils.CouponType.datos.rawValue || selectedChoice == Utils.CouponSelectedChoice.datos.rawValue {
            let datosCampaignExpirationDate = couponObject.campaign.endDate
            print(datosCampaignExpirationDate)
            if Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate) < expireDate! {
                dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
                let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate))
                result = dateFormatter.string(from: addOneDay!)
            }
        }
        
        // Expiration for a combo coupon
        if typeOfCoupon == Utils.CouponType.combo.rawValue {
            //dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
            let vozCampaignExpirationDate = getVozCampaignExpirationStringDate()
            let vozCampaignExpiration = Utils.convertStringToDate(stringDate: vozCampaignExpirationDate)
            
            let datosCampaignExpirationDate = couponObject.campaign.endDate
            let datosCampaignExpiration = Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate)
            
            if datosCampaignExpiration > vozCampaignExpiration {
                if datosCampaignExpiration > expireDate! {
                    result = dateFormatter.string(from: expireDate!)
                } else {
                    dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
                    //result = dateFormatter.string(from: datosCampaignExpiration)
                    let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: datosCampaignExpiration)
                    result = dateFormatter.string(from: addOneDay!)
                }
                
            } else {
                if vozCampaignExpiration > expireDate! {
                    result = dateFormatter.string(from: expireDate!)
                } else {
                    dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
                    //result = dateFormatter.string(from: vozCampaignExpiration)
                    let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: vozCampaignExpiration)
                    result = dateFormatter.string(from: addOneDay!)
                }
            }
        }
        print("Expiration date xxxxx", result)
        return result
    }
    
    
    // Calculates service expiration date
    public static func calculateExpirationDate(couponObject: CouponModel, serviceHrs: Int) -> String {
        
        // Getting days and hours of sponsored service time
        let days = serviceHrs / 24
        let hrs = serviceHrs % 24
        
        // If time is added only by hours, after 720 hrs it has a gap of 1 hour
        // to work around this, first add days and after add the remaning hours
        let date = Utils.convertStringToDate(stringDate: UserDefaults.standard.string(forKey: Utils.REDEMPTION_DATE)!)
        print("Expiration convertString 1:", date)
        let tempDate = Calendar.current.date(byAdding: .day, value: days, to: date)
        let expireDate = Calendar.current.date(byAdding: .hour, value: hrs, to: tempDate!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
        dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        dateFormatter.dateFormat = DATE_FORMAT
        var result = dateFormatter.string(from: expireDate!)
        
        // checking if campaign expiration date is before coupon expiration date
        var typeOfCoupon = ""
        var selectedChoice = ""
        if UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE) != nil {
            typeOfCoupon = UserDefaults.standard.string(forKey: Utils.ACTIVE_COUPON_TYPE)!
            selectedChoice = UserDefaults.standard.string(forKey: Utils.ACTIVE_SELECTED_COUPON_CHOICE)!
        }
        
        // Expiration for voz
        if typeOfCoupon == Utils.CouponType.voz.rawValue || selectedChoice == Utils.CouponSelectedChoice.voz.rawValue {
            let vozCampaignExpirationDate = getVozCampaignExpirationStringDate()
            if Utils.convertStringToDate(stringDate: vozCampaignExpirationDate) < expireDate! {
                //result = dateFormatter.string(from: Utils.convertStringToDate(stringDate: vozCampaignExpirationDate))
                let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: Utils.convertStringToDate(stringDate: vozCampaignExpirationDate))
                result = dateFormatter.string(from: addOneDay!)
            }
        }
        
        // Expiration for datos
        if typeOfCoupon == Utils.CouponType.datos.rawValue || selectedChoice == Utils.CouponSelectedChoice.datos.rawValue {
            let datosCampaignExpirationDate = couponObject.campaign.endDate
            print(datosCampaignExpirationDate)
            
            if Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate) < expireDate! {
                //result = dateFormatter.string(from: Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate))
                let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate))
                result = dateFormatter.string(from: addOneDay!)
            }
        }
        
        // Expiration for a combo coupon
        if typeOfCoupon == Utils.CouponType.combo.rawValue {
            //dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
            let vozCampaignExpirationDate = getVozCampaignExpirationStringDate()
            let vozCampaignExpiration = Utils.convertStringToDate(stringDate: vozCampaignExpirationDate)
            
            let datosCampaignExpirationDate = couponObject.campaign.endDate
            let datosCampaignExpiration = Utils.convertDatosCampaignExpirationToDate(date: datosCampaignExpirationDate)
            
            if datosCampaignExpiration > vozCampaignExpiration {
                if datosCampaignExpiration > expireDate! {
                    result = dateFormatter.string(from: expireDate!)
                } else {
                    dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
                    //result = dateFormatter.string(from: datosCampaignExpiration)
                    let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: datosCampaignExpiration)
                    result = dateFormatter.string(from: addOneDay!)
                }
                
            } else {
                if vozCampaignExpiration > expireDate! {
                    result = dateFormatter.string(from: expireDate!)
                } else {
                    dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
                    //result = dateFormatter.string(from: vozCampaignExpiration)
                    let addOneDay = Calendar.current.date(byAdding: .day, value: 1, to: vozCampaignExpiration)
                    result = dateFormatter.string(from: addOneDay!)
                }
            }
        }
        
        return result
    }
    
    // Gets Voz campaign expiration date
    public static func getVozCampaignExpirationStringDate() -> String {
        let vozCouponObject = Utils.getVozCouponObject()
        let campaignExpirationDate = vozCouponObject.endingDate + " 00:00:00"
        return campaignExpirationDate
    }
    
    // Set redemption date, current time
    public static func getCurrentDate() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        dateFormatter.dateFormat = DATE_FORMAT
        let result = dateFormatter.string(from: date)
        
        return result
    }
    
    // Get readable date
    public static func getReadableDate(date: Date) -> String{
        let dateFormatter = DateFormatter()
        //dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        dateFormatter.timeZone = TimeZone(identifier:TIME_ZONE_IDENTIFIER)
        dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        dateFormatter.dateFormat = READABLE_DATE_FORMAT
        let result = dateFormatter.string(from: date)
        
        return result
    }
    
    // Calculates expiration date
    public static func getExpirationDateString(dateString: String) -> String{
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: LOCALE_IDENTIFIER) as Locale
        dateFormatter.dateFormat = DATE_FORMAT
        let result = dateFormatter.string(from: date)
        
        return result
    }
    
    // Convert String to Date
    public static func convertStringToDate (stringDate: String) -> Date {
        let dateFormatters = DateFormatter()
        dateFormatters.dateFormat = "yyyy-MM-dd"
        dateFormatters.timeZone = TimeZone(identifier: TIME_ZONE_IDENTIFIER)
        dateFormatters.dateFormat = DATE_FORMAT
        dateFormatters.locale = NSLocale(localeIdentifier: "es_MX") as Locale
        let date = dateFormatters.date(from: stringDate)
        if date != nil {
            return date!
        } else {
            return Date()
        }
        
    }
    
    // Image day check
    public static func imageDayCheck() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(identifier: "GMT")
        formatter.locale = NSLocale(localeIdentifier: "es_MX") as Locale
        let result = formatter.string(from: date)
        return result
    }
    
    // Campaign expiration date has to be converted to a correct format
    // because API returns a different date format
    public static func convertDatosCampaignExpirationToDate(date: String) -> Date {
        let dateFormat = "dd-MMMM-yyyy"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        dateFormatter.timeZone = TimeZone(identifier: "GMT")
        dateFormatter.locale = NSLocale(localeIdentifier: "es_MX") as Locale
        dateFormatter.dateFormat = dateFormat
        guard let date = dateFormatter.date(from: date) else {
            fatalError()
        }
        return date
    }
    
    // Checks if service has expired
    public static func checkIfServiceHasExpired(currentDate: Date, expirationDate: Date) -> Bool {
        return currentDate > expirationDate
    }
    
    // Adds a divider line
    public static func lineDraw (viewLi:UIView, borderColor: UIColor){
        let border = CALayer()
        let width = CGFloat(1.5)
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: viewLi.frame.size.height - width,
                              width: viewLi.frame.size.width + 500,// workaround to fit iPhone X width
                              height: viewLi.frame.size.height)
        border.borderWidth = width
        viewLi.layer.addSublayer(border)
        viewLi.layer.masksToBounds = true
    }
    
    // Adds a divider line
    public static func lineDraw2 (viewLi:UIView, borderColor: UIColor) {
        let border = CALayer()
        let width = CGFloat(0.5)
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: viewLi.frame.size.height - width,
                              width: viewLi.frame.size.width,
                              height: viewLi.frame.size.height)
        border.borderWidth = width
        viewLi.layer.addSublayer(border)
        viewLi.layer.masksToBounds = true
    }
    
    // Gets coupon data from JSON
    public static func getCouponObject() -> CouponModel {
        
        if UserDefaults.standard.string(forKey: JSON_KEY) != nil &&  UserDefaults.standard.string(forKey: Utils.JSON_KEY) != ""  {
            
            let json: JSON = JSON.init(parseJSON: UserDefaults.standard.string(forKey: JSON_KEY)!)
            print("Get CouponObject Datos JSON:", json)
            
            // Coupon status
            let status = json["status"].bool
            let message = json["message"].string
            
            // VPN username and password
            let username = json["data_vpn"]["username"].string
            let password = json["data_vpn"]["password"].string
            
            // Company name and logo
            let companyName = json["company"]["name"].string
            let companyLogo = json["company"]["logo"].string
            
            // VPN server
            let serverRemote = json["servers"]["remote"].string
            let serverCert = json["servers"]["ca_cert"].string
            let serverName = json["servers"]["name"].string
            let serverPort = json["servers"]["port"].int
            
            // Campaign
            let campaignMaxMegabytes = json["campaign"]["mb_maximum"].int
            let campaignStartDateEs = json["campaign"]["start_date_es"].string
            let campaignMedia = json["campaign"]["media"].string
            let campaignStartDate = json["campaign"]["start_date"].string
            let campaignId = json["campaign"]["id"].int
            let campaignMediaType = json["campaign"]["media_type"].string
            let campaingnTermsCondition = json["campaign"]["terms_conditions"].string
            let campaignEndDateEs = json["campaign"]["end_date_es"].string
            let campaignName = json["campaign"]["name"].string
            let campaignBackgroundColor = json["campaign"]["background_color"].string
            let campaignEndDate = json["campaign"]["end_date"].string
            let campaignAccentuationColor = json["campaign"]["accentuation_color"].string
            let campaignTextColor = json["campaign"]["text_color"].string
            
            // Application
            var benefits = JSON().array
            if json["application_groups"].exists() {
                benefits = json["application_groups"].array! }
            else { benefits = json["benefits"].array! }
            //let benefits = json["application_groups"].array!//json["benefits"].array!
            
            let campaignApplications = benefits![0]["applications"].array!//json["applications"].array
            var applications = [Applications]()
            
            if campaignApplications != nil {
                for application in campaignApplications {
                    applications.append(Applications.init(androidPackageNames: application["android_package_names"].string!.components(separatedBy: ","), iOSPackageNames: application["ios_package_names"].string!.components(separatedBy: ","), id: application["id"].int!, hours: application["hours"].int!, maxMegabytes: application["mb_max"].int!, name: application["name"].string!, logoURL: application["logo"].string!))
                    
                }
            }
            // Creates coupon object
            if status != nil {
                print("Get CouponObject status:", status!)
                let couponObject = CouponModel.init(status: status!, message: message!, vpnData: VPNData.init(username: username!, password: password!), servers: Servers.init(port: serverPort!, remote: serverRemote!, caCert: serverCert!, name: serverName!),company: Company.init(name: companyName!, logoURL: companyLogo!), campaign: Campaign.init(mbMaximum: campaignMaxMegabytes!, startDateEs: campaignStartDateEs!, media: campaignMedia!, startDate: campaignStartDate!, id: campaignId!, mediaType: campaignMediaType!, termsCondition: campaingnTermsCondition!, endDateEs: campaignEndDateEs!, name: campaignName!, backgroungColor: campaignBackgroundColor!, endDate: campaignEndDate!, accentuationColor: campaignAccentuationColor!, textColor: campaignTextColor!), applications: applications)
                return couponObject
            }
            
        }
        
        let nilObject = CouponModel.init(status: false, message: "", vpnData: VPNData.init(username: "", password: ""), servers: Servers.init(port: 0, remote: "", caCert: "", name: ""),company: Company.init(name: "", logoURL: ""), campaign: Campaign.init(mbMaximum: 0, startDateEs: "", media: "", startDate: "", id: 0, mediaType: "",termsCondition: "", endDateEs: "", name: "", backgroungColor: "", endDate: "", accentuationColor: "", textColor: ""), applications: [Applications]())
        
        return nilObject
    }
    
    // Gets a voz coupon object from a JSON
//    public static func getVozCouponObject() -> VozCouponModel {
//        if UserDefaults.standard.string(forKey: "vozJson") != nil &&  UserDefaults.standard.string(forKey: "vozJson") != "" {
//            let json: JSON = JSON.init(parseJSON: UserDefaults.standard.string(forKey: "vozJson")!)
//            let success = json["Success"].bool!
//            if success {
//                let homeURL = json["HomeURL"].string!
//                let hoursValid = json["HoursValid"].string!
//                let endingDate = json["EndingDate"].string!
//                let campaignId = json["campaignId"].string!
//                let vozCoupon = VozCouponModel(endingDate: endingDate, homeURL: homeURL, campaignId: campaignId, hoursValid: hoursValid, success: success)
//                return vozCoupon
//            }
//        }
//        print("Utils Get Voz Coupon Object:", "Success False")
//        return VozCouponModel(endingDate: "", homeURL: "", campaignId: "0", hoursValid: "15", success: false)
//    }
    
    // Gets a voz coupon object from a JSON
    public static func getVozCouponObject() -> VozCouponModel {
        if UserDefaults.standard.string(forKey: "vozJson") != nil &&  UserDefaults.standard.string(forKey: "vozJson") != "" {
            let json: JSON = JSON.init(parseJSON: UserDefaults.standard.string(forKey: "vozJson")!)
            let homeURL = json["publifon-voz"]["HomeUrl"].string!
            let hoursValid = json["publifon-voz"]["Hours_Valid"].int!
            let endingDate = json["publifon-voz"]["EndingDate"].string!
            let campaignId = json["publifon-voz"]["campaignId"].int!
            let vozCoupon = VozCouponModel(endingDate: endingDate, homeURL: homeURL, campaignId: String(campaignId), hoursValid: String(hoursValid), success: true)
            return vozCoupon
        }
        print("Utils Get Voz Coupon Object:", "Success False")
        return VozCouponModel(endingDate: "", homeURL: "", campaignId: "0", hoursValid: "15", success: false)
    }
    
    // Gets correct format for free hours
    public static func formatHrsText(hrs: Int) -> String {
        var dayFormatWord = ""
        var formatedFreeHrs = ""
        
        if(hrs/24 != 0){dayFormatWord = " días";}
        else {dayFormatWord = " día";}
        if(hrs/24 == 0){
            formatedFreeHrs = String(hrs%24) + " hrs";
        } else if(hrs%24 != 0){
            formatedFreeHrs = String(hrs/24) + dayFormatWord + " y " +
                String(hrs%24) + " hrs";
        } else {
            formatedFreeHrs = String(hrs/24) + dayFormatWord;
        }
        return formatedFreeHrs
    }
    
    // Prints JSON Response
    public static func printJSON() {
    print("JSON: " + UserDefaults.standard.string(forKey: JSON_KEY)!)
    }
    
    // Gets UIColor from hex string color
    public static func hexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    // Returns an int from a hexcolor
    public static func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    
    // Fetches and sorts alphabetically a list of contacts
    public static func fetchContact() -> [Contact] {
        
        // Removing elements to avoid duplicate cotacts
        var contacts = [Contact]()
        
        // Requesting permision to access user contacts
        let store  = CNContactStore()
        store.requestAccess(for: .contacts) { (granted, error) in
            if let error = error {
                print("Falied to request contacts: ", error)
                return
            }
            
            // If we have permission go ahead and fetch user contacts
            if granted {
                print("Access to user contacts granted...")
                // Asking system for contact property keys
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey, CNContactImageDataAvailableKey, CNContactImageDataKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                
                do {
                    
                    try store.enumerateContacts(with: request, usingBlock:
                    { (contact, stopPointerIfYouWantToStopEnumerating) in
                        var numbers = [Numbers]()
                        
                        // Checking if user contact has an image and getting it
                        // if not assign a default contact image
                        var image = UIImage()
                        if contact.imageDataAvailable && contact.imageData != nil {
                            image = UIImage(data: contact.imageData!)!
                        } else {
                            image = UIImage(named: "user")!
                        }
                        
                        // Getting all contact numbers
                        for phoneNumber in contact.phoneNumbers {
                            if let number = phoneNumber.value as? CNPhoneNumber,
                                let label = phoneNumber.label {
                                let localizedLabel = CNLabeledValue<CNPhoneNumber>.localizedString(forLabel: label)
                                numbers.append(Numbers(name: localizedLabel, number: number.stringValue))
                            }
                        }
                        
                        // Adding contact to a list of contacts
                        contacts.append(Contact(name: contact.givenName, lastName: contact.familyName, numbers: numbers, image: image))
                        
                    })
                    
                } catch let err {
                    print("Failed to enumerate contacts: ", err)
                }
                
            } else {
                print("Access denied...")
            }
        }
        
        // return a sorted list of contacts by alphabet
        return contacts.sorted { $0.name.lowercased() < $1.name.lowercased()}
    }
    
    // Contact number parser
    public static func parseNumber(number: String) -> String {
        var result = BLANK_STRING
        var finalResult = BLANK_STRING
        var parse = [String]() // this variable is recycled several times
        // Deleting "(", ")", and spaces from phone number
        if number != BLANK_STRING {
            parse = number.components(separatedBy: "(")
            if parse.count > 0 {
                parse = parse[1].components(separatedBy: ")")
                for number in parse { result = result + number }
                parse = result.components(separatedBy: " ")
                for number in parse { finalResult = finalResult + number }
                return finalResult
            } else {
                return number
            }
        }
        return result
    }
    
    // User input rules method
    public static func cleanPhoneNumber(phoneNumber: String) -> String {
        let phone = phoneNumber.trimmingCharacters(in: CharacterSet(charactersIn: "01234567890.").inverted)
        
        switch phone.count {
        case 1,2,5,6,7,8,9:
            return ""
        case 10:
            print("CleanNumber " + "New: " + "521" + phone);
            return "521" + phone;
        case 11, 12, 13, 14:
            print("phone:", phone.prefix(3))
            if String(phone.prefix(3)) == "044" || String(phone.prefix(3)) == "045" {
                print("phone")
                return String(phone.suffix(phone.count-3))
            } else {
                return phone
            }
        default:
            return phone
        }
    }
    
    // Checking if APP Version is Updated
    public static func hasUpdatedAppVersion(localAppVersion: String, appStoreVersion: String) -> Bool {
        // Getting Local App Version
        let appVersion = localAppVersion.split(separator: ".")
        let intAppVersion = appVersion.map { Int($0)!}
        
        // Getting AppStore Version
        let currentVersion = appStoreVersion.split(separator: ".")
        let currentIntVersion = currentVersion.map { Int($0)!}
        
        // Setting state variables
        var isAppVersionGreater = false
        var isCurrentVersionGreater = false
        var versionArraySize = 0
        var appVersionArrayHasMoreElements = false
        var appCurrentVersionArrayHasMoreElements = false

        // Getting array size to iterate
        if intAppVersion.count > currentIntVersion.count {
            versionArraySize = currentIntVersion.count
            appVersionArrayHasMoreElements = true
        } else if intAppVersion.count < currentIntVersion.count {
            versionArraySize = intAppVersion.count
            appCurrentVersionArrayHasMoreElements = true
        } else {
            versionArraySize = currentIntVersion.count
        }
        
        // Comparing versions
        for index in 0..<versionArraySize {
            
            if intAppVersion[index] > currentIntVersion[index] {
                isAppVersionGreater = true
                print("App Version size", intAppVersion.count)
                break
            } else if intAppVersion[index] < currentIntVersion[index] {
                isCurrentVersionGreater = true
                print("Current Version size", currentIntVersion.count)
                break
            }
            
        }

        // If one of the arrays is longer and the compared digits are eaqual
        // we need to check if th extra digit is greater than 0, if it is
        // then the version with that digit is greater
        if isAppVersionGreater == false && isCurrentVersionGreater == false {
            if appVersionArrayHasMoreElements && intAppVersion[intAppVersion.count-1] != 0 {
                isAppVersionGreater = true
            } else  if appCurrentVersionArrayHasMoreElements && currentIntVersion[currentIntVersion.count-1] != 0 {
                isCurrentVersionGreater = true
            } else {
                print("AppVersion == CurrentVersion")
            }
        }
        
        print("Local Version is Greater?", isAppVersionGreater)
        print("AppStore Version is Greater", isCurrentVersionGreater)
        return isCurrentVersionGreater
    }
    
    public static func changeContactIconColor(imageView: UIImageView,
                                              image: UIImage,
                                              couponObject: CouponModel?) {
        let defaultImage = UIImage(named: "user")
        let originalImage = image
        if defaultImage == originalImage {
            let tintedImage = originalImage.withRenderingMode(.alwaysTemplate)
            imageView.image = tintedImage
            if couponObject != nil  {
                imageView.tintColor = Utils.hexToColor(hexString: ((couponObject?.campaign.accentuationColor)!))
            }
        }
    }
    
    public static func changeDefaultContactIconColor(imageView: UIImageView,
                                              image: UIImage) {
        let defaultImage = UIImage(named: "user")
        let originalImage = image
        if defaultImage == originalImage {
            let tintedImage = originalImage.withRenderingMode(.alwaysTemplate)
            imageView.image = tintedImage
            if couponObject != nil  {
                imageView.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            }
        }
        
    }
    
    // Configuring Request, returns a configured request
    public static func configureRequest(coupon: String, url: String) -> URLRequest {
        // Getting corresponding url
        // This code escapes unwanted characters
        let url : NSString = url+coupon as NSString
        let urlStr = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let searchURL : NSURL = NSURL(string: urlStr!)!
        
        // Setting up alamofire request
        var request = URLRequest(url: searchURL as URL)
        request.httpMethod = Utils.GET
        //request.timeoutInterval = 5
        request.timeoutInterval = 60
        
        print("Configured Request: ", request)
        return request
    }
    
    
    public func showChargerView(viewToCustom: UIView) {
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        let chargerView = UIView(frame: CGRect(x: 0, y: 0, width: viewToCustom.frame.width, height: viewToCustom.frame.height))
        chargerView.tag = 100
        activityView.center = viewToCustom.center
        activityView.startAnimating()
        chargerView.backgroundColor = UIColor.black.withAlphaComponent(0.55)
        chargerView.addSubview(activityView)
        viewToCustom.addSubview(chargerView)
        
        
        
    }
    
    public func stopChargerView(viewToCustom: UIView){
        if let viewWithTag = viewToCustom.viewWithTag(100) {
            viewWithTag.removeFromSuperview()
        }else{
            print("No existe view para remover")
        }
    }
        
    func isCameraPermission() -> Bool {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            switch authStatus {
                /*
                 Status Restricted -
                 The client is not authorized to access the hardware for the media type. The user cannot change the client's status, possibly due to active restrictions such as parental controls being in place.
                 */
            case .denied, .restricted:
                return false
            case .notDetermined:
                return true
            case .authorized:
               return true
            @unknown default:
                return false
            }
    }
    
    func showTermsAndConditionsAux(viewController: UIViewController){
        print("el user default: ", UserDefaults.standard.bool(forKey: "openedBefore"))
        if UserDefaults.standard.bool(forKey: "openedBefore") == false {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let menuNavigation = storyBoard.instantiateViewController(withIdentifier: "TermsConditionsAuxViewController") as! TermsConditionsAuxViewController
            viewController.present(menuNavigation, animated: true, completion: nil)
        }
    }
    
}


