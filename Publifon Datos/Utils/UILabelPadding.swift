//
//  UILabelPadding.swift
//  Publifon Datos
//
//  Created by Rodrigo Casillas on 3/24/19.
//  Copyright © 2019 Bluelabs. All rights reserved.
//

import Foundation
import UIKit

class UILabelPadding: UILabel {
    var newText: String = ""
    
    let padding = UIEdgeInsets(top: 2, left: 8, bottom: 2, right: 8)
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
    }
    
    override var intrinsicContentSize : CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    
    override var text: String? {
        didSet {
            if let text = text {
                print("Text changed.", text)
                newText = text
            } else {
                print("Text not changed.", text!)
            }
        }
    }
    
    public func getNewText() -> String {
        return newText
    }
    
}
